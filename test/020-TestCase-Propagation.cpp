// $Id$
// ---------------------------------------------------------------------------
#include <stdio.h>
#include <catch2/catch.hpp>

#include "HelioLib/Core.hpp"

#include "core/SizeParameters.hpp"
#include "dynamics/force/ZonalPerturbation.hpp"
#include "dynamics/events/DryMassEvent.hpp"
#include "AveragedProblemNlp.hpp"

#define EARTH_MU 3.9860064e+14
#define EARTH_RADIUS 6378137
#define EARTH_J2 0.00108263

TEST_CASE("Check dry mass event detector", "[single-file]") {
    double inc = 87.9 * M_PI / 180;
    double raan = 30 * M_PI / 180;

    double yInitCond[ODE_STATE_SIZE];
    yInitCond[IDX_SMA] = 1;
    yInitCond[IDX_EX] = 0;
    yInitCond[IDX_EY] = 0.001;
    yInitCond[IDX_HX] = tan(inc/2) * cos(raan);
    yInitCond[IDX_HY] = tan(inc/2) * sin(raan);
    yInitCond[IDX_L] = 0.;  // theta
    yInitCond[IDX_MASS] = 1;  // mass
    // 8 unknowns
    yInitCond[7] = 1.;  // costate sma
    yInitCond[8] = 0.1;  // costate ex
    yInitCond[9] = 0.1;  // costate ey
    yInitCond[10] = 0.1;  // costate hx
    yInitCond[11] = 0.1;  // costate hy
    yInitCond[12] = 0.0;  // costate unknown final lontitude theta
    yInitCond[13] =-0.1;  // costate mass

    yInitCond[IDX_L1] = 0.2;  // unknown final longitude theta
    yInitCond[IDX_T] = 0.;  // time

    LtThrusterData thrusterDef = LtThrusterData();
    thrusterDef.Fth = 10;
    thrusterDef.g0Isp = 0.7;
    thrusterDef.eff = 1;
    thrusterDef.PropType = PROP_NEP;

    TDynamicsAveragedEquinoctial *mDynamics = new TDynamicsAveragedEquinoctial(1., EARTH_J2, 0.5, thrusterDef);
    mDynamics->setScaling(1, 1, 1);
    StateNumericalPropagator *mPropagator = new StateNumericalPropagator(mDynamics, NULL, ODE_SOLVER_ID::ODE_DOPRI5);
    mPropagator->setTolerances(1e-9, 1e-9);

    SimulationEventManager *eventManager = new SimulationEventManager();
    DryMassEventDetector *detector = new DryMassEventDetector(0.9);
    eventManager->addEventHandler(detector);
    mPropagator->setEventHandler(eventManager);

    mPropagator->solve(0, yInitCond, 1., DYNAMICALMODEL_TYPE::D_NOT_SET);

    real_type yEndRef[2 * STATE_SIZE];
    mPropagator->getLastPoint(yEndRef);

#ifdef DEBUG_PRINT
    printf("yEndRef: ");
    for (int i = 0; i < 14; ++i) {
        printf("%12g ", yEndRef[i]);
    }
    printf("\n");
#endif

    double tol = 1e-9;
    REQUIRE(fabs(yEndRef[6] - 0.9) > tol);

    delete eventManager;
    delete detector;
    delete mDynamics;
    delete mPropagator;
}

TEST_CASE("Check eclipse events", "[single-file]") {
    double inc = 87.9 * M_PI / 180;
    double raan = 30 * M_PI / 180;

    double yInitCond[ODE_STATE_SIZE];
    yInitCond[IDX_SMA] = 1;
    yInitCond[IDX_EX] = 0;
    yInitCond[IDX_EY] = 0.001;
    yInitCond[IDX_HX] = tan(inc/2) * cos(raan);
    yInitCond[IDX_HY] = tan(inc/2) * sin(raan);
    yInitCond[IDX_L] = 0.;  // theta
    yInitCond[IDX_MASS] = 1;  // mass
    // 8 unknowns
    yInitCond[7] = 1.;  // costate sma
    yInitCond[8] = 0.1;  // costate ex
    yInitCond[9] = 0.1;  // costate ey
    yInitCond[10] = 0.1;  // costate hx
    yInitCond[11] = 0.1;  // costate hy
    yInitCond[12] = 0.0;  // costate unknown final lontitude theta
    yInitCond[13] =-0.1;  // costate mass

    yInitCond[IDX_L1] = 0.2;  // unknown final longitude theta
    yInitCond[IDX_T] = 0.;  // time

    LtThrusterData thrusterDef = LtThrusterData();
    thrusterDef.Fth = 1;
    thrusterDef.g0Isp = 0.7;
    thrusterDef.eff = 1;
    thrusterDef.PropType = PROP_NEP;

    TDynamicsAveragedEquinoctial *mDynamics = new TDynamicsAveragedEquinoctial(1., EARTH_J2, 0.5, thrusterDef);
    mDynamics->setScaling(1, 1, 1);
    StateNumericalPropagator *mPropagator = new StateNumericalPropagator(mDynamics, NULL, ODE_SOLVER_ID::ODE_DOPRI5);
    mPropagator->setTolerances(1e-9, 1e-9);

    mPropagator->solve(0, yInitCond, 1., DYNAMICALMODEL_TYPE::D_NOT_SET);

    real_type yEndRef[2 * STATE_SIZE];
    mPropagator->getLastPoint(yEndRef);

#ifdef DEBUG_PRINT
    printf("yEndRef: ");
    for (int i = 0; i < 14; ++i) {
        printf("%12g ", yEndRef[i]);
    }
    printf("\n");
#endif

    double tol = 1e-9;
    REQUIRE(fabs(yEndRef[6] - 0.9) > tol);

    delete mDynamics;
    delete mPropagator;
}
