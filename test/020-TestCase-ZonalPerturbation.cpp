// $Id$
// ---------------------------------------------------------------------------

#include <stdio.h>
#include <catch2/catch.hpp>

#include "HelioLib/Core.hpp"
#include "core/SizeParameters.hpp"// IDX_***
#include "dynamics/force/ZonalPerturbation.hpp"

//#define DEBUG_PRINT

#define EARTH_MU 3.9860064e+14
#define EARTH_RADIUS 6378137
#define EARTH_J2 0.00108263


//
TEST_CASE("Check raan rate", "[single-file]") {
    double sma = EARTH_RADIUS + 1200 * 1e3;
    double inc = 87.9 * M_PI / 180;
    double raan = 10 * M_PI / 180;
    double hx = tan(inc / 2) * cos(raan);
    double hy = tan(inc / 2) * sin(raan);
    double Oe[] = {sma, 0.0012, 0.00001, hx, hy, 0, 100};

    double pomdot, gomdot;
    ZonalPerturbation *pert = new ZonalPerturbation(EARTH_J2, 2, 6378136., EARTH_MU);
    pert->getRate(Oe, pomdot, gomdot, NULL, NULL);
    delete pert;

    REQUIRE(fabs(gomdot * 180 / M_PI * 86400 + 0.1997083979) < 1e-10);
}

//
TEST_CASE("Check raan rate with scaling", "[single-file]") {
    double sma = EARTH_RADIUS + 1200 * 1e3;
    double inc = 87.9 * M_PI / 180;
    double raan = 10 * M_PI / 180;
    double hx = tan(inc / 2) * cos(raan);
    double hy = tan(inc / 2) * sin(raan);
    double Oe[] = { 1., 0.0012, 0.00001, hx, hy, 0, 1. };

    double pomdot, gomdot;
    ZonalPerturbation* pert = new ZonalPerturbation(EARTH_J2, 2, 6378136., EARTH_MU);
    pert->setScaling(sma, EARTH_MU);
    pert->getRate(Oe, pomdot, gomdot, NULL, NULL);
    delete pert;

    double timeScale = sqrt(sma * sma * sma / EARTH_MU); // s
    REQUIRE(fabs(gomdot * 180. / M_PI * (86400. / timeScale) + 0.1997083979) < 1e-10);
}

//
TEST_CASE("Check pom rate", "[single-file]") {
    double sma = EARTH_RADIUS + 1200 * 1e3;
    double inc = 87.9 * M_PI / 180;
    double raan = 10. * M_PI / 180;
    double hx = tan(inc / 2) * cos(raan);
    double hy = tan(inc / 2) * sin(raan);
    double Oe[] = {sma, 0.0, 0.0012, hx, hy, 0, 100};

    double pomdot, gomdot;
    ZonalPerturbation *pert = new ZonalPerturbation(EARTH_J2, 2, EARTH_RADIUS, EARTH_MU);
    pert->getRate(Oe, pomdot, gomdot, NULL, NULL);
    delete pert;

    REQUIRE(fabs(pomdot * 180. / M_PI * 86400 + 2.7067076) < 1e-6);
}

//
TEST_CASE("Check mean anomaly rate owing to J2", "[single-file]") {
    double sma = EARTH_RADIUS + 1200 * 1e3;
    double inc = 87.9 * M_PI / 180;
    double raan = 10. * M_PI / 180;
    double hx = tan(inc / 2) * cos(raan);
    double hy = tan(inc / 2) * sin(raan);
    double Oe[] = {sma, 0.0, 0.0012, hx, hy, 0, 100};

    double longidot;
    ZonalPerturbation *pert = new ZonalPerturbation(EARTH_J2, 2, EARTH_RADIUS, EARTH_MU);
    pert->getLongitudeRate(Oe, longidot, NULL);
    delete pert;

    REQUIRE(fabs(longidot * 180. / M_PI * 86400 + 2.7140243428) < 1e-6);
}

//
TEST_CASE("Check derivatives of AoP rates", "[single-file]") {
    double sma = EARTH_RADIUS + 1200 * 1e3;
    double inc = 87.9 * M_PI / 180;
    double raan = 10 * M_PI / 180;
    double hx = tan(inc / 2) * cos(raan);
    double hy = tan(inc / 2) * sin(raan);
    double Oe[] = {sma, 0.0012, 0.0012, hx, hy, 0, 100};

    double pomdot_ref, gomdot_ref;
    double pomdotDerivatives_ref[STATE_SIZE];

    ZonalPerturbation *pert = new ZonalPerturbation(EARTH_J2, 2, 6378136., EARTH_MU);
    pert->getRate(Oe, pomdot_ref, gomdot_ref, pomdotDerivatives_ref, NULL);

    double h = 1e-6;
    double pomdot, gomdot;
    double pomdotDerivatives[STATE_SIZE];
    for (int i = 0; i < 5; ++i) {
        Oe[i] += h;
        pert->getRate(Oe, pomdot, gomdot, NULL, NULL);
        pomdotDerivatives[i] = (pomdot - pomdot_ref) / h;
        Oe[i] -= h;
    }

    delete pert;

    //for(int i=0; i<5; ++i) {
    //    printf( "pomdot[%d]: %g %g\n", i, pomdotDerivatives[i], pomdotDerivatives_ref[i]);
    //}

    double tol = 1e-3;
    REQUIRE(fabs((pomdotDerivatives[0] - pomdotDerivatives_ref[0]) / pomdotDerivatives_ref[0]) < tol);
    REQUIRE(fabs((pomdotDerivatives[1] - pomdotDerivatives_ref[1]) / pomdotDerivatives_ref[1]) < tol);
    REQUIRE(fabs((pomdotDerivatives[2] - pomdotDerivatives_ref[2]) / pomdotDerivatives_ref[2]) < tol);
    REQUIRE(fabs((pomdotDerivatives[3] - pomdotDerivatives_ref[3]) / pomdotDerivatives_ref[3]) < tol);
    REQUIRE(fabs((pomdotDerivatives[4] - pomdotDerivatives_ref[4]) / pomdotDerivatives_ref[4]) < tol);
}

TEST_CASE("Check derivatives of RAAN rates", "[single-file]") {
    double sma = EARTH_RADIUS + 1200 * 1e3;
    double inc = 87.9 * M_PI / 180;
    double raan = 10 * M_PI / 180;
    double hx = tan(inc / 2) * cos(raan);
    double hy = tan(inc / 2) * sin(raan);
    double Oe[7] = {sma, 0.0012, 0.0012, hx, hy, 0, 100};

    double pomdot_ref, gomdot_ref;
    double gomdotDerivatives_ref[STATE_SIZE];

    ZonalPerturbation *pert = new ZonalPerturbation(EARTH_J2, 2, 6378136., EARTH_MU);
    pert->getRate(Oe, pomdot_ref, gomdot_ref, NULL, gomdotDerivatives_ref);

    double h = 1e-6;
    double pomdot, gomdot;
    double gomdotDerivatives[STATE_SIZE];
    for (int i = 0; i < 5; ++i) {
        Oe[i] += h;
        pert->getRate(Oe, pomdot, gomdot, NULL, NULL);
        gomdotDerivatives[i] = (gomdot - gomdot_ref) / h;
        Oe[i] -= h;
    }

    delete pert;

#ifdef DEBUG_PRINT
    for (int i = 0; i < 5; ++i) {
        printf("gomdot[%d]: %g %g\n", i, gomdotDerivatives[i], gomdotDerivatives_ref[i]);
    }
#endif

    double tol = 1e-3;
    REQUIRE(fabs((gomdotDerivatives[0] - gomdotDerivatives_ref[0]) / gomdotDerivatives_ref[0]) < tol);
    REQUIRE(fabs((gomdotDerivatives[1] - gomdotDerivatives_ref[1]) / gomdotDerivatives_ref[1]) < tol);
    REQUIRE(fabs((gomdotDerivatives[2] - gomdotDerivatives_ref[2]) / gomdotDerivatives_ref[2]) < tol);
    REQUIRE(fabs((gomdotDerivatives[3] - gomdotDerivatives_ref[3]) / gomdotDerivatives_ref[3]) < tol);
    REQUIRE(fabs((gomdotDerivatives[4] - gomdotDerivatives_ref[4]) / gomdotDerivatives_ref[4]) < tol);
}

TEST_CASE("Check derivatives of longitude rate", "[single-file]") {
    double sma = EARTH_RADIUS + 1200 * 1e3;
    double inc = 87.9 * M_PI / 180;
    double raan = 10 * M_PI / 180;
    double hx = tan(inc / 2) * cos(raan);
    double hy = tan(inc / 2) * sin(raan);
    double Oe[7] = {sma, 0.0012, 0.0012, hx, hy, 0, 100};

    double longidot_ref;
    double longidotDerivatives_ref[STATE_SIZE];

    ZonalPerturbation *pert = new ZonalPerturbation(EARTH_J2, 2, 6378136., EARTH_MU);
    pert->getLongitudeRate(Oe, longidot_ref, longidotDerivatives_ref);

    double h = 1e-6;
    double longidot;
    double longidotDerivatives[STATE_SIZE];
    for (int i = 0; i < 5; ++i) {
        Oe[i] += h;
        pert->getLongitudeRate(Oe, longidot, NULL);
        longidotDerivatives[i] = (longidot - longidot_ref) / h;
        Oe[i] -= h;
    }

    delete pert;

#ifdef DEBUG_PRINT
    for (int i = 0; i < 5; ++i) {
        printf("longidot[%d]: %g %g\n", i, longidotDerivatives[i], longidotDerivatives_ref[i]);
    }
#endif

    double tol = 5e-1;
    REQUIRE(fabs((longidotDerivatives[0] - longidotDerivatives_ref[0]) / longidotDerivatives_ref[0]) < tol);
    tol = 1e-3;
    REQUIRE(fabs((longidotDerivatives[1] - longidotDerivatives_ref[1]) / longidotDerivatives_ref[1]) < tol);
    REQUIRE(fabs((longidotDerivatives[2] - longidotDerivatives_ref[2]) / longidotDerivatives_ref[2]) < tol);
    REQUIRE(fabs((longidotDerivatives[3] - longidotDerivatives_ref[3]) / longidotDerivatives_ref[3]) < tol);
    REQUIRE(fabs((longidotDerivatives[4] - longidotDerivatives_ref[4]) / longidotDerivatives_ref[4]) < tol);
}

//
TEST_CASE("Check dlambda/dt implementation for ZonalPerturbation", "[single-file]") {
    double inc = 87.9 * M_PI / 180;
    double raan = 10. * M_PI / 180;
    double hx = tan(inc / 2) * cos(raan);
    double hy = tan(inc / 2) * sin(raan);

    double Oe[] = {6478136, 0.0012, 0.0012, hx, hy, 1, 100};  // x = [a, ex, ey, hx, hy, L, m]
    double lambda[] = {0.6, 0.1, 0.1, 0.1, 0.1, 0.1, -0.5};       //
    double dOedt_ref[ODE_STATE_SIZE];
    double lambda_dot[STATE_SIZE];
    ZonalPerturbation *pert = new ZonalPerturbation(EARTH_J2, 2, EARTH_RADIUS, EARTH_MU);
    pert->getAverageSensitivity(0, Oe, lambda, dOedt_ref);

    double h = 1e-7;
    double dOedt[ODE_STATE_SIZE];
    double dfdx[STATE_SIZE * STATE_SIZE];
    int index[] = {0, 1, 2, 3, 4, 15, 12};
    for (int i = 0; i < STATE_SIZE; ++i) {
        Oe[i] += h;
        pert->getAverageSensitivity(0, Oe, lambda, dOedt);
        for (int j=0; j < STATE_SIZE; ++j) {
            int k = index[j];
            dfdx[i * STATE_SIZE + j] = (dOedt[k] - dOedt_ref[k]) / h;
        }
        Oe[i] -= h;
    }

#ifdef DEBUG_PRINT
    printf("dfdx:\n");
    for (int i = 0; i < STATE_SIZE; ++i) {
        for (int j=0; j < STATE_SIZE; ++j) {
            printf("%8g ", dfdx[i*STATE_SIZE + j]);
        }
        printf("\n");
    }
#endif

    // lambda_dot =-lambda' * dfdx
    for (int i = 0; i < 7; ++i) {
        lambda_dot[i] = 0;
        for (int j=0; j < 7; ++j) {
            lambda_dot[i] +=-lambda[j] * dfdx[i*7 + j];
        }
    }

#ifdef DEBUG_PRINT
    printf("dOedt_ref[7] : %g %g\n", dOedt_ref[7], lambda_dot[0]);
    printf("dOedt_ref[8] : %g %g\n", dOedt_ref[8], lambda_dot[1]);
    printf("dOedt_ref[9] : %g %g\n", dOedt_ref[9], lambda_dot[2]);
    printf("dOedt_ref[10]: %g %g\n", dOedt_ref[10], lambda_dot[3]);
    printf("dOedt_ref[11]: %g %g\n", dOedt_ref[11], lambda_dot[4]);
    printf("dOedt_ref[11]: %g %g\n", dOedt_ref[12], lambda_dot[5]);
    printf("dOedt_ref[13]: %g %g\n", dOedt_ref[13], lambda_dot[6]);
#endif

    double rtol = 3e-1;
    REQUIRE(fabs((dOedt_ref[7] - lambda_dot[0])) < rtol);
    REQUIRE(fabs((dOedt_ref[8] - lambda_dot[1]) / dOedt_ref[8]) < rtol);
    REQUIRE(fabs((dOedt_ref[9] - lambda_dot[2]) / dOedt_ref[9]) < rtol);
    REQUIRE(fabs((dOedt_ref[10] - lambda_dot[3]) / dOedt_ref[10]) < rtol);
    REQUIRE(fabs((dOedt_ref[11] - lambda_dot[4]) / dOedt_ref[11]) < rtol);
    delete pert;
}
