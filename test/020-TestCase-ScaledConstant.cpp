// $Id$
// ---------------------------------------------------------------------------

#include <catch2/catch.hpp>

#include "ocp/ScaledConstant.hpp"


TEST_CASE("Check scaling of a ScaledConstant", "[single-file]") {
    ScaledConstant c = ScaledConstant(1);
    c.scale(2);
    REQUIRE(c.value() == 2);
    REQUIRE(c.getUnscaledValue() == 1);
}

TEST_CASE("Check arithmetic operations of ScaledConstant", "[single-file]") {
    ScaledConstant c = ScaledConstant(1);
    c.scale(2);
    REQUIRE(c * 2. == 4);
    REQUIRE(c / 2. == 1);
    REQUIRE(c + 2. == 4);
    REQUIRE(c - 2. == 0);
}

TEST_CASE("Check multiplication of ScaledConstant", "[single-file]") {
    double v = 1.12345651;
    ScaledConstant a = ScaledConstant(v);
    double w = 2.12345651;
    ScaledConstant b = ScaledConstant(w);
    REQUIRE(a * b == v * w);
    REQUIRE(a * w == v * w);
    REQUIRE(v * b == v * w);
}

TEST_CASE("Check division of ScaledConstant", "[single-file]") {
    double v = 1.12345651;
    ScaledConstant a = ScaledConstant(v);
    double w = 2.12345651;
    ScaledConstant b = ScaledConstant(w);
    REQUIRE(a / b == v / w);
    REQUIRE(a / w == v / w);
    REQUIRE(v / b == v / w);
}

TEST_CASE("Check comparison operations of ScaledConstant", "[single-file]") {
    double v = 1.12345651;
    ScaledConstant a = ScaledConstant(v);
    double w = 4.22513;
    ScaledConstant b = ScaledConstant(w);
    REQUIRE(a < b);
    REQUIRE(b > a);
    REQUIRE(a == v);
}

TEST_CASE("Check assignement operation of ScaledConstant", "[single-file]") {
    double v = 1.12345651;
    ScaledConstant a = ScaledConstant(v);
    ScaledConstant b = a;
    REQUIRE(b.value() == v);
}
