// $Id$
// ---------------------------------------------------------------------------
#include <stdio.h>
#include <catch2/catch.hpp>

#include "HelioLib/Core.hpp"
#include "core/SizeParameters.hpp"// IDX_***
#include "dynamics/force/ZonalPerturbation.hpp"
#include "AveragedProblemNlp.hpp"

#define EARTH_MU 3.9860064e+14
#define EARTH_RADIUS 6378137
#define EARTH_J2 0.00108263


TEST_CASE("Check ODE output", "[single-file]") {
    double inc = 87.9 * M_PI / 180;
    double raan = 50 * M_PI / 180;

    double yInitCond[ODE_STATE_SIZE];
    yInitCond[IDX_SMA] = 1;
    yInitCond[IDX_EX] = 0;
    yInitCond[IDX_EY] = 0.001;
    yInitCond[IDX_HX] = tan(inc/2) * cos(raan);
    yInitCond[IDX_HY] = tan(inc/2) * sin(raan);
    yInitCond[IDX_L] = 0;  // theta
    yInitCond[IDX_MASS] = 1;  // mass

    // 8 unknowns
    yInitCond[7] = 1.;  // costate sma
    yInitCond[8] = 0.1;  // costate ex
    yInitCond[9] = 0.1;  // costate ey
    yInitCond[10] = 0.1;  // costate hx
    yInitCond[11] = 0.1;  // costate hy
    yInitCond[12] = 0;  // costate longitude
    yInitCond[13] =-0.1;  // costate mass
    // parameter
    yInitCond[IDX_L1] = 0.2;
    // time
    yInitCond[IDX_T] = 0.;

    LtThrusterData thrusterDef = LtThrusterData();
    thrusterDef.Fth = 10;
    thrusterDef.g0Isp = 0.7;
    thrusterDef.eff = 1;
    thrusterDef.PropType = PROP_NEP;

    TDynamicsAveragedEquinoctial *mDynamicsZero = new TDynamicsAveragedEquinoctial(1., 0., 0.5, thrusterDef);

    DynamicalContextVariables cVars;
    real_type longitude = 0;
    mDynamicsZero->computeCommonVariables(longitude, yInitCond, cVars);

    real_type dx[ODE_STATE_SIZE];
    mDynamicsZero->getDynamics(longitude, cVars, dx);

    double dxref[ODE_STATE_SIZE] = {0.399008700988, 0.00511174182924, 0.0049120859669, 0.00471647777189, 0.00471159544596, 0.2, -0.285714285714,
                     -0.668343242431, 0.0197901663412, 0.0198748325349, -0.000605129430118, -0.000722236184773, -0.229525319661, 0.40095389109,
                      0., 0.2};

    double tol = 1e-6;
    for (int i = 0; i < ODE_STATE_SIZE; ++i) {
        REQUIRE(fabs(dx[i] - dxref[i]) < tol);
    }

    delete mDynamicsZero;
}

TEST_CASE("Check free propagation", "[single-file]") {
    double inc = 87.9 * M_PI / 180;
    double raan = 30 * M_PI / 180;

    double yInitCond[ODE_STATE_SIZE];
    yInitCond[IDX_SMA] = 1;
    yInitCond[IDX_EX] = 0;
    yInitCond[IDX_EY] = 0.001;
    yInitCond[IDX_HX] = tan(inc/2) * cos(raan);
    yInitCond[IDX_HY] = tan(inc/2) * sin(raan);
    yInitCond[IDX_L] = 0.;  // theta
    yInitCond[IDX_MASS] = 1;  // mass
    // 8 unknowns
    yInitCond[IDX_PSMA] = 1.;  // costate sma
    yInitCond[IDX_PEX] = 0.1;  // costate ex
    yInitCond[IDX_PEY] = 0.1;  // costate ey
    yInitCond[IDX_PHX] = 0.1;  // costate hx
    yInitCond[IDX_PHY] = 0.1;  // costate hy
    yInitCond[IDX_PL] = 0.0;  // costate unknown final lontitude theta
    yInitCond[IDX_PM] =-0.1;  // costate mass

    yInitCond[IDX_L1] = 0.2;  // unknown final longitude theta
    yInitCond[IDX_T] = 0.;  // time

    LtThrusterData thrusterDef = LtThrusterData();
    thrusterDef.Fth = 10;
    thrusterDef.g0Isp = 0.7;
    thrusterDef.eff = 1;
    thrusterDef.PropType = PROP_NEP;

    TDynamicsAveragedEquinoctial *mDynamicsZero = new TDynamicsAveragedEquinoctial(1., 0., 0.5, thrusterDef);
    mDynamicsZero->setScaling(1, 1, 1);
    StateNumericalPropagator *mPropagatorZero = new StateNumericalPropagator(mDynamicsZero, NULL, ODE_SOLVER_ID::ODE_DOPRI5);
    mPropagatorZero->setTolerances(1e-9, 1e-9);
    mPropagatorZero->solve(0, yInitCond, 1., DYNAMICALMODEL_TYPE::D_NOT_SET);
    real_type yEndRefZero[ODE_STATE_SIZE];
    mPropagatorZero->getLastPoint(yEndRefZero);

    TDynamicsAveragedEquinoctial *mDynamics = new TDynamicsAveragedEquinoctial(1., EARTH_J2, 0.5, thrusterDef);
    mDynamics->setScaling(1, 1, 1);
    StateNumericalPropagator *mPropagator = new StateNumericalPropagator(mDynamics, NULL, ODE_SOLVER_ID::ODE_DOPRI5);
    mPropagator->setTolerances(1e-9, 1e-9);
    mPropagator->solve(0, yInitCond, 1., DYNAMICALMODEL_TYPE::D_NOT_SET);
    real_type yEndRef[ODE_STATE_SIZE];
    mPropagator->getLastPoint(yEndRef);

#ifdef DEBUG_PRINT
    printf("yEndRefZero: ");
    for (int i = 0; i < 14; ++i) {
        printf("%12g ", yEndRefZero[i]);
    }
    printf("\n");

    printf("yEndRef: ");
    for (int i = 0; i < 14; ++i) {
        printf("%12g ", yEndRef[i]);
    }
    printf("\n");

    printf("yEndRef-yEndRefZero: ");
    for (int i = 0; i < 14; ++i) {
        printf("%12g ", yEndRef[i] - yEndRefZero[i]);
    }
    printf("\n");
#endif

    double tol = 1e-9;
    for (int i = 0; i < 5; ++i) {
        REQUIRE(fabs(yEndRef[i] - yEndRefZero[i]) > tol);
    }

    delete mDynamicsZero;
    delete mPropagatorZero;
    delete mDynamics;
    delete mPropagator;
}
