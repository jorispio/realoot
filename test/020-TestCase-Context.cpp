// $Id$
// ---------------------------------------------------------------------------
#include <stdio.h>
#include <catch2/catch.hpp>

#include <array>

#include "AveragedProblemNlp.hpp"

// ---------------------------------------------------------------------------
TEST_CASE("Check initial guess array", "[single-file]") {
    LtProblemContext problemContext;
    problemContext.setInitialGuessByValue(0.571801, -0.007173, -0.004140, 0.0001, 0.0002, 0.061549, -0.056473);

    double tol = 1e-6;
    REQUIRE(fabs(problemContext.initguess[0] - 0.571801) < tol);
    REQUIRE(fabs(problemContext.initguess[1] + 0.007173) < tol);
    REQUIRE(fabs(problemContext.initguess[2] + 0.004140) < tol);
    REQUIRE(fabs(problemContext.initguess[3] - 0.0001) < tol);
    REQUIRE(fabs(problemContext.initguess[4] - 0.0002) < tol);    
    REQUIRE(fabs(problemContext.initguess[5] + 0.056473) < tol); // lm
    REQUIRE(fabs(problemContext.initguess[6] - 0.061549) < tol);

    REQUIRE(fabs(problemContext.initguess_duration - 0.061549) < tol);
}


TEST_CASE("Check state initialised with Keplerian elements", "[single-file]") {
    State *state = new State();
    state->setSma(7378136.);
    state->setEccentricity(0.1);
    state->setArgumentOfPericenter(M_PI / 3.);
    state->setInclination(50. * M_PI / 180.);
    state->setRightAscensionOfAscendingNode(0.1);

    double tol = 1e-6;
    REQUIRE(fabs(state->equiAdapt[0] - 7378136.) < tol);
    REQUIRE(fabs(state->equiAdapt[1] - 0.1 * cos(M_PI / 3. + 0.1)) < tol);
    REQUIRE(fabs(state->equiAdapt[2] - 0.1 * sin(M_PI / 3. + 0.1)) < tol);
    REQUIRE(fabs(state->equiAdapt[3] - tan(50. * M_PI / 360.) * cos(0.1)) < tol);
    REQUIRE(fabs(state->equiAdapt[4] - tan(50. * M_PI / 360.) * sin(0.1)) < tol);
    delete state;
}

TEST_CASE("Check state initialised with equinoctial elements", "[single-file]") {
    std::array<double,6> e = { 6478136., 0, 0.001, 0.9, 0.4, 0 };
    double mu = 3.9860064e+14;
    State* state = new State(e, 0., mu);

    double tol = 1e-6;
    REQUIRE(fabs(state->getSma() - 6478136.) < tol);
    REQUIRE(fabs(state->getEccentricity() - 0.001) < tol);
    REQUIRE(fabs(state->getInclination() - 1.5555673117462296) < tol);
    REQUIRE(fabs(state->getArgumentOfPericenter() + state->getRightAscensionOfAscendingNode() - M_PI/2.) < tol);
    REQUIRE(fabs(state->getRightAscensionOfAscendingNode() - 0.4182243295792291) < tol);
    delete state;
}
