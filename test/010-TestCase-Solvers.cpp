// $Id$
// ---------------------------------------------------------------------------
// Let Catch provide main()
#define CATCH_CONFIG_MAIN

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

#include <array>
#include <ctime>

#include <catch2/catch.hpp>

#include "core/LtProblemSolution.hpp"
#include "Enumerations.hpp"
#include "utils/UtilsInputScript.hpp"
#include "utils/maths/solver/SolverTypeEnum.hpp"
#include "utils/xml/UtilsStringXml.hpp"
#include "utils/xml/DOMTreeErrorReporter.hpp"
#include "LtAvgOcpMainInterface.hpp"

#define PRINT_REF

// ---------------------------------------------------------------------------
void printDebugData(LtProblemSolution &solution) {
    printf("return code      : %d\n", solution.res);
    printf("costate          : [%f %f %f %f %f %f %f]\n", 
        solution.solution[0], solution.solution[1], solution.solution[2], solution.solution[3],
        solution.solution[4], solution.solution[5], solution.solution[6]);
    printf("final state      : [%f %f %f %f %f %f %f]\n", solution.finalState[0], solution.finalState[1], solution.finalState[2],
        solution.finalState[3], solution.finalState[4], solution.finalState[5], solution.finalState[6]);
    printf("dvcost           : %f\n", solution.dvcost);
    printf("duration         : %f\n", solution.transferDuration);
    printf("nb of revolutions: %f\n\n", solution.nbRevolutions);
}    

// ---------------------------------------------------------------------------
double relativeError(double curVal, double refVal) {
    if (fabs(refVal) > 0) {
        return fabs((curVal - refVal) / refVal);
    }
    return fabs(curVal - refVal);
}
// ---------------------------------------------------------------------------
void setProblem(LtProblemDefinition &problemDefinition, LtProblemContext &problemContext,
                TerminalConstraintType terminalConstraintType, bool withJ2) {
    problemDefinition.objtype = OBJECTIVE_TYPE::C_TIME;  // objective function type
    problemDefinition.constraintType = terminalConstraintType;
    problemDefinition.dynamicalFormulation = DYNAMICALMODEL_TYPE::D_EQUINOCTIAL_AVERAGING;
    problemDefinition.mu = 3.9860064e+14;
    if (withJ2) {
        problemDefinition.cj2 = 0.00108262688;
    } else {
        problemDefinition.cj2 = 0;
    }
    problemDefinition.radius = 6378136;

    double incInit = 85 * M_PI / 180.;
    double raanInit = 30 * M_PI / 180.;
    std::array<double,6> oeInitial = {6378136 + 400 * 1e3, 0., 0.0001, tan(incInit / 2.) * cos(raanInit), tan(incInit / 2.) * sin(raanInit), 0.};
    problemDefinition.state0 = State(oeInitial, 200);

    double incEnd = 85 * M_PI / 180.;
    double raanEnd = 30 * M_PI / 180.;
    std::array<double,6> oeFinal = {6378136 + 1200 * 1e3, 0., 0.0012, tan(incEnd / 2.) * cos(raanEnd), tan(incEnd / 2.) * sin(raanEnd), 0.};
    problemDefinition.statef = State(oeFinal, 200);

    problemDefinition.spacecraft.propulsion.PropType = PROPULSION_TYPE::PROP_NEP;
    problemDefinition.spacecraft.propulsion.Fth = 0.012;
    problemDefinition.spacecraft.propulsion.g0Isp = 9.80665 * 1300;
    problemDefinition.spacecraft.propulsion.eff = 1.;
    problemDefinition.spacecraft.mass = 200;

    problemContext.performSimulation = false;
    problemContext.performOptimisation = true;
    problemContext.initguess[0] = 0.571801;
    problemContext.initguess[1] =-0.007173;
    problemContext.initguess[2] =-0.004140;
    problemContext.initguess[3] = 0.0001;
    problemContext.initguess[4] = 0.0002;
    problemContext.initguess[5] =-0.056473;
    problemContext.initguess[6] = 0.061549;
    problemContext.initguess_duration = 0.061549;
    problemContext.initGuessMultiStart = false;
    problemContext.initGuessMultiStartMaxAttempts = 0;
    problemContext.rTol = 1e-9;
    problemContext.maxIter = 100;
    problemContext.verbose = 0;
    problemContext.outputFilename[0] = '\0';
    problemContext.outputFormat = DYNAMICALMODEL_TYPE::D_KEPLERIAN;
    problemContext.outputCostates = false;
    problemContext.outputNbOfPoints = 0;
    problemContext.reportFilename[0] = '\0';
}

// ---------------------------------------------------------------------------
TEST_CASE("Optimization test with NLEQ: Case sma+ecc+inc+raan constraint ", "[single-file]") {
    LtProblemDefinition problemDefinition;
    LtProblemContext problemContext;

    setProblem(problemDefinition, problemContext, TerminalConstraintType::C_SMA_ECC_INC_RAAN, false);

    LtProblemSolution solution;
    processSolving(problemDefinition, problemContext, solution, false);

#ifdef PRINT_REF
    printDebugData(solution);
#endif

    REQUIRE(solution.res == 0);

    double tol = 1e-3;
    REQUIRE(fabs(solution.dvcost - 416.295728) < tol);
    REQUIRE(fabs(solution.transferDuration - 79.006988) < tol);
    REQUIRE(fabs(solution.nbRevolutions - 1133.195659) < tol);

    tol = 1e-2;
    REQUIRE(relativeError(solution.finalState[0], problemDefinition.statef.kepler[0]) < tol);
    REQUIRE(relativeError(sqrt(solution.finalState[1] * solution.finalState[1] + solution.finalState[2] * solution.finalState[2]), problemDefinition.statef.kepler[1]) < tol);
    REQUIRE(relativeError(2.*atan(sqrt(solution.finalState[3] * solution.finalState[3] + solution.finalState[4] * solution.finalState[4])), problemDefinition.statef.kepler[2]) < tol);
    REQUIRE(relativeError(atan2(solution.finalState[4], solution.finalState[3]), problemDefinition.statef.kepler[4]) < tol);
    REQUIRE(relativeError(solution.finalState[1], 0.000000) < tol);  // test all to detect behaviour/accuracy changes
    REQUIRE(relativeError(fabs(solution.finalState[2]), 0.001200) < tol); // the sign might not be correct because the constraint is on ecc and not (ex, ey)

    tol = 1e-6;
    REQUIRE(fabs(solution.solution[0] - 0.571800) < tol);
    REQUIRE(fabs(solution.solution[1] - 0.000000) < tol);
    REQUIRE(fabs(solution.solution[2] + 0.045909) < tol);
    REQUIRE(fabs(solution.solution[3] + 0.000000) < tol);
    REQUIRE(fabs(solution.solution[4] - 0.000000) < tol);
    REQUIRE(fabs(solution.solution[5] + 0.056473) < tol);
    REQUIRE(fabs(solution.solution[6] - 0.061549) < tol);
}
// ---------------------------------------------------------------------------
#ifdef USE_KINSOL
TEST_CASE("Optimization test with KinSol: Case sma+ecc+inc+raan constraint ", "[single-file]") {
    LtProblemDefinition problemDefinition;
    LtProblemContext problemContext;

    setProblem(problemDefinition, problemContext, TerminalConstraintType::C_SMA_ECC_INC_RAAN, false);

    problemContext.solverType = SolverType::SOLVER_KINSOL;

    LtProblemSolution solution;
    processSolving(problemDefinition, problemContext, solution, false);

#ifdef PRINT_REF
    printDebugData(solution);
#endif

    REQUIRE(solution.res == 0);

    // since the solver is different, the solution might differ from NLEQ solution

    double tol = 1e-3;
    REQUIRE(fabs(solution.dvcost - 416.2319096118) < tol);
    REQUIRE(fabs(solution.transferDuration - 78.9950727941) < tol);
    REQUIRE(fabs(solution.nbRevolutions - 1133.025227) < tol);

    tol = 1e-2;
    REQUIRE(relativeError(solution.finalState[0], problemDefinition.statef.kepler[0]) < tol);
    REQUIRE(relativeError(sqrt(solution.finalState[1] * solution.finalState[1] + solution.finalState[2] * solution.finalState[2]), problemDefinition.statef.kepler[1]) < tol);
    REQUIRE(relativeError(2. * atan(sqrt(solution.finalState[3] * solution.finalState[3] + solution.finalState[4] * solution.finalState[4])), problemDefinition.statef.kepler[2]) < tol);
    REQUIRE(relativeError(atan2(solution.finalState[4], solution.finalState[3]), problemDefinition.statef.kepler[4]) < tol);
    REQUIRE(relativeError(solution.finalState[1], 0.000000) < tol);  // test all to detect behaviour/accuracy changes
    REQUIRE(relativeError(solution.finalState[2], 0.001200) < tol);  // the sign might not be correct because the constraint is on ecc and not (ex, ey)

    tol = 1e-6;    
    REQUIRE(fabs(solution.solution[0] - 0.571886) < tol);
    REQUIRE(fabs(solution.solution[1] - 0.000000) < tol);
    REQUIRE(fabs(solution.solution[2] - 0.039251) < tol);
    REQUIRE(fabs(solution.solution[3] + 0.000000) < tol);
    REQUIRE(fabs(solution.solution[4] - 0.000000) < tol);
    REQUIRE(fabs(solution.solution[5] + 0.056465) < tol);
    REQUIRE(fabs(solution.solution[6] - 0.061540) < tol);
}
#endif  // USE_KINSOL
