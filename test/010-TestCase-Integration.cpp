// $Id$
// ---------------------------------------------------------------------------
// Let Catch provide main()
#define CATCH_CONFIG_MAIN

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <ctime>

#include <catch2/catch.hpp>

// script utils
#include "Enumerations.hpp"
#include "utils/UtilsInputScript.hpp"
#include "utils/xml/UtilsStringXml.hpp"
#include "utils/xml/DOMTreeErrorReporter.hpp"

#include "HelioLib/Core.hpp"


// ---------------------------------------------------------------------------
TEST_CASE("Integration test: Case 1 run ", "[single-file]") {
    bool xmlRead = false;
    LtProblemDefinition problemDefinition;
    LtProblemContext problemContext;
    std::string gInputScript("data/test_input_GTO_GEO.xml");

    // Initialize the XML4C2 system
    try {
        XMLPlatformUtils::Initialize();
    }
    catch (const XMLException &toCatch) {
        XERCES_STD_QUALIFIER cerr << "Error during Xerces-c Initialization.\n"
             << "  Exception message:"
             << StrX(toCatch.getMessage()) << XERCES_STD_QUALIFIER endl;
        return;
    }

    InputScriptReader *reader = new InputScriptReader();
    if (reader->readXml("data/test_input_GTO_GEO.xml", problemDefinition, problemContext)) {
        xmlRead = true;
    }

    XMLPlatformUtils::Terminate();
    delete reader;

    REQUIRE(xmlRead);

    double tol = 1e-6;
    REQUIRE(problemDefinition.constraintType == TerminalConstraintType::C_ORBIT);

    REQUIRE(fabs(problemDefinition.spacecraft.propulsion.Fth - 0.35) < tol);
    REQUIRE(fabs(problemDefinition.spacecraft.propulsion.g0Isp - 2000*9.80665) < tol);
    REQUIRE(fabs(problemDefinition.spacecraft.mass - 2000) < tol);
    // REQUIRE(fabs(problemDefinition.spacecraft.propulsion.eff - 1) < tol);

    double rEarth = 6378136;
    REQUIRE(fabs(problemDefinition.mu - 3.986004415e14) < tol);  // gravitational parameter
    REQUIRE(fabs(problemDefinition.cj2 - 0) < tol);  // zonal harmonic
    REQUIRE(fabs(problemDefinition.radius - rEarth) < tol);  // Earth radius

    //State state0;  // initial state vector
    double sma = (2 * rEarth + 36000000. + 200000.) / 2.;
    double ecc = (36000000. - 200000.) / (2 * rEarth + 36000000. + 200000.);
    REQUIRE(problemDefinition.state0.type == DYNAMICALMODEL_TYPE::D_ALL);
    REQUIRE(fabs(problemDefinition.state0.kepler[0] - sma) < tol);
    REQUIRE(fabs(problemDefinition.state0.equiAdapt[0] - sma) < tol);
    REQUIRE(fabs(problemDefinition.state0.equi[0] - sma * (1 - ecc * ecc)) < tol);
    REQUIRE(fabs(problemDefinition.state0.kepler[1] - ecc) < tol);
    REQUIRE(fabs(problemDefinition.state0.kepler[2] * RAD2DEG - 7.) < tol);
    REQUIRE(fabs(problemDefinition.state0.kepler[3] - 0.) < tol);
    REQUIRE(fabs(problemDefinition.state0.kepler[4] - 0.) < tol);
    REQUIRE(fabs(problemDefinition.state0.mass - 2000) < tol);

    //State statef;  // final (desired) state vector
    REQUIRE(problemDefinition.statef.type == DYNAMICALMODEL_TYPE::D_EQUINOCTIAL);
    REQUIRE(fabs(problemDefinition.statef.kepler[0] - 42378136.) < tol);
    REQUIRE(fabs(problemDefinition.statef.equiAdapt[0] - 42378136.) < tol);
    REQUIRE(fabs(problemDefinition.statef.equi[0] - 42378136 * (1. - 0.0012 * 0.0012)) < tol);
    REQUIRE(fabs(problemDefinition.statef.equiAdapt[1] - 0.) < tol);
    REQUIRE(fabs(problemDefinition.statef.equiAdapt[2] - 0.0012) < tol);
    REQUIRE(fabs(problemDefinition.statef.equiAdapt[3] - 0.) < tol);
    REQUIRE(fabs(problemDefinition.statef.equiAdapt[4] - 0.) < tol);

    REQUIRE(problemContext.performSimulation == false);
    REQUIRE(problemContext.performOptimisation == true);

    REQUIRE(problemContext.initGuessMultiStart == true);
    REQUIRE(problemContext.initGuessMultiStartMaxAttempts == 5);
    REQUIRE(fabs(problemContext.initguess[0] - 0.720328) < tol);
    REQUIRE(fabs(problemContext.initguess[1] + 0.739166) < tol);
    REQUIRE(fabs(problemContext.initguess[2] + 0.000000) < tol);
    REQUIRE(fabs(problemContext.initguess[3] + 0.312033) < tol);
    REQUIRE(fabs(problemContext.initguess[4] - 0.0000000) < tol);
    REQUIRE(fabs(problemContext.initguess[5] + 0.682988) < tol);
    REQUIRE(fabs(problemContext.initguess_duration - 0.9470926121328614) < tol);

    REQUIRE(fabs(problemContext.rTol - 1e-10) < tol);
    REQUIRE(problemContext.maxIter == 100);
    REQUIRE(problemContext.verbose == 2);

    REQUIRE(problemContext.outputFormat == DYNAMICALMODEL_TYPE::D_KEPLERIAN);
    REQUIRE(problemContext.outputCostates == true);
    REQUIRE(problemContext.outputNbOfPoints == -1);
    REQUIRE(problemContext.outputFilename.compare("trajectory_GTO2GEO.txt") == 0);
    REQUIRE(problemContext.reportFilename.compare("report_GTO2GEO.txt") == 0);
}

// ---------------------------------------------------------------------------
TEST_CASE("Integration test: read solver data ", "[single-file]") {
    bool xmlRead = false;
    LtProblemDefinition problemDefinition;
    LtProblemContext problemContext;
    std::string gInputScript("data/test_input_GTO_GEO.xml");

    // Initialize the XML4C2 system
    try {
        XMLPlatformUtils::Initialize();
    }
    catch (const XMLException &toCatch) {
        XERCES_STD_QUALIFIER cerr << "Error during Xerces-c Initialization.\n"
             << "  Exception message:"
             << StrX(toCatch.getMessage()) << XERCES_STD_QUALIFIER endl;
        return;
    }

    InputScriptReader *reader = new InputScriptReader();
    if (reader->readXml("data/test_input_GTO_GEO.xml", problemDefinition, problemContext)) {
        xmlRead = true;
    }

    XMLPlatformUtils::Terminate();
    delete reader;

    REQUIRE(xmlRead);

    REQUIRE(problemContext.performSimulation == false);
    REQUIRE(problemContext.performOptimisation == true);

    REQUIRE(problemContext.solverType == SolverType::SOLVER_NLEQ);
    REQUIRE(fabs(problemContext.rTol - 1e-10) < 1e-12);
    REQUIRE(problemContext.maxIter  == 100);
    REQUIRE(problemContext.verbose == 2);
}

// ---------------------------------------------------------------------------
TEST_CASE("Integration test: Case with eclipse", "[single-file]") {
    bool xmlRead = false;
    LtProblemDefinition problemDefinition;
    LtProblemContext problemContext;
    std::string gInputScript("data/test_input_GTO_GEO_Eclipse.xml");

    // Initialize the XML4C2 system
    try {
        XMLPlatformUtils::Initialize();
    }
    catch (const XMLException &toCatch) {
        XERCES_STD_QUALIFIER cerr << "Error during Xerces-c Initialization.\n"
             << "  Exception message:"
             << StrX(toCatch.getMessage()) << XERCES_STD_QUALIFIER endl;
        return;
    }

    InputScriptReader *reader = new InputScriptReader();
    if (reader->readXml("data/test_input_GTO_GEO_Eclipse.xml", problemDefinition, problemContext)) {
        xmlRead = true;
    }

    XMLPlatformUtils::Terminate();
    delete reader;

    REQUIRE(xmlRead);

    double tol = 1e-6;

    REQUIRE(fabs(problemDefinition.eclipse.occ_radius - 6378136.0) < tol);  // occulting radius

    // initial state vector
    REQUIRE(fabs(problemDefinition.eclipse.occ_orbit_epoch - 0) < tol);  // reference relative epoch
    REQUIRE(problemDefinition.eclipse.occ_orbit_type == DYNAMICALMODEL_TYPE::D_KEPLERIAN);  // orbit type

    // test keplerian parameters
    REQUIRE(fabs(problemDefinition.eclipse.occ_orbit[0] - 149596000000.0) < tol);
    REQUIRE(fabs(problemDefinition.eclipse.occ_orbit[1] - 0.0167086) < tol);
    REQUIRE(fabs(problemDefinition.eclipse.occ_orbit[2] * RAD2DEG - 23.44) < tol);
    REQUIRE(fabs(problemDefinition.eclipse.occ_orbit[3] - 0.) < tol);
    REQUIRE(fabs(problemDefinition.eclipse.occ_orbit[4] - 0.) < tol);
    REQUIRE(fabs(problemDefinition.eclipse.occ_orbit[5] - 0.) < tol);

    REQUIRE(problemDefinition.eclipse.active);
}
