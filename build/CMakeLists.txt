cmake_minimum_required(VERSION 3.3)
PROJECT(realoot)

# ====================================================================
set(APP_VERSION "1.0.0  [23/10/2013]")
configure_file( ../src/conf/version_config.hpp.in ../src/conf/version_config.hpp )

# Module path setup.
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules ${CMAKE_MODULE_PATH}")

option(BUILD_SHARED_LIB "Build shared library" OFF)

# option: set the quadrature order
add_definitions("-DQUAD_ORDER=12")
add_definitions("-DWITH_QUADRATURE")

set(SRC_PATH "${CMAKE_CURRENT_SOURCE_DIR}/../src/")
set(THIRDPARTY_SRC_PATH "../thirdparty/")

# ====================================================================
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2")

if (_MSC_VER OR MSVC)
	# Microsoft Visual Studio
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -W4")

else()
	# GCC like compiler
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -s")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wextra")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wnon-virtual-dtor")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wnoexcept")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wlogical-op")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wdeprecated")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-unused-parameter")

	#SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wconversion")
	#SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wunused-parameter")

	if(CMAKE_CXX_COMPILER_VERSION VERSION_GREATER "6.9")
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wint-in-bool-context")
	endif()

	if (NOT WIN32)
		add_definitions(-fPIC)
	endif()

endif()


# ===============================================================
# INCLUDE DIRS
include_directories("${HELIOLIB_INCLUDE_DIRS}")
include_directories("${XercesC_INCLUDE_DIRS}")
include_directories("${Boost_INCLUDE_DIRS}")        # required in UtilsStringXml for shared_ptr
include_directories("${SRC_PATH}")
include_directories("${SRC_PATH}/conf")
include_directories("${SRC_PATH}/core/")
include_directories("${SRC_PATH}/dynamics/")
include_directories("${SRC_PATH}/dynamics/frame/")
include_directories("${SRC_PATH}/continuation/")
include_directories("${SRC_PATH}/ocp/")
include_directories("${SRC_PATH}/utils/")
include_directories("${SRC_PATH}/utils/date/")
include_directories("${SRC_PATH}/utils/json/")
include_directories("${SRC_PATH}/utils/xml/")
include_directories("${SRC_PATH}/utils/maths/")
include_directories("${SRC_PATH}/utils/maths/integration/")
include_directories("${SRC_PATH}/utils/maths/quad/")
include_directories("${SRC_PATH}/utils/maths/solver/")
include_directories("${SRC_PATH}/utils/report/")
include_directories("${THIRDPARTY_DIRECTORY}")
include_directories("${THIRDPARTY_DIRECTORY}/Eigen")
include_directories("${THIRDPARTY_SRC_PATH}/Eigen")
include_directories("${THIRDPARTY_SRC_PATH}/Eigen/Eigen")
include_directories("${THIRDPARTY_SRC_PATH}")
include_directories("${THIRDPARTY_SRC_PATH}/blas")
include_directories("${THIRDPARTY_SRC_PATH}/netlib")
include_directories("${THIRDPARTY_SRC_PATH}/nlohmann")
include_directories("/usr/include/")
include_directories("/usr/local/include/")

if(USE_KINSOL)
	include_directories("${THIRDPARTY_SRC_PATH}/sundials-5.3.0/include/")
	include_directories("${THIRDPARTY_DIRECTORY}/sundials-5.3.0/include/")
endif()

# ====================================================================
# LINK LIBS
link_directories("/usr/lib")
link_directories("/usr/local/lib")
if (_MSC_VER OR MSVC)
else()
	link_libraries(dl)
endif()

link_directories(${HELIOLIB_LIBRARY_DIR})
link_libraries(${HELIOLIB_LIBRARIES})
link_libraries(sofa)

link_directories(${XercesC_LIBRARY})
link_libraries(${XercesC_LIBRARIES})

if (USE_KINSOL)
	link_directories(${SUNDIALS_LIBRARY_DIR})
	link_libraries(${SUNDIALS_SOLVER_LIB})
endif()
# ====================================================================
# SRC
set(THIRD_PARTY_SRCS
	${THIRDPARTY_SRC_PATH}/netlib/utilsNetlib.cpp
	${THIRDPARTY_SRC_PATH}/netlib/dgbfa.cpp
	${THIRDPARTY_SRC_PATH}/netlib/dgbsl.cpp
	${THIRDPARTY_SRC_PATH}/netlib/dgefa.cpp
	${THIRDPARTY_SRC_PATH}/netlib/dgesl.cpp
)

set(SRCS
	../src/core/State.cpp
	../src/core/Enumerations.cpp
	../src/continuation/ContinuationSolver.cpp
	../src/dynamics/NumericalPropagator.cpp
	../src/dynamics/StateNumericalPropagator.cpp
	../src/dynamics/thruster/Thruster.cpp
	#../src/dynamics/thruster/ThrusterSep.cpp
	../src/dynamics/thruster/ThrusterNep.cpp
	../src/dynamics/thruster/ThrusterFactory.cpp
	../src/dynamics/force/ZonalPerturbation.cpp
	../src/ocp/AveragedProblemNlp.cpp	
	../src/ocp/MinTimeOptimalControl.cpp	
	../src/ocp/OptimalControlBase.cpp	
	../src/ocp/ProblemSolver.cpp	
	../src/ocp/constraints/TerminalConstraintBase.cpp	
	../src/ocp/constraints/ConstraintOrbit.cpp	
	../src/ocp/constraints/ConstraintSmaEccInc.cpp	
	../src/ocp/constraints/ConstraintSmaExEyInc.cpp	
	../src/ocp/constraints/ConstraintSmaEccIncRaan.cpp
	../src/ocp/constraints/ConstraintEnergy.cpp	
	../src/ocp/ode/TDynamicsEquinoctial.cpp	
	../src/ocp/ode/TDynamicsAveragedEquinoctial.cpp
	../src/utils/report/EphemerisReport.cpp
	../src/utils/report/SolutionReport.cpp
	../src/utils/maths/integration/Integrator.cpp
	../src/utils/maths/solver/NonLinearEquationSolverOptions.cpp
	../src/utils/maths/solver/NonLinearEquationSolver.cpp
)

if(USE_KINSOL)
	SET(SRCS ${SRCS}
		../src/utils/maths/solver/KinSolWrapper.cpp)
endif()

SET(SRC_INTERFACE 
	../src/utils/AbstractInputScriptReader.cpp
	../src/utils/xml/DOMTreeErrorReporter.cpp
	../src/utils/json/InputScriptJsonReader.cpp
	../src/utils/xml/InputScriptXmlReader.cpp
	../src/utils/UtilsInputScript.cpp
	../src/utils/UtilsEnum.cpp
	../src/LtAvgOcpMainInterface.cpp
)
# ====================================================================
IF(BUILD_SHARED_LIB)
	add_library(LowThrustOcpShared SHARED ${SRCS} ${THIRD_PARTY_SRCS})

	install(TARGETS LowThrustOcpShared
        RUNTIME DESTINATION lib
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        COMPONENT library
    )
ENDIF()

# ====================================================================
add_library(LowThrustOcpStatic STATIC ${SRCS} ${SRC_INTERFACE} ${THIRD_PARTY_SRCS})

install(TARGETS LowThrustOcpStatic
	RUNTIME DESTINATION lib
	LIBRARY DESTINATION lib
	ARCHIVE DESTINATION lib
	COMPONENT library
)

# ====================================================================
add_executable(realootApp ../src/mainRealoot.cpp)
target_link_libraries(realootApp LowThrustOcpStatic)

install(TARGETS realootApp
	RUNTIME DESTINATION .
)
