README
=========

## Preambule

### Purpose
The application solves the averaged optimal minimum time orbit transfer problem. It is initially inspired by [MIPELEC] that uses the averaged optimal control formulation.
This application however includes important upgrade, such as:
  * different terminal conditions
  * inclusion of zonal gravitational field perturbation to model secular evolution of node and apoapsis lines, around an oblate body.  

IMPORTANT WARNINGS
1. The optimal control is the solution of the averaged optimal control problem. 
	* It cannot be used as such for high fidelity propagation, 
	* It cannot currently be used for rendezvous problems (e.g. interplanetary)
2. The solution starts diverging from the non-averaged optimal control problem solution for very large thrust accelerations.


### Example GTO transfers
<img src="examples/assets/GTO2GEO_3d.png" width=300/>
<img src="examples/assets/SSTO2GEO_3d.png" width=300/>
<img src="examples/assets/GTO2GEO_Eclipse_0p35N_36000_200.png" width=300/>

### Example Rapid trade analysis
<img src="examples/assets/ex3_surf_dv.png" width=200/>

## Compiling and Running

### Requirements
1. A C++11 compiler (The build chain is based on CMake)
2. The application should compile on Windows, Linux and Max. I have actually tested it on Windows (Cygwin) and Linux.
3. Optionally Python

### Download the sources
Download the latest version source code from the main page as a zip.


### Third-parties
There are mandatory third-party products:
1. HelioLib, my spaceflyght dynamics C++ library
2. Eigen, a very nice C++ math library. You can found it here: [eigen]
3. Apache Xerces-C++ XML Parser library is necessary for linking.
   * Xerces is available as binary distribution for Cygwin.
   * For Linux, install liberces-c-dev.
   * Any other case, download and compile the source.
4. Boost for Python wrapper

[eigen]: eigen.tuxfamily.org/index.php

### Compiling
The building and compilation is based on cross platform CMake configuration file.
Go into the root folder. 
In a console, type
```
mkdir build_directory
cd build_directory
cmake -DCMAKE_INSTALL_PREFIX=[your install path] ..
```

where [your install path] defines the installation destination directory ("prefix"). 
(If omitted, the build would most probably be installed in /usr/local.)

Then:
```
make
make install
```

The build [your install path] folder should contain:
- an executable, 
- lib directory with the static and share libraries, 
- the Python package,
- examples directory.

If you want to compile the unit tests, add DBUILD_TESTS to the cmake call, 
```
cmake -DBUILD_TESTS=ON .
```
and then run: ```ctest``` or ```make test```


If you do not want to the Python Package, 
```
cmake -DBUILD_PY_PKG=OFF .
```

### Using the Application
In the build directory, you should now be able to run the app by running the executable.
The program takes as input a script, which defines the problem and the program options. Have a look 
at input.xml for guidance on how to setup the problem.
The program is then launch with

	realoot [input xml file]



### Using the Module under python
Import
```
	from realoot import core, utils, main as lt
```

Then either, as for the application, open a script file with
```
	reader = utils.InputScriptReader()
	problemDefinition, problemContext = reader.readXml("examples/ex2.xml")
	lt.solve(problemDefinition, problemContext, problemSolution, True)
```
	
Or define manually your problem filling the structure
```
	problemContext = core.LtProblemContext()
	problemDefinition = core.LtProblemDefinition()
```
Then call
```
	lt.solve(problemDefinition, problemContext, problemSolution, True)
```

Please see the examples for details.

Likewise to the standalone application, a command line tool is also available with Python

	python -m realoot -i input_script.xml


## Credits
  * [MIPELEC](https://logiciels.cnes.fr/fr/content/mipelec)
  * [An averaging optimal control tool for low thrust minimum-time transfers](https://logiciels.cnes.fr/sites/default/files/attached_doc/An%20averaging%20optimal%20control%20tool%20for%20low%20thrust%20minimum-time%20transfers.pdf)
  
  
