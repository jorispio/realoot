//
#if defined(_WIN32)
#include <Python.h>
#include <cmath>
#else
#include <Python.h>
#include <cmath>
#endif

#if PY_MAJOR_VERSION < 2 || (PY_MAJOR_VERSION == 2 && PY_MINOR_VERSION < 6)
#error Minimum supported Python version is 2.6.
#endif

#include <boost/python.hpp>
#include <boost/python/class.hpp>
#include <boost/python/converter/registry.hpp>
#include <boost/python/copy_const_reference.hpp>
#include <boost/python/def.hpp>
#include <boost/python/docstring_options.hpp>
#include <boost/python/enum.hpp>        // to expose c++ enum
#include <boost/python/module.hpp>
#include <boost/python/operators.hpp>
#include <boost/python/overloads.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/self.hpp>
#include <boost/utility.hpp>

#include <boost/python/extract.hpp>
#include <boost/python/list.hpp>
#include <boost/python/to_python_converter.hpp>

#include "DataTypes.hpp"
#include "HelioLib/Core.hpp"

using namespace std;
using namespace boost::python;

// convenience macro to define constants getter
#define MACRO_DEFINE_CONSTANT_GETTER(arg)     \
    static inline double get_##arg() {        \
        return static_cast<double>(##arg);    \
    }

static inline double get_AU() {    return AU;    }
static inline double get_SUN_MU() {    return SUN_MU;    }
static inline double get_EARTH_MU() {    return EARTH_MU;    }
static inline double get_EARTH_J2() {    return EARTH_J2;    }
static inline double get_EARTH_RADIUS() {    return EARTH_RADIUS;    }
static inline double get_DEG2RAD() {    return DEG2RAD;    }
static inline double get_RAD2DEG() {    return RAD2DEG;    }
static inline double get_DAY2SEC() {    return DAY2SEC;    }
static inline double get_SEC2DAY() {    return SEC2DAY;    }
static inline double get_DAY2YEAR() {    return DAY2YEAR;    }
static inline double get_G0() {    return g0;    }


#define MACRO_DECLARE_CONSTANT(arg) def("_get_" #arg, &get_##arg);

BOOST_PYTHON_MODULE(_constants) {
    // Expose the astrodynamical constants.
    MACRO_DECLARE_CONSTANT(AU);
    MACRO_DECLARE_CONSTANT(SUN_MU);
    MACRO_DECLARE_CONSTANT(EARTH_MU);
    MACRO_DECLARE_CONSTANT(EARTH_J2);
    MACRO_DECLARE_CONSTANT(EARTH_RADIUS);
    MACRO_DECLARE_CONSTANT(DEG2RAD);
    MACRO_DECLARE_CONSTANT(RAD2DEG);
    MACRO_DECLARE_CONSTANT(DAY2SEC);
    MACRO_DECLARE_CONSTANT(SEC2DAY);
    MACRO_DECLARE_CONSTANT(DAY2YEAR);
    MACRO_DECLARE_CONSTANT(G0);
}

