//
#if defined(_WIN32)
#include <Python.h>
#include <cmath>
#else
#include <Python.h>
#include <cmath>
#endif

#if PY_MAJOR_VERSION < 2 || (PY_MAJOR_VERSION == 2 && PY_MINOR_VERSION < 6)
#error Minimum supported Python version is 2.6.
#endif

#include <boost/python.hpp>
#include <boost/python/class.hpp>
#include <boost/python/converter/registry.hpp>
#include <boost/python/copy_const_reference.hpp>
#include <boost/python/def.hpp>
#include <boost/python/docstring_options.hpp>
#include <boost/python/enum.hpp>        // to expose c++ enum
#include <boost/python/module.hpp>
#include <boost/python/operators.hpp>
#include <boost/python/overloads.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/self.hpp>
#include <boost/utility.hpp>
//#include <boost/python/vector_indexing_suite.hpp>

#include "core/DataTypes.hpp"
#include "core/Enumerations.hpp"
#include "core/LtProblemDefinition.hpp"
#include "core/LtProblemContext.hpp"
#include "core/LtEclipseParameters.hpp"
#include "HelioLib/Core.hpp"
#include "LtAvgOcpMainInterface.hpp"
#include "UtilsInputScript.hpp"

using namespace boost::python;
namespace bp = boost::python;
//namespace np = boost::python::numpy;

#include "converter/python_container_conversion.h"

typedef std::array<double, 7> array7;

//
#define PYTHON_REGISTER_CONTAINER_CONVERTER(T, policy)                                                                 \
    {                                                                                                                  \
        bp::type_info info = bp::type_id<T>();                                                   \
        const bp::converter::registration *reg = bp::converter::registry::query(info);           \
        if (reg == NULL) {                                                                                             \
            to_tuple_mapping<T>();                                                                                     \
            from_python_sequence<T, policy>();                                                                         \
        } else if ((*reg).m_to_python == NULL) {                                                                       \
            to_tuple_mapping<T>();                                                                                     \
            from_python_sequence<T, policy>();                                                                         \
        }                                                                                                              \
    }

class LtProblemContext_ : public LtProblemContext {
 public:
     void setInitialGuessFromList(bp::list& plist) {
        std::array<double, NB_UNKNOWN> larray;
        for (int i = 0; i < len(plist); ++i) {
            larray[i] = bp::extract<double>(plist[i]);
        }
        setInitialGuess(larray);
     }
};


bp::tuple readXml(std::string gInputScript) {
    InputScriptReader *reader = new InputScriptReader();
    LtProblemDefinition problemDefinition = LtProblemDefinition();
    LtProblemContext_ problemContext = LtProblemContext_();
    reader->readXml(gInputScript, problemDefinition, problemContext);
    delete reader;
    return bp::make_tuple(problemDefinition, problemContext);
}


bp::tuple readJson(std::string gInputScript) {
    InputScriptReader *reader = new InputScriptReader();
    LtProblemDefinition problemDefinition = LtProblemDefinition();
    LtProblemContext_ problemContext = LtProblemContext_();
    reader->readJson(gInputScript, problemDefinition, problemContext);
    delete reader;
    return bp::make_tuple(problemDefinition, problemContext);
}


int propagate(LtProblemDefinition &problemDefinition, LtProblemContext_ &problemContext, bool verbose, double *lastPoint) {
    ProblemScaling scaling = ProblemScaling();
    return processPropagation(problemDefinition, problemContext, verbose, lastPoint, scaling);
}

int solve(LtProblemDefinition problemDefinition, LtProblemContext_ problemContext, LtProblemSolution &problemSolution, bool verbose) {
    return processSolving(problemDefinition, problemContext, problemSolution, verbose);
}

BOOST_PYTHON_MODULE(_main) {

    Py_Initialize();

    PYTHON_REGISTER_CONTAINER_CONVERTER(std::vector<double>, variable_capacity_policy);
    PYTHON_REGISTER_CONTAINER_CONVERTER(array7, fixed_size_policy);
    
    //
    def("propagate", &propagate,
            "Propagate a minimum-time solution");
    def("solve", &solve,
            "Solve the low-thrust average minimum-time optimal control problem");

    //
    def("readXml", &readXml,
            "Read XML problem context file");

    def("readJson", &readJson,
            "Read JSON problem context file");

    // Exposing enums
    enum_<TerminalConstraintType>("TerminalConstraintType")
        .value("C_ENERGY", TerminalConstraintType::C_ENERGY)
        .value("C_ORBIT", TerminalConstraintType::C_ORBIT)
        .value("C_SMA_ECC_INC", TerminalConstraintType::C_SMA_ECC_INC)
        .value("C_SMA_ECC_INC_AOP", TerminalConstraintType::C_SMA_ECC_INC_AOP)
        .value("C_SMA_ECC_INC_RAAN", TerminalConstraintType::C_SMA_ECC_INC_RAAN)
        .export_values();

    enum_<DYNAMICALMODEL_TYPE>("OrbitRepresentation")
        .value("D_EQUINOCTIAL", DYNAMICALMODEL_TYPE::D_EQUINOCTIAL)
        .value("D_KEPLERIAN", DYNAMICALMODEL_TYPE::D_KEPLERIAN)
        .export_values();

    // Exposing structures / classes
    class_<LtProblemContext_>("LtProblemContext", init<>())
        .def_readwrite("sProblemName", &LtProblemContext::sProblemName)
        .def_readwrite("initGuessMultiStart", &LtProblemContext::initGuessMultiStart)
        .def_readwrite("initGuessMultiStartMaxAttempts", &LtProblemContext::initGuessMultiStartMaxAttempts)
        .add_property("initguess", &LtProblemContext::getInitguess, &LtProblemContext::setInitialGuess)
        .def_readwrite("initguess_duration", &LtProblemContext::initguess_duration)
        .def("getInitialGuess", &LtProblemContext::getInitguess)
        .def("setInitialGuess", &LtProblemContext_::setInitialGuessFromList)
        .def("setInitialGuess", &LtProblemContext::setInitialGuessByValue)
        .def_readwrite("rTol", &LtProblemContext::rTol)
        .def_readwrite("maxIter", &LtProblemContext::maxIter)
        .def_readwrite("verbose", &LtProblemContext::verbose)
        .def_readwrite("outputFilename", &LtProblemContext::outputFilename)
        .def_readwrite("outputFormat", &LtProblemContext::outputFormat)
        .def_readwrite("outputCostates", &LtProblemContext::outputCostates)
        .def_readwrite("outputNbOfPoints", &LtProblemContext::outputNbOfPoints)
        .def_readwrite("reportFilename", &LtProblemContext::reportFilename);

    class_<LtProblemDefinition>("LtProblemDefinition", init<>())
        .def_readwrite("objtype", &LtProblemDefinition::objtype)
        .def_readwrite("constraintType", &LtProblemDefinition::constraintType)
        .def_readwrite("dynamicalFormulation", &LtProblemDefinition::dynamicalFormulation)
        .def_readwrite("spacecraft", &LtProblemDefinition::spacecraft)
        .def_readwrite("mu", &LtProblemDefinition::mu)
        .def_readwrite("cj2", &LtProblemDefinition::cj2)
        .def_readwrite("radius", &LtProblemDefinition::radius)
        .def_readwrite("spacecraft", &LtProblemDefinition::spacecraft)
        .def_readwrite("state0", &LtProblemDefinition::state0)
        .def_readwrite("statef", &LtProblemDefinition::statef);

    class_<LtThrusterData>("ThrusterData", init<double, double>())
        //.def_readwrite("PropType", &ThrusterData::PropType)
        .def_readwrite("Fth", &LtThrusterData::Fth)
        .def_readwrite("g0Isp", &LtThrusterData::g0Isp)
        .def_readwrite("eff", &LtThrusterData::eff)
        .def_readwrite("maxSEPFth", &LtThrusterData::maxSEPFth)
        .def_readwrite("rscale", &LtThrusterData::rscale);

    class_<LtSpacecraftData>("SpacecraftData", init<std::string, LtThrusterData, double>())
        .def_readwrite("name", &LtSpacecraftData::name)
        .def_readwrite("propulsion", &LtSpacecraftData::propulsion)
        .def_readwrite("mass", &LtSpacecraftData::mass);

    class_<LtEclipseParameters>("EclipseParameters", init<>())
        .def_readwrite("name", &LtEclipseParameters::name)
        .def_readwrite("occultingBodyRadius", &LtEclipseParameters::occ_radius)
        .def_readwrite("occultingBodyOrbitType", &LtEclipseParameters::occ_orbit_type)
        .def_readwrite("occultingBodyOrbitEpoch", &LtEclipseParameters::occ_orbit_epoch)
        .add_property("occultingBodyOrbit", &LtEclipseParameters::getOrbitArray, &LtEclipseParameters::setOrbitArray);

    class_<LtProblemSolution>("LtProblemSolution", init<>())
        .def_readwrite("sProblemName", &LtProblemSolution::sProblemName)
        .def_readwrite("dvcost", &LtProblemSolution::dvcost)
        .def_readonly("duration", &LtProblemSolution::getTransferDuration)
        .def_readwrite("nbRevolutions", &LtProblemSolution::nbRevolutions)
        .def_readonly("solution", &LtProblemSolution::getSolutionVector)
        .def_readonly("finalLongitude", &LtProblemSolution::getSolutionLongitude)
        .def_readwrite("res", &LtProblemSolution::res);

    // exposing class
    class_<State>("State", init<>())
        .def(init<double, double, double, double, double, double, Frame*, double>())
        .def_readwrite("type", &State::type)
        .def_readwrite("mass", &State::mass)
        .def("setMass", &State::setMass)
        .def("getSma", &State::getSma)
        .def("setSma", &State::setSma)
        .add_property("sma", &State::getSma, &State::setSma)
        .def("setInclination", &State::setInclination)
        .add_property("inclination", &State::getInclination, &State::setInclination)
        .def("setEccentricity", &State::setEccentricity)
        .add_property("eccentricity", &State::getEccentricity, &State::setEccentricity)
        .def("setArgumentOfPericenter", &State::setArgumentOfPericenter)
        .add_property("argumentOfPericenter", &State::getArgumentOfPericenter , &State::setArgumentOfPericenter)                
        .def("setRightAscensionOfAscendingNode", &State::setRightAscensionOfAscendingNode)
        .add_property("rightAscensionOfAscendingNode", &State::getRightAscensionOfAscendingNode, &State::setRightAscensionOfAscendingNode);
}
