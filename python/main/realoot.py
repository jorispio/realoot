# @author Joris OLYMPIO
#
import getopt
import glob
import os
import sys

from . import _main as lt


def _optimize(script_filename, verbose):
    for file in glob.glob(script_filename):
        if verbose:
            print("  File: ", file)

        ~, file_extension = os.path.splitext(file)
        if '.json' is file_extension.lower():
            problemDefinition, problemContext = lt.readJson(file)
        elif '.xml' is file_extension.lower():
            problemDefinition, problemContext = lt.readXml(file)
        else:
            print("Unknown input file extension");
            return

        problemSolution = lt.LtProblemSolution()
        lt.solve(problemDefinition, problemContext, problemSolution, verbose)


def main(argv):
    help_str = "python -m realoot <inputfile>"

    try:
        opts, args = getopt.getopt(argv, "hi:", ["input="])
    except getopt.GetoptError:
        print(help_str)
        sys.exit(2)

    inputfile = None
    verbose = False
    for opt, arg in opts:
        if opt == "-h":
            print(help_str)
            sys.exit()
        elif opt == "-v":
            verbose = True
        elif opt in ("-i", "--input"):
            inputfile = arg

    if not inputfile:
        print(help_str)
        sys.exit()

    _optimize(inputfile, verbose)


if __name__ == "__main__":
    main(sys.argv[1:])
