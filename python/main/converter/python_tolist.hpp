#ifndef __CONVERTER_PYTHON_TOLIST_HPP_
#define __CONVERTER_PYTHON_TOLIST_HPP_

#include <boost/python/extract.hpp>
#include <boost/python/list.hpp>
#include <boost/python/to_python_converter.hpp>


template <typename ContainerType>
struct to_list {
    static PyObject *convert(ContainerType const &a) {
        boost::python::list result;
        typedef typename ContainerType::const_iterator const_iter;
        for (const_iter p = a.begin(); p != a.end(); p++) {
            result.append(boost::python::object(*p));
        }
        return boost::python::incref(result.ptr());
    }

    static const PyTypeObject *get_pytype() {
        return &PyList_Type;
    }
};

#endif  // __CONVERTER_PYTHON_TOLIST_HPP_
