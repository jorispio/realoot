#ifndef __CONVERTER_PYTHON_TOTUPLE_HPP_
#define __CONVERTER_PYTHON_TOTUPLE_HPP_

#include <boost/python/extract.hpp>
#include <boost/python/list.hpp>
#include <boost/python/to_python_converter.hpp>
#include <boost/python/tuple.hpp>


template <typename ContainerType>
struct to_tuple {
    static PyObject *convert(ContainerType const &a) {
        boost::python::list result;
        typedef typename ContainerType::const_iterator const_iter;
        for (const_iter p = a.begin(); p != a.end(); p++) {
            result.append(boost::python::object(*p));
        }
        return boost::python::incref(boost::python::tuple(result).ptr());
    }

    static const PyTypeObject *get_pytype() {
        return &PyTuple_Type;
    }
};

#endif  // __CONVERTER_PYTHON_TOTUPLE_HPP_
