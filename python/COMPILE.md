README
=========

## Preambule

The application solves the averaged optimal minimum time orbit transfer problem. It is initially inspired by [MIPELEC] that uses the averaged optimal control formulation.
This application however includes important upgrade, such as:
  * different terminal conditions
  * inclusion of zonal gravitational field perturbation to model secular evolution of node and apoapsis lines, around an oblate body.  

IMPORTANT WARNINGS
1. The optimal control is the solution of the averaged optimal control problem. 
	* It cannot be used as such for high fidelity propagation, 
	* It cannot currently be used for rendezvous problems (e.g. interplanetary)
2. The solution starts diverging from the non-averaged optimal control problem solution for very large thrust accelerations.


## Compiling and Running

### Requirements
1. A C++11 compiler (The build chain is based on CMake)
2. The application should compile on Windows, Linux and Max. I have actually tested it on Windows (Visual Studio and Cygwin) and Linux.
3. Optionally Python

### Download the sources
Download the latest version source code from the main page as a zip.


### Third-parties
There are mandatory third-party products:
1. Eigen, a very nice C++ math library. You can found it here: [eigen]
2. Apache Xerces-C++ XML Parser library is necessary for linking [xerces-c].
   * Xerces is available as binary distribution for Cygwin.
   * For Linux, install liberces-c-dev.
   * Any other case, download and compile the source.
3. Boost for Python wrapper. Binary can be found at [boost]

[eigen]: eigen.tuxfamily.org/index.php
[xerces-c]: https://xerces.apache.org/xerces-c/
[boost]: https://sourceforge.net/projects/boost/files/boost-binaries/1.72.0/


### Compiling
The building and compilation is based on cross platform CMake configuration file.
Go into the root folder. 
In a console, type
```
mkdir build_directory
cd build_directory
cmake -DCMAKE_INSTALL_PREFIX=[your install path] ..
```

where [your install path] defines the installation destination directory ("prefix"). 
(If omitted, the build would most probably be installed in /usr/local.)

Then:
```
make
make install
```

The build [your install path] folder should contain:
- an executable, 
- lib directory with the static and share libraries, 
- the Python package,
- examples directory.

If you want to compile the unit tests, add BUILD_TESTS to the cmake call, 
```
cmake -DBUILD_TESTS=ON .
```

If you do not want to the Python Package, 
```
cmake -DBUILD_PY_PKG=OFF .
```
Use PYTHON_ROOT_DIR to set your own installation of Python.

To run Python unit tests, run:
```
python -m unittest discover -v --start-directory tests
```

	
## Credits
  * [MIPELEC]: https://logiciels.cnes.fr/fr/content/mipelec
  * [An averaging optimal control tool for low thrust minimum-time transfers](https://logiciels.cnes.fr/sites/default/files/attached_doc/An%20averaging%20optimal%20control%20tool%20for%20low%20thrust%20minimum-time%20transfers.pdf)
  
  
