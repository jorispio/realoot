import main as constants
from main import LtProblemContext
import math

import unittest


class TestCore(unittest.TestCase):

    def test_constants(self):    
        assert constants.AU == 149597870660.0
        assert constants.MU_SUN == (1.32712440018 * 1e20)
        assert constants.MU_EARTH == 3.9860064e+14
        assert constants.EARTH_J2 == 0.00108263
        assert constants.EARTH_RADIUS == 6378137
        assert constants.DEG2RAD == math.pi / 180.
        assert constants.RAD2DEG == 180. / math.pi
        assert constants.DAY2SEC == 86400.
        assert constants.SEC2DAY == 1. / 86400.
        assert constants.DAY2YEAR == 365.25
        assert constants.G0 == 9.80665


if __name__ == "__main__":
    unittest.main()
