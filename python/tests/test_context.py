import main as lt
from main import LtProblemContext


import math
#import pkg_resources
import unittest


class TestContext(unittest.TestCase):
    def test_initguess_vector(self):
        problemContext = LtProblemContext()
        problemContext.setInitialGuess(0.584294, -0.007173, -0.004140, 0, 0,-0.063167, 0.1)
        self.assertListEqual(problemContext.initguess, [0.584294, -0.007173, -0.004140, 0.0, 0.0, 0.1,-0.063167]) # note last two items lm, ltheta are reversed


    def test_read_context_file(self):
        #resource_package = "realoot.examples"
        #filename = pkg_resources.resource_filename(resource_package, "ex2.xml")
        filename = "./examples/ex2.xml" 
        problemDefinition, problemContext = lt.readXml(filename)	    
        self.assertEqual(problemDefinition.state0.getSma(), 6778136.0)
        self.assertEqual(problemDefinition.statef.getSma(), 7678136.0)
        self.assertEqual(problemDefinition.state0.mass, 200.)
        self.assertEqual(problemDefinition.statef.mass, 0.)
        self.assertEqual(problemDefinition.spacecraft.propulsion.g0Isp, 9.80665 * 1300)
        self.assertEqual(problemDefinition.spacecraft.propulsion.Fth, 0.012)
        self.assertEqual(problemDefinition.cj2, 0.0)
        
        self.assertEqual(problemContext.reportFilename, "report_LEO2LEO.txt")
        self.assertEqual(problemContext.outputFilename, "trajectory_LEO2LEO.txt") 
        self.assertEqual(problemContext.maxIter, 100)


if __name__ == "__main__":
    unittest.main()
