# This example show the influence of the J2 on the cost of elliptic transfers.
from realoot.main import LtProblemContext, LtProblemDefinition, LtProblemSolution, SpacecraftData, ThrusterData, State, TerminalConstraintType
from realoot.main import solve
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import realoot as lt
from realoot.utils import deg2rad
import numpy as np
from numpy import asarray
from numpy import savez

import os.path
from os import path
import time 

RE = 6378136.

def save_data_txtfile(save_file, sma_0_grid, ecc_0_grid, inc_0_grid, grid_v, grid_t, grid_v_j2, grid_t_j2, statef):
    with open(save_file, "w") as f:
        f.write("#SMA_0, ECC_0, SMA_F, ECC_F, INC_F, DV, TOF, DV_J2, TOF_J2\n")
        for k in range(0, len(inc_0_grid)):
            for i in range(0, len(sma_0_grid)):
                for j in range(0, len(ecc_0_grid)):
                    f.write("{} {} {} {} {} {} {} {} {} {}\n".format(sma_0_grid[i], ecc_0_grid[j], inc_0_grid[k],
                    statef.sma, statef.eccentricity, statef.inclination, 
                    grid_v[k][j][i], grid_t[k][j][i], grid_v_j2[k][j][i], grid_t_j2[k][j][i]))
    pass


def generate_costate(state0, statef):
    pass

def solve_batch(problemContext, problemDefinition, sma_0_grid, ecc_0_grid, inc_0_grid, save_file=None):    
    problemSolution = LtProblemSolution()    

    #problemContext.setInitialGuess(0.584294, -0.007173, -0.004140, 0, 0,-0.063167, 0.1)  # LEO
    problemContext.setInitialGuess(0.720328, -0.739166, -0.000000, -0.312033, 0.0000000, -0.682988, 0.9470926121328614) # GTO/GEO
   

    if save_file and path.exists(save_file):
        print("Loading cache file: ", save_file)
        npzfile = np.load(save_file)
        sma_0_grid = npzfile['x']
        ecc_0_grid = npzfile['y']
        inc_0_grid = npzfile['z']
        grid_v = npzfile['r']
        grid_t = npzfile['z']
        grid_v_j2 = npzfile['t']
        grid_t_j2 = npzfile['u']        
        num  = sum(y <= 0. for row in grid_v for x in row for y in x)
        print("Convergence ratio: {} %".format(100 - num * 100. / len(sma_0_grid) / len(ecc_0_grid)))
    else:
        grid_v = [[[0 for _ in sma_0_grid] for _ in ecc_0_grid] for _ in inc_0_grid]
        grid_t = [[[0 for _ in sma_0_grid] for _ in ecc_0_grid] for _ in inc_0_grid]
        grid_v_j2 = [[[0 for _ in sma_0_grid] for _ in ecc_0_grid] for _ in inc_0_grid]
        grid_t_j2 = [[[0 for _ in sma_0_grid] for _ in ecc_0_grid] for _ in inc_0_grid]

    print("grid: {}x{}x{}".format(len(inc_0_grid), len(sma_0_grid), len(ecc_0_grid)))
    
    i = 0
    ncv = 0
    ncv_j2 = 0
    elapsed_times = []
    elapsed_times_j2 = []
    for k in range(0, len(inc_0_grid)):
        inc_0 = inc_0_grid[k]
        for i in range(0, len(sma_0_grid)):
            sma_0 = sma_0_grid[i]
            for j in range(0, len(ecc_0_grid)):
                if grid_v[k][j][i] == 0:
                    ecc_0 = ecc_0_grid[j]

                    #problemContext.setInitialGuess(0.584294, -0.007173, -0.004140, 0, 0,-0.063167, 0.1)  # LEO
                    problemContext.setInitialGuess(0.720328, -0.739166, -0.000000, -0.312033, 0.0000000, -0.682988, 0.9470926121328614) # GTO/GEO

                    problemContext.initguess_duration = 0.06        
                    problemDefinition.state0.setSma(RE + sma_0 * 1e3);
                    problemDefinition.state0.setEccentricity(ecc_0)

                    try:
                        # solve with without j2
                        problemDefinition.cj2 = 0
                        cpu_t = time.time()
                        res = solve(problemDefinition, problemContext, problemSolution, False)
                        elapsed = time.time() - cpu_t
                        elapsed_times.append(elapsed)
                        if res == 0:
                            grid_v[k][j][i] = problemSolution.dvcost
                            grid_t[k][j][i] = problemSolution.duration
                        else:            
                            ncv = ncv + 1

                        # solve with j2, reusing sol with j2
                        problemDefinition.cj2 = 0.00108263
                        itrial = 0
                        while itrial < 3:
                            cpu_t = time.time()
                            res = solve(problemDefinition, problemContext, problemSolution, False)
                            elapsed = time.time() - cpu_t
                            elapsed_times_j2.append(elapsed)
                            if res == 0:
                                grid_v_j2[k][j][i] = problemSolution.dvcost
                                grid_t_j2[k][j][i] = problemSolution.duration
                                break
                            else:
                                generate_costate(problemDefinition.state0, problemDefinition.statef);
                                problemContext.setInitialGuess(0.720328, -0.739166, -0.000000, -0.312033, 0.0000000, -0.682988, 0.9470926121328614)
                            itrial = itrial + 1
                        if res == 0:
                            pass
                        else:            
                            ncv_j2 = ncv_j2 + 1
                            
                    except:
                        ncv = ncv + 1        

                    problemContext.setInitialGuess(problemSolution.solution)
                    problemContext.initguess_duration = problemSolution.finalLongitude
                else:
                    print("skip")
                    ncv = ncv + 1

            # save numpy array as npy file
            if save_file:
                savez(save_file, x=asarray(sma_0_grid), y=asarray(ecc_0_grid), z=asarray(inc_0_grid), r=asarray(grid_v), s=asarray(grid_t), t=asarray(grid_v_j2), u=asarray(grid_t_j2))

        # save numpy array as npy file
        if save_file:
            savez(save_file, x=asarray(sma_0_grid), y=asarray(ecc_0_grid), z=asarray(inc_0_grid), r=asarray(grid_v), s=asarray(grid_t), t=asarray(grid_v_j2), u=asarray(grid_t_j2))
                            
                            
    # save numpy array as npy file
    if save_file:
        grid_v = asarray(grid_v)
        grid_t = asarray(grid_t)
        savez(save_file, x=asarray(sma_0_grid), y=asarray(ecc_0_grid), z=asarray(inc_0_grid), r=asarray(grid_v), s=asarray(grid_t), t=asarray(grid_v_j2), u=asarray(grid_t_j2))
        
    print('Averaged cpu time (wo j2): ', sum(elapsed_times) / len(elapsed_times))
    print('Averaged cpu time (w j2) : ', sum(elapsed_times_j2) / len(elapsed_times_j2))
    print('Convergence ratio (wo j2): ', 100 * (1 - ncv / 1. / len(sma_0_grid) / len(ecc_0_grid)))
    print('Convergence ratio (w j2) : ', 100 * (1 - ncv_j2 / 1. / len(sma_0_grid) / len(ecc_0_grid)))
        
    return grid_v, grid_t, grid_v_j2, grid_t_j2


def run_example6_case(state0, statef, spacecraft, sma_0_grid, ecc_0_grid, inc_0_grid, prefix=""):	
    problemContext = LtProblemContext()
    problemContext.performOptimisation = True
    problemContext.setInitialGuess(0.584294, -0.007173, -0.004140, 0, 0,-0.063167, 0.1)
    problemContext.initguess_duration = 0.06
    problemContext.verbose = True
    problemContext.rtol=1e-8
    problemContext.max_iter=100
    problemContext.verbose=0
    problemContext.reportFilename=""    
    problemContext.initGuessMultiStart = False
    problemContext.initGuessMultiStartMaxAttempts = 0

    problemDefinition = LtProblemDefinition()
    problemDefinition.mu = 3.9860064e+14
    problemDefinition.cj2 = 0.00108263
    problemDefinition.radius = 6378136.
    problemDefinition.constraintType = TerminalConstraintType.C_SMA_ECC_INC
    problemDefinition.spacecraft = spacecraft	
    problemDefinition.state0 = state0
    problemDefinition.statef = statef

    grid_v, grid_t, grid_v_j2, grid_t_j2 = solve_batch(problemContext, problemDefinition, sma_0_grid, ecc_0_grid, inc_0_grid, save_file='ex6_data_out_'+prefix+'_woj2.npy.npz')

    save_data_txtfile('ex6_txt_' + prefix + '.txt', sma_0_grid, ecc_0_grid, inc_0_grid, grid_v, grid_t, grid_v_j2, grid_t_j2, statef)

    # plot transfer duration and transfer cost 
    fig = plt.figure()
    ax = plt.axes()
    for j in range(0, len(ecc_0_grid)):
        ax.plot(sma_0_grid, grid_v_j2[j], label="with j2")
        ax.plot(sma_0_grid, grid_v[j], label="keplerian")
    ax.grid(c='k', ls='-', alpha=0.3)
    plt.legend()
    plt.xlabel("sma_0, km")
    plt.ylabel("transfer cost, m/s")  
    plt.title("Transfer costs in minimum time")
    plt.savefig('ex6_sma_' + prefix + '_dv.png', dpi=240, format='png', bbox_inches='tight')
    
    fig = plt.figure()
    ax = plt.axes()
    for i in range(0, len(sma_0_grid)):
        ax.plot(ecc_0_grid, grid_v_j2[:][i], label="with j2")
        ax.plot(ecc_0_grid, grid_v[:][i], label="keplerian")
    ax.grid(c='k', ls='-', alpha=0.3)
    plt.legend()
    plt.xlabel("ecc, -")
    plt.ylabel("transfer cost, m/s")  
    plt.title("Transfer costs in minimum time")
    plt.savefig('ex6_ecc_' + prefix + '_dv.png', dpi=240, format='png', bbox_inches='tight')
    
    
    
def run_example6_gto():
    # 185km x 35786km x 28.5deg, 
    # P/m=6W/kg F/Pi=50mN/W => F=0.6 N, Isp=, m=2000kg
    spacecraft = SpacecraftData("SC", ThrusterData(0.6, 9.80665 * 1600), 2000)
    state0 = State()
    state0.setSma(6378136);
    state0.setInclination(deg2rad(28.5));
    state0.setEccentricity(0.001);
    state0.setArgumentOfPericenter(deg2rad(90));
    state0.setRightAscensionOfAscendingNode(0);
    state0.setMass(2000);
    
    statef = State()
    statef.setSma(6378136 + 37686 * 1e3);
    statef.setInclination(deg2rad(0.));
    statef.setEccentricity(0.00001);
    statef.setArgumentOfPericenter(deg2rad(90));
    statef.setRightAscensionOfAscendingNode(0);
    
    hp = 15000
    ha_0_grid = np.arange(35000, 65000, 5000)    
    sma_0_grid = (ha_0_grid + hp) / 2 + 6378.136
    ecc_0_grid = np.divide(ha_0_grid - hp, ha_0_grid + hp + 2 * 6378.136)
    inc_0_grid = np.arange(0, 20, 4.)  # 10
    run_example6_case(state0, statef, spacecraft, sma_0_grid, ecc_0_grid, inc_0_grid, "gto");
    

if __name__ == "__main__":
    run_example6_gto()


