# This example describes how to manually set the transfer problem
# Transfer from Moon Frozen Orbit to Low Lunar Orbit. Ephemeris are retrieved and plotted. 
# Solution:
#    lambda = [-0.104987 0.205405 0.394496 0.000008 -0.001137 -0.455471], theta = 0.259643
#    tof = 14.677 days
#    dm  = 9.130 kg
#    dv  = 785.466 m/s
from realoot.main import LtProblemContext, LtProblemDefinition, LtProblemSolution, SpacecraftData, ThrusterData, State, TerminalConstraintType
from realoot.main import solve
import realoot.main as constants
from realoot.utils import readEphemeris, deg2rad
import numpy as np
from math import sin, cos, tan, atan, sqrt


def run_example4():
    problemContext = LtProblemContext()
    problemContext.performOptimisation = True
    problemContext.setInitialGuess(0.584294, -0.007173, -0.004140, 0, 0,-0.063167, 0.1)
    problemContext.initguess_duration = 0.06
    problemContext.verbose = True
    problemContext.rtol=1e-8
    problemContext.max_iter=100
    problemContext.verbose=2
    problemContext.reportFilename="ex4_report.xml"
    problemContext.initGuessMultiStart = True
    problemContext.initGuessMultiStartMaxAttempts = 5
    problemContext.outputFilename = 'llo_ephe.txt'
    
    # Moon environment
    problemDefinition = LtProblemDefinition()
    problemDefinition.mu = 4902.86e+9
    problemDefinition.cj2 = 0.0002027*0
    problemDefinition.radius = 1737100.
    problemDefinition.constraintType = TerminalConstraintType.C_ORBIT
    problemDefinition.spacecraft = SpacecraftData("SC", ThrusterData(0.090, 9.80665 * 1250), 150)

    problemDefinition.state0 = State()
    problemDefinition.state0.setSma(3500000);
    problemDefinition.state0.setInclination(deg2rad(89.9));
    problemDefinition.state0.setEccentricity(0.4987);
    problemDefinition.state0.setArgumentOfPericenter(deg2rad(90.0));
    problemDefinition.state0.setRightAscensionOfAscendingNode(deg2rad(0.0));
    problemDefinition.state0.setMass(150);

    # FIXME: RAAN and AoP are not updated with epoch
    hx = tan(deg2rad(90.)/2) * cos(deg2rad(0))
    hy = tan(deg2rad(90.)/2) * sin(deg2rad(0))
    problemDefinition.statef = State(1777000., 0.0, 0.0, hx, hy, 0, None, problemDefinition.mu);

    problemSolution = LtProblemSolution()

    solve(problemDefinition, problemContext, problemSolution, True)
    print("dV=", problemSolution.dvcost)
    print("duration=", problemSolution.duration)
    print("nb revolutions=", problemSolution.nbRevolutions)
    print("Solution vector=", problemSolution.initguess)


    t, oe, u = readEphemeris(problemContext.outputFilename)
    ecc = sqrt(oe[:,1]**2 + oe[:,2]**2)
    inc = np.rad2deg(atan(oe[:,3]**2 + oe[:,4]**2) * 2.)

    fig = plt.figure()
    plt.subplot(131)
    plt.plot(t, oe[:,1] / 1000.)
    plt.xlabel("time, day")
    plt.ylabel("sma, km")
    plt.subplot(132)
    plt.plot(t, ecc)
    plt.xlabel("time, day")
    plt.ylabel("ecc, -")
    plt.subplot(133)
    plt.plot(t, inc)
    plt.xlabel("time, day")
    plt.ylabel("inc, deg")
    plt.grid()
    plt.suptitle("Orbital elements")
    plt.savefig('ex4_oe.png', dpi=240, format='png', bbox_inches='tight')
    plt.show()


if __name__ == "__main__":
	run_example4()
