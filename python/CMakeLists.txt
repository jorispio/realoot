cmake_minimum_required(VERSION 3.3)

SET(REALOOT_PROJECT_VERSION 1.0.5)
PROJECT(pyRealoot VERSION ${REALOOT_PROJECT_VERSION})

OPTION(PY_STATIC_LIB "With PYTHON" ON)
SET(PYREALOOT_INSTALL_PATH "${CMAKE_INSTALL_PREFIX}/realoot/realoot")
message(STATUS "Python module installation directory: ${PYREALOOT_INSTALL_PATH}")

OPTION(CREATE_PIP_PACKAGING "Create Python Package" ON)

# ====================================================================
# option: set the quadrature order
ADD_DEFINITIONS("-DQUAD_ORDER=24")
ADD_DEFINITIONS("-DWITH_QUADRATURE")
LINK_DIRECTORIES(${HELIOLIB_LIBRARY_DIR})
LINK_LIBRARIES(${HELIOLIB_LIBRARIES})

# ====================================================================
# Module path setup.
SET(CMAKE_MODULE_PATH  "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})

# Find Python interpreter.
IF(BUILD_PY_PKG)
    FIND_PACKAGE(Python REQUIRED COMPONENTS Interpreter Development)
    if(Python_FOUND)
        message(STATUS "Python interpreter version: ${Python_VERSION}")
        message(STATUS "Python interpreter        : ${Python_EXECUTABLE}")
        message(STATUS "Python include dirs       : ${Python_INCLUDE_DIRS}")

        SET(PYTHONINTERP_FOUND TRUE)
    else()
        SET(PYTHONINTERP_FOUND FALSE)
        SET(BUILD_PY_PKG FALSE)
        message(WARNING "Python not found. Disabling build.")
    endif()

    if (Python_VERSION_MAJOR EQUAL 2)
        message(FATAL_ERROR "Not compatible with Python2. Try upgrading your Python version. Make sure you have: libpython3-dev.")
    endif()
ELSE()
	SET(PYTHONINTERP_FOUND FALSE)
ENDIF()

SET(PYTHON_LIBRARIES ${Python_LIBRARIES})

# ====================================================================

IF(PYTHONINTERP_FOUND)
    SET(Boost_USE_STATIC_LIBS     OFF)
    SET(Boost_USE_MULTITHREADED   OFF)

    IF (UNIX AND NOT APPLE)
        IF (CYGWIN)
            message("Looking for Boost for Cygwin")
            FIND_PACKAGE(Boost COMPONENTS system python3)
        ELSE()
            message("Looking for Boost python-py${Python_VERSION_MAJOR}${Python_VERSION_MINOR}")
            FIND_PACKAGE(Boost COMPONENTS system "python-py${Python_VERSION_MAJOR}${Python_VERSION_MINOR}") 
            #FIND_PACKAGE(Boost COMPONENTS system python3-py35) 
            #FIND_PACKAGE(Boost COMPONENTS system python3-${Python_VERSION_MAJOR}${Python_VERSION_MINOR})
        ENDIF()
    ELSE()
        SET(Boost_USE_STATIC_LIBS     ${PY_STATIC_LIB})
        message(STATUS "Looking for Boost " python${Python_VERSION_MAJOR}${Python_VERSION_MINOR})
        FIND_PACKAGE(Boost COMPONENTS system python)
    ENDIF()

    message(STATUS "Boost include dirs: ${Boost_INCLUDE_DIRS}")
    message(STATUS "Boost libraries   : ${Boost_LIBRARIES}")

    if(NOT Boost_FOUND)
        #message(FATAL_ERROR "Missing Boost components, exiting.")
    endif()

ENDIF()

IF (WIN32)
    ADD_DEFINITIONS(${Boost_LIB_DIAGNOSTIC_DEFINITIONS})
ENDIF()

# Need to use Boost share library to have a common registry between modules (if any), 
# with BOOST_ALL_DYN_LINK
# Otherwise, for static linking we can use BOOST_PYTHON_STATIC_LIB.
if (PY_STATIC_LIB)
    add_definitions("-DBOOST_PYTHON_STATIC_LIB")
else()
    add_definitions("-DBOOST_ALL_DYN_LINK")
endif()

# ====================================================================
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2")
if (_MSC_VER OR MSVC)
	# Microsoft Visual Studio
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -W4")

else()
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m64")
    #SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wl,--export-all-symbols")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wextra")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wnon-virtual-dtor")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wnoexcept")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wlogical-op")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wconversion")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wdeprecated")
    IF(CMAKE_CXX_COMPILER_VERSION VERSION_GREATER "6.9")
	    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wrestrict")
	    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Waligned-new")
    ENDIF()

    if (NOT WIN32)
      add_definitions(-fPIC)
    endif()

endif()

# ====================================================================
include_directories("${Python_INCLUDE_DIRS}")
include_directories("${Boost_INCLUDE_DIRS}")
include_directories("${XercesC_INCLUDE_DIRS}")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/../src")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/../src/core")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/../src/utils")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/../src/ocp")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/../src/dynamics")
include_directories("${THIRDPARTY_SRC_DIRECTORY}")
include_directories("${THIRDPARTY_DIRECTORY}")
include_directories("${THIRDPARTY_DIRECTORY}/Eigen")
include_directories("${HELIOLIB_INCLUDE_DIRS}")

if(USE_KINSOL)
	include_directories("${THIRDPARTY_SRC_DIRECTORY}/sundials-5.3.0/include/")
	include_directories("${THIRDPARTY_DIRECTORY}/sundials-5.3.0/include/")
endif()


link_directories("${Boost_LIBRARY_DIRS}")
link_directories("${XercesC_LIBRARY}")
link_directories("${CMAKE_CURRENT_SOURCE_DIR}/../build/")
link_directories(${HELIOLIB_LIBRARY_DIR})

if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    link_libraries(stdc++)
    link_libraries(dl)
    link_libraries(${XercesC_LIBRARIES})
    link_libraries(${Boost_LIBRARIES})
else()
link_libraries(${XercesC_LIBRARIES})
endif()
link_libraries(LowThrustOcpStatic)

# ====================================================================

# Specific file update
configure_file("${CMAKE_CURRENT_SOURCE_DIR}/__init__.py.in" "${CMAKE_CURRENT_SOURCE_DIR}/__init__.py" @ONLY)
configure_file("${CMAKE_CURRENT_SOURCE_DIR}/setup.py.in" "${CMAKE_CURRENT_SOURCE_DIR}/setup.py" @ONLY)

install(DIRECTORY DESTINATION "${PYREALOOT_INSTALL_PATH}")
install (FILES "${CMAKE_CURRENT_SOURCE_DIR}/__init__.py"        
         DESTINATION "${PYREALOOT_INSTALL_PATH}")
install (FILES "${CMAKE_CURRENT_SOURCE_DIR}/__main__.py"
         DESTINATION "${PYREALOOT_INSTALL_PATH}")
install (FILES "${CMAKE_CURRENT_SOURCE_DIR}/README.md"        
         DESTINATION "${PYREALOOT_INSTALL_PATH}")
install (FILES "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.md"        
         DESTINATION "${PYREALOOT_INSTALL_PATH}")

# ====================================================================
# Packaging 
if(CREATE_PIP_PACKAGING)
    SET(PYREALOOT_PIPPKG_ROOT_PATH "realoot_py_pkg_${Python_VERSION_MAJOR}.${Python_VERSION_MINOR}")
    SET(PYREALOOT_PIPPKG_INSTALL_PATH "${PYREALOOT_PIPPKG_ROOT_PATH}/realoot")
    message(STATUS "Python PIP package directory: ${PYREALOOT_PIPPKG_ROOT_PATH}")

    install (FILES "${CMAKE_CURRENT_SOURCE_DIR}/__init__.py"        
             DESTINATION "${PYREALOOT_PIPPKG_INSTALL_PATH}")
    install (FILES "${CMAKE_CURRENT_SOURCE_DIR}/__main__.py"
             DESTINATION "${PYREALOOT_PIPPKG_INSTALL_PATH}")
    install (FILES "${CMAKE_CURRENT_SOURCE_DIR}/README.md"
             DESTINATION "${PYREALOOT_PIPPKG_INSTALL_PATH}")
    install (FILES "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.md"
             DESTINATION "${PYREALOOT_PIPPKG_INSTALL_PATH}")
    install (FILES "${CMAKE_CURRENT_SOURCE_DIR}/setup.py"        
             DESTINATION "${PYREALOOT_PIPPKG_ROOT_PATH}")
    install (FILES "${CMAKE_CURRENT_SOURCE_DIR}/setup.cfg"        
             DESTINATION "${PYREALOOT_PIPPKG_ROOT_PATH}")
    install (FILES "${CMAKE_CURRENT_SOURCE_DIR}/MANIFEST.in"        
             DESTINATION "${PYREALOOT_PIPPKG_ROOT_PATH}")
endif()

# ====================================================================
# Setup the installation path.
add_subdirectory(main)
add_subdirectory(core)
add_subdirectory(utils)
add_subdirectory(examples)
add_subdirectory(tests)
