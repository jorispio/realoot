# - Try to find HelioLib lib
#
#   find_package(HelioLib)
#
# Once done this will define
#
#  HELIOLIB_FOUND - system has HelioLib lib with correct version
#  HELIOLIB_INCLUDE_DIRS - the HelioLib include directory
#  HELIOLIB_LIBRARY_DIR - the HelioLib library directory
#  HELIOLIB_LIBRARIES - the HelioLib binary libraries
#  HELIOLIB_VERSION - HelioLib version
#

macro(_heliolib_get_version)
  file(READ "${HELIOLIB_INCLUDE_DIR}/libversion_config.hpp" _heliolib_version_header)

  string(REGEX MATCH "define[ \t]+HELIOLIB_MAJOR_VERSION[ \t]+([0-9]+)" _heliolib_major_version_match "${_heliolib_version_header}")
  set(HELIOLIB_MAJOR_VERSION "${CMAKE_MATCH_1}")
  string(REGEX MATCH "define[ \t]+HELIOLIB_MINOR_VERSION[ \t]+([0-9]+)" _heliolib_minor_version_match "${_heliolib_version_header}")
  set(HELIOLIB_MINOR_VERSION "${CMAKE_MATCH_1}")

  set(HELIOLIB_VERSION ${HELIOLIB_MAJOR_VERSION}.${HELIOLIB_MINOR_VERSION})
endmacro(_heliolib_get_version)


if (HELIOLIB_INCLUDE_DIR)
  # in cache 
  _heliolib_get_version()
  set(HELIOLIB_FOUND TRUE)

else (HELIOLIB_INCLUDE_DIR)
  # search first if an HelioLibConfig.cmake is available in the system,
  # if successful this would set HELIOLIB_INCLUDE_DIR and the rest of
  # the script will work as usual
  find_package(HelioLib NO_MODULE QUIET)

  if(NOT HELIOLIB_INCLUDE_DIR)
    find_path(HELIOLIB_INCLUDE_DIR NAMES HelioLib.hpp
        HINTS
        ENV HELIOLIB_ROOT_DIR
        PATHS
        ${CMAKE_INSTALL_PREFIX}/include
        /usr/local/HelioLib/include/
        /usr/local/HelioLib/include/HelioLib
        /usr/local/include/HelioLib/
        /usr/local/include/
        /usr/local/
      )
  endif(NOT HELIOLIB_INCLUDE_DIR)

  
  if(HELIOLIB_INCLUDE_DIR)
    _heliolib_get_version()

    set(HELIOLIB_INCLUDE_DIR "${HELIOLIB_INCLUDE_DIR};${HELIOLIB_INCLUDE_DIR}/../")
  endif(HELIOLIB_INCLUDE_DIR)

  if(NOT HELIOLIB_LIBRARY_DIR)  
    find_path(HELIOLIB_LIBRARY_DIR 
        NAMES libheliolib.a
        HINTS
        ENV HELIOLIB_ROOT_DIR
        PATHS
        ${CMAKE_INSTALL_PREFIX}/lib/
        /usr/local/lib
        /usr/local/HelioLib/lib/
        /usr/local/lib/HelioLib/
    )
  endif(NOT HELIOLIB_LIBRARY_DIR)

  set(HELIOLIB_LIBRARIES "heliolib;boost_regex") # add dependancy

  # look for SOFA
  set(HELIOLIB_LIBRARIES "${HELIOLIB_LIBRARIES};sofa") # add dependancy

  set(HELIOLIB_INCLUDE_DIRS "${HELIOLIB_INCLUDE_DIR}")
  
  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(HelioLib DEFAULT_MSG HELIOLIB_INCLUDE_DIR HELIOLIB_LIBRARY_DIR)

  mark_as_advanced(HELIOLIB_INCLUDE_DIR HELIOLIB_INCLUDE_DIRS HELIOLIB_LIBRARY_DIR)

endif(HELIOLIB_INCLUDE_DIR)

set(HELIOLIB_INCLUDE_DIRS "${HELIOLIB_INCLUDE_DIR}")