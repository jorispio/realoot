#ifndef BLAS_CPP_H
#define BLAS_CPP_H

int daxpy(int n, double da, double *dx,  int incx, double *dy, int incy);
int dscal(int n, double da, double *dx, int incx);
int idamax(int n, double *dx, int incx);
double ddot(int n, double *dx, int incx, double *dy, int incy);

#endif
