#ifndef NETLIB_CPP_H
#define NETLIB_CPP_H

#include "utilsNetlib.hpp"

#define daxpy daxpy_
#define ddot ddot_
#define dscal dscal_
#define idamax idamax_

void DGBFA(double *abd,long lda,long n,long ml,long mu,long ipvt[],long *info);
void DGBSL(double *abd,long lda,long n,long ml,long mu,long ipvt[],double b[],long job);

//void DGESL(double *a, long lda, long n, long ipvt[], double b[], long job);
//void DGEFA(double *a,long lda,long n, long ipvt[],long *info);

#endif
