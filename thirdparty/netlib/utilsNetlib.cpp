#include <math.h>
#include <stdlib.h>
#include <algorithm>    // std::min
#include <math.h>
#include "utilsNetlib.hpp"

#ifndef _MSC_VER
#pragma GCC push_options
#pragma GCC optimize ("unroll-loops")
#endif

// ---------------------------------------------------------------------
/** Scales a vector by a constant. 
 *	
 *	Initial version from BLAS:
 *      jack dongarra, linpack, 3/11/78.   
 *      modified 3/93 to return if incx .le. 0.   
 *      modified 12/3/93, array(1) declarations changed to array(*)  
 *
 *	@param [in]	N	N is INTEGER. number of elements in input vector(s)
 *	@param[in]	DA	DA is DOUBLE PRECISION. On entry, DA specifies the scalar alpha.
 *	@param[in,out]	DX	DX is DOUBLE PRECISION array, dimension ( 1 + ( N - 1 )*abs( INCX ) )
 *	@param[in]	INCX	INCX is INTEGER. storage spacing between elements of DX	   
 */
// ---------------------------------------------------------------------
int dscal(int n, double da, double *dx, int incx)
{
#define DX(I) dx[(I)-1]
    if (n <= 0 || incx <= 0) {
		return 0;
    }

    int nincx = n * incx;
    for (int i = 1; incx < 0 ? i >= nincx : i <= nincx; i += incx) {
		DX(i) = da * DX(i);
    }
    return 0;
}

// ---------------------------------------------------------------------
/** Constant times a vector plus a vector.   A * X + Y
 *
 *	Initial version from BLAS:	   
 *      jack dongarra, linpack, 3/11/78.   
 *      modified 12/3/93, array(1) declarations changed to array(*)   
 * 
 * @param [in]	N	          N is INTEGER         number of elements in input vector(s)
 * @param [in]	DA	          DA is DOUBLE PRECISION           On entry, DA specifies the scalar alpha.
 * @param [in]	DX	          DX is DOUBLE PRECISION array, dimension ( 1 + ( N - 1 )*abs( INCX ) )
 * @param [in]	INCX	          INCX is INTEGER         storage spacing between elements of DX
 * @param [in,out]	DY	          DY is DOUBLE PRECISION array, dimension ( 1 + ( N - 1 )*abs( INCY ) )
 * @param [in]	INCY	INCY is INTEGER storage spacing between elements of DY
 */
// ---------------------------------------------------------------------	   
int daxpy(int n, double da, double *dx, int incx, double *dy, int incy)
{
	//Map<RowVectorXi> x(dx, n);
	//Map<RowVectorXi> y(dy, n);
	//z = da * x + y;
#define DY(I) dy[(I)-1]
#define DX(I) dx[(I)-1]

    if ((n <= 0) || (da == 0.)) {
		return 0;
    }

    int ix = 1;
    int iy = 1;
    if (incx < 0) {
		ix = (-(n) + 1) * incx + 1;
    }
    if (incy < 0) {
		iy = (-(n) + 1) * incy + 1;
    }
    for (int i = 1; i <= n; ++i) {
		DY(iy) += da * DX(ix);
		ix += incx;
		iy += incy;

    }
    return 0;
} /* daxpy_ */

// ---------------------------------------------------------------------
/** Finds the index of element having max. absolute value.   
 *
 *	Initial version from BLAS:	    
 *       jack dongarra, linpack, 3/11/78.   
 *       modified 3/93 to return if incx .le. 0.   
 *       modified 12/3/93, array(1) declarations changed to array(*)   
 *       Parameter adjustments
 *	   
 * @param [in]	N	          N is INTEGER         number of elements in input vector(s)
 * @param [in]	DX	         DX is DOUBLE PRECISION array, dimension ( 1 + ( N - 1 )*abs( INCX ) )
 * @param [in]	INCX	          INCX is INTEGER         storage spacing between elements of SX
 */
// ---------------------------------------------------------------------
int idamax(int n, double *dx, int incx)
{
    --dx;
    
    int ret_val = 0;
    if (n < 1 || incx <= 0) {
		return ret_val;
    }
    ret_val = 1;
    if (n == 1) {
		return ret_val;
    }
	
    int ix = 1;
    double dmax__ = fabs(dx[1]);
    ix += incx;
    int i__1 = n;
    for (int i__ = 2; i__ <= i__1; ++i__) {
		double d__1;
		if ((d__1 = dx[ix], fabs(d__1)) > dmax__) {
			ret_val = i__;
			dmax__ = (d__1 = dx[ix], fabs(d__1));
		}
		ix += incx;
    }
    return ret_val;
} /* idamax_ */

// ---------------------------------------------------------------------
/** Forms the dot product of two vectors. 
 * 
 *	Initial version from BLAS:	   
 *       uses unrolled loops for increments equal to one.   
 *       jack dongarra, linpack, 3/11/78.   
 *       modified 12/3/93, array(1) declarations changed to array(*)   
 *
 * @param [in]	N	          N is INTEGER         number of elements in input vector(s)
 * @param [in]	DX	          DX is DOUBLE PRECISION array, dimension ( 1 + ( N - 1 )*abs( INCX ) )
 * @param [in]	INCX	          INCX is INTEGER         storage spacing between elements of DX
 * @param [in]	DY	          DY is DOUBLE PRECISION array, dimension ( 1 + ( N - 1 )*abs( INCY ) )
 * @param [in]	INCY	          INCY is INTEGER         storage spacing between elements of DY	   
*/
// ---------------------------------------------------------------------
double ddot(int n, double *dx, int incx, double *dy, int incy)
{
#define DY(I) dy[(I)-1]
#define DX(I) dx[(I)-1]

    double ret_val = 0.;
    double dtemp = 0.;
    if (n <= 0) {
        return ret_val;
    }

    int ix = 1;
    int iy = 1;
    if (incx < 0) {
        ix = (-(n) + 1) * incx + 1;
    }
    if (incy < 0) {
        iy = (-(n) + 1) * incy + 1;
    }
    for (int i = 1; i <= n; ++i) {
        dtemp += DX(ix) * DY(iy);
        ix += incx;
        iy += incy;
    }
    ret_val = dtemp;
    return ret_val;
} /* ddot_ */

// ---------------------------------------------------------------------
#ifndef _MSC_VER
#pragma GCC pop_options
#endif
