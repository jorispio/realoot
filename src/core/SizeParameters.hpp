// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __REALOOT_PARAMETERS_HPP__
#define __REALOOT_PARAMETERS_HPP__

#include <limits> // epsilon

/* dynamical system size */
#define STATE_SIZE 7
#define PARAMETER_SIZE 2
#define ODE_STATE_SIZE (2 * STATE_SIZE + PARAMETER_SIZE)
#define STM_SIZE ((2 * STATE_SIZE + 1) * (2 * STATE_SIZE + 1))

/* default integration tolerances */
#define RTOL 1e-10
#define ATOL 1e-12

#define TRAJECTORY_MAX_NPTS 50000

#define TRAJECTORY_PTS_DIM (2 * STATE_SIZE)

#define NB_UNKNOWN  7

#define IDX_SMA     0
#define IDX_EX     1
#define IDX_EY     2
#define IDX_HX     3
#define IDX_HY     4
#define IDX_L     5
#define IDX_MASS 6
#define IDX_PSMA 7
#define IDX_PEX  8
#define IDX_PEY  9
#define IDX_PHX  10
#define IDX_PHY  11
#define IDX_PL   12
#define IDX_PM   13
#define IDX_L1   14
#define IDX_T    15

#define P_IDX_PSMA 0
#define P_IDX_PEX  1
#define P_IDX_PEY  2
#define P_IDX_PHX  3
#define P_IDX_PHY  4
#define P_IDX_PM   5
#define P_IDX_L1   6

// ---------------------------------------------------------------------------
#endif  // __REALOOT_PARAMETERS_HPP__
