// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef REALOOT_PROBLEMSOLUTION_HPP
#define REALOOT_PROBLEMSOLUTION_HPP

#include <algorithm>
#include <array>
#include <iterator>
#include <string>

#include "core/DataTypes.hpp"
#include "Scaling.hpp"
#include "core/LtProblemDefinition.hpp"

typedef struct LtProblemSolution {
    std::string sProblemName;
    double dm;  // kg
    double dvcost;  // m/s
    double thrustDuration; // day
    double transferDuration;  // day
    double nbRevolutions;
    double finalState[7];
    std::array<double, NB_UNKNOWN> solution;
    double finalLongitude;
    ProblemScaling scaling;
    int res;

    void setSolutionVector(const double x[NB_UNKNOWN]) {
        for (int i=0; i< NB_UNKNOWN; ++i) solution[i] = x[i];
    }

    std::array<double, NB_UNKNOWN> getSolutionVector() const {
        return solution;
    }

    /** Set thrust duration (day). */
    void setTransferDuration(double duration) {
        transferDuration = duration;
    }
    inline double getTransferDuration() { return transferDuration; }

    void setSolutionLongitude(double longitude) {
        longitude = longitude;
    }
    inline double getSolutionLongitude() { return finalLongitude;  }

    // -------------------------------------------------------------------------
    double computeDeltaV(LtProblemDefinition& problemDefinition) {
        double g0Isp = problemDefinition.spacecraft.propulsion.g0Isp;
        double m0 = problemDefinition.state0.mass;
        dvcost = g0Isp * log(m0 / (m0 - dm));
        return dvcost;
    }

    // -------------------------------------------------------------------------
    double computeThrustDuration(LtProblemDefinition& problemDefinition) {
        double Fth = problemDefinition.spacecraft.propulsion.Fth;
        double qFuel = Fth / problemDefinition.spacecraft.propulsion.g0Isp;
        thrustDuration = (dm / qFuel) * SEC2DAY;
        return thrustDuration;
    }

    double getTotalCoastDuration() {
        return transferDuration - thrustDuration;
    }

} LtProblemSolution;

#endif  // REALOOT_PROBLEMSOLUTION_HPP
