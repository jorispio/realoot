// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef REALOOT_PROBLEMCONTEXT_HPP
#define REALOOT_PROBLEMCONTEXT_HPP

#include <array>
#include <string>

#include "core/DataTypes.hpp"
#include "core/Enumerations.hpp"
#include "core/State.hpp"
#include "core/LtSpacecraftData.hpp"
#include "continuation/ContinuationData.hpp"
#include "utils/maths/solver/SolverTypeEnum.hpp"

typedef struct LtProblemContext {
    std::string sProblemName = "";
    bool performSimulation = false;
    bool performOptimisation = false;

    SolverType solverType = SOLVER_NLEQ;

    bool initGuessMultiStart = false;
    int initGuessMultiStartMaxAttempts = 0;
    std::array<double, NB_UNKNOWN> initguess;
    double initguess_duration = 0;

    double rTol = 1e-8;
    int maxIter = 100;
    int verbose = 0;

    ContinuationOptions continuation;

    std::string outputFilename;
    DYNAMICALMODEL_TYPE outputFormat = DYNAMICALMODEL_TYPE::D_EQUINOCTIAL;
    bool outputCostates = true;
    int outputNbOfPoints = 100;

    std::string reportFilename = "";

    LtProblemContext() {
        sProblemName = "";
        outputFilename = "";
        reportFilename = "";
        rTol = 1e-8;
        maxIter = 100;
        verbose = 0;
        outputNbOfPoints = 100;
    }

    void setInitialGuessByValue(double la, double lex, double ley, double lhx, double lhy, double ll, double lm) {
        initguess[P_IDX_PSMA] = la;
        initguess[P_IDX_PEX] = lex;
        initguess[P_IDX_PEY] = ley;
        initguess[P_IDX_PHX] = lhx;
        initguess[P_IDX_PHY] = lhy;
        initguess[P_IDX_PM] = lm;
        initguess[P_IDX_L1] = ll;
        initguess_duration = ll;
    }

    void setInitialGuess(std::array<double, NB_UNKNOWN>& larray) {
        initguess = larray;
    }

    std::array<double, NB_UNKNOWN> getInitguess() const {
        return initguess;
    }

    void setInitguessDuration(double d) {
        initguess_duration = d;
        initguess[P_IDX_L1] = d;
    }

    double getInitguessDuration() {
        return initguess_duration;
    }
} LtProblemContext;

#endif  // REALOOT_PROBLEMCONTEXT_HPP

