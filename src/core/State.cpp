// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "State.hpp"

// ---------------------------------------------------------------------------
State::State() {
    type = DYNAMICALMODEL_TYPE::D_NOT_SET;
    epoch = 0;
    r << 0, 0, 0;
    v << 0, 0, 0;
    for (int i = 0; i < 6; ++i) {
        equi[i] = 0;
        equiAdapt[i] = 0;
        kepler[i] = 0;
        lambda[i] = 0;
    }
    mass = 0;
    frame = NULL;
}

// ---------------------------------------------------------------------------
/** Constructor of equinoctial state.
 * @param e  adapted equinoctial parameters (a, ex, ey, hx, hy, L), with ex=e*cos(pom+gom), ey=e*sin(pom+gom),hx=tan(inc/2)*cos(gom), hy=tan(inc/2)*sin(gom)
 */
// ---------------------------------------------------------------------------
State::State(DYNAMICALMODEL_TYPE typeIn, const std::array<double, 6>& e, double m, double mu) {
    type = typeIn;
    epoch = 0;
    for (int i = 0; i < 6; ++i) {
        if (type == DYNAMICALMODEL_TYPE::D_EQUINOCTIAL) {
            equi[i] = e[i];  // {p, ex, ey, hx, hy, L}
            equiAdapt[i] = e[i];  // {a, ex, ey, hx, hy, L}
        }
        if (type == DYNAMICALMODEL_TYPE::D_KEPLERIAN) {
            kepler[i] = e[i];
        }
        lambda[i] = 0.;
    }

    if (type == DYNAMICALMODEL_TYPE::D_EQUINOCTIAL) {
        updateKepler(mu);
    } 
    if (type == DYNAMICALMODEL_TYPE::D_KEPLERIAN) {
        updateEqui(mu);
    }
    updateCartesian(mu);

    mass = m;
    frame = NULL;
}

// ---------------------------------------------------------------------------
/** Constructor of equinoctial state.
 * equi      {sma * (1-e^2), ex=e*cos(pom+gom), ey=e*sin(pom+gom),hx=tan(inc/2)*cos(gom), hy=tan(inc/2)*sin(gom)}
 * equiAdapt {sma, ex=e*cos(pom+gom), ey=e*sin(pom+gom),hx=tan(inc/2)*cos(gom), hy=tan(inc/2)*sin(gom)}
 * kepler    {sma, e, inc, pom, gom}
 * @param e  adapted equinoctial parameters (a, ex, ey, hx, hy, L), with ex=e*cos(pom+gom), ey=e*sin(pom+gom),hx=tan(inc/2)*cos(gom), hy=tan(inc/2)*sin(gom)
 */
// ---------------------------------------------------------------------------
State::State(const std::array<double, 6>& e, double m, double mu) {
    type = DYNAMICALMODEL_TYPE::D_EQUINOCTIAL;
    epoch = 0;
    for (int i = 0; i < 6; ++i) {
        equi[i] = e[i];
        equiAdapt[i] = e[i];
        lambda[i] = 0.;
    }
    equi[0] = e[0] * (1 - e[1]*e[1] - e[2]*e[2]);  // p

    mass = m;
    frame = NULL;

    updateKepler(mu);
    updateCartesian(mu);
}

// ---------------------------------------------------------------------------
/** Generic creator of state.
 * @param x state vector for cartesian or equinoctial state, consistent with dynamics.
 * @param m mass
 */
// ---------------------------------------------------------------------------
State::State(DYNAMICALMODEL_TYPE typeIn, const Vector6& x, double m, double mu) {
    type = DYNAMICALMODEL_TYPE::D_ALL;
    epoch = 0;
    frame = NULL;

    for (int i = 0; i < 6; ++i) {
        lambda[i] = 0.;
    }

    if (typeIn == DYNAMICALMODEL_TYPE::D_CARTESIAN) {
        r = x.segment(0, 3);
        v = x.segment(3, 3);

        try {
            EquinoctialOrbit equino(CartesianCoordinates(r, v), (Frame*)FramesFactory::EME2000, GenericDate(), mu);

            double sma = equino.getSma();
            double ecc2 = pow(equino.getEquinoctialEx(), 2) + pow(equino.getEquinoctialEy(), 2);
            equi[0] = sma * (1 - ecc2);
            equi[1] = equino.getEquinoctialEx();
            equi[2] = equino.getEquinoctialEy();
            equi[3] = equino.getHx();
            equi[4] = equino.getHy();
            equi[5] = equino.getTrueLongitude();  // true longitude

            equiAdapt[0] = sma;
            equiAdapt[1] = equino.getEquinoctialEx();
            equiAdapt[2] = equino.getEquinoctialEy();
            equiAdapt[3] = equino.getHx();
            equiAdapt[4] = equino.getHy();
            equiAdapt[5] = equino.getTrueLongitude();  // true longitude

            // keplerian orbit
            kepler[0] = sma;
            kepler[1] = equino.getEccentricity();
            kepler[2] = equino.getInclination();
            kepler[3] = equino.getArgumentPericenter();
            kepler[4] = equino.getRightAscensionOfAscendingNode();
            kepler[5] = equino.getMeanAnomaly();  // mean anomaly
        } catch (HelioLibException &e) {
            char str[255];
            snprintf(str, 255, "In State() (type=%d), while computing Equinoctial and Keplerian coordinates, with |r|=[%f %f %f], |v|=[%f %f %f], mu=%f",
                                    type,
                                    r(0), r(1), r(2),
                                    v(0), v(1), v(2),
                                    mu);
            throw LtException(str, e.what());
        }
    } else if (typeIn == DYNAMICALMODEL_TYPE::D_EQUINOCTIAL) {
        equi[0] = x(0);  // m
        equi[1] = x(1);
        equi[2] = x(2);
        equi[3] = x(3);
        equi[4] = x(4);
        equi[5] = x(5);

        equiAdapt[0] = x(0) / (1 - x(1)*x(1) - x(2) * x(2));  // sma
        equiAdapt[1] = x(1);
        equiAdapt[2] = x(2);
        equiAdapt[3] = x(3);
        equiAdapt[4] = x(4);
        equiAdapt[5] = x(5);

        // conversion to cartesian
        try {
            updateKepler(mu);
            updateCartesian(mu);
        } catch (HelioLibException &e) {
            throw LtException("In State() while computing Keplerian coordinates", e.what());
        }
    } else {
        throw LtException("State: Unknown dynamical model type for conversion.");
    }

    mass = m;
}
// ---------------------------------------------------------------------------
/** Constructor for keplerian state
 * @param inc inclination, in radians
 * @param pom argument of pericenter, in radians
 * @param gom RAAN, in radians
 * @param man mean anomaly, in radians
 * @param mu gravitational constant
 */
// ---------------------------------------------------------------------------
State::State(double sma,
             double ecc,
             double inc,
             double pom,
             double gom,
             double man,
             const Frame* frameI,
             double mu) {
    type = DYNAMICALMODEL_TYPE::D_ALL;
    epoch = 0;
    frame = (Frame*)frameI;

    for (int i = 0; i < 6; ++i) {
        lambda[i] = 0.;
    }
    mass = 0;

    // keplerian parameters
    kepler[0] = sma;
    kepler[1] = ecc;
    kepler[2] = inc;
    kepler[3] = pom;
    kepler[4] = gom;
    kepler[5] = man;  // mean anomaly

    updateEqui(mu);
    updateCartesian(mu);
}
// ---------------------------------------------------------------------------
void State::updateKepler(double mu) {
    kepler[0] = equiAdapt[0];
    kepler[1] = sqrt(equi[1] * equi[1] + equi[2] * equi[2]);  // ecc
    kepler[2] = 2 * atan(sqrt(equi[3] * equi[3] + equi[4] * equi[4]));  // inc
    kepler[4] = atan2(equi[4], equi[3]);  // raan
    kepler[3] = atan2(equi[2], equi[1]) - kepler[4];  // aop
    kepler[5] = equiAdapt[5] - kepler[3] - kepler[4];  // mean anomaly
}
// ---------------------------------------------------------------------------
void State::updateEqui(double mu) {
    double sma = kepler[0];
    double ecc = kepler[1];
    KeplerianOrbit keporb(sma, ecc, kepler[2], kepler[3], kepler[4], kepler[5], MEAN_ANOMALY, frame, GenericDate(), mu);

    // get equinoctial parameters
    double ecc2 = ecc * ecc;
    equi[0] = sma * (1 - ecc2);  // p
    equi[1] = keporb.getEquinoctialEx();
    equi[2] = keporb.getEquinoctialEy();
    equi[3] = keporb.getHx();
    equi[4] = keporb.getHy();
    equi[5] = keporb.getTrueLongitude();  // true longitude

    equiAdapt[0] = sma;
    equiAdapt[1] = keporb.getEquinoctialEx();
    equiAdapt[2] = keporb.getEquinoctialEy();
    equiAdapt[3] = keporb.getHx();
    equiAdapt[4] = keporb.getHy();
    equiAdapt[5] = keporb.getTrueLongitude();  // true longitude
}
// ---------------------------------------------------------------------------
void State::updateCartesian(double mu) {
    // cartesian coordinates
    double sma = kepler[0];
    double ecc = kepler[1];
    KeplerianOrbit keporb(sma, ecc, kepler[2], kepler[3], kepler[4], kepler[5], MEAN_ANOMALY, frame, GenericDate(), mu);
    CartesianCoordinates pv = keporb.getCartesianCoordinates();
    r = pv.getPosition();
    v = pv.getVelocity();
}
// ---------------------------------------------------------------------------
/** Scale the state.
*/
// ---------------------------------------------------------------------------
void State::scale(double a_m, double a_ms) {
    r /= a_m;
    v /= a_ms;
    equi[0] /= a_m;
    equiAdapt[0] /= a_m;
    kepler[0] /= a_m;
}

// ---------------------------------------------------------------------------
void State::setEpoch(double epochMjd2000_) {
    epoch = epochMjd2000_;
}
// ---------------------------------------------------------------------------
void State::setSma(double sma) {
    equiAdapt[0] = sma;
    kepler[0] = sma;
    equi[0] = sma * (1 - kepler[1] * kepler[1]);
}

// ---------------------------------------------------------------------------
void State::setInclination(double inc) {
    kepler[2] = inc;
    equiAdapt[3] = tan(inc / 2.) * cos(kepler[4]);
    equiAdapt[4] = tan(inc / 2.) * sin(kepler[4]);
    equi[3] = equiAdapt[3];
    equi[4] = equiAdapt[4];
}

// ---------------------------------------------------------------------------
void State::setEccentricity(double ecc) {
    kepler[1] = ecc;
    equiAdapt[1] = ecc * cos(kepler[3] + kepler[4]);
    equiAdapt[2] = ecc * sin(kepler[3] + kepler[4]);
    equi[1] = equiAdapt[1];
    equi[2] = equiAdapt[2];
}
// ---------------------------------------------------------------------------
void State::setArgumentOfPericenter(double pom) {
    kepler[3] = pom;
    equiAdapt[1] = kepler[1] * cos(pom + kepler[4]);
    equiAdapt[2] = kepler[1] * sin(pom + kepler[4]);
    equi[1] = equiAdapt[1];
    equi[2] = equiAdapt[2];
}
// ---------------------------------------------------------------------------
void State::setRightAscensionOfAscendingNode(double gom) {
    kepler[4] = gom;
    equiAdapt[1] = kepler[1] * cos(kepler[3] + gom);
    equiAdapt[2] = kepler[1] * sin(kepler[3] + gom);
    equiAdapt[3] = tan(kepler[2] / 2.) * cos(gom);
    equiAdapt[4] = tan(kepler[2] / 2.) * sin(gom);
    equi[1] = equiAdapt[1];
    equi[2] = equiAdapt[2];
    equi[3] = equiAdapt[1];
    equi[4] = equiAdapt[2];
}
// ---------------------------------------------------------------------------
void State::setMass(double mass_) {
    mass = mass_;
}

// ---------------------------------------------------------------------------
/** Propagate orbit bulletin
 */
// ---------------------------------------------------------------------------
void State::PropagateToDate(double epochMjd2000, double mu) {
    double dt_sec = (epochMjd2000 - epoch) * 86400.;
    /*if (fabs(dt_sec) > 0)*/ {

        double ecc2 = equi[1] * equi[1] + equi[2] * equi[2];
        EquinoctialOrbit equin(equi[0] / (1 - ecc2),
                               equi[1],
                               equi[2],
                               equi[3],
                               equi[4],
                               equi[5],
                               TRUE_LONGITUDE,
                               frame,
                               GenericDate(),
                               mu);

        EquinoctialOrbit eq = equin.shiftedBy(dt_sec);
        equi[0] = eq.getSma() * (1 - eq.getEccentricity());
        equi[1] = eq.getEquinoctialEx();
        equi[2] = eq.getEquinoctialEy();
        equi[3] = eq.getHx();
        equi[4] = eq.getHy();
        equi[5] = eq.getTrueLongitude();

        equiAdapt[0] = eq.getSma();
        equiAdapt[1] = eq.getEquinoctialEx();
        equiAdapt[2] = eq.getEquinoctialEy();
        equiAdapt[3] = eq.getHx();
        equiAdapt[4] = eq.getHy();
        equiAdapt[5] = eq.getTrueLongitude();

        kepler[0] = eq.getSma();
        kepler[1] = eq.getEccentricity();
        kepler[2] = eq.getInclination();
        kepler[3] = eq.getArgumentPericenter();
        kepler[4] = eq.getRightAscensionOfAscendingNode();
        kepler[5] = eq.getMeanAnomaly();

        CartesianCoordinates pv = eq.getPVCoordinates();
        r = pv.getPosition();
        v = pv.getVelocity();
    }
}

