// $Id$
// ---------------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LT_IPOPT_DATATYPES_HPP
#define LT_IPOPT_DATATYPES_HPP

#include <iostream>
#include <vector>

#include <Eigen/Core>
#include "core/SizeParameters.hpp"// IDX_***, STATE_SIZE, STM_SIZE

typedef unsigned int uint;
typedef /*long*/ double real_type;
typedef /*long*/ double real;

// USING_PART_OF_NAMESPACE_EIGEN


using namespace Eigen;

// In EIGEN, to have VECTORIZATION, array size should be multiple of 16bytes, 128bits.
// like: Vector2d, Vector4f, Vector4d, Matrix2f, Matrix2d, Matrix4f, Matrix4d
// or be dynamically allocated.
// Note that double are aligned over 8 bytes, long double over 10bytes and float over 4bytes.
// dynamic matrix and vector types
typedef Eigen::Matrix<real_type, Eigen::Dynamic, 1> VECTORVAR;
typedef Matrix<real_type, Eigen::Dynamic, Eigen::Dynamic> MATRIXVAR;

// static matrix and vector types
typedef Matrix<real_type, STATE_SIZE, 1> S7VECTOR;
typedef Matrix<real_type, STATE_SIZE, STATE_SIZE> S7MATRIX;
typedef Matrix<real_type, 2 * STATE_SIZE, 1> S14VECTOR;
typedef Matrix<real_type, 2 * STATE_SIZE, 2 * STATE_SIZE> S14MATRIX;      // vectorizable
typedef Eigen::Matrix<real_type, STATE_SIZE, 2 * STATE_SIZE> S7_14MATRIX;  // vectorizable

typedef Eigen::Matrix<real_type, STATE_SIZE, 3> Matrix7x3;
typedef Eigen::Matrix<real_type, 3, STATE_SIZE> Matrix3x7;

typedef Matrix<real_type, 6, 1> Vector6;
typedef Matrix<real_type, 7, 1> Vector7;
typedef Eigen::Matrix<real_type, 7, 7> Matrix7;

typedef Matrix<real_type, 3, 1> Vector3;
typedef Eigen::Matrix<real_type, 3, 3> Matrix3;
typedef Eigen::Matrix<real_type, 6, 6> Matrix6;
typedef Eigen::Matrix<real_type, 6, 3> Matrix6x3;

#define S3MATRIX Matrix3  // 3x3 matrix
#define SVECTOR Vector3  // 3x1 vector

/* --------------------------------------------------------------------- */
#ifdef USE_MULTI_THREAD
#define MULTI_THREAD_PRESENT
#endif

/* --------------------------------------------------------------------- */
#endif  // LT_IPOPT_DATATYPES_HPP

