// $Id$
// ---------------------------------------------------------------------------
#ifndef SRC_CORE_FDERROR_HPP_
#define SRC_CORE_FDERROR_HPP_

#include <algorithm>        // for min
#include <vector>           // for vector

#include "core/DataTypes.hpp"
#include "core/Enumerations.hpp"
#include "LtProblemContext.hpp"
#include "core/State.hpp"

// create structure to store comparison data of finite differences
struct FDError {
 private:
    int row;
    int col;
    real_type ref;
    real_type val;
    real_type err;

 public:
    /** Creator. */
    FDError(int irow, int jcol, real_type ref, real_type val, real_type err)
        : row(irow),
         col(jcol),
         ref(ref),
         val(val),
         err(err) { }

    static double getRelativeError(double ref_value, double approx_value) {
        return fabs(approx_value - ref_value) / std::max(fabs(approx_value), 1.);
    }

    static std::vector<FDError>
    getRelativeError(const MATRIXVAR& m1, const MATRIXVAR& m2, real_type derivative_test_tol) {
        int nrows = static_cast<int>(std::min(m1.rows(), m2.rows()));
        int ncols = static_cast<int>(std::min(m1.cols(), m2.cols()));

        std::vector<FDError> fderror;

        for (int irow = 0; irow < nrows; ++irow) {
            for (int jcol = 0; jcol < ncols; ++jcol) {
                real_type ref = m1(irow, jcol);
                real_type approx = m2(irow, jcol);
                real_type diffError = FDError::getRelativeError(ref, approx);

                if ((fabs(diffError) > derivative_test_tol) || std::isnan(diffError)) {
                    fderror.push_back(FDError(irow, jcol, ref, approx, diffError));
                }
            }
        }

        return fderror;
    }

    /** Print derivative checker status header. */
    static void printHeader() {
        printf("  row col         ref.val         ~        fd.val               error\n");
    }

    /** Print derivative checker status. */
    void print() {
        printf("   %2d %2d  %23.16e  ~ %23.16e  [%10.3e]\n", row, col, ref, val, err);
    }

    /** Print derivative checker status.
     * @param ref reference value
     * @param val finite-difference value
     * @param err derivative error
     *
     */
    static void print(real_type ref, real_type val, real_type err) {
        printf("          %23.16e  ~ %23.16e  [%10.3e]\n", ref, val, err);
    }
};

/* --------------------------------------------------------------------- */
#endif  // SRC_CORE_FDERROR_HPP_

