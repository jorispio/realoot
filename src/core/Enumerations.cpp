/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string>
#include "Enumerations.hpp"

std::string getTerminalConstraintString(TerminalConstraintType c) {
    switch (c) {
        case C_ENERGY: return "C_ENERGY";
        case C_ORBIT: return "C_ORBIT";
        case C_SMA_ECC_INC:  return "C_SMA_ECC_INC";
        case C_SMA_ECC_INC_AOP: return "C_SMA_ECC_INC_AOP";
        case C_SMA_ECC_INC_RAAN: return "C_SMA_ECC_INC_RAAN";
    }
    return "N/A";
}

