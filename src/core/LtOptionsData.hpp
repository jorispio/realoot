// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef REALOOT_OPTIONS_STRUCT_HPP
#define REALOOT_OPTIONS_STRUCT_HPP

#include <string>

#include "core/DataTypes.hpp"
#include "core/Enumerations.hpp"

/* --------------------------------------------------------------------- */
typedef struct LT_OPTIONS {
    /* LT-IPOPT specific options */
    int Verbose = 1;         // verbosity level of the algorithm
    uint maxTrialPoints = 0;  // number of multistart

    std::string print_output_frame;  // output data frame
    double print_sol_period = -1;  // period of the output trajectory point when written to file

    DYNAMICALMODEL_TYPE print_traj_format = DYNAMICALMODEL_TYPE::D_EQUINOCTIAL;

    /** ODE Solver options */
    struct ode {
        /* ODE solver options */
        // std::string solvername;
        ODE_SOLVER_ID solverId;   // solver name (DOPRI5, DOP853)
        double abstol = 1e-8;     // absolute tolerance of the ODE solver
        double reltol = 1e-8;     // relative tolerance of the ODE solver
        double step = 0.001;      // initial integration step, or constant integration step (fixed step integrator)
        int nmax = 100;           // maximum number of steps
    } ode;

    bool compute_trajectory = false;

    //
    char ephemeris_file[255];  // ephemeris file. Id of new space object start at 10
    uint ephemeris_type;

    void scale(double a_m, double a_ms) {
        double a_s = a_m / a_ms;
        ode.step /= a_s;
    }
} LT_OPTIONS;

#endif  // REALOOT_OPTIONS_STRUCT_HPP

