// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2021 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __REALOOT_ECLIPSE_PARAMETERS_HPP__
#define __REALOOT_ECLIPSE_PARAMETERS_HPP__

#include "HelioLib/core/HelioEnumerations.hpp"
#include "HelioLib/Frames.hpp"
#include "HelioLib/Orbits.hpp"
#include "HelioLib/Physics.hpp"

struct LtEclipseParameters {
    /** Indicate whether eclipse are used in the simulation. */
    bool active;
    /** occulting body name */
    std::string name;
    /** occulting radius. */
    double occ_radius;
    /** orbit relative epoch with respect to simulation time. */
    double occ_orbit_epoch;
    /** orbit parameter type. */
    DYNAMICALMODEL_TYPE occ_orbit_type;
    /** orbit parameters of the occulting body with respect to the Sun. */
    std::array<double, 6> occ_orbit;
    /** */
    double cb_mu = MU_SUN;
    /** */
    Frame* occ_orbit_frame = (Frame*) FramesFactory::EME2000;
    /** */
    double eps;

    /** Constructor. */
    LtEclipseParameters() {
        active = false;
        name = "Occulting Body";
        occ_radius = 6378136.0;
        occ_orbit_type = DYNAMICALMODEL_TYPE::D_NOT_SET;        
        occ_orbit_epoch = 0;
        eps = 1e-3;
    }

    /** Set occulting body orbit parameters accordingly with occ_orbit_type. */
    void setOrbitArray(std::array<double, 6>& larray) {
        occ_orbit = larray;
    }

    /** @return occulting body orbit parameters accordingly with occ_orbit_type. */
    std::array<double, 6> getOrbitArray() const {
        return occ_orbit;
    }

    /** @return the position of the occulting body around the Sun. */
    Vector3 getOccultingBodyPosition(double relative_epoch) const {
        if (occ_orbit_type == D_EQUINOCTIAL) {
            EquinoctialOrbit orbit(
                occ_orbit[0], occ_orbit[1], occ_orbit[2], occ_orbit[3], occ_orbit[4], occ_orbit[5], PositionAngle::MEAN_LONGITUDE,
                occ_orbit_frame,
                GenericDate(), cb_mu);
            return orbit.propagate(relative_epoch).getPosition();

        }
        else if (occ_orbit_type == D_KEPLERIAN) {
            KeplerianOrbit orbit(
                occ_orbit[0], occ_orbit[1], occ_orbit[2], occ_orbit[3], occ_orbit[4], occ_orbit[5], PositionAngle::MEAN_ANOMALY,
                occ_orbit_frame,
                GenericDate(), cb_mu);
            return orbit.propagate(relative_epoch).getPosition();
        }

        return Vector3::Zero();
    }

    /** Indicate if the satellite is currently in eclipse. */
    EclipseCondition getCondition(double relative_epoch, const Vector3& R_Sc_Cb) {
        Vector3 rsun =-getOccultingBodyPosition(relative_epoch);
        return EclipseCylindrical::get_eclipse_condition(rsun,
                Vector3::Zero(), 
                R_Sc_Cb, 
                occ_radius);
    }

    /** Estimate eclipse entry/exit position for the current spacraft position. */
    double getConditionRatio(double relative_epoch, const Vector3& R_Sc_Cb) {
        Vector3 rsun =-getOccultingBodyPosition(relative_epoch);
        return EclipseCylindrical::get_eclipse_ratio(rsun,
                Vector3::Zero(), 
                occ_radius, 
                R_Sc_Cb);        
    }
};

#endif  // __REALOOT_ECLIPSE_PARAMETERS_HPP__
