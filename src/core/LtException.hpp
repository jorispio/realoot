// $Id$
// ---------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2015 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CORE_LTEXCEPTION_HPP_
#define _CORE_LTEXCEPTION_HPP_

#include <stdio.h>
#include <string.h>

#include <iostream>
#include <exception>

#if  defined(_MSC_VER) || defined(__STDC_LIB_EXT1__)
#define strncpy strncpy_s
#endif

class LtException : public std::exception {
 private:
    char msg[1024];

 public:
    // basic exception
    explicit LtException(const char* msh) {
        printf("%s\n", msh);
        strncpy(msg, msh, 1024);
    }

    // Exception is a concatenation of messages.
    LtException(const char* ms1, const char* ms2) {
        snprintf(msg, 1024, "%s\n%s", ms1, ms2);
        printf("%s\n", msg);
    }

    virtual const char* what() const throw() {
        return msg;
    }
};

// ---------------------------------------------------------------------
#endif  // _CORE_LTEXCEPTION_HPP_

