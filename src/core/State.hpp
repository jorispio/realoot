// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef REALOOT_STATE_HPP
#define REALOOT_STATE_HPP

#include "HelioLib/Core.hpp"
#include "HelioLib/Frames.hpp"
#include "HelioLib/Orbits.hpp"

#include "LtException.hpp"

// ---------------------------------------------------------------------------
class State {
 private:
    /** Frame */
    Frame* frame;
    void updateKepler(double mu);
    void updateEqui(double mu);
    void updateCartesian(double mu);

 public:
    /** Coordinates type. */
    DYNAMICALMODEL_TYPE type;

    /** Epoch of this state, MJD2000 */
    double epoch;

    /** position vector */
    Vector3 r;
    /** velocity vector */
    Vector3 v;

    /** equinoctial coordinates {p, ex, ey, hx, hy, L} */
    double equi[6];

    /** equinoctial coordinates {a, ex, ey, hx, hy, L} */
    double equiAdapt[6];

    /** */
    double kepler[6];

    /** mass */
    double mass;

    /** costate vector */
    double lambda[6];

 public:
    /** Simple creator. */
    State();

    /** */
    State(DYNAMICALMODEL_TYPE typeIn, const std::array<double, 6>& oe, double mass, double mu = 1);    
    State(DYNAMICALMODEL_TYPE typeIn, const Vector6 &oe, double mass, double mu = 1);

    /** Creator of equinoctial state. */
    State(const std::array<double, 6>& e, double m, double mu = 1);

    /** Creator for keplerian state */
    State(double sma,
          double ecc,
          double inc,
          double pom,
          double gom,
          double man,
          const Frame* frame,
          double mu = 1);

    /** */
    void scale(double a_m, double a_ms);

    /** */
    void PropagateToDate(double epochMjd2000, double mu);

    void setEpoch(double epochMjd2000_);

    void setSma(double sma);
    inline double getSma() { return kepler[0]; }

    void setEccentricity(double ecc);
    inline double getEccentricity() { return kepler[1]; }

    void setInclination(double inc);
    inline double getInclination() { return kepler[2]; }

    void setArgumentOfPericenter(double pom);
    inline double getArgumentOfPericenter() { return kepler[3]; }

    void setRightAscensionOfAscendingNode(double gom);
    inline double getRightAscensionOfAscendingNode() { return kepler[4]; }

    void setMass(double mass);
    inline double getMass() { return mass; };
};
// ---------------------------------------------------------------------------
#endif  // REALOOT_STATE_HPP

