// $Id$
// ---------------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef REALOOT_ENUMS_HPP
#define REALOOT_ENUMS_HPP

#include <string>

/*
 */
enum OBJECTIVE_TYPE {
    C_TIME,      // time minimisation
    C_QUAD       // quadratic cost for |u|
};



/* Departure condition */
enum DepartureCondition {
    dPosition = 200,
};

// ---------------------------------------------------------------------
/* Terminal Constraint */
enum TerminalConstraintType {
    C_ENERGY, C_ORBIT, C_SMA_ECC_INC, C_SMA_ECC_INC_AOP, C_SMA_ECC_INC_RAAN
};
std::string getTerminalConstraintString(TerminalConstraintType c);

/* --------------------------------------------------------------------- */
enum EXITFLAGS {
    LT_Solver_Is_Running,
    LT_Solve_Succeeded,
    LT_Solve_MinChange,
    LT_Too_Many_Iterations,
    LT_Continuation_Failed,
    LT_Cannot_Find_Initial_Guess,
    LT_Bad_Initialization = -99
};

/* --------------------------------------------------------------------- */
#endif  // REALOOT_ENUMS_HPP

