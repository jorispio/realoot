// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef REALOOT_PROBLEMDEFINITION_HPP
#define REALOOT_PROBLEMDEFINITION_HPP

#include "core/DataTypes.hpp"
#include "core/Enumerations.hpp"
#include "core/State.hpp"
#include "core/LtSpacecraftData.hpp"
#include "core/LtEclipseParameters.hpp"

// ---------------------------------------------------------------------------
// Problem context
// It contains
// - the spacecraft definition
// - the LT problem definition (constraints, t0, tf)
// - the LT problem initial guess
//
struct LtProblemDefinition {
    OBJECTIVE_TYPE objtype;  // objective function type

    TerminalConstraintType constraintType;

    DYNAMICALMODEL_TYPE dynamicalFormulation;  // dynamical model {Cartesian, Keplerian}

    LtSpacecraftData spacecraft;

    LtEclipseParameters eclipse;
    
    double mu;  // gravitational parameter
    double cj2;  // zonal harmonic
    double radius;  // Earth radius

    double tof;      // total time of flight

    State state0;  // initial state vector
    State statef;  // final (desired) state vector
};

#endif  // REALOOT_PROBLEMDEFINITION_HPP

