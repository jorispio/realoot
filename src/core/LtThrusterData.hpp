// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef REALOOT_THRUSTER_DATA_HPP
#define REALOOT_THRUSTER_DATA_HPP

#include <string>
#include "core/Enumerations.hpp"
#include "HelioLib/Core.hpp"

/* --------------------------------------------------------------------- */

typedef struct LtThrusterData {
    PROPULSION_TYPE PropType;  // propulsion type (NEP, SEP)
    double Fth;               // nominal thrust amplitude
    double g0Isp;             // exhaust velocity
    double eff;               // efficiency
    
    bool constantAcceleration; // constant acceleration. Flow rate = 0    
    
    // specific to SEP
    double maxSEPFth;         // maximum thrust for the SEP system
    double rscale;            // distance 1AU

    LtThrusterData() {
        PropType = PROPULSION_TYPE::PROP_NEP;
        Fth = 0;
        g0Isp = 0;
        eff = 1;
        maxSEPFth = 0;
        rscale = 1;
        constantAcceleration = false;
    }

    LtThrusterData(double Fth_, double g0Isp_) {
        PropType = PROPULSION_TYPE::PROP_NEP;
        Fth = Fth_;
        g0Isp = g0Isp_;
        rscale = 1;
        eff = 1.;
        constantAcceleration = false;
    }

    double getFlowRate() {
        if (constantAcceleration) {
            return 0.;
        }
        return Fth / g0Isp;
    }
} LtThrusterData;


#endif  // REALOOT_THRUSTER_DATA_HPP

