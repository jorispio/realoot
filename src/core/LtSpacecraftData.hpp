// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef REALOOT_SPACECRAFT_DATA_HPP
#define REALOOT_SPACECRAFT_DATA_HPP

#include <string>

#include "core/Enumerations.hpp"
#include "LtThrusterData.hpp"


typedef struct LtSpacecraftData {
    std::string name;
    double dry_mass;          // dry mass (integration will stop if reached)
    double mass;              // initial spacecraft mass
    LtThrusterData propulsion;  // propulsion

    LtSpacecraftData() : name(""), dry_mass(1), mass(1), propulsion(LtThrusterData()) {  }

    LtSpacecraftData(std::string name_, LtThrusterData propulsion_, double mass_) : name(name_), mass(mass_), propulsion(propulsion_) {
        dry_mass = 0.15 * mass;
    }

    LtSpacecraftData(std::string name_, LtThrusterData propulsion_, double mass_, double dry_mass_)
     : name(name_), dry_mass(dry_mass_), mass(mass_), propulsion(propulsion_) {  }
} LtSpacecraftData;

// ---------------------------------------------------------------------
#endif  // REALOOT_SPACECRAFT_DATA_HPP

