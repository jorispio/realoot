// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __UTILS_ENUM_HPP__
#define __UTILS_ENUM_HPP__

#include <stdio.h>
#include <string>

#ifdef _WIN32
#include <string.h> 
#define strncasecmp _strnicmp
#define strcasecmp _stricmp
#else
#include <strings.h> // strcasecmp
#endif

#include "Enumerations.hpp"
#include "ContinuationVariable.hpp"
#include "LtException.hpp"
#include "HelioLib/Core.hpp"  // DYNAMICALMODEL_TYPE
#include "utils/maths/solver/SolverTypeEnum.hpp"

DYNAMICALMODEL_TYPE getOrbitEnum(const char* orbitParameterTypeStr);

TerminalConstraintType getConstraintEnum(const char* terminalConstraintTypeStr);

ContinuationVariable getContinuationVariableEnum(const char* variableNameStr);

SolverType getSolverTypeEnum(const char* solverTypeStr);
// -------------------------------------------------------------------------
#endif  // __UTILS_ENUM_HPP__
