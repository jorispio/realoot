// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2020 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "AbstractInputScriptReader.hpp"

#include <strings.h>  // strcasecmp

#include "UtilsEnum.hpp"  // getOrbitEnum


int AbstractInputScriptReader::set_mode(const char* modeParameterTypeStr, LtProblemContext& problemContext) {
    int nerror = 0;

    if (strcasecmp(modeParameterTypeStr, "Simulation") == 0) {
        problemContext.performSimulation = true;
        problemContext.performOptimisation = false;
    }
    else if (strcasecmp(modeParameterTypeStr, "Optimisation") == 0) {
        problemContext.performSimulation = false;
        problemContext.performOptimisation = true;
    }
    else {
        problemContext.performSimulation = false;
        problemContext.performOptimisation = false;
        printf("Mode/value type is not defined {Simulation, Optimisation}\n");
        ++nerror;
    }
    return nerror;
}

int AbstractInputScriptReader::set_objective(const char* objective, LtProblemDefinition& problemDefinition) {
    if (strcmp(objective, "MIN TIME") == 0) {
        problemDefinition.objtype = C_TIME;
    }
    return 0;
}

void AbstractInputScriptReader::set_format(const char* strAttrData, LtProblemContext& problemContext) {
    problemContext.outputFormat = getOrbitEnum(strAttrData);
    if (problemContext.outputFormat == DYNAMICALMODEL_TYPE::D_NOT_SET) {
        problemContext.outputFormat = D_EQUINOCTIAL;

        // error
        char orbitParameterTypeStr[255];
        strncpy(orbitParameterTypeStr, strAttrData, 255);
        printf("Ouput orbital elements type is not properly defined: %s {CARTESIAN, EQUINOCTIAL, KEPLERIAN}\n", orbitParameterTypeStr);
    }
}
