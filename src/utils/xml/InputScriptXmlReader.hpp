// $Id$
// ---------------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef INPUT_SCRIPT_XML_READER_HPP
#define INPUT_SCRIPT_XML_READER_HPP

#include <string>

 //  Includes for XML
#include <wchar.h>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/framework/StdOutFormatTarget.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLUni.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>
#include <xercesc/dom/DOMLSSerializerFilter.hpp>
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/sax/ErrorHandler.hpp>
#include <xercesc/dom/DOMLSSerializerFilter.hpp>

//#include "ode/TDynamicsAveragedEquinoctial.hpp"
#include "Enumerations.hpp"
#include "LtSpacecraftData.hpp"
#include "LtProblemContext.hpp"
#include "LtProblemDefinition.hpp"
#include "StateNumericalPropagator.hpp"
#include "xml/UtilsStringXml.hpp"
#include "AbstractInputScriptReader.hpp"

XERCES_CPP_NAMESPACE_USE

//DYNAMICALMODEL_TYPE getOrbitEnum(const char* orbitParameterTypeStr);

class InputScriptXmlReader : public AbstractInputScriptReader {
private:
    char* getChildElementValue(DOMElement* parent, const char* tag);
    char* getElementAttributeValue(DOMElement* parent, const char* tag, const char* attribute);

    double getDataUnitValue(DOMElement* parent, const char* tag);
    double getSpeedDataUnitValue(DOMElement* parent, const char* tag);
    double getAngleDataUnitValue(DOMElement* parent, const char* tag);
    
    int getOrbitFromElements(DOMElement* parent, DYNAMICALMODEL_TYPE orbitParameterType, std::array<double, 6>& orbit, LtProblemDefinition& problemDefinition, bool allowLongitude);

    int readOrbitalPosition(DOMElement* parent, std::array<double, 6>& orbit);

public:
    InputScriptXmlReader();
    ~InputScriptXmlReader();

    void Init();
    void Close();

    bool readXml(std::string gInputScript, LtProblemDefinition& problemDefinition, LtProblemContext& problemContext);
};

#endif  // INPUT_SCRIPT_XML_READER_HPP
