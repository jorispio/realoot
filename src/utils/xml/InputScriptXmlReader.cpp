// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "InputScriptXmlReader.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <ctime>

#include <algorithm>
#include <string>

#include "Enumerations.hpp"
#include "HelioLib/Core.hpp"
#include "LtSpacecraftData.hpp"
#include "ode/TDynamicsAveragedEquinoctial.hpp"
#include "utils/maths/solver/SolverTypeEnum.hpp"
#include "UtilsEnum.hpp"
#include "StateNumericalPropagator.hpp"
#include "xml/DOMTreeErrorReporter.hpp"

 // ---------------------------------------------------------------------------
 // DEFAULT VALUES
#define DEFAULT_MU         EARTH_MU
#define DEFAULT_RADIUS     EARTH_RADIUS

//#define DEBUG_READING

/* ------------------------------------------------------------------------- */
bool StringToBoolean(const char* str) {
    if (strcasecmp(str, "yes") == 0) {
        return true;
    }
    if (strcasecmp(str, "true") == 0) {
        return true;
    }
    return false;
}

// -------------------------------------------------------------------------
InputScriptXmlReader::InputScriptXmlReader() : AbstractInputScriptReader() {

}
// -------------------------------------------------------------------------
InputScriptXmlReader::~InputScriptXmlReader() {
}
// -------------------------------------------------------------------------
void InputScriptXmlReader::Init() {
    // Initialize the XML4C2 system
    try {
        XMLPlatformUtils::Initialize();
    }
    catch (const XMLException& toCatch) {
        XERCES_STD_QUALIFIER cerr << "Error during Xerces Initialization.\n"
            << "  Exception message:"
            << StrX(toCatch.getMessage()) << XERCES_STD_QUALIFIER endl;
    }
}
// -------------------------------------------------------------------------
void InputScriptXmlReader::Close() {
    XMLPlatformUtils::Terminate();
}

// -------------------------------------------------------------------------
char* InputScriptXmlReader::getChildElementValue(DOMElement* parent, const char* tag) {

    DOMElement* child = dynamic_cast<DOMElement*>(parent->getElementsByTagName(XMLString::transcode(tag))->item(0));
    if (child) {
        char* tempVal = XMLString::transcode((XMLCh*)child->getTextContent());
        char* val = tempVal;
        //XMLString::release(&tempVal);
        return val;
    }
    return NULL;
}
// -------------------------------------------------------------------------
char* InputScriptXmlReader::getElementAttributeValue(DOMElement* parent, const char* tag, const char* attributeTag) {

    DOMElement* child = dynamic_cast<DOMElement*>(parent->getElementsByTagName(XMLString::transcode(tag))->item(0));
    if (child) {
        const XMLCh* tempAttributeTag = XMLString::transcode(attributeTag);        
        const XMLCh* attributeValue = child->getAttribute(tempAttributeTag);
        if (attributeValue) {
            char* tempAttributeVal = XMLString::transcode(attributeValue);
            //char* val = tempAttributeVal;
            //XMLString::release(&tempAttributeVal);
            return tempAttributeVal;
        }
    }
    return NULL;
}
/* ------------------------------------------------------------------------- */
double InputScriptXmlReader::getDataUnitValue(DOMElement* parent, const char* tag) {
    double scale = 1.;
    const char* strAttrData = getElementAttributeValue(parent, tag, "unit");
    if ((strAttrData != NULL) && (strAttrData[0] != (char) 0)) {
        if (strcmp(strAttrData, "m") == 0) {
            scale = 1.;
        }
        else if (strcmp(strAttrData, "km") == 0) {
            scale = 1000.;
        }
        else if (strcmp(strAttrData, "AU") == 0) {
            scale = AU;
        }
        else { 
            printf("Unknown distance unit [%s]\n", strAttrData);
        }
    }
    return scale;
}
/* ------------------------------------------------------------------------- */
// convert to m/s
double InputScriptXmlReader::getSpeedDataUnitValue(DOMElement* parent, const char* tag) {
    double scale = 1.;
    const char* strAttrData = getElementAttributeValue(parent, tag, "unit");
    if ((strAttrData != NULL) && (strAttrData[0] != (char) 0)) {
        if (strcmp(strAttrData, "m/s") == 0) {
            scale = 1.;
        }
        else if (strcmp(strAttrData, "km/s") == 0) {
            scale = 1000.;
        }
        else if (strcmp(strAttrData, "km/h") == 0) {
            scale = 1000. / 3600;
        }
        else {
            printf("Unknown velocity unit [%s]\n", strAttrData);
        }
    }
    return scale;
}
/* ------------------------------------------------------------------------- */
// convert to radians
double InputScriptXmlReader::getAngleDataUnitValue(DOMElement* parent, const char* tag) {
    double scale = DEG2RAD;
    const char* strAttrData = getElementAttributeValue(parent, tag, "unit");
    if ((strAttrData != NULL) && (strAttrData[0] != (char) 0)) {
        if (strcmp(strAttrData, "rad") == 0) {
            scale = 1.;
        }
        else if (strcmp(strAttrData, "deg") == 0) {
            scale = DEG2RAD;
        }
        else {
            printf("Unknown angular unit [%s]\n", strAttrData);
        }
    }
    return scale;
}

/* ------------------------------------------------------------------------- */
int InputScriptXmlReader::getOrbitFromElements(DOMElement* parent, DYNAMICALMODEL_TYPE orbitParameterType, std::array<double, 6>& orbit, LtProblemDefinition& problemDefinition, bool allowLongitude) {
    int nerror = 0;
    if (orbitParameterType == D_KEPLERIAN) {

        bool hasSmaDefined = false;
        const char* strElementData = getChildElementValue(parent, "sma");
        if (strElementData) {
            hasSmaDefined = true;
            orbit[0] = atof(strElementData);
            
            double unitScale = getDataUnitValue(parent, "sma");
            orbit[0] *= unitScale;
            if (orbit[0] < 0) {
                ++nerror;
                printf("SMA shall be strictly positive.");
            }
        }

        if (hasSmaDefined) {
            strElementData = getChildElementValue(parent, "ecc");
            if (strElementData) {
                orbit[1] = atof(strElementData);
                if ((orbit[1] < 0) || (orbit[1] > 1)) {
                    ++nerror;
                    printf("Eccentricity shall be between 0 and 1.\n");
                }
            }
            else { 
                ++nerror; 
                printf("Eccentricity is not defined.\n");
            }
        }
        else {
            double ha = 0, hp = 0;
            strElementData = getChildElementValue(parent, "ha");
            if (strElementData) {
                ha = atof(strElementData);
                double unitScale = getDataUnitValue(parent, "ha");
                ha *= unitScale;
                if (ha < 0) {
                    ++nerror;
                    printf("Apoapsis altitude shall be strictly positive.\n");
                }
            }
            else { ++nerror; }

            strElementData = getChildElementValue(parent, "hp");
            if (strElementData) {
                hp = atof(strElementData);
                double unitScale = getDataUnitValue(parent, "hp");
                hp *= unitScale;                
                if (hp < 0) {
                    ++nerror;
                    printf("Periapsis altitude shall be strictly positive.\n");
                }
            }
            else { ++nerror; }

            if (ha < hp) {                
                ++nerror;
                printf("Apoapsis < Periapsis!.\n");
            }

            orbit[0] = problemDefinition.radius + (ha + hp) / 2.;
            orbit[1] = (ha - hp) / (2 * orbit[0]);
        }

        strElementData = getChildElementValue(parent, "inc");
        if (strElementData) {
            orbit[2] = atof(strElementData);
            double unitScale = getAngleDataUnitValue(parent, "inc");
            orbit[2] *= unitScale;
        }
        else { 
            ++nerror; 
            printf("Inclination is not defined!.\n");
        }

        strElementData = getChildElementValue(parent, "aop");
        if (strElementData) {
            orbit[3] = atof(strElementData);
            double unitScale = getAngleDataUnitValue(parent, "aop");
            orbit[3] *= unitScale;
        }
        else { 
            ++nerror; 
            printf("Argument of apoapsis is not defined!.\n");
        }

        strElementData = getChildElementValue(parent, "raan");
        if (strElementData) {
            orbit[4] = atof(strElementData);
            double unitScale = getAngleDataUnitValue(parent, "raan");
            orbit[4] *= unitScale;
        }
        else { 
            ++nerror; 
            printf("Right Ascension of Ascending node is not defined!.\n");    
        }

        if (allowLongitude) {
            nerror += readOrbitalPosition(parent, orbit);
        }

#ifdef DEBUG_READING
        printf("   orbit (KEPLERIAN: sma=%f, ecc=%f, inc=%f, aop=%f, raan=%f\n", orbit[0], orbit[1], orbit[2], orbit[3], orbit[4]);
#endif

    }
    else if (orbitParameterType == D_EQUINOCTIAL) {

        const char* strElementData = getChildElementValue(parent, "sma");
        if (strElementData) {
            orbit[0] = atof(strElementData);
            double unitScale = getDataUnitValue(parent, "sma");
            orbit[0] *= unitScale;            
            if (orbit[0] < 0) ++nerror;
        }
        else { ++nerror; }

        strElementData = getChildElementValue(parent, "ex");
        if (strElementData) {
            orbit[1] = atof(strElementData);
        }
        else { 
            ++nerror; 
            printf("Eccentricity X-component is not defined.\n");
        }

        strElementData = getChildElementValue(parent, "ey");
        if (strElementData) {
            orbit[2] = atof(strElementData);
        }
        else { 
            ++nerror; 
            printf("Eccentricity Y-component is not defined.\n");
        }

        strElementData = getChildElementValue(parent, "hx");
        if (strElementData) {
            orbit[3] = atof(strElementData);
        }
        else { ++nerror; }

        strElementData = getChildElementValue(parent, "hy");
        if (strElementData) {
            orbit[4] = atof(strElementData);
        }
        else { ++nerror; }

        if (allowLongitude) {
            nerror += readOrbitalPosition(parent, orbit);
        }

    }
    else if ((orbitParameterType == D_CARTESIAN) && (allowLongitude)) {

        const char* strElementData = getChildElementValue(parent, "x");
        if (strElementData) {
            orbit[0] = atof(strElementData);
            double unitScale = getDataUnitValue(parent, "x");
            orbit[0] *= unitScale;
        }
        else { ++nerror; }

        strElementData = getChildElementValue(parent, "y");
        if (strElementData) {
            orbit[1] = atof(strElementData);
            double unitScale = getDataUnitValue(parent, "y");
            orbit[1] *= unitScale;
        }
        else { ++nerror; }

        strElementData = getChildElementValue(parent, "z");
        if (strElementData) {
            orbit[2] = atof(strElementData);
            double unitScale = getDataUnitValue(parent, "z");
            orbit[2] *= unitScale;
        }
        else { ++nerror; }

        strElementData = getChildElementValue(parent, "vx");
        if (strElementData) {
            orbit[3] = atof(strElementData);
            double unitScale = getSpeedDataUnitValue(parent, "vx");
            orbit[3] *= unitScale;
        }
        else { ++nerror; }

        strElementData = getChildElementValue(parent, "vy");
        if (strElementData) {
            orbit[4] = atof(strElementData);
            double unitScale = getSpeedDataUnitValue(parent, "vy");
            orbit[4] *= unitScale;
        }
        else { ++nerror; }

        strElementData = getChildElementValue(parent, "vz");
        if (strElementData) {
            orbit[5] = atof(strElementData);
            double unitScale = getSpeedDataUnitValue(parent, "vz");
            orbit[5] *= unitScale;
        }
        else { ++nerror; }
    }
    else {
        nerror = 100;
    }

    return nerror;
}

/* ------------------------------------------------------------------------- */
int InputScriptXmlReader::readOrbitalPosition(DOMElement* parent, std::array<double, 6>& orbit) {
    int nerror = 0;
    const char* strAttrData = getChildElementValue(parent, "L");
    if (strAttrData) {
        orbit[5] = atof(strAttrData);
        double unitScale = getAngleDataUnitValue(parent, "L");
        orbit[5] *= unitScale;
    }
    else { 
        // mean anomaly
        strAttrData = getChildElementValue(parent, "mean_anomaly");
        if (strAttrData) {
            orbit[5] = atof(strAttrData);
            double unitScale = getAngleDataUnitValue(parent, "mean_anomaly");
            orbit[5] *= unitScale;
        }
        else {   
            ++nerror; 
            printf("Orbital position {Longitude, mean anomaly} is not defined but required!.\n");
        }
    }
    return nerror;
}
/* ------------------------------------------------------------------------- */
bool InputScriptXmlReader::readXml(std::string gInputScript, LtProblemDefinition& problemDefinition, LtProblemContext& problemContext) {
    if (gInputScript.empty()) {
        return false;
    }

    Default(problemDefinition, problemContext);

    // create parser
    XercesDOMParser* parser = new XercesDOMParser();
    /*
    parser->setValidationScheme(gValScheme);
    parser->setDoNamespaces(gDoNamespaces);
    parser->setDoSchema(gDoSchema);
    parser->setHandleMultipleImports (true);
    parser->setValidationSchemaFullChecking(gSchemaFullChecking);
    parser->setCreateEntityReferenceNodes(gDoCreate);
    */

    // attach an error handler to the parser.
    DOMTreeErrorReporter* errReporter = new DOMTreeErrorReporter();
    parser->setErrorHandler(errReporter);

    //  Parse the XML file, catching any XML exceptions
    bool errorsOccured = false;
    try {
        parser->parse(gInputScript.c_str());
    }
    catch (const OutOfMemoryException&) {
        XERCES_STD_QUALIFIER cerr << "OutOfMemoryException" << XERCES_STD_QUALIFIER endl;
        errorsOccured = true;
    }
    catch (const XMLException& e) {
        XERCES_STD_QUALIFIER cerr << "An error occurred during parsing\n   Message: "
            << StrX(e.getMessage()) << XERCES_STD_QUALIFIER endl;
        errorsOccured = true;
    }
    catch (const DOMException& e) {
        const unsigned int kMaxChars = 2047;
        XMLCh errText[kMaxChars + 1];

        XERCES_STD_QUALIFIER cerr << "\nDOM Error during parsing: '" << gInputScript << "'\n"
            << "DOMException code is:  " << e.code << XERCES_STD_QUALIFIER endl;

        if (DOMImplementation::loadDOMExceptionMsg(e.code, errText, kMaxChars))
            XERCES_STD_QUALIFIER cerr << "Message is: " << StrX(errText) << XERCES_STD_QUALIFIER endl;

        errorsOccured = true;
    }
    catch (...) {
        XERCES_STD_QUALIFIER cerr << "An error occurred during parsing\n " << XERCES_STD_QUALIFIER endl;
        errorsOccured = true;
    }

    // If the parse was successful, output the document data from the DOM tree
    if (!errorsOccured && !errReporter->getSawErrors()) {
        // get the DOM representation
        DOMDocument* doc = parser->getDocument();

        try {
            int nerror = 0;
            DOMElement* docElement = doc->getDocumentElement();

            DOMNodeList* nodeListMode = docElement->getElementsByTagName(myXerces::uStr("mode").ptr());
            if (nodeListMode->getLength() > 0) {
                DOMNode* curNode = nodeListMode->item(0);
                DOMElement* parent = reinterpret_cast<DOMElement*>(curNode);

                const XMLCh* strAttrData = parent->getAttribute(myXerces::uStr("value").ptr());
                if (!strAttrData) {
                    printf("Mode/value type is not defined {Simulation, Optimisation}\n");
                }
                else {
                    char modeParameterTypeStr[255];
                    strcpy(modeParameterTypeStr, myXerces::cStr(strAttrData).ptr());
                    nerror += set_mode(modeParameterTypeStr, problemContext);
                }
            }

            DOMNodeList* nodeListSc = docElement->getElementsByTagName(myXerces::uStr("spacecraft").ptr());
            if (!nodeListSc->getLength()) {
                printf("Spacecraft is not defined: missing tag <spacecraft>\n");
                return false;
            }
            
            DOMNode* curNode = nodeListSc->item(0);
            DOMElement* parent = reinterpret_cast<DOMElement*>(curNode);
            const XMLCh* xmlAttrData = parent->getAttribute(myXerces::uStr("name").ptr());
#ifdef DEBUG_READING
            std::cout << "spacecraft name: " << myXerces::cStr(xmlAttrData).ptr() << std::endl;
#endif
            problemDefinition.spacecraft.name = std::string(myXerces::cStr(xmlAttrData).ptr());

            const char* strAttrData = getChildElementValue(parent, "thrust");
            if (strAttrData) {
#ifdef DEBUG_READING
                std::cout << "   thrust: " << strAttrData << std::endl;
#endif
                problemDefinition.spacecraft.propulsion.Fth = atof(strAttrData);
            }
            else { ++nerror; }

            strAttrData = getChildElementValue(parent, "Isp");
            if (strAttrData) {
#ifdef DEBUG_READING
                std::cout << "   isp: " << strAttrData << std::endl;
#endif
                problemDefinition.spacecraft.propulsion.g0Isp = g0 * atof(strAttrData);
            }
            else { ++nerror; }

            strAttrData = getChildElementValue(parent, "mass");
            if (strAttrData) {
#ifdef DEBUG_READING
                std::cout << "   mass: " << strAttrData << std::endl;
#endif
                problemDefinition.spacecraft.mass = atof(strAttrData);
                problemDefinition.state0.mass = problemDefinition.spacecraft.mass;
            }
            else { ++nerror; }

            if (nerror > 0) {
                printf("An error occured when reading spacecraft\n");
            }

            // Eclipse
            DOMNodeList* nodeListEclipse = docElement->getElementsByTagName(myXerces::uStr("eclipse").ptr());
            if (nodeListEclipse->getLength() > 0) {
                DOMNode* curNode = nodeListEclipse->item(0);
                DOMElement* parent = reinterpret_cast<DOMElement*>(curNode);                
                int neclerr = 0;

                const char* strAttrData = getChildElementValue(parent, "name");
                problemDefinition.eclipse.name = std::string(strAttrData);

                problemDefinition.eclipse.occ_radius = 0;
                strAttrData = getChildElementValue(parent, "radius");
                if (strAttrData) {
                    problemDefinition.eclipse.occ_radius = atof(strAttrData);
                    double unitScale = getSpeedDataUnitValue(parent, "radius");
                    problemDefinition.eclipse.occ_radius *= unitScale;
                }
                if (problemDefinition.eclipse.occ_radius <= 0) {
                    ++nerror;
                    ++neclerr;
                    printf("Eclipse occulting body radius shall be strictly positive!\n");
                }

                problemDefinition.eclipse.occ_orbit_epoch = 0;
                strAttrData = getChildElementValue(parent, "epoch");
                if (strAttrData) {
                    problemDefinition.eclipse.occ_orbit_epoch = atof(strAttrData);
                }

                DOMNodeList* nodeListOccBodyOrbit = parent->getElementsByTagName(myXerces::uStr("orbit").ptr());
                if (nodeListOccBodyOrbit->getLength() > 0) {
                    DOMNode* curNode = nodeListOccBodyOrbit->item(0);
                    DOMElement* parent = reinterpret_cast<DOMElement*>(curNode);                    

                    const XMLCh* strAttrData = parent->getAttribute(myXerces::uStr("type").ptr());
                    if (!strAttrData) {
                        printf("Occulting body orbit type is not defined {CARTESIAN, EQUINOCTIAL, KEPLERIAN}\n");
                    }
                    else {
                        DYNAMICALMODEL_TYPE orbitParameterType = getOrbitEnum(myXerces::cStr(strAttrData).ptr());
                        problemDefinition.eclipse.occ_orbit_type = orbitParameterType;
                        if (orbitParameterType == DYNAMICALMODEL_TYPE::D_NOT_SET) {
                            // error
                            char orbitParameterTypeStr[255];
                            strncpy(orbitParameterTypeStr, myXerces::cStr(strAttrData).ptr(), 255);
                            printf("Occulting body orbit type is not properly defined: '%s' {CARTESIAN, EQUINOCTIAL, KEPLERIAN}\n", orbitParameterTypeStr);
                        }

                        neclerr += getOrbitFromElements(parent, orbitParameterType, problemDefinition.eclipse.occ_orbit, problemDefinition, true);
                        problemDefinition.eclipse.active = neclerr == 0;
                    }

                }
                else {
                    ++nerror;
                    printf("Eclipse is used without occulting body orbit! The feature will be disabled.\n");
                }
            }

            // Dynamics
            DOMNodeList* nodeListDyn = docElement->getElementsByTagName(myXerces::uStr("dynamics").ptr());
            if (nodeListDyn->getLength() > 0) {
                DOMNode* curNode = nodeListDyn->item(0);
                DOMElement* parent = reinterpret_cast<DOMElement*>(curNode);

                int nerror = 0;

                const char* strAttrData = getChildElementValue(parent, "mu");
                if (strAttrData) {
#ifdef DEBUG_READING
                    std::cout << "   mu: " << strAttrData << std::endl;
#endif
                    problemDefinition.mu = atof(strAttrData);
                }
                else { ++nerror; }

                strAttrData = getChildElementValue(parent, "j2");
                if (strAttrData) {
#ifdef DEBUG_READING
                    std::cout << "   j2: " << strAttrData << std::endl;
#endif
                    problemDefinition.cj2 = atof(strAttrData);
                }
                else { problemDefinition.cj2 = 0; }

                strAttrData = getChildElementValue(parent, "radius");
                if (strAttrData) {
#ifdef DEBUG_READING
                    std::cout << "radius: " << strAttrData << std::endl;
#endif
                    problemDefinition.radius = atof(strAttrData);
                }
                else { problemDefinition.radius = DEFAULT_RADIUS; }

                if (nerror > 0) {
                    printf("An error occured when reading dynamics\n");
                }
            }

            DOMNodeList* nodeListOcp = docElement->getElementsByTagName(myXerces::uStr("ocp").ptr());
            if (!nodeListOcp->getLength()) {
                printf("Optimal Control Problem is not defined: missing tag <ocp>\n");
                return false;
            }
            if (nodeListOcp->getLength() > 0) {
                DOMNode* curNode = nodeListOcp->item(0);
                DOMElement* parent = reinterpret_cast<DOMElement*>(curNode);
                const XMLCh* xmlAttrData = parent->getAttribute(myXerces::uStr("objective").ptr());
#ifdef DEBUG_READING
                std::cout << "objective: " << myXerces::cStr(xmlAttrData).ptr() << std::endl;
#endif

                set_objective(myXerces::cStr(xmlAttrData).ptr(), problemDefinition);

                int nerror = 0;

                // Init guess
                DOMNodeList* nodeListInitiguess = parent->getElementsByTagName(myXerces::uStr("initguess").ptr());
                if (!nodeListInitiguess->getLength()) {
                    printf("Initial guess is not defined: missing tag <initguess>\n");
                    return false;
                }
                parent = reinterpret_cast<DOMElement*>(nodeListInitiguess->item(0));

                xmlAttrData = parent->getAttribute(myXerces::uStr("allow_multi_start").ptr());
                if (xmlAttrData) {
#ifdef DEBUG_READING
                    std::cout << "   allow_multi_start: " << myXerces::cStr(xmlAttrData).ptr() << std::endl;
#endif
                    problemContext.initGuessMultiStart = StringToBoolean(myXerces::cStr(xmlAttrData).ptr());
                }
                else {
                    problemContext.initGuessMultiStart = false;
                }

                const char* strAttrData = getChildElementValue(parent, "max_attempts");
                if (strAttrData) {
#ifdef DEBUG_READING
                    std::cout << "   max_attempts: " << strAttrData << std::endl;
#endif
                    problemContext.initGuessMultiStartMaxAttempts = std::max(static_cast<int>(atof(strAttrData)), 1);
                }
                else { problemContext.initGuessMultiStartMaxAttempts = 5; };

                strAttrData = getChildElementValue(parent, "costate_sma");
                if (strAttrData) {
                    problemContext.initguess[P_IDX_PSMA] = atof(strAttrData);
                }
                else { ++nerror; }

                strAttrData = getChildElementValue(parent, "costate_ex");
                if (strAttrData) {
                    problemContext.initguess[P_IDX_PEX] = atof(strAttrData);
                }
                else { ++nerror; }

                strAttrData = getChildElementValue(parent, "costate_ey");
                if (strAttrData) {
                    problemContext.initguess[P_IDX_PEY] = atof(strAttrData);
                }
                else { ++nerror; }

                strAttrData = getChildElementValue(parent, "costate_hx");
                if (strAttrData) {
                    problemContext.initguess[P_IDX_PHX] = atof(strAttrData);
                }
                else { ++nerror; }

                strAttrData = getChildElementValue(parent, "costate_hy");
                if (strAttrData) {
                    problemContext.initguess[P_IDX_PHY] = atof(strAttrData);
                }
                else { ++nerror; }

                //    std::cout << "   costate_l  : " << myXerces::cStr(strAttrData).ptr() << std::endl;
                //    problemContext.initguess[5] = atof(myXerces::cStr(strAttrData).ptr());

                strAttrData = getChildElementValue(parent, "costate_mass");
                if (strAttrData) {
                    problemContext.initguess[P_IDX_PM] = atof(strAttrData);
                }
                else { ++nerror; }

                strAttrData = getChildElementValue(parent, "nrev");
                if (strAttrData) {
                    problemContext.setInitguessDuration(atof(strAttrData));
                }
                else { ++nerror; }

#ifdef DEBUG_READING
                std::cout << "   costate_sma: " << problemContext.initguess[0] << std::endl;
                std::cout << "   costate_ex : " << problemContext.initguess[1] << std::endl;
                std::cout << "   costate_ey : " << problemContext.initguess[2] << std::endl;
                std::cout << "   costate_hx : " << problemContext.initguess[3] << std::endl;
                std::cout << "   costate_hy : " << problemContext.initguess[4] << std::endl;
                std::cout << "   costate_mass: " << problemContext.initguess[5] << std::endl;
                std::cout << "   nrev: " << problemContext.initguess_duration << std::endl;
#endif

                nodeListOcp = docElement->getElementsByTagName(myXerces::uStr("terminal_constraint").ptr());
                if (!nodeListOcp->getLength()) {
                    printf("TerminalConstraint type is not defined: missing tag <terminal_constraint>\n");
                    return false;
                }
                parent = reinterpret_cast<DOMElement*>(nodeListOcp->item(0));
                xmlAttrData = parent->getAttribute(myXerces::uStr("type").ptr());
                if (xmlAttrData) {
#ifdef DEBUG_READING
                    std::cout << "   constraint: " << myXerces::cStr(xmlAttrData).ptr() << std::endl;
#endif
                    problemDefinition.constraintType = getConstraintEnum(myXerces::cStr(xmlAttrData).ptr());
                }
                else {
                    ++nerror;
                    printf("Constraint type is not properly defined: {ORBIT, ENERGY, SMA_ECC_INC, SMA_EX_EY_INC, SMA_ECC_INC_RAAN}\n");
                }

                // solver
                nodeListOcp = docElement->getElementsByTagName(myXerces::uStr("solver").ptr());
                if (!nodeListOcp->getLength()) {
                    printf("Solver is not defined: missing tag <solver>\n");
                    return false;
                }
                parent = reinterpret_cast<DOMElement*>(nodeListOcp->item(0));

                strAttrData = getChildElementValue(parent, "type");
                if (strAttrData) {
                    problemContext.solverType = getSolverTypeEnum(strAttrData);
                }
                else {
                    problemContext.solverType = SolverType::SOLVER_NLEQ;
                }

                strAttrData = getChildElementValue(parent, "rtol");
                if (strAttrData) {
#ifdef DEBUG_READING
                    std::cout << "   rtol: " << strAttrData << std::endl;
#endif
                    problemContext.rTol = atof(strAttrData);
                }
                else { problemContext.rTol = 1e-9; }

                strAttrData = getChildElementValue(parent, "max_iter");
                if (strAttrData) {
#ifdef DEBUG_READING
                    std::cout << "   maxiter: " << strAttrData << std::endl;
#endif
                    problemContext.maxIter = static_cast<int>(atof(strAttrData));
                }
                else { problemContext.maxIter = 50; }

                strAttrData = getChildElementValue(parent, "verbose");
                if (strAttrData) {
#ifdef DEBUG_READING
                    std::cout << "   verbose: " << strAttrData << std::endl;
#endif
                    problemContext.verbose = static_cast<int>(atof(strAttrData));
                }
                else { problemContext.verbose = 0; }


                // continuation
                nodeListOcp = docElement->getElementsByTagName(myXerces::uStr("continuation").ptr());
                if (nodeListOcp && nodeListOcp->getLength()) {
                    parent = reinterpret_cast<DOMElement*>(nodeListOcp->item(0));

                    strAttrData = getChildElementValue(parent, "variable");
                    if (strAttrData) {
                        problemContext.continuation.variable = getContinuationVariableEnum(strAttrData);
                        problemContext.continuation.enable = problemContext.continuation.variable != ContinuationVariable::NOT_SET;
                    }

                    strAttrData = getChildElementValue(parent, "initial_value");
                    if (strAttrData) {
#ifdef DEBUG_READING
                        std::cout << "   continuation.initial_value: " << strAttrData << std::endl;
#endif
                        problemContext.continuation.initial_value = atof(strAttrData);
                    }

                    strAttrData = getChildElementValue(parent, "tol");
                    if (strAttrData) {
#ifdef DEBUG_READING
                        std::cout << "   continuation.tol: " << strAttrData << std::endl;
#endif
                        problemContext.continuation.tol = atof(strAttrData);
                    }
                    else { problemContext.continuation.tol = 1e-6; }

                    strAttrData = getChildElementValue(parent, "max_iter");
                    if (strAttrData) {
#ifdef DEBUG_READING
                        std::cout << "   continuation.max_iter: " << strAttrData << std::endl;
#endif
                        problemContext.continuation.max_iter = (int)atof(strAttrData);
                    }
                    else { problemContext.continuation.max_iter = 20; }

                    strAttrData = getChildElementValue(parent, "min_step");
                    if (strAttrData) {
#ifdef DEBUG_READING
                        std::cout << "   continuation.min_step: " << strAttrData << std::endl;
#endif
                        problemContext.continuation.min_step = atof(strAttrData);
                    }
                    else { problemContext.continuation.min_step = 0.1; }

                    strAttrData = getChildElementValue(parent, "max_step");
                    if (strAttrData) {
#ifdef DEBUG_READING
                        std::cout << "   continuation.max_step: " << strAttrData << std::endl;
#endif
                        problemContext.continuation.max_step = atof(strAttrData);
                    }
                    else { problemContext.continuation.max_step = 0.5; }

                    strAttrData = getChildElementValue(parent, "report");
                    if (strAttrData) {
#ifdef DEBUG_READING
                        std::cout << "   continuation.report: " << strAttrData << std::endl;
#endif
                        problemContext.continuation.report_name = std::string(myXerces::cStr(strAttrData).ptr());
                    }
                }

                if (nerror > 0) {
                    printf("An error occured when reading ocp\n");
                }
            }

            XMLCh* xOrbitTagStr = XMLString::transcode("orbit");
            DOMNodeList* nodeListOrbit = docElement->getElementsByTagName(xOrbitTagStr);
            if (nodeListOrbit->getLength() > 0) {
                DOMNode* curNode = nodeListOrbit->item(0);
                DOMElement* parent = reinterpret_cast<DOMElement*>(curNode);

                const XMLCh* strAttrData = parent->getAttribute(myXerces::uStr("type").ptr());
                if (!strAttrData) {
                    printf("Initial Orbit type is not defined {CARTESIAN, EQUINOCTIAL, KEPLERIAN}\n");
                }
                else {
                    DYNAMICALMODEL_TYPE orbitParameterType = getOrbitEnum(myXerces::cStr(strAttrData).ptr());
                    if (orbitParameterType == DYNAMICALMODEL_TYPE::D_NOT_SET) {
                        // error
                        char orbitParameterTypeStr[255];
                        strncpy(orbitParameterTypeStr, myXerces::cStr(strAttrData).ptr(), 255);
                        printf("Initial Orbit type is not properly defined: '%s' {CARTESIAN, EQUINOCTIAL, KEPLERIAN}\n", orbitParameterTypeStr);
                    }

                    std::array<double, 6> orbit;
                    int nerror = getOrbitFromElements(parent, orbitParameterType, orbit, problemDefinition, true);
                    if (nerror > 0) {
                        printf("An error occured when reading orbit (%d)\n", nerror);
                    }
                    else {
                        if (orbitParameterType == D_EQUINOCTIAL) {
                            problemDefinition.state0 = State(orbit, problemDefinition.spacecraft.mass);
                        }
                        else if (orbitParameterType == D_KEPLERIAN) {
                            problemDefinition.state0 = State(orbit[0], orbit[1], orbit[2], orbit[3], orbit[4], orbit[5],
                                NULL, DEFAULT_MU);
                            problemDefinition.state0.mass = problemDefinition.spacecraft.mass;

                        }
                        else if (orbitParameterType == D_CARTESIAN) {
                            // TODO
                        }
                    }
                }
            }

            XMLCh* xTargetTagStr = XMLString::transcode("target");
            DOMNodeList* nodeListTarget = docElement->getElementsByTagName(xTargetTagStr);
            if (nodeListTarget->getLength() > 0) {
                DOMNode* curNode = nodeListTarget->item(0);
                DOMElement* parent = (DOMElement*)curNode;

                const XMLCh* strAttrData = parent->getAttribute(myXerces::uStr("type").ptr());
                if (!strAttrData) {
                    printf("Target/type type is not defined {CARTESIAN, EQUINOCTIAL, KEPLERIAN}\n");
                }
                else {
                    DYNAMICALMODEL_TYPE orbitParameterType = getOrbitEnum(myXerces::cStr(strAttrData).ptr());
                    if (orbitParameterType == DYNAMICALMODEL_TYPE::D_NOT_SET) {
                        // error
                        char orbitParameterTypeStr[255];
                        strncpy(orbitParameterTypeStr, myXerces::cStr(strAttrData).ptr(), 255);
                        printf("Target/type type is not properly defined: %s {CARTESIAN, EQUINOCTIAL, KEPLERIAN}\n", orbitParameterTypeStr);
                    }

                    std::array<double, 6> orbit;
                    int nerror = getOrbitFromElements(parent, orbitParameterType, orbit, problemDefinition, false);
                    if (nerror > 0) {
                        printf("An error occured when reading target (%d)\n", nerror);
                    }
                    else {
                        if (orbitParameterType == D_EQUINOCTIAL) {
                            problemDefinition.statef = State(orbit, 0);
                        }
                        else if (orbitParameterType == D_KEPLERIAN) {
                            problemDefinition.statef = State(orbit[0], orbit[1], orbit[2], orbit[3], orbit[4], orbit[5],
                                NULL, DEFAULT_MU);
                        }
                        else if (orbitParameterType == D_CARTESIAN) {
                            // TODO
                        }
                    }
                }
            }

            XMLCh* xOutputTagStr = XMLString::transcode("output");
            DOMNodeList* nodeListOutput = docElement->getElementsByTagName(xOutputTagStr);
            if (nodeListOutput->getLength() > 0) {
                DOMElement* parent = reinterpret_cast<DOMElement*>(nodeListOutput->item(0));

                nodeListOutput = parent->getElementsByTagName(myXerces::uStr("ephemerides").ptr());
                if (nodeListOutput->getLength() > 0) {
                    DOMElement* parentTag = reinterpret_cast<DOMElement*>(nodeListOutput->item(0));
                    const XMLCh* xmlAttrData = parentTag->getAttribute(myXerces::uStr("filename").ptr());
                    if (xmlAttrData) {
                        problemContext.outputFilename = std::string(myXerces::cStr(xmlAttrData).ptr());
                        std::cout << "   filename: " << problemContext.outputFilename << std::endl;
                    }
                    else {
                        problemContext.outputFilename = "";
                        printf("No output file defined!\n");
                    }

                    // (optionnal)
                    const char* strAttrData = getChildElementValue(parent, "format");
                    if (strAttrData) {
                        set_format(strAttrData, problemContext);
                    }

                    // (optionnal)
                    strAttrData = getChildElementValue(parent, "density");
                    if (strAttrData) {
                        problemContext.outputNbOfPoints = static_cast<int>(atof(strAttrData));
                    }

                    // (optionnal)
                    strAttrData = getChildElementValue(parent, "print_costate");
                    if (strAttrData) {
                        std::cout << "   print_costate: " << strAttrData << std::endl;
                        problemContext.outputCostates = StringToBoolean(strAttrData);
                    }
                    else {
                        problemContext.outputCostates = true;
                    }
                }

                // output report
                nodeListOutput = parent->getElementsByTagName(myXerces::uStr("report").ptr());
                if (nodeListOutput->getLength() > 0) {
                    DOMElement* parentTag = (DOMElement*)nodeListOutput->item(0);
                    const XMLCh* strAttrData = parentTag->getAttribute(myXerces::uStr("filename").ptr());
                    if (strAttrData) {
                        problemContext.reportFilename = std::string(myXerces::cStr(strAttrData).ptr());
                        std::cout << "   report  : " << problemContext.reportFilename << std::endl;
                    }
                    else {
                        problemContext.reportFilename = "";
                        printf("No report file defined!\n");
                    }
                }
            }

            errorsOccured = nerror > 0;
        }
        catch (const DOMXPathException& e) {
            XERCES_STD_QUALIFIER cerr << "An error occurred during processing of the XPath expression. Msg is:"
                << XERCES_STD_QUALIFIER endl
                << StrX(e.getMessage()) << XERCES_STD_QUALIFIER endl;
            errorsOccured = true;
        }
        catch (const DOMException& e) {
            XERCES_STD_QUALIFIER cerr << "An error occurred during processing of the XPath expression. Msg is:"
                << XERCES_STD_QUALIFIER endl
                << StrX(e.getMessage()) << XERCES_STD_QUALIFIER endl;
            errorsOccured = true;
        }
    }

    //  Delete the parser itself.  Must be done prior to calling Terminate, below.
    delete parser;

    // And call the termination method
    XMLPlatformUtils::Terminate();

    return !errorsOccured;
}
