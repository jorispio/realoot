// $Id$
// ---------------------------------------------------------------------------
/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef SRC_UTILS_XML_DOMTREEERRORREPORTER_HPP_
#define SRC_UTILS_XML_DOMTREEERRORREPORTER_HPP_

#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/sax/ErrorHandler.hpp>

#if defined(XERCES_NEW_IOSTREAMS)
    #include <iostream>
#else
    #include <iostream.h>
#endif


XERCES_CPP_NAMESPACE_USE


class DOMTreeErrorReporter : public ErrorHandler {
 private:
    //  fSawErrors
    //      This is set if we get any errors, and is queryable via a getter
    //      method. Its used by the main code to suppress output if there are
    //      errors.
    bool    fSawErrors;
    bool    fSawWarnings;

 public:
    DOMTreeErrorReporter() :
       fSawErrors(false), fSawWarnings(false)  {
    }

    ~DOMTreeErrorReporter() {
    }

    //  Implementation of the error handler interface
    void warning(const SAXParseException& toCatch);
    void error(const SAXParseException& toCatch);
    void fatalError(const SAXParseException& toCatch);
    void resetErrors();

    bool getSawErrors() const {
        return fSawErrors;
    }

    bool getSawWarnings() const {
        return fSawWarnings;
    }
};


// ---------------------------------------------------------------------------
//  This is a simple class that lets us do easy (though not terribly efficient)
//  trancoding of XMLCh data to local code page for display.
// ---------------------------------------------------------------------------
class StrX {
 public:
    explicit StrX(const XMLCh* const toTranscode) {
        // Call the private transcoding method
        fLocalForm = XMLString::transcode(toTranscode);
    }

    ~StrX() {
        XMLString::release(&fLocalForm);
    }

    const char* localForm() const {
        return fLocalForm;
    }

 private:
    // This is the local code page form of the string.
    char*   fLocalForm;
};

inline XERCES_STD_QUALIFIER ostream& operator<<(XERCES_STD_QUALIFIER ostream& target, const StrX& toDump) {
    target << toDump.localForm();
    return target;
}

#endif  // SRC_UTILS_XML_DOMTREEERRORREPORTER_HPP_
