// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef UTILS_STRING_SML_HPP
#define UTILS_STRING_SML_HPP
// ---------------------------------------------------------------------------
#include <assert.h>

#include <iostream>

#include <boost/shared_ptr.hpp>

#include <xercesc/util/PlatformUtils.hpp>       // Initialize, Terminate
#include <xercesc/util/XMLString.hpp>           // transcode
#include <xercesc/dom/DOM.hpp>                  // DOMxxx

// ---------------------------------------------------------------------------
template< class CharType >
class ZeroTerminatingStr {
 private:
    boost::shared_ptr< CharType >   myArray;

 public:
    ZeroTerminatingStr(CharType const* s, void (*deleter)( CharType* ))
        : myArray( const_cast< CharType* >( s ), deleter ) { assert( deleter != 0 ); }

    CharType const* ptr() const { return myArray.get(); }
};

namespace myXerces {
typedef ::XMLCh     Utf16Char;

inline void dispose(Utf16Char* p) { xercesc::XMLString::release(&p); }
inline void dispose(char* p) { xercesc::XMLString::release(&p); }

inline ZeroTerminatingStr< Utf16Char > uStr(char const* s) {
    return ZeroTerminatingStr< Utf16Char >(xercesc::XMLString::transcode(s), &dispose);
}

inline ZeroTerminatingStr< char > cStr(Utf16Char const* s) {
    return ZeroTerminatingStr< char >(xercesc::XMLString::transcode(s), &dispose);
}

struct Lib {
    Lib()  { xercesc::XMLPlatformUtils::Initialize(); }
    ~Lib() { xercesc::XMLPlatformUtils::Terminate(); }
};
}   // namespace myXerces

// ---------------------------------------------------------------------------
#endif  // UTILS_STRING_SML_HPP
