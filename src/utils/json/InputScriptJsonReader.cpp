// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2020 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "InputScriptJsonReader.hpp"

#include <string>
#include <fstream>
#include <streambuf>

#include "UtilsEnum.hpp"

 // ---------------------------------------------------------------------------
 // DEFAULT VALUES
#define DEFAULT_MU         EARTH_MU
#define DEFAULT_RADIUS     EARTH_RADIUS

//#define DEBUG_READING

// for convenience
using json = nlohmann::json;

std::string to_string(json var, const std::string default_value) {
    if (!var.is_null()) {
        return var.get<std::string>();
    }
    return default_value;
}

double to_double(json var, double default_value) {
    if (!var.is_null()) {
        return var.get<double>();
    }
    return default_value;
}

int to_int(json var, int default_value) {
    if (!var.is_null()) {
        return var.get<int>();
    }
    return default_value;
}

bool to_boolean(json var, const bool default_value) {
    if (!var.is_null()) {
        return var.get<bool>();
    }
    return default_value;
}

// -------------------------------------------------------------------------
InputScriptJsonReader::InputScriptJsonReader() : AbstractInputScriptReader() {

}
// -------------------------------------------------------------------------
InputScriptJsonReader::~InputScriptJsonReader() {

}
// -------------------------------------------------------------------------
void InputScriptJsonReader::Init() {

}
// -------------------------------------------------------------------------
void InputScriptJsonReader::Close() {

}
// -------------------------------------------------------------------------
int InputScriptJsonReader::getOrbitFromElements(json parent, DYNAMICALMODEL_TYPE orbitParameterType, std::array<double, 6>& orbit, LtProblemDefinition& problemDefinition, bool allowLongitude) {
    int nerror = 0;

    if (orbitParameterType == D_KEPLERIAN) {
        bool hasSmaDefined = !parent["sma"].is_null();
        if (hasSmaDefined) {
            orbit[0] = to_double(parent["sma"], -1);
            if (orbit[0] < 0) {
                std::cout << "SMA cannot be < 0" << std::endl;
                ++nerror;
            }

            if (!parent["ecc"].is_null()) {
                orbit[1] = to_double(parent["ecc"], -1);
                if ((orbit[1] < 0) || (orbit[1] > 1)) {
                    std::cout << "Eccentricity must be in [0, 1[" << std::endl;
                    ++nerror;
                }
            }
            else {
                std::cout << "Missing value for eccentricity" << std::endl;
                ++nerror; 
            }
        }
        else {
            double ha = 0, hp = 0; 
            if (!parent["ha"].is_null()) {
                ha = to_double(parent["ha"], -1);
                if (ha < 0) ++nerror;
            }
            else { ++nerror; }

            if (!parent["hp"].is_null()) {
                hp = to_double(parent["hp"], -1);
                if (hp < 0) ++nerror;
            }
            else { ++nerror; }

            if (ha < hp) {
                std::cout << "The condition Ha > Hp is not meet" << std::endl;
                ++nerror;
            }

            orbit[0] = problemDefinition.radius + (ha + hp) / 2.;
            orbit[1] = (ha - hp) / (2 * orbit[0]);
        }

        if (!parent["inc"].is_null()) {
            orbit[2] = to_double(parent["inc"], 0.0) * DEG2RAD;
        }
        else {
            std::cout << "Missing value for inclination" << std::endl; 
            ++nerror; 
        }

        if (!parent["aop"].is_null()) {
            orbit[3] = to_double(parent["aop"], 0.0) * DEG2RAD;
        }
        else { 
            std::cout << "Missing value for AoP" << std::endl; 
            ++nerror; 
        }

        if (!parent["raan"].is_null()) {
            orbit[4] = to_double(parent["raan"], 0.0) * DEG2RAD;
        }
        else { 
            std::cout << "Missing value for RAAN" << std::endl; 
            ++nerror; 
        }

        if (allowLongitude) {
            nerror += readOrbitalPosition(parent, orbit);
        }

#ifdef DEBUG_READING
        printf("   orbit (KEPLERIAN: sma=%f, ecc=%f, inc=%f, aop=%f, raan=%f\n", orbit[0], orbit[1], orbit[2], orbit[3], orbit[4]);
#endif

    }
    else if (orbitParameterType == D_EQUINOCTIAL) {

        if (!parent["sma"].is_null()) {
            orbit[0] = to_double(parent["sma"], -1);
            if (orbit[0] < 0) ++nerror;
        }
        else { ++nerror; }

        if (!parent["ex"].is_null()) {
            orbit[1] = to_double(parent["ex"], 0);
        }
        else { ++nerror; }

        if (!parent["ey"].is_null()) {
            orbit[2] = to_double(parent["ey"], 0);
        }
        else { ++nerror; }

        if (!parent["hx"].is_null()) {
            orbit[3] = to_double(parent["hx"], 0);
        }
        else { ++nerror; }

        if (!parent["hy"].is_null()) {
            orbit[4] = to_double(parent["hy"], 0);
        }
        else { ++nerror; }

        if (allowLongitude) {
            nerror += readOrbitalPosition(parent, orbit);
        }

    }
    else if ((orbitParameterType == D_CARTESIAN) && (allowLongitude)) {

        if (!parent["x"].is_null()) {
            orbit[0] = to_double(parent["x"], 0);
        }
        else { ++nerror; }

        if (!parent["y"].is_null()) {
            orbit[1] = to_double(parent["y"], 0);
        }
        else { ++nerror; }

        if (!parent["z"].is_null()) {
            orbit[2] = to_double(parent["z"], 0);
        }
        else { ++nerror; }

        if (!parent["vx"].is_null()) {
            orbit[3] = to_double(parent["vx"], 0);
        }
        else { ++nerror; }

        if (!parent["vy"].is_null()) {
            orbit[4] = to_double(parent["vy"], 0);
        }
        else { ++nerror; }

        if (!parent["vz"].is_null()) {
            orbit[5] = to_double(parent["vz"], 0);
        }
        else { ++nerror; }

        if (((orbit[0] == 0) && (orbit[1] == 0) && (orbit[2] == 0)) || ((orbit[3] == 0) && (orbit[4] == 0) && (orbit[5] == 0))) {
            ++nerror;
        }
    }
    else {
        nerror = 100;
    }

    return nerror;
}
/* ------------------------------------------------------------------------- */
int InputScriptJsonReader::readOrbitalPosition(json parent, std::array<double, 6>& orbit) {
    int nerror = 0;
    if (!parent["L"].is_null()) {
        orbit[5] = to_double(parent["L"], 0) * DEG2RAD;
    }
    else {             
        if (!parent["mean_anomaly"].is_null()) {
            orbit[5] = to_double(parent["mean_anomaly"], 0) * DEG2RAD;
        }
        else 
        {
            std::cout << "Missing value for longitude" << std::endl; 
            ++nerror; 
        }
    }
    return nerror;
}
// -------------------------------------------------------------------------
bool InputScriptJsonReader::readJson(std::string gInputScriptFile, LtProblemDefinition& problemDefinition, LtProblemContext& problemContext) {
    if (gInputScriptFile.empty()) {
        return false;
    }

    Default(problemDefinition, problemContext);

    std::ifstream t(gInputScriptFile);
    std::string gInputScript((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

    try {
        auto j3 = json::parse(gInputScript);
    }
    catch (json::parse_error e) {
        std::cout << e.what() << std::endl;
        return false;
    }

    auto j3 = json::parse(gInputScript);

    int nerror = 0;
    nerror += set_mode(j3["mode"].get<std::string>().c_str(), problemContext);
    
    auto jspacecraft = j3["spacecraft"];
    if (!jspacecraft.is_null()) {
        problemDefinition.spacecraft.name = to_string(jspacecraft["name"], "REALOOT");
        problemDefinition.spacecraft.propulsion.Fth = to_double(jspacecraft["thrust"], 0.0);
        problemDefinition.spacecraft.propulsion.g0Isp = g0 * to_double(jspacecraft["Isp"], 0.0);
        problemDefinition.spacecraft.propulsion.constantAcceleration = to_boolean(jspacecraft["constantAcc"], false);
        problemDefinition.spacecraft.mass = to_double(jspacecraft["mass"], 0.0);
        problemDefinition.state0.mass = problemDefinition.spacecraft.mass;
    }
    else {
        ++nerror;
        printf("Spacecraft is not defined!\n");
    }

    auto jdynamics = j3["dynamics"];
    if (!jdynamics.is_null()) {
        problemDefinition.mu = to_double(jdynamics["mu"], EARTH_MU);
        problemDefinition.cj2 = to_double(jdynamics["j2"], 0);
        problemDefinition.radius = to_double(jdynamics["radius"], EARTH_RADIUS);
    }

    auto jeclipse = j3["eclipse"];
    if (!jeclipse.is_null()) {
        auto jocculting_body = jeclipse["occulting_body"];
        if (!jocculting_body.is_null()) {
            int neclerr = 0;

            problemDefinition.eclipse.name = to_string(jocculting_body["name"], "OccultingBody");
            problemDefinition.eclipse.occ_radius = to_double(jocculting_body["radius"], 0.);
            if (problemDefinition.eclipse.occ_radius <= 0) {
                ++nerror;
                ++neclerr;
                printf("Eclipse occulting body radius shall be strictly positive!\n");
            }

            problemDefinition.eclipse.occ_orbit_epoch = to_double(jocculting_body["epoch"], 0.);

            auto occulting_body_orbit = jocculting_body["orbit"];
            if (!occulting_body_orbit.is_null()) {                

                DYNAMICALMODEL_TYPE orbitParameterType = getOrbitEnum(to_string(occulting_body_orbit["type"], "KEPLERIAN").c_str());
                if (orbitParameterType == DYNAMICALMODEL_TYPE::D_NOT_SET) {
                    // error
                    char orbitParameterTypeStr[255];
                    strncpy(orbitParameterTypeStr, to_string(occulting_body_orbit["type"]).c_str(), 255);
                    printf("Occulting body orbit type is not properly defined: '%s' {CARTESIAN, EQUINOCTIAL, KEPLERIAN}\n", orbitParameterTypeStr);
                }

                problemDefinition.eclipse.occ_orbit_type = orbitParameterType;
                neclerr += getOrbitFromElements(occulting_body_orbit, orbitParameterType, problemDefinition.eclipse.occ_orbit, problemDefinition, true);
                problemDefinition.eclipse.active = neclerr == 0;
            }
            else {
                ++nerror;
                printf("Eclipse is used without occulting body orbit! The feature will be disabled.\n");
            }
        }
    }

    auto jocp = j3["ocp"];
    if (!jocp.is_null()) {
        set_objective(to_string(jocp["objective"], "MIN_TIME").c_str(), problemDefinition);
    
        auto jinitguess = jocp["initguess"];
        if (!jinitguess.is_null()) {
            problemContext.initGuessMultiStart = to_boolean(jinitguess["allow_multi_start"], false);

            problemContext.initGuessMultiStartMaxAttempts = to_int(jinitguess["max_attempts"], 1);

            problemContext.initguess[P_IDX_PSMA] = to_double(jinitguess["costate_sma"], 1.);
            problemContext.initguess[P_IDX_PEX] = to_double(jinitguess["costate_ex"], 1e-12);
            problemContext.initguess[P_IDX_PEY] = to_double(jinitguess["costate_ey"], 1e-12);
            problemContext.initguess[P_IDX_PHX] = to_double(jinitguess["costate_hx"], 1e-12);
            problemContext.initguess[P_IDX_PHY] = to_double(jinitguess["costate_hy"], 1e-12);
            problemContext.initguess[P_IDX_PM] = to_double(jinitguess["costate_mass"], -0.1);
            problemContext.setInitguessDuration(to_double(jinitguess["nrev"], 1.));
#ifdef DEBUG_READING
            std::cout << "   costate_sma: " << problemContext.initguess[0] << std::endl;
            std::cout << "   costate_ex : " << problemContext.initguess[1] << std::endl;
            std::cout << "   costate_ey : " << problemContext.initguess[2] << std::endl;
            std::cout << "   costate_hx : " << problemContext.initguess[3] << std::endl;
            std::cout << "   costate_hy : " << problemContext.initguess[4] << std::endl;
            std::cout << "   costate_mass: " << problemContext.initguess[5] << std::endl;
            std::cout << "   nrev: " << problemContext.initguess_duration << std::endl;
#endif
        }

        auto jterminal_constraint = jocp["terminal_constraint"];
        if (!jterminal_constraint.is_null()) {
            if (!jterminal_constraint["type"].is_null()) {
                problemDefinition.constraintType = getConstraintEnum(to_string(jterminal_constraint["type"], "ORBIT").c_str());
            }
            else {
                ++nerror;
                printf("Constraint type is not properly defined: {ORBIT, ENERGY, SMA_ECC_INC, SMA_EX_EY_INC, SMA_ECC_INC_RAAN}\n");
            }
        }
        else {
            ++nerror;
            printf("Terminal constraint is not defined!\n");
        }

        auto jsolver = jocp["solver"];
        if (!jsolver.is_null()) {
            problemContext.rTol = to_double(jsolver["rtol"], 1e-9);
            problemContext.maxIter = to_int(jsolver["max_iter"], 50);
            problemContext.verbose = to_int(jsolver["verbose"], 0);
            problemContext.solverType = getSolverTypeEnum(to_string(jsolver["type"], "NLEQ").c_str());
        }

        auto jcontinuation = jocp["continuation"];
        if (!jcontinuation.is_null()) {            
            if (!jcontinuation["variable"].is_null()) {                
                problemContext.continuation.variable = getContinuationVariableEnum(to_string(jcontinuation["variable"], "NOT_SET").c_str());
                problemContext.continuation.enable = problemContext.continuation.variable != ContinuationVariable::NOT_SET;
            }

            problemContext.continuation.tol = to_int(jcontinuation["tol"], 1e-6);
            problemContext.continuation.max_iter = to_int(jcontinuation["max_iter"], 20);
            problemContext.continuation.max_step = to_double(jcontinuation["max_step"], 0.5);
            problemContext.continuation.min_step = to_double(jcontinuation["min_step"], 0.1);            

            problemContext.continuation.initial_value = to_double(jcontinuation["initial_value"], 0.);
            if (!jcontinuation["report"].is_null()) {
                problemContext.continuation.report_name = to_string(jcontinuation["report"], "");
            }
        }
    }

    auto jorbit = j3["orbit"];
    if (!jorbit.is_null()) {
        DYNAMICALMODEL_TYPE orbitParameterType = getOrbitEnum(to_string(jorbit["type"], "KEPLERIAN").c_str());
        if (orbitParameterType == DYNAMICALMODEL_TYPE::D_NOT_SET) {
            // error
            char orbitParameterTypeStr[255];
            strncpy(orbitParameterTypeStr, to_string(jorbit["type"]).c_str(), 255);
            printf("Initial Orbit type is not properly defined: '%s' {CARTESIAN, EQUINOCTIAL, KEPLERIAN}\n", orbitParameterTypeStr);
        }

        std::array<double, 6> orbit;
        int nerror = getOrbitFromElements(jorbit, orbitParameterType, orbit, problemDefinition, true);
        if (nerror > 0) {
            printf("An error occured when reading orbit (%d)\n", nerror);
            return false;
        }
        else {
            if (orbitParameterType == D_EQUINOCTIAL) {
                problemDefinition.state0 = State(orbit, problemDefinition.spacecraft.mass);
            }
            else if (orbitParameterType == D_KEPLERIAN) {
                problemDefinition.state0 = State(orbit[0], orbit[1], orbit[2], orbit[3], orbit[4], orbit[5],
                    NULL, DEFAULT_MU);
                problemDefinition.state0.mass = problemDefinition.spacecraft.mass;

            }
            else if (orbitParameterType == D_CARTESIAN) {
                // TODO
            }
        }
    }

    auto jtarget = j3["target"];
    if (!jtarget.is_null()) {
        DYNAMICALMODEL_TYPE orbitParameterType = getOrbitEnum(to_string(jtarget["type"], "").c_str());
        if (orbitParameterType == DYNAMICALMODEL_TYPE::D_NOT_SET) {
            // error
            char orbitParameterTypeStr[255];
            strncpy(orbitParameterTypeStr, to_string(jtarget["type"]).c_str(), 255);
            printf("Initial Orbit type is not properly defined: '%s' {CARTESIAN, EQUINOCTIAL, KEPLERIAN}\n", orbitParameterTypeStr);
        }

        std::array<double, 6> orbit;
        int nerror = getOrbitFromElements(jtarget, orbitParameterType, orbit, problemDefinition, true);
        if (nerror > 0) {
            printf("An error occured when reading target (%d)\n", nerror);
            return false;
        }
        else {
            if (orbitParameterType == D_EQUINOCTIAL) {
                problemDefinition.statef = State(orbit, 0);
            }
            else if (orbitParameterType == D_KEPLERIAN) {
                problemDefinition.statef = State(orbit[0], orbit[1], orbit[2], orbit[3], orbit[4], orbit[5],
                    NULL, DEFAULT_MU);
            }
            else if (orbitParameterType == D_CARTESIAN) {
                // TODO
            }
        }
    }

    auto joutput = j3["output"];
    if (!joutput.is_null()) {
        auto jephemerides = joutput["ephemerides"];
        if (!jephemerides.is_null()) {
            problemContext.outputFilename = to_string(jephemerides["filename"], "");

            auto jformat = jephemerides["format"];
            if (!jformat.is_null()) {
                set_format(to_string(jformat, "").c_str(), problemContext);
            }

            auto jdensity = jephemerides["density"];
            if (!jdensity.is_null()) {
                problemContext.outputNbOfPoints = to_int(jdensity, 1);
            }

            auto jprintcostate = jephemerides["print_costate"];
            problemContext.outputCostates = to_boolean(jprintcostate, true);
        }

        auto jreport = joutput["report"];
        if (!jreport.is_null()) {
            problemContext.reportFilename = to_string(jreport["filename"], "");
        }
    }

    return !(nerror > 0);
}
