// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2020 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __INPUT_SCRIPT_JSON_READER_HPP__
#define __INPUT_SCRIPT_JSON_READER_HPP__

//#include "ode/TDynamicsAveragedEquinoctial.hpp"
#include "Enumerations.hpp"
#include "LtSpacecraftData.hpp"
#include "LtProblemContext.hpp"
#include "LtProblemDefinition.hpp"
#include "StateNumericalPropagator.hpp"
#include "xml/UtilsStringXml.hpp"
#include "AbstractInputScriptReader.hpp"

#include <nlohmann/single_include/json.hpp>

class InputScriptJsonReader : public AbstractInputScriptReader {
 private:
    int getOrbitFromElements(nlohmann::json parent, DYNAMICALMODEL_TYPE orbitParameterType, std::array<double, 6>& orbit, LtProblemDefinition& problemDefinition, bool allowLongitude);

    int readOrbitalPosition(nlohmann::json parent, std::array<double, 6>& orbit);

 public:
    InputScriptJsonReader();
    ~InputScriptJsonReader();
    void Init();
    void Close();

    bool readJson(std::string gInputScript, LtProblemDefinition& problemDefinition, LtProblemContext& problemContext);
};

#endif  // __INPUT_SCRIPT_JSON_READER_HPP__
