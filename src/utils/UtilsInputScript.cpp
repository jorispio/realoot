// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UtilsInputScript.hpp"


// -------------------------------------------------------------------------
InputScriptReader::InputScriptReader() {

}
// -------------------------------------------------------------------------
InputScriptReader::~InputScriptReader() {
}
// -------------------------------------------------------------------------
void InputScriptReader::Default(LtProblemDefinition &problemDefinition, LtProblemContext &problemContext) {
    problemDefinition.objtype = OBJECTIVE_TYPE::C_TIME;
    problemDefinition.constraintType = TerminalConstraintType::C_ORBIT;
    problemDefinition.dynamicalFormulation = DYNAMICALMODEL_TYPE::D_EQUINOCTIAL_AVERAGING;
    problemDefinition.spacecraft.name = "";
    problemDefinition.eclipse.active = false;

    // set a default value for optional parameters
    problemContext.reportFilename = "";
    problemContext.outputFilename = "";
    problemContext.outputFormat = DYNAMICALMODEL_TYPE::D_EQUINOCTIAL;
    problemContext.outputNbOfPoints = -1;
}

/* ------------------------------------------------------------------------- */
bool InputScriptReader::readXml(std::string gInputScript, LtProblemDefinition &problemDefinition, LtProblemContext &problemContext) {
    InputScriptXmlReader* reader = new InputScriptXmlReader();
    reader->Init();
    bool isOk = reader->readXml(gInputScript, problemDefinition, problemContext);
    reader->Close();    
    delete reader;
    return isOk;
}

// -------------------------------------------------------------------------
bool InputScriptReader::readJson(std::string gInputScript, LtProblemDefinition &problemDefinition, LtProblemContext &problemContext) {
    InputScriptJsonReader* reader = new InputScriptJsonReader();
    bool isOk = reader->readJson(gInputScript, problemDefinition, problemContext);
    delete reader;
    return isOk;
}
