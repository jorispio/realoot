// $Id$
/* ---------------------------------------------------------------------------
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "EphemerisReport.hpp"

#include "ocp/OptimalControlBase.hpp"
#include "HelioLib/physics/EclipseCylindrical.hpp"

// ---------------------------------------------------------------------------
EphemerisReport::EphemerisReport(StateNumericalPropagator *propagator, LtProblemDefinition& problemDefinition, LtProblemContext& problemContext)
 : propagator(propagator), problemDefinition(problemDefinition), problemContext(problemContext) {

    // output format: Cartesian, Keplerian, Equinoctial
    trajectoryOutputFormat = problemContext.outputFormat;
}

// ---------------------------------------------------------------------------
/** @desc Print the solution trajectory into the file filename
 * @input fid  file identifier
 * @input printCostates
 * @input a_m
 * @input a_s
 * @input a_kg
 * @input outputNbOfPoints
 */
// ---------------------------------------------------------------------------
bool EphemerisReport::printSolutionToFile(const char* outputFilename,
                                                    bool printCostates,
                                                    double a_m, double a_s, double a_kg, double a_longitude) {
    if ((trajectoryOutputFormat != D_CARTESIAN) && (trajectoryOutputFormat != D_EQUINOCTIAL) && (trajectoryOutputFormat != D_KEPLERIAN) && (trajectoryOutputFormat != D_EQUINOCTIAL_AVERAGING)) {
        return false;
    }

    FILE* fid = fopen(outputFilename, "w");
    if (!fid) {
        return false;
    }

    // adapt header to desired output format
    if (trajectoryOutputFormat == D_CARTESIAN) {
        fprintf(fid, "%%\n%%     t (MJD)                  X[DU]              Y[DU]             Z[DU]             "
                     "Vx[VU]             Vy[VU]             Vz[VU]             mass[kg]");
    } else if ((trajectoryOutputFormat == D_EQUINOCTIAL) || (trajectoryOutputFormat == D_EQUINOCTIAL_AVERAGING)) {
        fprintf(fid, "%%\n%%     t (MJD)                  p[DU]                ex[-]                 ey[-]             "
                     "    hx[-]                 hy[-]                 L[deg]               mass[kg]");
    } else if (trajectoryOutputFormat == D_KEPLERIAN) {
        fprintf(fid, "%%\n%%     t (MJD)                   sma[m]                ecc[-]             inc[deg]             "
                     "  aop[deg]             raan[deg]                 L[deg]             mass[kg]");
    } else {
        fprintf(fid, "%%\n%%     t (MJD)                  p[DU]                ex[-]                 ey[-]             "
                     "    hx[-]                 hy[-]                 L[rad]               mass[kg]");
    }

    // we cannot convert the costate, so this depends on the dynamical formulation
    DYNAMICALMODEL_TYPE dynamicalFormulation = propagator->getDynamics()->getModelType();
    if (printCostates) {
        if (dynamicalFormulation == D_CARTESIAN) {
            fprintf(
                fid,
                "            lx            ly            lz            lvx            lvy            lvz            lm");
        } else if (dynamicalFormulation == D_EQUINOCTIAL) {
            fprintf(fid, "            lp                lex              ley               lhx                lhy          "
                        "     lL                lm   ");
        } else if (dynamicalFormulation == D_EQUINOCTIAL_AVERAGING) {
            fprintf(fid, "             lsma              lex              ley               lhx                lhy          "
                        "     lL                lm   ");
        } else {
            //
        }
    }

    fprintf(fid, "          |u|");
    TOptimalControlBase* optimalControl = propagator->getOptimalControl();
    if (optimalControl)     {
        fprintf(fid, "          uT          uN          uW          psi(deg)          xi(deg)");
    }

    if (problemDefinition.eclipse.active) {
        fprintf(fid, "          eclipse[bool]");
        fprintf(fid, "          X_OCCB(m)       Y_OCCB(m)       Z_OCCB(m)");
    }
    fprintf(fid, "\n");

    double t_mjd = 0;

    std::vector<real_type> timeOut;
    std::vector<real_type> thetaOut;
    std::vector<S14VECTOR> tableXout;
    propagator->getTrajectoryPoints(timeOut, thetaOut, tableXout);
    
    std::vector<double> uout;
    propagator->getControlProfile(uout);
    
    double constMu = propagator->getDynamics()->getMu();

    Vector3 uTnw = Vector3(0, 0, 0);
    for (int i = 0; i < (int)tableXout.size(); i++) {
        S14VECTOR vecRaw = tableXout.at(i);
        double theta = thetaOut.at(i) * a_longitude;
        S14VECTOR vec = convertPoint(vecRaw, theta, dynamicalFormulation, trajectoryOutputFormat, constMu);

        double psi = 0, xhi = 0;
        if (optimalControl)     {
            try {
                uTnw = optimalControl->getOptimalControlDirection(vecRaw.data());
                psi = atan2(uTnw(1), uTnw(0)) * 180. / M_PI;
                xhi = atan(uTnw(2) / sqrt(uTnw(0)*uTnw(0) + uTnw(1)*uTnw(1))) * 180. / M_PI;
            } catch (...) {
                // silent
                uTnw << 0, 0, 0;
            }
        }

        if (trajectoryOutputFormat == D_CARTESIAN) {
            vec(0) *= a_m;
            vec(1) *= a_m;
            vec(2) *= a_m;
            vec(3) *= a_m / a_s;
            vec(4) *= a_m / a_s;
            vec(5) *= a_m / a_s;
        } else {
            vec(0) *= a_m;
            if (trajectoryOutputFormat == D_KEPLERIAN) {
                vec(2) *= 180. / M_PI;    // inc
                vec(3) *= 180. / M_PI;    // aop
                vec(4) *= 180. / M_PI;    // raan
            } else {
                // EQUINOCTIAL, nothing to do
            }
            vec(5) = thetaOut.at(i) * 180. / M_PI * a_longitude;
        }

        // date
        fprintf(fid, "%17.11f", t_mjd + timeOut.at(i) * a_s / 86400.);  // t

        // state
        fprintf(fid,
                    " %21.15g %21.15g %21.15g %21.15g %21.15g %21.15g",
                    vec(0),
                    vec(1),
                    vec(2),  // R / p, ex, ey
                    vec(3),
                    vec(4),
                    vec(5));  // V / hx, hy, L

        // mass
        fprintf(fid, " %21.15g", vec(6) * a_kg); /* m */

        if (printCostates) {
            // costate
            fprintf(fid,
                " %17.11g %17.11g %17.11g %17.11g %17.11g %17.11g %17.11g",
                vec(7),
                vec(8),
                vec(9),  // lambda_R or lp, lex, ley
                vec(10),
                vec(11),
                vec(12),  // lambda_V or lhx, lhy, lL
                vec(13));  // lambda_m
        }

        double uampl = uout.at(i);
        fprintf(fid, " %12.8f", uampl);  // amplitude of the thrust

        if (optimalControl) {
            fprintf(fid, " %12.8f %12.8f %12.8f", uTnw(0), uTnw(1), uTnw(2));  // thrust direction
            fprintf(fid, " %12.8f %12.8f", psi, xhi);  // thrust angles
        }

        if (problemDefinition.eclipse.active) {
            // recompute the eclipse condition here. This is necessary because in the propagation
            // eclipse condition is only computed in the average model.
            Vector3 rposition = convertPointToCartersianPosition(vecRaw, theta, dynamicalFormulation, constMu) * a_m;
            double relative_epoch_s = t_mjd * 86400. + timeOut.at(i) * a_s;
            EclipseCondition condition = problemDefinition.eclipse.getCondition(relative_epoch_s, rposition);
            fprintf(fid, "  %d", condition != EclipseCondition::NotInEclipse); 

            // print occulting body position
            Vector3 r_cb = problemDefinition.eclipse.getOccultingBodyPosition(relative_epoch_s);
            fprintf(fid, " %12.8f %12.8f %12.8f", r_cb(0), r_cb(1), r_cb(2));
        }

        fprintf(fid, "\n");
    }

    fprintf(fid, "\n%%END\n");
    fclose(fid);

    return true;
}


// ---------------------------------------------------------------------------
S14VECTOR EphemerisReport::convertPoint(S14VECTOR &y, double theta, 
                DYNAMICALMODEL_TYPE dynamicalFormulation, 
                DYNAMICALMODEL_TYPE trajectoryOutputFormat, double constMu) const {
    S14VECTOR vec(14);

    // convert to cartesian coordinates if necessary
    if (dynamicalFormulation == D_EQUINOCTIAL) {
        // if print option is equinoctial
        if (trajectoryOutputFormat == D_EQUINOCTIAL) {
            // nothing specific to do, data are already in equinoctial
            vec << y[0], y[1], y[2], y[3], y[4], y[5], y[6], y[7], y[8], y[9], y[10], y[11], y[12], y[13];
        } else if (trajectoryOutputFormat == D_CARTESIAN) {
            // convert equinoctial elements into cartesian elements
            double p = y[IDX_SMA];
            double ex = y[IDX_EX];
            double ey = y[IDX_EY];
            double hx = y[IDX_HX];
            double hy = y[IDX_HY];
            double l = y[IDX_L];

            double e2 = (ex * ex + ey * ey);
            double a = p / (1 - e2);
            EquinoctialOrbit orbit(
                a, ex, ey, hx, hy, l, MEAN_LONGITUDE,
                (Frame*)(FramesFactory::EME2000),
                GenericDate(), constMu);
            CartesianCoordinates pv = orbit.getPVCoordinates();
            vec << pv.getPosition(), pv.getVelocity(),
                y[IDX_MASS],                                         // mass
                y[7], y[8], y[9], y[10], y[11], y[12], y[13];  // costate

        } else if (trajectoryOutputFormat == D_KEPLERIAN) {
            double eccSq = y[IDX_EX] * y[IDX_EX] + y[IDX_EY] * y[IDX_EY];
            double sma = y[0] / (1 - eccSq);
            double ecc = sqrt(eccSq);
            double inc = 2 * atan(sqrt(y[IDX_HX] * y[IDX_HX] + y[IDX_HY] * y[IDX_HY]));
            double raan = atan2(y[IDX_HY], y[IDX_HX]);
            double pom = atan2(y[IDX_EY], y[IDX_EX]) - raan;
            vec << sma, ecc, inc, pom, raan, y[IDX_L], y[IDX_MASS], /* costate */y[7], y[8], y[9], y[10], y[11], y[12], y[13];

            //
        } else {
            throw LtException("Unknown trajectory format!");
        }
    } else if (dynamicalFormulation == D_EQUINOCTIAL_AVERAGING) {
        // if print option is equinoctial
        if (trajectoryOutputFormat == D_EQUINOCTIAL_AVERAGING) {
            // nothing specific to do, data are already in equinoctial, but sma
            vec << y[0], y[1], y[2], y[3], y[4], theta, y[IDX_MASS], /* costate */y[7], y[8], y[9], y[10], y[11], y[12], y[13];

        } else if (trajectoryOutputFormat == D_EQUINOCTIAL) {
            // nothing specific to do, data are already in equinoctial
            double p = y[0];  // * (1 - y[1] * y[1] - y[2] * y[2]);
            vec << p, y[1], y[2], y[3], y[4], theta, y[IDX_MASS], /* costate */y[7], y[8], y[9], y[10], y[11], y[12], y[13];

        } else if (trajectoryOutputFormat == D_KEPLERIAN) {
            double eccSq = y[IDX_EX] * y[IDX_EX] + y[IDX_EY] * y[IDX_EY];
            double sma = y[IDX_SMA];
            double ecc = sqrt(eccSq);
            double inc = 2. * atan(sqrt(y[IDX_HX] * y[IDX_HX] + y[IDX_HY] * y[IDX_HY]));  // hx=tan(i/2)*cos(W),hy=tan(i/2)*sin(W))
            double raan = atan2(y[IDX_HY], y[IDX_HX]);
            double pom = atan2(y[IDX_EY], y[IDX_EX]) - raan;  // ex=e*cos(w+W), ey=e*sin(w+W)
            vec << sma, ecc, inc, pom, raan, theta, y[IDX_MASS], /* costate */y[7], y[8], y[9], y[10], y[11], y[12], y[13];

        } else if (trajectoryOutputFormat == D_CARTESIAN) {
            // convert equinoctial elements into cartesian elements
            double sma = y[IDX_SMA];
            double ex = y[IDX_EX];
            double ey = y[IDX_EY];
            double hx = y[IDX_HX];
            double hy = y[IDX_HY];
            double l = theta;

            EquinoctialOrbit orbit(
                sma, ex, ey, hx, hy, l, MEAN_LONGITUDE, (Frame*)FramesFactory::EME2000, GenericDate(), constMu);
            CartesianCoordinates pv = orbit.getPVCoordinates();
            vec << pv.getPosition(), pv.getVelocity(),
                y[IDX_MASS],                                         // mass
                y[7], y[8], y[9], y[10], y[11], y[12], y[13];  // costate

        } else {
            throw LtException("Unknown trajectory format!");
        }
    } else {
        throw LtException("Unknown dynamical model!");
    }

    return vec;
}

// ---------------------------------------------------------------------------
Vector3 EphemerisReport::convertPointToCartersianPosition(S14VECTOR &y, double theta, DYNAMICALMODEL_TYPE dynamicalFormulation, double constMu) const {
    S14VECTOR vec = convertPoint(y, theta, dynamicalFormulation, D_CARTESIAN, constMu);
    return vec.segment(0, 3);
}

