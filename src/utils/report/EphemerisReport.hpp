// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __REALOOT_EPHEMERIS_REPORT_HPP__
#define __REALOOT_EPHEMERIS_REPORT_HPP__

#include <vector>

#include "core/DataTypes.hpp"
#include "core/Enumerations.hpp"
#include "core/SizeParameters.hpp"
#include "core/LtProblemContext.hpp"
#include "core/LtProblemDefinition.hpp"
#include "core/LtSpacecraftData.hpp"
#include "dynamics/NumericalPropagator.hpp"
#include "ocp/Scaling.hpp"
#include "StateNumericalPropagator.hpp"

class EphemerisReport {
 private:
    StateNumericalPropagator *propagator;
    LtProblemDefinition problemDefinition;
    LtProblemContext problemContext;

    DYNAMICALMODEL_TYPE trajectoryOutputFormat;

    /** */
    S14VECTOR convertPoint(S14VECTOR &y, double theta, DYNAMICALMODEL_TYPE dynamicalFormulation, DYNAMICALMODEL_TYPE outputFormat, double constMu) const;

    /** */
    Vector3 convertPointToCartersianPosition(S14VECTOR &y, double theta, DYNAMICALMODEL_TYPE dynamicalFormulation, double constMu) const;

 public:
    EphemerisReport(StateNumericalPropagator* propagator, LtProblemDefinition& problemDefinition, LtProblemContext& problemContext);
    ~EphemerisReport() {}

    /** Write ephemeris file. */
    bool printSolutionToFile(const char* outputFilename,
                                                    bool printCostates,
                                                    double a_m, double a_s, double a_kg, double a_longitude);
};

// --------------------------------------------------------------------- 
#endif  // __REALOOT_EPHEMERIS_REPORT_HPP__