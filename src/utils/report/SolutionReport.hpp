// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __REALOOT_SOLUTION_REPORT_HPP__
#define __REALOOT_SOLUTION_REPORT_HPP__

#include <vector>

#include "core/DataTypes.hpp"
#include "core/Enumerations.hpp"
#include "core/SizeParameters.hpp"
#include "core/LtProblemContext.hpp"
#include "core/LtProblemDefinition.hpp"
#include "core/LtProblemSolution.hpp"
#include "core/LtSpacecraftData.hpp"
#include "ocp/Scaling.hpp"


class SolutionReport {
 private:

 public:
    SolutionReport();
    ~SolutionReport() {}

    /** Write ephemeris file. */
    bool printSolutionToFile(LtProblemContext &problemContext, 
                LtProblemDefinition &problemDefinition,
                ProblemScaling &scaling, 
                LtProblemSolution &solution);
};

// --------------------------------------------------------------------- 
#endif  // __REALOOT_SOLUTION_REPORT_HPP__
