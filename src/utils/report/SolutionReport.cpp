// $Id$
/* ---------------------------------------------------------------------------
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SolutionReport.hpp"


/* ------------------------------------------------------------------------- */
SolutionReport::SolutionReport() {

}

/* ------------------------------------------------------------------------- */
bool SolutionReport::printSolutionToFile(LtProblemContext &problemContext, 
                                    LtProblemDefinition &problemDefinition,
                                    ProblemScaling &scaling, 
                                    LtProblemSolution &solution) {
    FILE* fid = fopen(problemContext.reportFilename.c_str(), "w");
    if (fid != NULL) {
        fprintf(fid, "<solution>\n");
        fprintf(fid, "   <objective>MIN_TIME</objective>\n");
        fprintf(fid, "   <constraint>%s</constraint>\n", getTerminalConstraintString(problemDefinition.constraintType).c_str());
        fprintf(fid, "   <longitude unit=\"rad\">%f</longitude>\n", solution.solution[6] / scaling.longitude);
        fprintf(fid, "   <revs>%.1f</revs>\n", solution.solution[6] / scaling.longitude / (2 * M_PI));
        fprintf(fid, "   <tof unit=\"day\">%.3f</tof>\n", solution.transferDuration);
        fprintf(fid, "   <dm unit=\"kg\">%.3f<dm>\n", solution.dm);
        fprintf(fid, "   <dv unit=\"m/s\">%.3f</dv>\n", solution.dvcost);

        if (problemDefinition.eclipse.active) {
            solution.computeThrustDuration(problemDefinition);
            fprintf(fid, "   <total_eclipse_duration unit=\"day\">%.3f<total_eclipse_duration>\n", solution.getTotalCoastDuration());
        }

        fprintf(fid, "   <unknown>\n");
        fprintf(fid, "      <lambda_1>%.8f</lambda_1>\n", solution.solution[0]);
        fprintf(fid, "      <lambda_2>%.8f</lambda_2>\n", solution.solution[1]);
        fprintf(fid, "      <lambda_3>%.8f</lambda_3>\n", solution.solution[2]);
        fprintf(fid, "      <lambda_4>%.8f</lambda_4>\n", solution.solution[3]);
        fprintf(fid, "      <lambda_5>%.8f</lambda_5>\n", solution.solution[4]);
        fprintf(fid, "      <lambda_6>%.8f</lambda_6>\n", solution.solution[5]);
        fprintf(fid, "      <theta>%.8f</theta>\n", solution.solution[6]);
        fprintf(fid, "   </unknown>\n");
        fprintf(fid, "   <final_state>\n");
        fprintf(fid, "      <sma>%.3f</sma>\n", solution.finalState[0]);
        fprintf(fid, "      <ex>%.8f</ex>\n", solution.finalState[1]);
        fprintf(fid, "      <ey>%.8f</ey>\n", solution.finalState[2]);
        fprintf(fid, "      <hx>%.8f</hx>\n", solution.finalState[3]);
        fprintf(fid, "      <hy>%.8f</hy>\n", solution.finalState[4]);
        fprintf(fid, "   </final_state>\n");
        fprintf(fid, "   <scale1>%g</scale1>\n", scaling.time);
        fprintf(fid, "   <scale2>%g</scale2>\n", scaling.longitude);
        fprintf(fid, "</solution>\n");
        fclose(fid);

        return true;
    }

    return false;
}
