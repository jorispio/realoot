// $Id$
// ---------------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef UTILS_INPUT_SCRIPT_HPP
#define UTILS_INPUT_SCRIPT_HPP

#include <string>

#include "xml/InputScriptXmlReader.hpp"
#include "json/InputScriptJsonReader.hpp"

class InputScriptReader {
 private:

 public:
    InputScriptReader();
    ~InputScriptReader();

    void Default(LtProblemDefinition &problemDefinition, LtProblemContext &problemContext);

    bool readXml(std::string gInputScript, LtProblemDefinition &problemDefinition, LtProblemContext &problemContext);
    
    bool readJson(std::string gInputScript, LtProblemDefinition &problemDefinition, LtProblemContext &problemContext);
};

#endif  // UTILS_INPUT_SCRIPT_HPP
