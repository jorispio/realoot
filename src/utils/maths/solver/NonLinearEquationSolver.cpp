// $Id$
// -----------------------------------------------------------------------
/*
 *   This file is part of SpaceMechanicsToolBox.
 *
 * Copyright (C) 2016 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
/**
 * Author: Joris OLYMPIO
 * This code is an import of the NLEQ1 Fortran code
 *    version 2.4.0.1
 *    Written by        U. Nowak, L. Weimann
 *    Copyright     (c) Konrad-Zuse-Zentrum fuer
 *
 */

#include <cmath>
#include <limits>
#include <iomanip>
#include <iostream>
#include <type_traits>
#include <math.h>
#include <cstring>
#include <stdio.h>
#include <algorithm>    // std::min

#include "NonLinearEquationSolver.hpp"
#include "HelioLib/Core.hpp"
#include "netlib/netlib.hpp"

#define IRWKI   51
#define LRWKI   10

#define ONE     1.
#define ZERO    0.
#define TEN     10.
#define HALF    0.5

// monitoring
#define MONOFF(a)   {}
#define MONON(a)    {}
#define MONEND      {}


//#define DEBUG
//#define DEBUG_STEP
//#define DEBUG_FINITE_DIFFERENCES

template <typename T> int sign(T val) {
    return (T(0) <= val) - (val < T(0));
}

// The type of the return value is that of A and B.
// If B >= 0 then the result is ABS(A), else it is -ABS(A).
template <typename T> T sign(T A, T B) {
    return T(sign(B)) * fabs(A);
}

// ------------------------------------------------------------
NonLinearEquationSolver::NonLinearEquationSolver() {

    // machine constant
    EPMACH = std::numeric_limits<double>::epsilon();
    SMALL  = std::numeric_limits<double>::min();
    GREAT  = std::numeric_limits<double>::max();
}
// ------------------------------------------------------------
NonLinearEquationSolver::~NonLinearEquationSolver() {

}

// ------------------------------------------------------------
/**
 * @brief Solve the square system of non-linear equations.
 *         F(X) = 0
 * With F: R^N -> R^N and X in R^N
 *
 * @param N         dimension of the square problem
 * @param X         initial decision vector
 * @param XSCAL     scaling vector
 * @param RTOL      relative convergence tolerance
 * @param options   options of the NLP solver
 * @return exitcode 0 is successful
 */
// ------------------------------------------------------------
int NonLinearEquationSolver::solve(int N, double *X, double *XSCAL,
            double rtol,
            NonLinearEquationSolverOptions options) {
    // init integer arrays (cold start)
    int LIWK = std::max(N + 50, 57);  // minimum: N + 50
    long int *IWK = new long int[LIWK];
    for (int i = 0; i < LIWK; ++i) {
        IWK[i] = 0;
    }
    IWK[31] = options.NITMAX;  // max number of iterations
    IWK[36] = options.NBROY;

    // init real arrays (cold start)
    int LRWK = std::max((N+13)*N + 61, 250);  // (N+NBROY+13)*N+61
    double *RWK = new double[LRWK];
    for (int i = 0; i < LRWK; ++i) {
        RWK[i] = 0.;
    }
    RWK[20] = options.AJDEL;
    RWK[21] = options.FCSTRT;
    RWK[22] = options.FCMIN;
    RWK[23] = options.SIGMA;
    RWK[24] = options.SIGMA2;

    RWK[26] = options.AJDEL;
    RWK[27] = options.AJMIN;
    RWK[28] = options.ETADIF;
    RWK[29] = options.ETAINI;

    long int *IOPT = new long int[52];
    for (int i = 0; i < 50; ++i) {
      IOPT[i] = 0;
    }
    IOPT[1] = options.QSUCC;
    IOPT[2] = options.MODE;
    IOPT[3] = options.JACGEN;
    IOPT[4] = options.MSTOR;
    IOPT[9] = options.ISCAL;
    IOPT[11] = options.MPRERR;  // printing error MPRERR
    IOPT[12] = 1;  // Logical unit number for error messages (LUERR)
    IOPT[13] = options.MPRMON;  // Print iteration Monitor
    IOPT[14] = 1;  // Logical unit number for iteration monitor (LUMON)
    IOPT[15] = options.MPRSOL;  // Print solution (Initial values and solution values)
    //
    IOPT[31] = options.NONLIN;
    IOPT[32] = options.QRANK1;
    IOPT[33] = options.QORDI;
    IOPT[34] = options.QSIMPL;
    IOPT[35] = options.QNSCAL;
    IOPT[38] = options.IBDAMP;
    IOPT[39] = options.IORMON;

    // Solve
    int IERR = 0;
    NLEQ1(N, X, XSCAL,
                rtol, IOPT,
                IERR, LIWK, IWK, LRWK, RWK);

    // free resources
    if (IOPT) {
        delete [] IOPT;
    }
    if (IWK) {
        delete [] IWK;
    }
    if (RWK) {
        delete [] RWK;
    }

    return IERR;
}

//    ------------------------------------------------------------
/**
C*  Title
C
C     Numerical solution of nonlinear (NL) equations (EQ)
C     especially designed for numerically sensitive problems.
C
C*  Written by        U. Nowak, L. Weimann
C*  Purpose           Solution of systems of highly nonlinear equations
C*  Method            Damped affine invariant Newton method
C                     (see references below)
C*  Category          F2a. - Systems of nonlinear equations
C*  Keywords          Nonlinear equations, Newton methods
C*  Version           2.3
C*  Revision          September 1991
C*  Latest Change     June 1992
C*  Library           CodeLib
C*  Code              Fortran 77, Double Precision
C*  Environment       Standard Fortran 77 environment on PC's,
C                     workstations and hosts.
C*  Copyright     (c) Konrad Zuse Zentrum fuer
C                     Informationstechnik Berlin
C                     Heilbronner Str. 10, D-1000 Berlin 31
C                     phone 0049+30+89604-0,
C                     telefax 0049+30+89604-125
C*  Contact           Lutz Weimann
C                     ZIB, Numerical Software Development
C                     phone: 0049+30+89604-185 ;
C                     e-mail:
C                     RFC822 notation: weimann@sc.zib-berlin.de
C                     X.400: C=de;A=dbp;P=zib-berlin;OU=sc;S=Weimann
C
C*    References:
C
C     /1/ P. Deuflhard:
C         Newton Methods for Nonlinear Problems. -
C         Affine Invariance and Adaptive Algorithms.
C         Series Computational Mathematics 35, Springer (2004)
C
C     /2/ U. Nowak, L. Weimann:
C         A Family of Newton Codes for Systems of Highly Nonlinear
C         Equations - Algorithm, Implementation, Application.
C         ZIB, Technical Report TR 90-10 (December 1990)
C
C  ---------------------------------------------------------------
C
C* Licence
C    You may use or modify this code for your own non commercial
C    purposes for an unlimited time.
C    In any case you should not deliver this code without a special
C    permission of ZIB.
C    In case you intend to use the code commercially, we oblige you
C    to sign an according licence agreement with ZIB.
C
C* Warranty
C    This code has been tested up to a certain level. Defects and
C    weaknesses, which may be included in the code, do not establish
C    any warranties by ZIB. ZIB does not take over any liabilities
C    which may follow from acquisition or application of this code.
C
C* Software status
C    This code is under care of ZIB and belongs to ZIB software class 1.
C
C     ------------------------------------------------------------
C
C*    Summary:
C     ========
C     Damped Newton-algorithm for systems of highly nonlinear
C     equations - damping strategy due to Ref. (1).
C
C     (The iteration is done by subroutine N1INT currently. NLEQ1
C      itself does some house keeping and builds up workspace.)
C
C     Jacobian approximation by numerical differences or user
C     supplied subroutine JAC.
C
C     The numerical solution of the arising linear equations is
C     done by means of the subroutines *GEFA and *GESL ( Gauss-
C     algorithm with column-pivoting and row-interchange ) in the
C     dense matrix case, or by the subroutines *GBFA and *GBSL in
C     the band matrix case from LINPACK (replace '*' by 'S' or 'D'
C     for single or double precision version respectively).
C     For special purposes these routines may be substituted.
C
C     This is a driver routine for the core solver N1INT.
C
C     ------------------------------------------------------------
C
C*    Parameters list description (* marks inout parameters)
C     ======================================================
C
C*    External subroutines (to be supplied by the user)
C     =================================================
C
C     (Caution: Arguments declared as (input) must not
C               be altered by the user subroutines ! )
C
C     FCN(N,X,F,IFAIL) Ext    Function subroutine
C       N              Int    Number of vector components (input)
C       X(N)           Dble   Vector of unknowns (input)
C       F(N)           Dble   Vector of function values (output)
C       IFAIL          Int    FCN evaluation-failure indicator. (output)
C                             On input:  Has always value 0 (zero).
C                             On output: Indicates failure of FCN eval-
C                                uation, if having a value <= 2.
C                             If <0: NLEQ1 will be terminated with
C                                    error code = 82, and IFAIL stored
C                                    to IWK(23).
C                             If =1: A new trial Newton iterate will
C                                    computed, with the damping factor
C                                    reduced to it's half.
C                             If =2: A new trial Newton iterate will
C                                    computed, with the damping factor
C                                    reduced by a reduct. factor, which
C                                    must be output through F(1) by FCN,
C                                    and it's value must be >0 and < 1.
C                             Note, that if IFAIL = 1 or 2, additional
C                             conditions concerning the damping factor,
C                             e.g. the minimum damping factor or the
C                             bounded damping strategy may also influ-
C                             ence the value of the reduced damping
C                             factor.
C
C     JAC(N,LDJAC,X,DFDX,IFAIL)
C                       Ext    Jacobian matrix subroutine
C       N                 Int    Number of vector components (input)
C       LDJAC             Int    Leading dimension of Jacobian array
C                                (input)
C       X(N)              Dble   Vector of unknowns (input)
C       DFDX(LDJAC,N)     Dble   DFDX(i,k): partial derivative of
C                                I-th component of FCN with respect
C                                to X(k) (output)
C       IFAIL             Int    JAC evaluation-failure indicator.
C                                (output)
C                                Has always value 0 (zero) on input.
C                                Indicates failure of JAC evaluation
C                                and causes termination of NLEQ1,
C                                if set to a negative value on output
C
C
C*    Input parameters of NLEQ1
C     =========================
C
C     N              Int    Number of unknowns
C   * X(N)           Dble   Initial estimate of the solution
C   * XSCAL(N)       Dble   User scaling (lower threshold) of the
C                           iteration vector X(N)
C   * RTOL           Dble   Required relative precision of
C                           solution components -
C                           RTOL>=EPMACH*TEN*N
C   * IOPT(50)       Int    Array of run-time options. Set to zero
C                           to get default values (details see below)
C
C*    Output parameters of NLEQ1
C     ==========================
C
C   * X(N)           Dble   Solution values ( or final values,
C                           respectively )
C   * XSCAL(N)       Dble   After return with IERR>=0, it contains
C                           the latest internal scaling vector used
C                           After return with IERR==-1 in onestep-
C                           mode it contains a possibly adapted
C                           (as described below) user scaling vector:
C                           If (XSCAL[I]< SMALL) XSCAL[I] = SMALL ,
C                           If (XSCAL[I]> GREAT) XSCAL[I] = GREAT .
C                           For SMALL and GREAT, see section machine
C                           constants below  and regard note 1.
C   * RTOL           Dble   Finally achieved (relative) accuracy.
C                           The estimated absolute error of component i
C                           of x_out is approximately given by
C                             abs_err[I] = RTOL * XSCAL_out[I] ,
C                           where (approximately)
C                             XSCAL_out[I] =
C                                max(abs(X_out[I]),XSCAL_in[I]).
C     IERR           Int    Return value parameter
C                           =-1 sucessfull completion of one iteration
C                               step, subsequent iterations are needed
C                               to get a solution. (stepwise mode only)
C                           = 0 successfull completion of the iteration,
C                               solution has been computed
C                           > 0 see list of error messages below
C
C     Note 1.
C        The machine dependent values SMALL, GREAT and EPMACH are
C        gained from calls of the machine constants function D1MACH.
C        As delivered, this function is adapted to use constants
C        suitable for all machines with IEEE arithmetic. If you use
C        another type of machine, you have to change the DATA state-
C        ments for IEEE arithmetic in D1MACH into comments and to
C        uncomment the set of DATA statements suitable for your machine.
C
C*    Workspace parameters of NLEQ1
C     =============================
C
C     LIWK           Int    Declared dimension of integer workspace.
C                           Required minimum (for standard linear system
C                           solver) : N+50
C  *  IWK(LIWK)      Int    Integer Workspace
C     LRWK           Int    Declared dimension of real workspace.
C                           Required minimum (for standard linear system
C                           solver and Jacobian computed by numerical
C                           approximation - if the Jacobian is computed
C                           by a user subroutine JAC, decrease the
C                           expressions noted below by N):
C                           for full case Jacobian: (N+NBROY+13)*N+61
C                           for a band-matrix Jacobian:
C                              (2*ML+MU+NBROY+14)*N+61 with
C                           ML = lower bandwidth , MU = upper bandwidth
C                           NBROY = Maximum number of Broyden steps
C                           (Default: if Broyden steps are enabled, e.g.
C                                                IOPT(32)=1            -
C                                       NBROY=N (full Jacobian),
C                                            =ML+MU+1 (band Jacobian),
C                                       but at least NBROY=10
C                                     else (if IOPT(32)=0) -
C                                       NBROY=0 ;
C                            see equally named IOPT and IWK-fields below)
C   * RWK(LRWK)      Dble   Real Workspace
C
C     Note 2a.  A test on sufficient workspace is made. If this
C               test fails, IERR is set to 10 and an error-message
C               is issued from which the minimum of required
C               workspace size can be obtained.
C
C     Note 2b.  The first 50 elements of IWK and RWK are partially
C               used as input for internal algorithm parameters (for
C               details, see below). In order to set the default values
C               of these parameters, the fields must be set to zero.
C               Therefore, it's recommanded always to initialize the
C               first 50 elements of both workspaces to zero.
C
C*   Options IOPT:
C    =============
C
C     Pos. Name   Default  Meaning
C
C       1  QSUCC  0        =0 (false) initial call:
C                             NLEQ1 is not yet initialized, i.e. this is
C                             the first call for this nonlinear system.
C                             At successfull return with MODE=1,
C                             QSUCC is set to 1.
C                          =1 (true) successive call:
C                             NLEQ1 is initialized already and is now
C                             called to perform one or more following
C                             Newton-iteration steps.
C                             ATTENTION:
C                                Don't destroy the contents of
C                                IOPT[I] for 1 <= i <= 50 ,
C                                IWK(j)  for 1 <= j < NIWKFR and
C                                RWK(k)  for 1 <= k < NRWKFR.
C                                (Nevertheless, some of the options, e.g.
C                                 FCMIN, SIGMA, MPR..., can be modified
C                                 before successive calls.)
C       2  MODE   0        =0 Standard mode initial call:
C                             Return when the required accuracy for the
C                             iteration vector is reached. User defined
C                             parameters are evaluated and checked.
C                             Standard mode successive call:
C                             If NLEQ1 was called previously with MODE=1,
C                             it performs all remaining iteration steps.
C                          =1 Stepwise mode:
C                             Return after one Newton iteration step.
C       3  JACGEN 0        Method of Jacobian generation
C                          =0 Standard method is JACGEN=2
C                          =1 User supplied subroutine JAC will be
C                             called to generate Jacobian matrix
C                          =2 Jacobian approximation by numerical
C                             differentation (see subroutines N1JAC
C                             and N1JACB)
C                          =3 Jacobian approximation by numerical
C                             differentation with feedback control
C                             (see subroutines N1JCF and N1JCFB)
C       4  MSTOR  0        =0 The Jacobian A is a dense matrix
C                          =1 A is a band matrix
C       5                  Reserved
C       6  ML     0        Lower bandwidth of A (excluding the
C                          diagonal);
C                          IOPT(6) ignored, if IOPT(4)!=1
C       7  MU     0        Upper bandwidth of A (excluding the
C                          diagonal);
C                          IOPT(7) ignored, if IOPT(4)!=1
C       8                  Reserved
C       9  ISCAL  0        Determines how to scale the iterate-vector:
C                          =0 The user supplied scaling vector XSCAL is
C                             used as a (componentwise) lower threshold
C                             of the current scaling vector
C                          =1 The vector XSCAL is always used as the
C                             current scaling vector
C      10                  Reserved
C      11  MPRERR 0        Print error messages
C                          =0 No output
C                          =1 Error messages
C                          =2 Warnings additionally
C                          =3 Informal messages additionally
C      12  LUERR  6        Logical unit number for error messages
C      13  MPRMON 0        Print iteration Monitor
C                          =0 No output
C                          =1 Standard output
C                          =2 Summary iteration monitor additionally
C                          =3 Detailed iteration monitor additionally
C                          =4,5,6 Outputs with increasing level addi-
C                             tional increasing information for code
C                             testing purposes. Level 6 produces
C                             in general extremely large output!
C      14  LUMON  6        Logical unit number for iteration monitor
C      15  MPRSOL 0        Print solutions
C                          =0 No output
C                          =1 Initial values and solution values
C                          =2 Intermediate iterates additionally
C      16  LUSOL  6        Logical unit number for solutions
C      17..18              Reserved
C      19  MPRTIM 0        Output level for the time monitor
C                          = 0 : no time measurement and no output
C                          = 1 : time measurement will be done and
C                                summary output will be written -
C                                regard note 5a.
C      20  LUTIM  6        Logical output unit for time monitor
C      21..30              Reserved
C      31  NONLIN 3        Problem type specification
C                          =1 Linear problem
C                             Warning: If specified, no check will be
C                             done, if the problem is really linear, and
C                             NLEQ1 terminates unconditionally after one
C                             Newton-iteration step.
C                          =2 Mildly nonlinear problem
C                          =3 Highly nonlinear problem
C                          =4 Extremely nonlinear problem
C      32  QRANK1 0        =0 (false) Rank-1 updates by Broyden-
C                             approximation are inhibited.
C                          =1 (true) Rank-1 updates by Broyden-
C                             approximation are allowed.
C      33  QORDI  0        =0 (false) Standard program mode
C                          =1 (true)  Special program mode:
C                             Ordinary Newton iteration is done, e.g.:
C                             No damping strategy and no monotonicity
C                             test is applied
C      34  QSIMPL 0        =0 (false) Standard program mode
C                          =1 (true)  Special program mode:
C                             Simplified Newton iteration is done, e.g.:
C                             The Jacobian computed at the starting
C                             point is fixed for all subsequent
C                             iteration steps, and
C                             no damping strategy and no monotonicity
C                             test is applied.
C      35  QNSCAL 0        Inhibit automatic row scaling:
C                          =0 (false) Automatic row scaling of
C                             the linear system is activ:
C                             Rows i=1,...,N will be divided by
C                             max j=1,...,N (abs(a(i,j)))
C                          =1 (true) No row scaling of the linear
C                             system. Recommended only for well row-
C                             scaled nonlinear systems.
C      36..37              Reserved
C      38  IBDAMP          Bounded damping strategy switch:
C                          =0 The default switch takes place, dependent
C                             on the setting of NONLIN (=IOPT(31)):
C                             NONLIN = 0,1,2,3 -> IBDAMP = off ,
C                             NONLIN = 4 -> IBDAMP = on
C                          =1 means always IBDAMP = on
C                          =2 means always IBDAMP = off
C      39  IORMON          Convergence order monitor
C                          =0 Standard option is IORMON=2
C                          =1 Convergence order is not checked,
C                             the iteration will be always proceeded
C                             until the solution has the required
C                             precision RTOL (or some error condition
C                             occured)
C                          =2 Use additional 'weak stop' criterion:
C                             Convergence order is monitored
C                             and termination due to slowdown of the
C                             convergence may occur.
C                          =3 Use additional 'hard stop' criterion:
C                             Convergence order is monitored
C                             and termination due to superlinear
C                             convergence slowdown may occur.
C                          In case of termination due to convergence
C                          slowdown, the warning code IERR=4 will be
C                          set.
C                          In cases, where the Newton iteration con-
C                          verges but superlinear convergence order has
C                          never been detected, the warning code IERR=5
C                          is returned.
C      40..45              Reserved
C      46..50              User options (see note 5b)
C
C     Note 3:
C         If NLEQ1 terminates with IERR=2 (maximum iterations)
C         or  IERR=3 (small damping factor), you may try to continue
C         the iteration by increasing NITMAX or decreasing FCMIN
C         (see RWK) and setting QSUCC to 1.
C
C     Note 4 : Storage of user supplied banded Jacobian
C        In the band matrix case, the following lines may build
C        up the analytic Jacobian A;
C        Here AFL denotes the quadratic matrix A in dense form,
C        and ABD the rectangular matrix A in banded form :
C
C                   ML = IOPT(6)
C                   MU = IOPT(7)
C                   MH = MU+1
C                   DO 20 J = 1,N
C                     I1 = MAX0(1,J-MU)
C                     I2 = MIN0(N,J+ML)
C                     DO 10 I = I1,I2
C                       K = I-J+MH
C                       ABD(K,J) = AFL(I,J)
C           10        CONTINUE
C           20      CONTINUE
C
C         The total number of rows needed in  ABD  is  ML+MU+1 .
C         The  MU by MU  upper left triangle and the
C         ML by ML  lower right triangle are not referenced.
C
C     Note 5a:
C        The integrated time monitor calls the machine dependent
C        subroutine SECOND to get the current time stamp in form
C        of a real number (Single precision). As delivered, this
C        subroutine always return 0.0 as time stamp value. Refer
C        to the compiler- or library manual of the FORTRAN compiler
C        which you currently use to find out how to get the current
C        time stamp on your machine.
C
C     Note 5b:
C         The user options may be interpreted by the user replacable
C         routines N1SOUT, N1FACT, N1SOLV - the distributed version
C         of N1SOUT currently uses IOPT(46) as follows:
C         0 = standard plotdata output (may be postprocessed by a user-
C             written graphical program)
C         1 = plotdata output is suitable as input to the graphical
C             package GRAZIL (based on GKS), which has been developed
C             at ZIB.
C
C
C*   Optional INTEGER input/output in IWK:
C    =======================================
C
C     Pos. Name          Meaning
C
C      1   NITER  IN/OUT Number of Newton-iterations
C      2                 reserved
C      3   NCORR  IN/OUT Number of corrector steps
C      4   NFCN   IN/OUT Number of FCN-evaluations
C      5   NJAC   IN/OUT Number of Jacobian generations or
C                        JAC-calls
C      6                 reserved
C      7                 reserved
C      8   NFCNJ  IN/OUT Number of FCN-evaluations for Jacobian
C                        approximation
C      9   NREJR1 IN/OUT Number of rejected Newton iteration steps
C                        done with a rank-1 approximated Jacobian
C     10..11             Reserved
C     12   IDCODE IN/OUT Output: The 8 decimal digits program identi-
C                        fication number ppppvvvv, consisting of the
C                        program code pppp and the version code vvvv.
C                        Input: If containing a negative number,
C                        it will only be overwritten by the identi-
C                        fication number, immediately followed by
C                        a return to the calling program.
C     13..15             Reserved
C     16   NIWKFR OUT    First element of IWK which is free to be used
C                        as workspace between Newton iteration steps
C                        for standard linear solvers: 51
C     17   NRWKFR OUT    First element of RWK which is free to be used
C                        as workspace between Newton iteration steps.
C                        For standard linear solvers and numerically
C                        approximated Jacobian computed by one of the
C                        expressions:
C                        (N+7+NBROY)*N+61        for a full Jacobian
C                        (2*ML+MU+8+NBROY)*N+61  for a banded Jacobian
C                        If the Jacobian is computed by a user routine
C                        JAC, subtract N in both expressions.
C     18   LIWKA  OUT    Length of IWK currently required
C     19   LRWKA  OUT    Length of RWK currently required
C     20..22             Reserved
C     23   IFAIL  OUT    Set in case of failure of N1FACT (IERR=80),
C                        N1SOLV (IERR=81), FCN (IERR=82) or JAC(IERR=83)
C                        to the nonzero IFAIL value returned by the
C                        routine indicating the failure .
C     24   ICONV  OUT    Current status of of the convergence monitor
C                        (only if convergence order monitor is on -
C                         see IORMON(=IOPT(39)))
C                        =0: No convergence indicated yet
C                        =1: Damping factor is 1.0d0
C                        =2: Superlinear convergence in progress
C                        =3: Quadratic convergence in progress
C     25..30             Reserved
C     31   NITMAX IN     Maximum number of permitted iteration
C                        steps (default: 50)
C     32                 Reserved
C     33   NEW    IN/OUT Count of consecutive rank-1 updates
C     34..35             Reserved
C     36   NBROY  IN     Maximum number of possible consecutive
C                        iterative Broyden steps. The total real
C                        workspace needed (RWK) depends on this value
C                        (see LRWK above).
C                        Default is N (see parameter N),
C                        if MSTOR=0 (=IOPT(4)),
C                        and ML+MU+1 (=IOPT(6)+IOPT(7)+1), if MSTOR=1
C                        (but minimum is always 10) -
C                        provided that Broyden is allowed.
C                        If Broyden is inhibited, NBROY is always set to
C                        zero.
C     37..50             Reserved
C
C*   Optional REAL input/output in RWK:
C    ====================================
C
C     Pos. Name          Meaning
C
C      1..16             Reserved
C     17   CONV   OUT    The achieved relative accuracy after the
C                        current step
C     18   SUMX   OUT    Natural level (not Normx of printouts)
C                        of the current iterate, i.e. Sum(DX[I]**2),
C                        where DX = scaled Newton correction.
C     19   DLEVF  OUT    Standard level (not Normf of printouts)
C                        of the current iterate, i.e. Norm2(F(X)),
C                        where F =  nonlinear problem function.
C     20   FCBND  IN     Bounded damping strategy restriction factor
C                        (Default is 10)
C     21   FCSTRT IN     Damping factor for first Newton iteration -
C                        overrides option NONLIN, if set (see note 6)
C     22   FCMIN  IN     Minimal allowed damping factor (see note 6)
C     23   SIGMA  IN     Broyden-approximation decision parameter
C                        Required choice: SIGMA>=1. Increasing this
C                        parameter make it less probable that the algo-
C                        rith performs rank-1 updates.
C                        Rank-1 updates are inhibited, if
C                        SIGMA>1/FCMIN is set. (see note 6)
C     24   SIGMA2 IN     Decision parameter about increasing damping
C                        factor to corrector if predictor is small.
C                        Required choice: SIGMA2>=1. Increasing this
C                        parameter make it less probable that the algo-
C                        rith performs rank-1 updates.
C     25                 Reserved
C     26   AJDEL  IN     Jacobian approximation without feedback:
C                        Relative pertubation for components
C                        (Default: sqrt(epmach*10), epmach: relative
C                         machine precision)
C     27   AJMIN  IN     Jacobian approximation without feedback:
C                        Threshold value (Default: 0.0d0)
C                          The absolute pertubation for component k is
C                          computed by
C                          DELX := AJDEL*max(abs(Xk),AJMIN)
C     28  ETADIF  IN     Jacobian approximation with feedback:
C                        Target value for relative pertubation ETA of X
C                        (Default: 1.0d-6)
C     29  ETAINI  IN     Jacobian approximation with feedback:
C                        Initial value for denominator differences
C                        (Default: 1.0d-6)
C     30..50             Reserved
C
C     Note 6:
C       The default values of the internal parameters may be obtained
C       from the monitor output with at least IOPT field MPRMON set to 2
C       and by initializing the corresponding RWK-fields to zero.
C
C*   Error and warning messages:
C    ===========================
C
C      1    Termination, since jacobian matrix became singular
C      2    Termination after NITMAX iterations ( as indicated by
C           input parameter NITMAX=IWK[31] )
C      3    Termination, since damping factor became to small
C      4    Warning: Superlinear or quadratic convergence slowed down
C           near the solution.
C           Iteration has been stopped therefore with an approximation
C           of the solution not such accurate as requested by RTOL,
C           because possibly the RTOL requirement may be too stringent
C           (i.e. the nonlinear problem is ill-conditioned)
C      5    Warning: Iteration stopped with termination criterion
C           (using RTOL as requested precision) satisfied, but no
C           superlinear or quadratic convergence has been indicated yet.
C           Therefore, possibly the error estimate for the solution may
C           not match good enough the really achieved accuracy.
C     10    Integer or real workspace too small
C     20    Bad input to dimensional parameter N
C     21    Nonpositive value for RTOL supplied
C     22    Negative scaling value via vector XSCAL supplied
C     30    One or more fields specified in IOPT are invalid
C           (for more information, see error-printout)
C     80    Error signalled by linear solver routine N1FACT,
C           for more detailed information see IFAIL-value
C           stored to IWK(23)
C     81    Error signalled by linear solver routine N1SOLV,
C           for more detailed information see IFAIL-value
C           stored to IWK(23)
C           (not used by standard routine N1SOLV)
C     82    Error signalled by user routine FCN (Nonzero value
C           returned via IFAIL-flag; stored to IWK(23) )
C     83    Error signalled by user routine JAC (Nonzero value
C           returned via IFAIL-flag; stored to IWK(23) )
C
C     Note 7 : in case of failure:
C        -    use non-standard options
C        -    or turn to Newton-algorithm with rank strategy
C        -    use another initial guess
C        -    or reformulate model
C        -    or apply continuation techniques
C
C*    Machine dependent constants used:
C     =================================
C
C     DOUBLE PRECISION EPMACH  in  N1PCHK, N1INT
C     DOUBLE PRECISION GREAT   in  N1PCHK
C     DOUBLE PRECISION SMALL   in  N1PCHK, N1INT, N1SCAL
C
C*    Subroutines called: N1PCHK, N1INT
C
C     ------------------------------------------------------------
C*    End Prologue
C
C*    Summary of changes:
C     ===================
C
C     2.2.1  91, May 30     Time monitor included
C     2.2.2  91, May 30     Bounded damping strategy implemented
C     2.2.3  91, June 19    AJDEL, AJMIN as RWK-options for JACGEN==2,
C                           ETADIF, ETAINI as RWK-opts. for JACGEN==3
C                           FCN-count changed for anal. Jacobian
C     2.2.4  91, August  9  Convergence order monitor included
C     2.2.5  91, August 13  Standard Broyden updates replaced by
C                           iterative Broyden
C     2.2.6  91, Sept.  16  Damping factor reduction by FCN-fail imple-
C                           mented
C     2.3    91, Dec.   20  New Release for CodeLib
C
C     ------------------------------------------------------------
C
C     PARAMETER (IRWKI=xx, LRWKI=yy)
C     IRWKI: Start position of internally used RWK part
C     LRWKI: Length of internally used RWK part
C     (current values see parameter statement below)
C
C     INTEGER L4,L5,L51,L6,L61,L62,L63,L7,L71,L8,L9,L10,L11,L12,L121,
C             L13,L14,L20
C     Starting positions in RWK of formal array parameters of internal
C     routine N1INT (dynamically determined in driver routine NLEQ1,
C     dependent on N and options setting)
C
C     Further RWK positions (only internally used)
C
C     Position  Name     Meaning
C
C     IRWKI     FCKEEP   Damping factor of previous successfull iter.
C     IRWKI+1   FCA      Previous damping factor
C     IRWKI+2   FCPRI    A priori estimate of damping factor
C     IRWKI+3   DMYCOR   Number My of latest corrector damping factor
C                        (kept for use in rank-1 decision criterium)
C     IRWKI+4   SUMXS    natural level of accepted simplified correction
C     IRWKI+(5..LRWKI-1) Free
C
C     Internal arrays stored in RWK (see routine N1INT for descriptions)
C
C     Position  Array         Type   Remarks
C
C     L4        A(M1,N)       Perm   M1=N (full mode) or
C                                    M1=2*IOPT(6)+IOPT(7)+1 (band mode)
C     L41       DXSAVE(N,NBROY)
C                             Perm   NBROY=IWK(36) (Default: N or 0)
C     L5        DX(N)         Perm
C     L51       DXQ(N)        Perm
C     L6        XA(N)         Perm
C     L61       F(N)          Perm
C     L62       FW(N)         Perm
C     L63       XWA(N)        Perm
C     L7        FA(N)         Perm
C     L71       ETA(N)        Perm   Only used for JACGEN=IOPT(3)=3
C     L9        XW(N)         Temp
C     L11       DXQA(N)       Temp
C     L12       T1(N)         Temp
C     L121      T2(N)         Temp
C     L13       T3(N)         Temp
C     L14                     Temp   Start position of array workspace
C                                    needed for linear solver
C
*/
void NonLinearEquationSolver::NLEQ1(int N,
                    /*void (*FCN)(), void (*JAC)(), */
                    double *X, double *XSCAL,
                    double RTOL, long int IOPT[],
                    int &IERR, int LIWK, long int IWK[], int LRWK, double RWK[]) {
    #define IOPT(i)  IOPT[i]
    #define DXSAVE(I, J) DXSAVE[I * NBROY + J]

    //int IRWKI, LRWKI;
    int NITMAX, LUSOL, MPRSOL;
    int NRWKFR, NRFRIN, NRW, NIWKFR, NIFRIN, NONLIN, JACGEN;
    int  L4, L41, L5, L51, L6, L61, L62, L63, L7, L71, L8, L9, L11, L12, L121, L13, L14, L20;
    double FC, FCMIN;
    bool QBDAMP = false;

    int IVER = 21112300;
    const char *PRODCT = "NLEQ1";

    IERR = 0;
    bool QVCHK = IWK[12] < 0;
    IWK[12] = IVER;
    if (QVCHK) return;

    // Print error messages?
    int MPRERR = IOPT[11];
    int LUERR = IOPT[12];
    if (LUERR == 0) {
        LUERR = 6;
        IOPT[12] = LUERR;
    }

    // Print iteration monitor?
    int MPRMON = IOPT[13];
    int LUMON = IOPT[14];
    if ((LUMON < 0) || (LUMON > 99)) {
        LUMON = 6;
        IOPT[14] = LUMON;
    }

    // Print intermediate solutions?
    MPRSOL = IOPT[15];
    LUSOL = IOPT[16];
    if (LUSOL == 0) {
        LUSOL = 6;
        IOPT[16] = LUSOL;
    }

    // Print time summary statistics?
    int MPRTIM = IOPT[19];
    int LUTIM = IOPT[20];
    if (LUTIM == 0) {
        LUTIM = 6;
        IOPT[20] = LUTIM;
    }
    bool QSUCC = IOPT[1] == 1;
    bool QINIMO = (MPRMON >= 1) && (!QSUCC);

    // Print NLEQ1 heading lines
    if (QINIMO) {
        printf("   N L E Q 1  *****  V e r s i o n  2 . 3 ***\n");
        printf(" Newton-Method for the solution of nonlinear systems\n");
    }

    // Check input parameters and options
    IERR = N1PCheck(N, X, XSCAL, RTOL, IOPT, LIWK, IWK, LRWK, RWK);

    // Exit, if any parameter error was detected till here
    if (IERR != 0) return;

    int MSTOR = IOPT[4];
    int ML = 0, MU = 0;
    int M1 = 0, M2 = 0;
    if (MSTOR == 0) {
        M1 = N;
        M2 = N;
    } else if (MSTOR == 1) {
        ML = IOPT[6];
        MU = IOPT[7];
        M1 = 2*ML + MU + 1;
        M2 = ML + MU + 1;
    } else {
        // should no happen
    }

    JACGEN = IOPT[3];
    if (JACGEN == 0) JACGEN=2;
    IOPT[3] = JACGEN;
    bool QRANK1 = IOPT[32] == 1;
    bool QSIMPL = IOPT[34] == 1;
    int NBROY;
    if (QRANK1) {
        NBROY = IWK[36];
        if (NBROY == 0) NBROY = std::max(M2, 10);
        IWK[36] = NBROY;
    } else {
        NBROY = 0;
    }

    // WorkSpace: RWK
    L4 = IRWKI + LRWKI;
    L41 = L4 + M1 * N;
    L5 = L41 + NBROY * N;
    L51 = L5 + N;
    L6 = L51 + N;
    L61 = L6 + N;
    L62 = L61 + N;
    L63 = L62 + N;
    L7 = L63 + N;
    L71 = L7 + N;
    if (JACGEN != 3) {
        L8 = L71;
    } else {
        L8 = L71 + N;
    }
    NRWKFR = L8;
    L9 = L8;
    L11 = L9 + N;
    L12 = L11 + N;
    L121 = L12 + N;
    L13 = L121 + N;
    L14 = L13 + N;
    NRW = L14 - 1;
    // End WorkSpace at NRW
    // WorkSpace: IWK
    L20 = 51;
    NIWKFR = L20;
    if (QRANK1 | QSIMPL) NIWKFR = NIWKFR + N;
    int NIW = L20 - 1;
    // End WorkSpace at NIW
    IWK[16] = NIW + 1;
    IWK[17] = NRW + 1;
    NIFRIN = NIW + 1;
    NRFRIN = NRW + 1;

    if ((NRW > LRWK) | (NIW > LIWK)) {
        IERR = 10;
    } else {
        if (QINIMO) {
            double PERCR = double(NRW) / double(LRWK) * 100.0;
            double PERCI = double(NIW) / double(LIWK) * 100.0;

            // Print statistics concerning workspace usage
            if (MPRMON >= 3) {
                printf(" Real    Workspace declared as %9d is used up to %9d (%5.1f percent)\n", LRWK, NRW, PERCR);
                printf(" Integer Workspace declared as %9d is used up to %9d (%5.1f percent)\n", LIWK, NIW, PERCI);
            }
        }
        if (QINIMO) {
            if (MPRMON >= 3) {
                printf(" N = %4d\n", N);
                printf(" Prescribed relative %10.2g\n", RTOL);


                if (JACGEN == 1) {
                    printf(" The Jacobian is supplied by a user subroutine\n");
                }
                else if (JACGEN == 2) {
                    printf(" The Jacobian is supplied by numerical differentiation (without feedback strategy)\n");
                }
                else if (JACGEN == 3) {
                    printf(" The Jacobian is supplied by numerical differentiation (feedback strategy included)\n");
                }

                if (MSTOR == 0) {
                    printf(" The Jacobian will be stored in full\n");
                }
                else if (MSTOR == 1) {
                    printf(" The Jacobian will be stored in banded\n");
                    printf(" Lower bandwidth : %3d   Upper bandwidth : %3d\n", ML, MU);
                }
                if (IOPT[35] == 1) {
                    printf(" Automatic row scaling of the Jacobian is inhibited\n");
                }
                else {
                    printf(" Automatic row scaling of the Jacobian is allowed\n");
                }
            }
        }

        NONLIN = IOPT[31];
        if (IOPT[38] == 0) QBDAMP = NONLIN == 4;
        else if (IOPT[38] == 1) QBDAMP = true;
        else if (IOPT[38] == 2) QBDAMP = false;

        if (QBDAMP) {
            if (RWK[20] < ONE) RWK[20] = TEN;
        }

        if (QINIMO) {
            if (MPRMON >= 3) {
                if (QRANK1) {
                    printf(" Rank-1 updates are allowed\n");
                }
                else {
                    printf(" Rank-1 updates are inhibited\n");
                }

                if (NONLIN == 1) {
                    printf(" Problem is specified as being linear\n");
                }
                else if (NONLIN == 2) {
                    printf(" Problem is specified as being mildly nonlinear\n");
                }
                else if (NONLIN == 3) {
                    printf(" Problem is specified as being highly nonlinear\n");
                }
                else if (NONLIN == 4) {
                    printf(" Problem is specified as being extremely nonlinear\n");
                }

                if (QBDAMP) {
                    printf(" Bounded damping strategy is active. Bounding factor is %10.3g\n", RWK[20]);
                }
                else {
                    printf(" Bounded damping strategy is off\n");
                }

                if (IOPT[33] == 1) printf(" Special mode Ordinary Newton iteration will be done\n");

                if (IOPT[34] == 1) printf(" Simplified Newton iteration will be done\n");
            }
        }

        // Maximum permitted number of iteration steps
        NITMAX = IWK[31];
        if (NITMAX < 0) NITMAX = 50;
        IWK[31] = NITMAX;

        if (QINIMO && (MPRMON >= 3)) {
            printf(" Maximum permitted number of iteration steps : %6d\n", NITMAX);
        }
        // Initial damping factor for highly nonlinear problems
        bool QFCSTR = RWK[21] > ZERO;
        if (!QFCSTR) {
            RWK[21] = 1.0e-2;
            if (NONLIN == 4) RWK[21] = 1.0e-4;
        }

        // Minimal permitted damping factor
        if (RWK[22] <= ZERO) {
            RWK[22] = 1.0e-4;
            if (NONLIN == 4) RWK[22] = 1.0e-8;
        }
        FCMIN = RWK[22];

        // Rank1 decision parameter SIGMA
        if (RWK[23] < ONE) RWK[23] = 3.0;
        if (!QRANK1) RWK[23] = 10.0 / FCMIN;

        // Decision parameter about increasing too small predictor
        // to greater corrector value
        if (RWK[24] < ONE) RWK[24] = 10.0 / FCMIN;

        // Starting value of damping factor (FCMIN<=FC<=1.0)
        if ((NONLIN <= 2) && (!QFCSTR)) {
            //for linear or mildly nonlinear problems
            FC = ONE;
        } else {
            // for highly or extremely nonlinear problems
            FC = RWK[21];
        }

        // Simplied Newton iteration implies ordinary Newton it. mode
        if (IOPT[34] == 1) IOPT[33]=1;

        // If ordinary Newton iteration, factor is always one
        if (IOPT[33] == 1) FC = 1.0;
        RWK[21] = FC;
        if ((MPRMON >= 3) && (!QSUCC)) {
            printf(" Starting value for damping factor FCSTART = %9.2g\n", RWK[21]);
            printf(" Minimum allowed damping factor    FCMIN   = %9.2g\n", FCMIN);
            printf(" Rank-1 updates decision parameter SIGMA   = %9.2g\n", RWK[23]);
        }

        // Store lengths of currently required workspaces
        IWK[18] = NIFRIN - 1;
        IWK[19] = NRFRIN - 1;

        // Initialize and start time measurements monitor
        if (( IOPT(1) == 0) && (MPRTIM != 0 )) {
           /*MONINI (" NLEQ1", LUTIM);
           MONDEF (0,"NLEQ1");
           MONDEF (1,"FCN");
           MONDEF (2,"Jacobi");
           MONDEF (3,"Lin-Fact");
           MONDEF (4,"Lin-Sol");
           MONDEF (5,"Output");
           MONSRT ();*/
        }

        IERR = -1;
        // If IERR is unmodified on exit, successive steps are required
        // to complete the Newton iteration
        if (NBROY == 0) NBROY=1;

        N1INT(N,/*FCN,JAC,*/
            X, XSCAL, RTOL, NITMAX,
            NONLIN, IOPT, IERR,
            LRWK, RWK, NRFRIN, LIWK, IWK, NIFRIN,
            M1, M2, NBROY,
            RWK + L4, // A
            RWK + L41, // DXSAVE
            RWK + L5, // DX
            RWK + L51,// DXQ
            RWK + L6, // XA
            RWK + L63, // XWA
            RWK + L61, // F
            RWK + L7, // FA
            RWK + L71,// ETA
            RWK + L9, //XW
            RWK + L62, // FW
            RWK + L11, // DXQA
            RWK + L12, // T1
            RWK + L121, // T2
            RWK + L13,// T3 (32)
            RWK[21], RWK[22], RWK[23], RWK[24], RWK[IRWKI + 1], RWK[IRWKI],
            RWK[IRWKI + 2], RWK[IRWKI + 3], RWK[17], RWK[18], RWK[19], MSTOR,
            MPRERR, MPRMON, MPRSOL, LUERR, LUMON, LUSOL, IWK[1], IWK[3], IWK[4],
            IWK[5], IWK[8], IWK[9], IWK[33], QBDAMP);

        if ((MPRTIM != 0) && (IERR != -1) && (IERR != 10)) {
            MONEND;
        }

    // Free workspaces, so far not used between steps
        IWK[16] = NIWKFR;
        IWK[17] = NRWKFR;
    }

    // Print statistics
    if ((MPRMON>=1) && (IERR!=-1) && (IERR!=10)) {
        printf("   ******  Statistics *  %8s *******\n", PRODCT);
        printf("   ***  Newton iterations :  %7ld  ***\n", IWK[1]);
        printf("   ***  Corrector steps   :  %7ld  ***\n", IWK[3]);
        printf("   ***  Rejected rk-1 st. :  %7ld  ***\n", IWK[9]);
        printf("   ***  Jacobian eval.    :  %7ld  ***\n", IWK[5]);
        printf("   ***  Function eval.    :  %7ld  ***\n", IWK[4]);
        printf("   ***  ...  for Jacobian :  %7ld  ***\n", IWK[8]);
        printf("   *************************************\n");
    }

    // Print workspace requirements, if insufficient
    if (IERR == 10) {
        if (MPRERR >= 1) printf("******************** Workspace Error ********************\n");

        if (NRW>LRWK) {
          if (MPRERR >= 1) printf("Real Workspace dimensioned as %9d must be enlarged at least up to %9d\n", LRWK, NRFRIN - 1);
        }
        if (NIW>LIWK) {
          if (MPRERR >= 1)
              printf("Integer Workspace dimensioned as %9d must be enlarged at least up to %9d\n", LIWK, NIFRIN - 1);
        }
    }
    //End of subroutine NLEQ1

    #undef IOPT
    #undef DXSAVE
}

// ------------------------------------------------------------
/**
C     N 1 P C H K : Checking of input parameters and options
C                   for NLEQ1.
C
C*    Parameters:
C     ===========
C
C     See parameter description in driver routine.
C
C*    Subroutines called: D1MACH
C
C*    Machine dependent constants used:
C     =================================
C
C     EPMACH = relative machine precision
C     GREAT = squareroot of maxreal divided by 10
C     SMALL = squareroot of "smallest positive machine number
C             divided by relative machine precision"
      DOUBLE PRECISION EPMACH,GREAT,SMALL
C
*/
int NonLinearEquationSolver::N1PCheck(int N, double *X, double *XSCAL, double RTOL,
        long int *IOPT,
        int &LIWK, long int *IWK, int &LRWK, double *RWK) {
    int IERR = 0;

    // range of permitted values for options
    int IOPTL[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                  -9999999, -9999999, -9999999, -9999999, -9999999};
    int IOPTU[] = {0, 1, 1, 3, 1, 0, 9999999, 9999999, 0, 1, 0, 3, 99, 6, 99, 3, 99, 0, 0, 1,
                   99, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1, 1, 1, 1,
                   0, 0, 2, 3, 0, 0 , 0, 0, 0, 0,
                   9999999, 9999999, 9999999, 9999999, 9999999};

    // Print error messages?
    int MPRERR = IOPT[11];
    int LUERR = IOPT[12];  // output stream id
    if ((LUERR <= 0) || (LUERR > 99)) {
        LUERR = 6;
        IOPT[12] = LUERR;
    }

    //Checking dimensional parameter N
    if ( N <= 0 ) {
        if (MPRERR >= 1)
            printf(" Error: Bad input to dimensional parameter N supplied choose N positive, your input is: N = %5d\n", N);
        IERR = 20;
    }

    // Problem type specification by user
    int NONLIN = IOPT[31];
    if (NONLIN == 0) NONLIN = 3;
    IOPT[31] = NONLIN;

    // Checking and conditional adaption of the user-prescribed RTOL
    if (RTOL <= ZERO) {
        if (MPRERR >= 1)
            printf("Error: Nonpositive RTOL supplied\n");
        IERR = 21;
    } else {
        double TOLMIN = EPMACH * TEN * double(N);
        if (RTOL < TOLMIN) {
          RTOL = TOLMIN;
          if (MPRERR >= 2)
            printf("Warning: User prescribed RTOL increased to reasonable smallest value RTOL = %11.2f\n", RTOL);
        }
        double TOLMAX = 1.0e-1;
        if (RTOL > TOLMAX) {
          RTOL = TOLMAX;
          if (MPRERR >= 2)
              printf("Warning: User prescribed RTOL decreased to reasonable largest value RTOL = %11.2f\n", RTOL);
        }
    }

    // Test user prescribed accuracy and scaling on proper values
    if (N <= 0) return IERR;

    double DEFSCL;
    if (NONLIN >= 3) {
        DEFSCL = RTOL;
    } else {
        DEFSCL = ONE;
    }

    for ( int I = 0; I < N; ++I) {
        if (XSCAL[I] < ZERO) {
            if (MPRERR >= 1) {
                printf("Error: Negative value in XSCAL (%5d) supplied\n", I);
            }
            IERR = 22;
        }
        if (fabs(XSCAL[I]) < EPMACH) {
            XSCAL[I] = DEFSCL;
        }
        if ((XSCAL[I] > ZERO) && (XSCAL[I] < SMALL)) {
            if (MPRERR >= 2) {
                printf("Warning: XSCAL(%5d) = %9.2f too small increased to %9.2f\n", I, XSCAL[I], SMALL);
            }
            XSCAL[I] = SMALL;
        }
        if (XSCAL[I]>GREAT) {
            if (MPRERR >= 2) {
                printf("Warning: XSCAL(%5d) = %9.2f too big decreased to %9.2f\n", I, XSCAL[I], GREAT);
            }
            XSCAL[I] = GREAT;
        }
    } // end for

    //Special dependence on Jacobian's storage mode for ML and MU
    int MSTOR = IOPT[4];
    if (MSTOR == 0) {
        IOPTU[6] = 0;
        IOPTU[7] = 0;
    } else if (MSTOR == 1) {
        IOPTU[6] = N - 1;
        IOPTU[7] = N - 1;
    } else {
        printf(" Error: Bad parameter for MSTOR (MSTOR = %d)\n", MSTOR);
        IERR = 23;
    }

    // Checks options
    for (int I = 0; I < 30; ++I) {
        if ((IOPT[I] < IOPTL[I]) || (IOPT[I] > IOPTU[I])) {
            IERR = 30;
            if (MPRERR >= 1) {
                printf(" Invalid option specified: IOPT(%2d)=%12ld range of permitted values is %8d to %8d\n",
                            I, IOPT[I], IOPTL[I], IOPTU[I]);
            }
        }
    } // end for

    return IERR;
} // End of subroutine N1PCheck

// ------------------------------------------------------------
/**
C     N 1 I N T : Core routine for NLEQ1 .
C     Damped Newton-algorithm for systems of highly nonlinear
C     equations especially designed for numerically sensitive
C     problems.
C
C*    Parameters:
C     ===========
C
C       N,FCN,JAC,X,XSCAL,RTOL
C                         See parameter description in driver routine
C
C       NITMAX      Int    Maximum number of allowed iterations
C       NONLIN      Int    Problem type specification
C                          (see IOPT-field NONLIN)
C       IOPT        Int    See parameter description in driver routine
C       IERR        Int    See parameter description in driver routine
C       LRWK        Int    Length of real workspace
C       RWK(LRWK)   Dble   Real workspace array
C       NRWKFR      Int    First free position of RWK on exit
C       LIWK        Int    Length of integer workspace
C       IWK(LIWK)   Int    Integer workspace array
C       NIWKFR      Int    First free position of IWK on exit
C       M1          Int    Leading dimension of Jacobian array A
C                          for full case Jacobian: N
C                          for banded Jacobian: 2*ML+MU+1;
C                          ML, MU see IOPT-description in driver routine
C       M2          Int    for full case Jacobian: N
C                          for banded Jacobian: ML+MU+1
C       NBROY       Int    Maximum number of possible consecutive
C                          iterative Broyden steps. (See IWK(36))
C       A(M1,N)     Dble   Holds the Jacobian matrix (decomposed form
C                          after call of linear decomposition
C                          routine)
C       DXSAVE(X,NBROY)
C                   Dble   Used to save the quasi Newton corrections of
C                          all previously done consecutive Broyden
C                          steps.
C       DX(N)       Dble   Current Newton correction
C       DXQ(N)      Dble   Simplified Newton correction J(k-1)*X(k)
C       XA(N)       Dble   Previous Newton iterate
C       XWA(N)      Dble   Scaling factors used for latest decomposed
C                          Jacobian for column scaling - may differ
C                          from XW, if Broyden updates are performed
C       F(N)        Dble   Function (FCN) value of current iterate
C       FA(N)       Dble   Function (FCN) value of previous iterate
C       ETA(N)      Dble   Jacobian approximation: updated scaled
C                          denominators
C       XW(N)       Dble   Scaling factors for iteration vector
C       FW(N)       Dble   Scaling factors for rows of the system
C       DXQA(N)     Dble   Previous Newton correction
C       T1(N)       Dble   Workspace for linear solvers and internal
C                          subroutines
C       T2(N)       Dble   Workspace array for internal subroutines
C       T3(N)       Dble   Workspace array for internal subroutines
C       FC          Dble   Current Newton iteration damping factor.
C       FCMIN       Dble   Minimum permitted damping factor. If
C                          FC becomes smaller than this value, one
C                          of the following may occur:
C                          a.    Recomputation of the Jacobian
C                                matrix by means of difference
C                                approximation (instead of Rank1
C                                update), if Rank1 - update
C                                previously was used
C                          b.    Fail exit otherwise
C       SIGMA       Dble   Decision parameter for rank1-updates
C       SIGMA2      Dble   Decision parameter for damping factor
C                          increasing to corrector value
C       FCA         Dble   Previous Newton iteration damping factor.
C       FCKEEP      Dble   Keeps the damping factor as it is at start
C                          of iteration step.
C       CONV        Dble   Scaled maximum norm of the Newton-
C                          correction. Passed to RWK-field on output.
C       SUMX        Dble   Square of the natural level (see equal-
C                          named IOPT-output field)
C       SUMXS       Dble   Square of the "simplified" natural level
C                          (see equal-named RWK-internal field)
C       DLEVF       Dble   Square of the standard level (see equal-
C                          named IOPT-output field)
C       MSTOR       Int    see description of IOPT-field MSTOR
C       MPRERR,MPRMON,MPRSOL,LUERR,LUMON,LUSOL,
C       NITER,NCORR,NFCN,NJAC,NFCNJ,NREJR1,NEW :
C                          See description of equal named IWK-fields
C                          in the driver subroutine
C       QBDAMP      Logic  Flag, that indicates, whether bounded damping
C                          strategy is active:
C                          true  = bounded damping strategy is active
C                          false = normal damping strategy is active
C
C*    Internal double variables
C     =========================
C
C       AJDEL    See RWK(26) (num. diff. without feedback)
C       AJMIN    See RWK(27) (num. diff. without feedback)
C       CONVA    Holds the previous value of CONV .
C       DMUE     Temporary value used during computation of damping
C                factors predictor.
C       EPDIFF   sqrt(10*epmach) (num. diff. with feedback)
C       ETADIF   See description of RWK(28) (num. diff. with feedback)
C       ETAINI   Initial value for all ETA-components (num. diff. fb.)
C       ETAMAX   Maximum allowed pertubation (num. diff. with feedback)
C       ETAMIN   Minimum allowed pertubation (num. diff. with feedback)
C       FCDNM    Used to compute the denominator of the damping
C                factor FC during computation of it's predictor,
C                corrector and aposteriori estimate (in the case of
C                performing a Rank1 update) .
C       FCK2     Aposteriori estimate of FC.
C       FCH      Temporary storage of new corrector FC.
C       FCMIN2   FCMIN**2 . Used for FC-predictor computation.
C       FCNUMP   Gets the numerator of the predictor formula for FC.
C       FCNMP2   Temporary used for predictor numerator computation.
C       FCNUMK   Gets the numerator of the corrector computation
C                of FC .
C       SUMXA    Natural level of the previous iterate.
C       TH       Temporary variable used during corrector- and
C                aposteriori computations of FC.
C
C*    Internal integer variables
C     ==========================
C
C     IFAIL      Gets the return value from subroutines called from
C                N1INT (N1FACT, N1SOLV, FCN, JAC)
C     ISCAL      Holds the scaling option from the IOPT-field ISCAL
C     MODE       Matrix storage mode (see IOPT-field MODE)
C     NRED       Count of successive corrector steps
C     NILUSE     Gets the amount of IWK used by the linear solver
C     NRLUSE     Gets the amount of RWK used by the linear solver
C     NIWLA      Index of first element of IWK provided to the
C                linear solver
C     NRWLA      Index of first element of RWK provided to the
C                linear solver
C     LIWL       Holds the maximum amount of integer workspace
C                available to the linear solver
C     LRWL       Holds the maximum amount of real workspace
C                available to the linear solver
C
C
C*    Internal logical variables
C     ==========================
C
C     QGENJ      Jacobian updating technique flag:
C                =true  : Call of analytical subroutine JAC or
C                           numerical differentiation
C                =false : rank1- (Broyden-) update
C     QINISC     Iterate initial-scaling flag:
C                =true  : at first call of N1SCAL
C                =false : at successive calls of N1SCAL
C     QSUCC      See description of IOPT-field QSUCC.
C     QJCRFR     Jacobian refresh flag:
C                set to true if damping factor gets too small
C                and Jacobian was computed by rank1-update.
C                Indicates, that the Jacobian needs to be recomputed
C                by subroutine JAC or numerical differentation.
C     QLINIT     Initialization state of linear solver workspace:
C                =false : Not yet initialized
C                =true  : Initialized - N1FACT has been called at
C                           least one time.
C     QSCALE     Holds the value of .NOT.QNSCAL. See description
C                of IOPT-field QNSCAL.
C
C*    Subroutines called:
C     ===================
C
C       N1FACT, N1SOLV, N1JAC,  N1JACB, N1JCF,  N1JCFB, N1LVLS,
C       N1SCRF, N1SCRB, N1SOUT, N1PRV1, N1PRV2, N1SCAL,
C       MONON,  MONOFF
C
C*    Functions called:
C     =================
C
C       D1MACH, WNORM
C
*/
void NonLinearEquationSolver::N1INT(int N, /*void (*FCN)(), void (*JAC)(), */
        double* X, double *XSCAL, double RTOL,
        int &NITMAX, int NONLIN,
        long int *IOPT, int &IERR,
        int LRWK, double* RWK, int NRWKFR, int LIWK, long int *IWK, int NIWKFR, int M1, int M2, int NBROY,
        double* A, double *DXSAVE, double *DX, double *DXQ, double *XA, double *XWA, double* F, double* FA, double* ETA,
        double* XW, double* FW, double* DXQA, double* T1, double* T2, double* T3,
        double FC, double FCMIN,
        double SIGMA, double SIGMA2, double FCA, double FCKEEP,
        double FCPRI, double DMYCOR,
        double &CONV, double &SUMX, double &DLEVF, int &MSTOR, int &MPRERR,
        int MPRMON, int MPRSOL,
        int LUERR, int LUMON, int LUSOL,
        long int &NITER, long int &NCORR, long int &NFCN, long int &NJAC, long int &NFCNJ,
        long int &NREJR1, long int &NEW, bool QBDAMP) {
#define A(i, j) A[i * N + j]
#define DXSAVE(I, J) DXSAVE[I * NBROY + J]
#define IOPT(i)  IOPT[i]

    // ----------------------------------------------------------
    // 1 Initialization
    // ----------------------------------------------------------
    // 1.1 Control-flags and -integers
    bool QSUCC = IOPT[1] == 1;
    bool QSCALE = !(IOPT[35] == 1);
    bool QORDI  = IOPT[33] == 1;
    bool QSIMPL = IOPT[34] == 1;
    bool QRANK1 = IOPT[32] == 1;

    int IORMON = IOPT[39];
    if (IORMON == 0) IORMON=2;

    int ISCAL = IOPT[9];
    int MODE = IOPT[2];
    int JACGEN = IOPT[3];
    bool QMIXIO = (LUMON == LUSOL) && (MPRMON != 0) && (MPRSOL != 0);
    bool QLU    = !QSIMPL;
    int MPRTIM = IOPT[19];

    double AJDEL = 0, AJMIN = 0, CONVA = 0;
    double DLEVXA, DXANRM;
    double FCDNM, /*FCMIN2,*/FCNUMP = 0, FCCOR, FCNMP2, FCH, FCMON;
    double FCK2, FCNUMK, FCREDU, DLEVFN = 0, SUMXA = 0, TH;
    double CLIN0, CLIN1 = 0, CALPHA = 0, CALPHK, ALPHAE, ALPHAK = 0, ALPHAA = 0, SUMXA0 = 0, SUMXA1 = 0, SUMXA2, SUMXTE;


    int ICONV;
    bool QMSTOP = false;
    long int IFAIL;
    bool QGENJ, QINISC;
    int NIWLA = 0, NRWLA = 0, NILUSE = 0, NRLUSE = 0;
    int IDUMMY = 0;
    int LIWL = 0, LRWL = 0;
    double SUMXS;

    IERR = 0;

    bool done = false;  // main iteration loop exit flag

    // ----------------------------------------------------------
    // 1.2 Derivated dimensional parameters
    int ML = 0, MU = 0;
    if (MSTOR == 0) {
        ML = 0;
    } else if (MSTOR == 1) {
        ML = M1 - M2;
        MU = M2 - 1 - ML;
    }

    // ----------------------------------------------------------
    // 1.3 Derivated internal parameters
    double FCMIN2 = FCMIN * FCMIN;
    double RSMALL = sqrt(TEN * RTOL);

    // ----------------------------------------------------------
    // 1.4 Adaption of input parameters, if necessary
    if (FC < FCMIN) FC = FCMIN;
    if (FC > ONE) FC = ONE;

    // ----------------------------------------------------------
    // 1.5 Initial preparations
    bool QJCRFR = false;
    bool QLINIT = false;
    IFAIL = 0;
    double FCBND = ZERO;
    if (QBDAMP) FCBND = RWK[20];

    // ----------------------------------------------------------
    // 1.5.1 Numerical differentiation related initializations
    double ETADIF = 0, ETAINI = 0, ETAMIN = 0, ETAMAX = 0, EPDIFF = 0;
    if (JACGEN == 2) {
          AJDEL = RWK[26];
          if (AJDEL <= SMALL) AJDEL = sqrt(EPMACH*TEN);
          AJMIN = RWK[27];
    } else if (JACGEN == 3) {
          ETADIF = RWK[28];
          if (ETADIF <= SMALL) ETADIF = 1.0e-6;
          ETAINI = RWK[29];
          if (ETAINI <= SMALL) ETAINI = 1.0e-6;
          EPDIFF = sqrt(EPMACH*TEN);
          ETAMAX = sqrt(EPDIFF);
          ETAMIN = EPDIFF * ETAMAX;
    } else {
        //
    }

    // ----------------------------------------------------------
    // 1.5.2 Miscellaneous preparations of first iteration step
    if (!QSUCC) {
        NITER = 0;
        NCORR = 0;
        NREJR1 = 0;
        NFCN = 0;
        NJAC = 0;
        NFCNJ = 0;
        QGENJ = true;
        QINISC = true;
        FCKEEP = FC;
        FCA = FC;
        FCPRI = FC;
        FCK2 = FC;
        FCMON = FC;
        CONV = ZERO;
        if (JACGEN == 3) {
            for (int L1 = 0; L1 < N; ++L1) {
                ETA[L1] = ETAINI;
            } // end for
        }
        for (int L1 = 0; L1 < N; ++L1) {
            XA[L1] = X[L1];
        }

        ICONV = 0;
        ALPHAE = ZERO;
        SUMXA1 = ZERO;
        SUMXA0 = ZERO;
        CLIN0  = ZERO;
        QMSTOP = false;

        //------------------------------------------------------
        // 1.6 Print monitor header
        if ((MPRMON >= 2) && (!QMIXIO)) {
            printf("******************************************************************\n");
            printf("        It        Normf           Normx         Damp.Fct.   New\n");
        }

        //--------------------------------------------------------
        //1.7 Startup step
        // --------------------------------------------------------
        //1.7.1 Computation of the residual vector
        if (MPRTIM!=0)  MONON(1)
            IFAIL = Function(N, X, F);
            if (MPRTIM!=0)  MONOFF(1);
            NFCN = NFCN+1;
            // Exit, if ...
            if (IFAIL != 0) {
                IERR = 82;  // NonLinearSolverExitCode::ErrorUserFcn;  // (82)
                done = true;
            }
    } else {
        QINISC = false;
    } // end if (!QSUCC) {

    //
    // Main iteration loop
    // ===================
    while ((IERR == 0) && (!done)) {

#ifdef DEBUG_STEP
printf("DEBUG X = %f %f %f %f %f %f %f\n", X[0], X[1], X[2], X[3], X[4], X[5], X[6]);
#endif

        // --------------------------------------------------------
        // 2 Startup of iteration step
        if (!QJCRFR) {
            // ------------------------------------------------------
            // 2.1 Scaling of variables X(N)
            N1SCAL(N, X, XA, XSCAL, XW, ISCAL, QINISC, IOPT, LRWK, RWK);
            QINISC = false;
            if (NITER != 0) {
                // ----------------------------------------------------
                // 2.2 Aposteriori estimate of damping factor
                for (int L1 = 0; L1 < N; ++L1) {
                    DXQA[L1] = DXQ[L1];
                }
                if (!QORDI) {
                    FCNUMP = ZERO;
                    for (int L1 = 0; L1 < N; ++L1) {
                        FCNUMP = FCNUMP + (DX[L1] / XW[L1]) * (DX[L1] / XW[L1]);
                    }
                    TH = FC - ONE;
                    FCDNM = ZERO;
                    for (int L1 = 0; L1 < N; ++L1) {
                        FCDNM = FCDNM + ((DXQA[L1] + TH * DX[L1]) / XW[L1]) * ((DXQA[L1] + TH * DX[L1]) / XW[L1]);
                    }

                    // --------------------------------------------------
                    // 2.2.2 Decision criterion for Jacobian updating technique:
                    //      QGENJ==true numerical differentation,
                    //      QGENJ==false rank1 updating
                    QGENJ = true;
                    if (fabs(FC - FCPRI) < EPMACH) {
                        QGENJ = (FC < ONE) || (FCA < ONE) || (DMYCOR <= FC * SIGMA) || (!QRANK1) || (NEW + 2 > NBROY);
                        FCA = FC;
                    } else {
                        DMYCOR = FCA * FCA * HALF * sqrt(FCNUMP / FCDNM);
                        if (NONLIN <= 3) {
                            FCCOR = std::min(ONE, DMYCOR);
                        } else {
                            FCCOR = std::min(ONE, HALF * DMYCOR);
                        }
                        FCA = std::max(std::min(FC, FCCOR), FCMIN);

                        if (MPRMON >= 5) {
                            printf(" +++ aposteriori estimate +++\n");
                            printf(" FCCOR  = %18.10g  FC     = %18.10g\n", FCCOR, FC);
                            printf(" DMYCOR = %18.10g  FCNUMP = %18.10g\n", DMYCOR, FCNUMP);
                            printf(" FCDNM  = %18.10g\n", FCDNM);
                            printf(" ++++++++++++++++++++++++++++\n");
                        }
                    }
                    FCK2 = FCA;
                    // ------------------------------------------------------
                    // 2.2.1 Computation of the numerator of damping factor predictor
                    FCNMP2 = ZERO;
                    for (int L1 = 0; L1 < N; ++L1) {
                        FCNMP2 = FCNMP2 + (DXQA[L1] / XW[L1]) * (DXQA[L1]/XW[L1]);
                    }
                    FCNUMP = FCNUMP * FCNMP2;
                }
            }
        }
        QJCRFR = false;

        // --------------------------------------------------------
        // 2.3 Jacobian matrix (stored to array A(M1,N))
        // --------------------------------------------------------
        // 2.3.1 Jacobian generation by routine JAC or difference approximation (If QGENJ==true)
        // - or -
        // Rank-1 update of Jacobian (If QGENJ==false)
        if (QGENJ && (!QSIMPL || NITER==0)) {
            NEW = 0;
            if (JACGEN == 1) {
                if (MPRTIM!=0)  MONON(2);

                IFAIL = Jacobian(N, M1, X, A);

                if (MPRTIM != 0)  MONOFF(2);
            } else {
                if (MSTOR == 0) {
                    if (MPRTIM!=0)  MONON(2);
                    if (JACGEN == 3) {
                        N1JCF(/*FCN,*/N, N, X, F, A, XW, ETA,
                            ETAMIN, ETAMAX,
                            ETADIF, CONV, NFCNJ, T1, IFAIL);

                    } else if (JACGEN == 2) {
                        N1JAC(/*FCN,*/ N, N, X, F, A, XW,
                            AJDEL, AJMIN,
                            NFCNJ, T1, IFAIL);

                    }

                    if (MPRTIM != 0)  MONOFF(2);
                } else if (MSTOR == 1) {
                    if (MPRTIM != 0)  MONON(2);
                    if (JACGEN == 3) {

                        N1JCFB(/*FCN,*/N, M1, ML, X, F, A, XW,
                             ETA,
                             ETAMIN,
                             ETAMAX, ETADIF, CONV, NFCNJ, T1, T2, T3, IFAIL);

                    } else if (JACGEN == 2) {

                        N1JACB(/*FCN, */N, M1, ML, X, F,
                              A,
                              XW,  AJDEL, AJMIN, NFCNJ, T1, T2, T3,
                              IFAIL);

                    }

                    if (MPRTIM!=0)  MONOFF(2);
                }
            }

            NJAC = NJAC + 1;

            // Exit, If ...
            if ((JACGEN == 1) && (IFAIL < 0)) {
                IERR = 83;  //NonLinearSolverExitCode::ErrorUserJac;  // (83)
                break;
            }

            if ((JACGEN !=1 ) && (IFAIL!=0)) {
                IERR = 82;
                break;
            }
        } else if (!QSIMPL) {
            NEW = NEW + 1;
        } // end if (QGENJ && (!QSIMPL || NITER==0)) {

#ifdef DEBUG
printf("DEBUG QGENJ = %d, QSIMPL = %d, NITER = %d\n", QGENJ, QSIMPL, NITER == 0);
printf("DEBUG MSTOR = %d\n", MSTOR);
printf("DEBUG AJDEL = %12.10g, AJMIN = %12.10g\n", AJDEL, AJMIN);
printf("DEBUG JACGEN = %d\n", JACGEN);
printf("DEBUG IERR = %d\n", IERR);
#endif

#ifdef DEBUG_STEP
//printf("DEBUG X = %f %f %f %f %f %f %f\n", X[0], X[1], X[2], X[3], X[4], X[5], X[6]);
printf("DEBUG F = %12.10g %12.10g %12.10g %12.10g %12.10g %12.10g %12.10g\n", F[0], F[1], F[2], F[3], F[4], F[5], F[6]);
#endif

#ifdef DEBUG_JACOBIAN
printf("DEBUG A = ");
for (int i=0; i < N; ++i) {
    for (int j=0; j < N; ++j) {
        printf("%12.8g ", A(i, j));
    }
    printf("\n");
}
#endif

        if ( (NEW == 0) && (QLU || NITER == 0) ) {
            // ------------------------------------------------------
            //  2.3.2.1 Save scaling values
            for (int L1 = 0; L1 < N; ++L1) {
                XWA[L1] = XW[L1];
            }
            // ------------------------------------------------------
            //  2.3.2.2 Prepare Jac. for use by band-solver DGBFA/DGBSL
            if (MSTOR == 1) {
                for (int L1 = 0; L1 < N; ++L1) {
                    for (int L2 = M2 - 1; L2 >= 0; --L2) {
                        A(L2 + ML, L1) = A(L2, L1);
                    }
                }
            }

            // ------------------------------------------------------
            //  2.4 Prepare solution of the linear system
            // ------------------------------------------------------
            //  2.4.1 internal column scaling of matrix A
            if (MSTOR == 0) {
                for (int K = 0; K < N; ++K) {
                    double S1 = -XW[K];
                    for (int L1 = 0; L1 < N; ++L1) {
                        A(L1, K) = A(L1, K) * S1;
                    }
                }
            } else if (MSTOR == 1) {
                for (int K = 0; K < N; ++K) {
                    int L2 = std::max(1 + M2 - K, ML + 1);
                    int L3 = std::min(N + M2 - K, M1);
                    double S1 = -XW[K];
                    for (int L1 = L2; L1 < L3; ++L1) {
                        A(L1, K) = A(L1, K) * S1;
                    }
                }
            }

            // ----------------------------------------------------
            //  2.4.2 Row scaling of matrix A
            if (QSCALE) {
                if (MSTOR == 0) {
                   N1SCRF(N, N, A, FW);
                } else if (MSTOR == 1) {
                   N1SCRB(N, M1, ML, MU, A, FW);
                }
            } else {
                for (int K = 0; K < N; ++K) {
                    FW[K] = ONE;
                }
            }
        }

        // --------------------------------------------------------
        // 2.4.3 Save and scale values of F(N)
        for (int L1 = 0; L1 < N; ++L1) {
            FA[L1] = F[L1];
            T1[L1] = F[L1] * FW[L1];
        }

        // --------------------------------------------------------
        // 3 Central part of iteration step
        // --------------------------------------------------------
        // 3.1 Solution of the linear system
        // --------------------------------------------------------
        // 3.1.1 Decomposition of (N,N)-matrix A
        if (!QLINIT) {
            NIWLA = IWK[18] + 1;
            NRWLA = IWK[19] + 1;
            LIWL = LIWK - NIWLA + 1;
            LRWL = LRWK - NRWLA + 1;
        }
        if ( (NEW == 0) && (QLU || (NITER == 0))) {
            if (MPRTIM != 0)  MONON(3);

             N1FACT(N, M1, ML, MU, A, IOPT, IFAIL, LIWL, IWK + NIWLA,
                       NILUSE, LRWL, RWK + NRWLA, NRLUSE);

            if (MPRTIM != 0)  MONOFF(3);

            if (!QLINIT) {
                NIWKFR = NIWKFR + NILUSE;
                NRWKFR = NRWKFR + NRLUSE;
                // Store lengths of currently required workspaces
                IWK[18] = NIWKFR - 1;
                IWK[19] = NRWKFR - 1;
            }

            // Exit Repeat If ...
            if (IFAIL != 0) {
                if (IFAIL == 1) {
                    IERR = 1;
                } else {
                    IERR = 80;
                }
                break;
            }
        }
        QLINIT = true;

        // --------------------------------------------------------
        // 3.1.2 Solution of linear (N,N)-system
        if (NEW == 0) {
            if (MPRTIM!=0)  MONON(4);

            N1SOLV(N, M1, ML, MU, A, T1, IOPT, IFAIL, LIWL, IWK + NIWLA,
                      IDUMMY, LRWL, RWK + NRWLA, IDUMMY);
            if (MPRTIM != 0)  MONOFF(4);

            // Exit Repeat If ...
            if (IFAIL != 0)  {
              IERR = 81;
              break;
            }
        } else {
            double ALFA1 = ZERO;
            double ALFA2 = ZERO;
            for (int I = 0; I < N; ++I) {
                ALFA1 = ALFA1 + DX[I]*DXQ[I] / (XW[I]*XW[I]);
                ALFA2 = ALFA2 + DX[I]*DX[I] / (XW[I]*XW[I]);
            }
            double ALFA = ALFA1 / ALFA2;
            double BETA = ONE - ALFA;
            for (int I = 0; I < N; ++I) {
                T1[I] = (DXQ[I] + (FCA - ONE)*ALFA*DX[I]) / BETA;
            }
            if (NEW == 1) {
                for (int I = 0; I < N; ++I) {
                    DXSAVE(I, 1) = DX[I];
                }
            }

            for (int I = 0; I < N; ++I) {
                DXSAVE(I, NEW + 1) = T1[I];
                DX[I] = T1[I];
                T1[I] = T1[I]/XW[I];
            }
        }

        // --------------------------------------------------------
        // 3.2 Evaluation of scaled natural level function SUMX
        //    scaled maximum error norm CONV
        //    evaluation of (scaled) standard level function
        //    DLEVF ( DLEVF only, if MPRMON>=2 )
        //    and computation of ordinary Newton corrections
        //    DX(N)
        if (!QSIMPL) {
             N1LVLS(N, T1, XW, F, DX, CONV, SUMX, DLEVF, MPRMON, NEW == 0);
        } else {
             N1LVLS(N, T1, XWA, F, DX, CONV, SUMX, DLEVF, MPRMON, NEW == 0);
        }

#ifdef DEBUG
printf("DEBUG XW = %f %f %f %f %f %f %f\n", XW[0], XW[1], XW[2], XW[3], XW[4], XW[5], XW[6]);
printf("DEBUG XWA = %f %f %f %f %f %f %f\n", XWA[0], XWA[1], XWA[2], XWA[3], XWA[4], XWA[5], XWA[6]);
printf("DEBUG T1 = %f %f %f %f %f %f %f\n", T1[0], T1[1], T1[2], T1[3], T1[4], T1[5], T1[6]);
printf("DEBUG DX = %f %f %f %f %f %f %f\n", DX[0], DX[1], DX[2], DX[3], DX[4], DX[5], DX[6]);
printf("DEBUG SUMX = %f\n", SUMX);
#endif

        for (int L1 = 0; L1 < N; ++L1) {
            XA[L1] = X[L1];
        }
        SUMXA = SUMX;
        DLEVXA = sqrt(SUMXA / static_cast<double>(N));
        CONVA = CONV;
        DXANRM = WNORM(N, DX, XW);

        // --------------------------------------------------------
        // 3.3 A - priori estimate of damping factor FC
        if ((NITER != 0) && (NONLIN != 1) && (NEW == 0) && (!QORDI)) {
            // ------------------------------------------------------
            // 3.3.1 Computation of the denominator of a-priori estimate
            FCDNM = ZERO;
            for (int L1 = 0; L1 < N; ++L1) {
                FCDNM = FCDNM + ((DX[L1] - DXQA[L1]) / XW[L1]) * ((DX[L1] - DXQA[L1]) / XW[L1]);
            }
            FCDNM = FCDNM * SUMX;
            //------------------------------------------------------
            // 3.3.2 New damping factor
            double DMYPRI;
            if ((FCDNM > FCNUMP*FCMIN2) || ((NONLIN == 4) && (FCA * FCA * FCNUMP < 4.0*FCDNM))) {
                DMYPRI = FCA * sqrt(FCNUMP / FCDNM);
                FCPRI = std::min(DMYPRI, ONE);

              if (NONLIN == 4) FCPRI = std::min(HALF * DMYPRI, ONE);

            } else {
                FCPRI = ONE;
                DMYPRI = -1.0;
            }

            if (MPRMON >= 5) {
                printf(" +++ apriori estimate +++', \n");
                printf(" FCPRI  = %18.10g  FC     = %18.10g\n", FCPRI, FC);
                printf(" FCA    = %18.10g  DMYPRI = %18.10g\n", FCA, DMYPRI);
                printf(" FCNUMP = %18.10g  FCDNM  = %18.10g\n", FCNUMP, FCDNM);
                printf(" ++++++++++++++++++++++++\n");
            }
//C$Test-end
            FC = std::max(FCPRI, FCMIN);
            if (QBDAMP) {
                double FCBH = FCA*FCBND;
                if (FC > FCBH) {
                    FC = FCBH;
                    if (MPRMON >= 4)
                        printf(" *** incr. rest. act. (a prio) ***\n");
                }
                FCBH = FCA/FCBND;
                if (FC < FCBH) {
                    FC = FCBH;
                    if (MPRMON >= 4)
                        printf(" *** decr. rest. act. (a prio) ***\n");
                }
            }
        }
//CWEI
        if (IORMON >= 2) {
            SUMXA2 = SUMXA1;
            SUMXA1 = SUMXA0;
            SUMXA0 = DLEVXA;
            if (fabs(SUMXA0 - ZERO) < EPMACH) SUMXA0 = SMALL;
            // Check convergence rates (linear and superlinear)
            // ICONV : Convergence indicator
            // =0: No convergence indicated yet
            // =1: Damping factor is 1.0d0
            // =2: Superlinear convergence detected (alpha >=1.2)
            // =3: Quadratic convergence detected (alpha > 1.8)
            FCMON = std::min(FC, FCMON);
            if (FCMON < ONE) {
                ICONV = 0;
                ALPHAE = ZERO;
            }

            if ((fabs(FCMON - ONE) < EPMACH) && (ICONV == 0)) ICONV = 1;

            if (NITER >= 1) {
                CLIN1 = CLIN0;
                CLIN0 = SUMXA0/SUMXA1;
            }
            if ((ICONV >= 1) && (NITER >= 2)) {
                ALPHAK = ALPHAE;
                ALPHAE = ZERO;
                if (CLIN1 <= 0.95) ALPHAE = log(CLIN0) / log(CLIN1);
                if (fabs(ALPHAK) > ZERO) ALPHAK = 0.5 * (ALPHAE + ALPHAK);

                ALPHAA = std::min(ALPHAK, ALPHAE);
                CALPHK = CALPHA;
                CALPHA = ZERO;

                if (fabs(ALPHAE) > ZERO) CALPHA = SUMXA1 / pow(SUMXA2, ALPHAE);

                SUMXTE = sqrt(CALPHA * CALPHK) * pow(SUMXA1, ALPHAK) - SUMXA0;
                if ((ALPHAA >= 1.2) && (ICONV == 1)) ICONV = 2;
                if (ALPHAA > 1.8) ICONV = 3;
                if (MPRMON >= 4) {
                    printf(" ** ICONV: %d  ALPHA: %9.2f\n", ICONV, ALPHAE);
                    printf("    CONST-ALPHA: %9.2f  CONST-LIN: %9.2f **\n", CALPHA, CLIN0);
                    printf(" ** ALPHA-POST: %9.2f CHECK: %9.2f", ALPHAK, SUMXTE);
                    printf("*************************\n");
                }

                if ( (ICONV >= 2) && (ALPHAA < 0.9) ) {
                    if (IORMON == 3) {
                        IERR = 4;
                        break;
                    } else {
                        QMSTOP = true;
                    }
                }
            }
        }
        FCMON = FC;

        // --------------------------------------------------------
        // 3.4 Save natural level for later computations of corrector and print iterate
        FCNUMK = SUMX;
        if (MPRMON >= 2) {
            if (MPRTIM != 0)  MONON(5);

            N1PRV1(DLEVF, DLEVXA, FCKEEP, NITER, NEW, MPRMON, LUMON, QMIXIO);

            if (MPRTIM != 0)  MONOFF(5);
        }
        int NRED = 0;
        bool QNEXT = false;
        bool QREP  = false;

//        QREP = ITER > ITMAX   or  QREP = ITER > 0
        //
        //  ================================
        //  Damping-factor reduction loop
        while (!(QNEXT || QJCRFR)) {
            //    ------------------------------------------------------
            // 3.5 Preliminary new iterate
            for (int L1 = 0; L1 < N; ++L1) {
                X[L1] = XA[L1] + DX[L1] * FC;
            }

            // -----------------------------------------------------
            // 3.5.2 Exit, if problem is specified as being linear
            // Exit Repeat If ...
            if ( NONLIN == 1 ) {
                IERR = 0;
                break;
            }

            // ------------------------------------------------------
            // 3.6.1 Computation of the residual vector
            if (MPRTIM != 0)  MONON(1)
            IFAIL = Function(N, X, F);

            if (MPRTIM != 0)  MONOFF(1);

            NFCN = NFCN + 1;

            //     Exit, If ...
            if (IFAIL < 0) {
                IERR = 82;
                break;
            }
            if ((IFAIL == 1) || (IFAIL == 2)) {
                if (QORDI) {
                    IERR = 82;
                    break;
                }
                if (IFAIL == 1) {
                    FCREDU = HALF;
                } else {
                    FCREDU = F[0];

                    // Exit, If ...
                    if ((FCREDU <= 0) || (FCREDU >= 1)) {
                        IERR = 82;
                        done = true;
                        break;  // leave Damping-factor reduction loop
                    }
                }

                if (MPRMON >= 2) {
                    printf("FCN could not be evaluated  %ld %7.5f    %2ld\n", NITER, FC, NEW);
                }
                FCH = FC;
                FC = FCREDU * FC;
                if (FCH > FCMIN) FC = std::max(FC, FCMIN);
                if (QBDAMP) {
                    double FCBH = FCH / FCBND;
                    if (FC < FCBH) {
                        FC = FCBH;
                        if (MPRMON >= 4) printf(" *** decr. rest. act. (FCN redu.) ***\n");
                    }
                }
                if (FC < FCMIN) {
                    IERR = 3;
                    done = true;
                    break;  // leave Damping-factor reduction loop
                }

                // Break DO (Until) ...
                break;
            }

            if (QORDI) {
                //  -------------------------------------------------------
                // 3.6.2 Convergence test for ordinary Newton iteration
                // Exit Repeat If ...
                if (DXANRM <= RTOL) {
                    IERR = 0;
                    break;
                }
            } else {
                for (int L1 = 0; L1 < N; ++L1) {
                    T1[L1] = F[L1] * FW[L1];
                }

                // --------------------------------------------------
                // 3.6.3 Solution of linear (N,N)-system
                if (MPRTIM != 0)  MONON(4);

                N1SOLV(N, M1, ML, MU, A, T1, IOPT, IFAIL, LIWL, IWK + NIWLA, IDUMMY, LRWL, RWK + NRWLA, IDUMMY);

                if (MPRTIM != 0)  MONOFF(4);

                // Exit Repeat If ...
                if (IFAIL != 0)  {
                    IERR = 81;
                    break;
                }
                if (NEW > 0) {
                    for (int I = 0; I < N; ++I) {
                        DXQ[I] = T1[I] * XWA[I];
                    }

                    for (int ILOOP = 0; ILOOP < NEW; ++ILOOP) {
                        double SUM1 = ZERO;
                        double SUM2 = ZERO;
                        for (int I = 0; I < N; ++I) {
                            SUM1 = SUM1 + (DXQ[I] * DXSAVE(I, ILOOP)) / (XW[I] * XW[I]);
                            SUM2 = SUM2 + (DXSAVE(I, ILOOP) / XW[I]) * (DXSAVE(I, ILOOP) / XW[I]);
                        }
                        double BETA = SUM1 / SUM2;
                        for (int I = 0; I < N; ++I) {
                            DXQ[I] = DXQ[I] + BETA * DXSAVE(I, ILOOP + 1);
                            T1[I] = DXQ[I] / XW[I];
                        }
                    }
                }

                // ----------------------------------------------------
                //  3.6.4 Evaluation of scaled natural level function
                //        SUMX
                //        scaled maximum error norm CONV and evaluation
                //        of (scaled) standard level function DLEVFN
                if (!QSIMPL) {
                     N1LVLS(N, T1, XW, F, DXQ, CONV, SUMX, DLEVFN, MPRMON, NEW == 0);
                } else {
                     N1LVLS(N, T1, XWA, F, DXQ, CONV, SUMX, DLEVFN, MPRMON, NEW == 0);
                }
                double DXNRM = WNORM(N, DXQ, XW);

                // -----------------------------------------------------
                //  3.6.5 Convergence test
                // Exit Repeat If ...
                if (( DXNRM <= RTOL) && (DXANRM <= RSMALL) && (fabs(FC - ONE) < EPMACH)) {
                    IERR = 0;
                    done = true;
                    break;
                }

                FCA = FC;

                // ----------------------------------------------------
                //  3.6.6 Evaluation of reduced damping factor
                double TH = FCA - ONE;
                double FCDNM = ZERO;
                for (int L1=0; L1 < N; ++L1) {
                    FCDNM = FCDNM + ((DXQ[L1] + TH*DX[L1]) / XW[L1]) * ((DXQ[L1] + TH*DX[L1]) / XW[L1]);
                }
                if (fabs(FCDNM) > ZERO) {
                    DMYCOR = FCA * FCA * HALF * sqrt(FCNUMK / FCDNM);
                } else {
                    DMYCOR = 1.0e+35;
                }
                if (NONLIN <= 3) {
                    FCCOR = std::min(ONE, DMYCOR);
                } else {
                    FCCOR = std::min(ONE, HALF * DMYCOR);
                }
//C$Test-begin
                if (MPRMON >= 5) {
                    printf(" +++ corrector computation +++\n");
                    printf(" FCCOR  = %18.10g  FC     = %18.10g\n", FCCOR, FC);
                    printf(" DMYCOR = %18.10g  FCNUMK = %18.10g\n", DMYCOR, FCNUMK);
                    printf(" FCDNM  = %18.10g  FCA    = %18.10g\n", FCDNM, FCA);
                    printf(" +++++++++++++++++++++++++++++\n");
                }
//C$Test-end
            }

            // ------------------------------------------------------
            // 3.7 Natural monotonicity test
            if ((SUMX > SUMXA) && (!QORDI)) {
                //  ----------------------------------------------------
                // 3.8 Output of iterate
                if (MPRMON >= 3) {
                    if (MPRTIM != 0)  MONON(5);

                    N1PRV2(DLEVFN, sqrt(SUMX/double(N)), FC, NITER, MPRMON, LUMON, QMIXIO, '*');

                    if (MPRTIM != 0)  MONOFF(5);
                }
                if (QMSTOP) {
                    IERR = 4;
                    done = true;
                    break;
                }
                double FCH = std::min(FCCOR, HALF * FC);
                if (FC > FCMIN) {
                    FC = std::max(FCH, FCMIN);
                } else {
                    FC = FCH;
                }
                if (QBDAMP) {
                    double FCBH = FCA/FCBND;
                    if (FC < FCBH) {
                        FC = FCBH;
                        if (MPRMON >= 4)
                            printf("*** decr. rest. act. (a post) ***\n");
                    }
                }
//CWEI
                FCMON = FC;

//C$Test-begin
                if (MPRMON >= 5) {
                    printf(" +++ corrector setting 1 +++\n");
                    printf(" FC     = %18.10g\n", FC);
                    printf(" +++++++++++++++++++++++++++\n");
                }
//C$Test-end
                QREP = true;
                NCORR = NCORR + 1;
                NRED = NRED + 1;

                // ----------------------------------------------------
                // 3.10 If damping factor is too small:
                // Refresh Jacobian,if current Jacobian was computed
                // by a Rank1-update, else fail exit
                QJCRFR  = (FC < FCMIN) || ((NEW > 0) && (NRED > 1));
                // Exit Repeat If ...
                if (QJCRFR && (NEW == 0)) {
                    IERR = 3;
                    done = true;
                    break;
                }
            } else {
                if (!QORDI && !QREP && (FCCOR > SIGMA2 * FC)) {
                    if (MPRMON >= 3) {
                        if (MPRTIM != 0)  MONON(5);
                        N1PRV2(DLEVFN, sqrt(SUMX/double(N)), FC, NITER, MPRMON, LUMON, QMIXIO, '+');
                        if (MPRTIM != 0)  MONOFF(5);
                    }
                    FC = FCCOR;
//C$Test-begin
                    if (MPRMON >= 5) {
                        printf(" +++ corrector setting 2 +++\n");
                        printf(" FC     = %18.10g\n", FC);
                        printf(" +++++++++++++++++++++++++++\n");
                    }
//C$Test-end
                    QREP = true;
                } else {
                    QNEXT = true;
                }
            }
        } // end while (!(QNEXT || QJCRFR))
        // UNTIL ( expression - negated above)
        //  End of damping-factor reduction loop
        //  =======================================

        if (QJCRFR) {
            //    ------------------------------------------------------
            //    3.11 Restore former values for repeting iteration step
            NREJR1 = NREJR1 + 1;
            for (int L1=0; L1 < N; ++L1) {
                X[L1] = XA[L1];
            }
            for (int L1 = 0; L1 < N; ++L1) {
                F[L1] = FA[L1];
            }
            if (MPRMON >= 2) {
                printf(" Not accepted damping factor %ld %7.5f    %2ld\n", NITER, FC, NEW);
            }
            FC = FCKEEP;
            FCA = FCK2;
            if (NITER == 0) {
                FC = FCMIN;
            }
            QGENJ = true;
        } else {
            // ------------------------------------------------------
            // 4 Preparations to start the following iteration step
            // ------------------------------------------------------
            // 4.1 Print values
            if ((MPRMON >= 3) && (!QORDI)) {
                if (MPRTIM != 0)  MONON(5);

                N1PRV2(DLEVFN, sqrt(SUMX / double(N)), FC, NITER + 1, MPRMON, LUMON, QMIXIO, '*');

                if (MPRTIM!=0)  MONOFF(5);
            }

            // Print the natural level of the current iterate and return
            // it in one-step mode
            SUMXS = SUMXA;
            SUMX = SUMXA;
            if ((MPRSOL >= 2) && (NITER != 0)) {
                if (MPRTIM != 0)  MONON(5);

                N1SOUT(N, XA, 2, IOPT, RWK, LRWK, IWK, LIWK, MPRSOL, LUSOL);

                if (MPRTIM != 0)  MONOFF(5);
            } else if ((MPRSOL >= 1) && (NITER == 0)) {
                if (MPRTIM != 0)  MONON(5);
                N1SOUT(N, XA, 1, IOPT, RWK, LRWK, IWK, LIWK, MPRSOL, LUSOL);
                if (MPRTIM != 0)  MONOFF(5);
            }
            NITER = NITER + 1;
            DLEVF = DLEVFN;

            // Exit Repeat If ...
            if (NITER >= NITMAX) {
                IERR = 2;
                done = true;
                break;
            }

            FCKEEP = FC;

            // ------------------------------------------------------
            // 4.2 Return, if in one-step mode
            //
            // Exit Subroutine If ...
            if (MODE == 1) {
                IWK[18] = NIWLA-1;
                IWK[19] = NRWLA-1;
                IOPT[1] = 1;
                return;
            }
        }
    }  // end while ((IERR == 0) && (!done)) {

    // ==========================
    // ----------------------------------------------------------
    // 9 Exits
    // ----------------------------------------------------------
    // 9.1 Solution exit
    //
    double APREC = -1.;

    if ((IERR == 0) || (IERR == 4)) {
        if (NONLIN != 1) {
            if (!QORDI) {
                if ( IERR == 0 ) {
                    for (int L1 = 0; L1 < N; ++L1) {
                        X[L1] = X[L1] + DXQ[L1];
                    }
                } else {
                    APREC = sqrt(SUMXA / static_cast<double>(N));
                    if ((ALPHAA > ZERO) && (IORMON == 3)) {
                        for (int L1 = 0; L1 < N; ++L1) {
                            X[L1] = X[L1] + DX[L1];
                        }
                    }
                }

                // Print final monitor output
                if (MPRMON >= 2) {
                    if (IERR == 0) {
                        if (MPRTIM != 0)  MONON(5);

                        N1PRV2(DLEVFN, sqrt(SUMX / static_cast<double>(N)), FC, NITER + 1, MPRMON, LUMON, QMIXIO, '*');

                        if (MPRTIM != 0)  MONOFF(5);
                    } else if (IORMON == 3) {
                        if (MPRTIM != 0)  MONON(5);

                        N1PRV1(DLEVFN, sqrt(SUMXA / double(N)), FC, NITER, NEW, MPRMON, LUMON, QMIXIO);

                        if (MPRTIM != 0)  MONOFF(5);
                    }
                }

                if (IORMON >= 2) {
                    if (ICONV <= 1 && (fabs(ALPHAE) > ZERO) && (fabs(ALPHAK) > ZERO)) IERR = 5;
                }
            } else {
                if (QORDI) {
                    APREC = sqrt(SUMXA / double(N));
                }
            }

            if (MPRMON >= 1) {
                if (QORDI || (IERR == 4)) {
                    printf(" Solution of nonlinear system of equations obtained within %3ld iteration steps\n", NITER);
                    printf(" Achieved relative accuracy %10.3g\n", APREC);
                } else {
                    printf(" Solution of nonlinear system of equations obtained within %3ld iteration steps\n", NITER+1);
                    printf(" Achieved relative accuracy %10.3g\n", APREC);
                }
            }
        } else {
            if (MPRMON >= 1) {
                printf(" Solution of linear system of equations obtained by NLEQ1\nNo estimate available for the achieved relative accuracy\n");
            }
        }
    }
    // ----------------------------------------------------------
    // 9.2 Fail exit messages
    // ----------------------------------------------------------
    if (MPRMON >= 1) {
        PrintErrorMessage(IERR, MPRERR, NITMAX, NITER, FC, QMSTOP, ICONV, IFAIL);
    }

    // ----------------------------------------------------------
    // 9.2.4.2 Convergence criterion satisfied before superlinear
    //        convergence has been established
    if ((IERR > 5) && (fabs(DLEVFN) < EPMACH)) IERR=0;

    if ((IERR == 5) && (MPRERR >= 1) && (MPRMON >= 1)) {
        printf("Warning: No quadratic or superlinear convergence established yet\n");
        printf("        your solution may perhaps may be less accurate as indicated by the standard error estimate\n");
    }

    if ((IERR >= 80) && (IERR <= 83)) IWK[23] = IFAIL;

    // ----------------------------------------------------------
    // 9.3 Common exit
    if ((MPRERR >= 3) && (IERR != 0) && (IERR != 4) && (NONLIN != 1)) {
        printf("    Achieved relative accuracy %10.3g \n", CONVA);
        APREC = CONVA;
    }
    RTOL = APREC;
    SUMX = SUMXA;
    if ((MPRSOL >= 2) && (NITER != 0)) {
        MODE = 2;
        if (QORDI) MODE = 3;
        if (MPRTIM!=0)  MONON(5);

        N1SOUT(N, XA, MODE, IOPT, RWK, LRWK, IWK, LIWK, MPRSOL, LUSOL);

        if (MPRTIM != 0)  MONOFF(5);
    } else if ((MPRSOL>=1) && (NITER == 0)) {
        if (MPRTIM != 0)  MONON(5)
         N1SOUT(N, XA, 1, IOPT, RWK, LRWK, IWK, LIWK, MPRSOL, LUSOL);
        if (MPRTIM != 0)  MONOFF(5);
    }

    if (!QORDI) {

        if (IERR != 4) NITER = NITER + 1;

        DLEVF = DLEVFN;

        if (MPRSOL >= 1) {
            // Print Solution or final iteration vector
            int MODEFI;
            if (IERR == 0) {
                MODEFI = 3;
            } else {
                MODEFI = 4;
            }
            if (MPRTIM!=0)  MONON(5);

            N1SOUT(N, X, MODEFI, IOPT, RWK, LRWK, IWK, LIWK, MPRSOL, LUSOL);

            if (MPRTIM != 0) MONOFF(5);
        }
    }

    // Return the latest internal scaling to XSCAL
    for (int I = 0; I < N; ++I) {
        XSCAL[I] = XW[I];
    }
    //      End of exits

    #undef A
    #undef DXSAVE
    #undef IOPT
} // End of subroutine N1INT

//  ------------------------------------------------------------
void NonLinearEquationSolver::PrintErrorMessage(int IERR, int MPRERR, int NITMAX, int NITER, double FC, bool QMSTOP, int ICONV, long int IFAIL) {
    // 9.2.1 Termination, since jacobian matrix became singular
    if ((IERR == 1) && (MPRERR >= 1)) {
        printf("Iteration terminates due to singular jacobian matrix\n");
    }

    // ----------------------------------------------------------
    // 9.2.2 Termination after more than NITMAX iterations
    if ((IERR == 2) && (MPRERR >= 1)) {
        printf(" Iteration terminates after NITMAX %3d  Iteration steps\n", NITMAX);
    }
    // ----------------------------------------------------------
    // 9.2.3 Damping factor FC became too small
    if ((IERR == 3) && (MPRERR >= 1)) {
        printf(" Damping factor has become too small: lambda = %10.3g\n", FC);
    }

    // ----------------------------------------------------------
    // 9.2.4.1 Superlinear convergence slowed down
    if ((IERR == 4) && (MPRERR >= 1)) {
        if (QMSTOP) {
            printf(" Warning: Monotonicity test failed after;  superlinear convergence was already checked; RTOL requirement may be too stringent\n");
        if (ICONV == 3)
            printf(" Warning: quadratic convergence slowed down;  RTOL requirement may be too stringent\n");
        } else {
            if (ICONV == 2)
                printf(" Warning: Monotonicity test failed after;  superlinear convergence was already checked; RTOL requirement may be too stringent\n");
            if (ICONV == 3)
                printf(" Warning: quadratic convergence slowed down;  RTOL requirement may be too stringent\n");
          }
    }

    // ----------------------------------------------------------
    // 9.2.5 Error exit due to linear solver routine N1FACT
    if ((IERR == 80) && (MPRERR >= 1)) {
        printf(" Error %ld signalled by linear solver N1FACT\n", IFAIL);
    }

    // ----------------------------------------------------------
    // 9.2.6 Error exit due to linear solver routine N1SOLV
    if ((IERR == 81) && (MPRERR >= 1)) {
        printf(" Error %ld signalled by linear solver N1SOLV\n", IFAIL);
    }

    // ----------------------------------------------------------
    // 9.2.7 Error exit due to fail of user function FCN
    if ((IERR == 82) && (MPRERR >= 1)) {
        printf(" Error %ld signalled by user function FCN\n", IFAIL);
    }

    // ----------------------------------------------------------
    // 9.2.8 Error exit due to fail of user function JAC
    if ((IERR == 83) && (MPRERR >= 1)) {
        printf(" Error %ld signalled by user function JAC\n", IFAIL);
    }


    if ((IERR == 82 || IERR == 83) && (NITER <= 1) && (MPRERR >= 1)) {
        printf(" Try to find a better initial guess for the solution\n");
    }
}
//  ------------------------------------------------------------
/**
 *
 *     S C A L E : To be used in connection with NLEQ1 .
 *       Computation of the internal scaling vector XW used for the
 *       Jacobian matrix, the iterate vector and it's related
 *       vectors - especially for the solution of the linear system
 *       and the computations of norms to avoid numerical overflow.
 *
 *    Input parameters
 *     ================
 *
 *     N         Int     Number of unknowns
 *     X(N)      Dble    Current iterate
 *     XA(N)     Dble    Previous iterate
 *     XSCAL(N)  Dble    User scaling passed from parameter XSCAL
 *                       of interface routine NLEQ1
 *     ISCAL     Int     Option ISCAL passed from IOPT-field
 *                       (for details see description of IOPT-fields)
 *     QINISC    Logical = true  : Initial scaling
 *                       = false : Subsequent scaling
 *     IOPT(50)  Int     Options array passed from NLEQ1 parameter list
 *     LRWK      Int     Length of real workspace
 *     RWK(LRWK) Dble    Real workspace (see description above)
 *
 *    Output parameters
 *     =================
 *
 *     XW(N)     Dble   Scaling vector computed by this routine
 *                      All components must be positive. The follow-
 *                      ing relationship between the original vector
 *                      X and the scaled vector XSCAL holds:
 *                      XSCAL[I] = X[I]/XW[I] for I=1,...N
 *
 *    Subroutines called: D1MACH
 *
 */
void NonLinearEquationSolver::N1SCAL(int N, const double *X, const double *XA, const double *XSCAL,
                    double *XW, int ISCAL, bool QINISC,
                    long int *IOPT, int LRWK, double *RWK) {

    for (int L1 = 0; L1 < N; ++L1) {
        if (ISCAL == 1) {
            XW[L1] = XSCAL[L1];
        } else {
            XW[L1] = std::max((fabs(X[L1]) + fabs(XA[L1])) * HALF, SMALL);
            XW[L1] = std::max(XSCAL[L1], XW[L1]);
        }
    }

    int MPRMON = IOPT[13];
    if (MPRMON >= 6) {
        printf(" \n");
        printf(" ++++++++++++++++++++++++++++++++++++++++++\n");
        printf("      X-components   Scaling-components    \n");
        for (int L1 = 0; L1 < N; ++L1) {
            printf("  %18.10f   %18.10f\n", X[L1], XW[L1]);
        }
        printf(" ++++++++++++++++++++++++++++++++++++++++++\n");
        printf(" \n");
    }
}
//  ------------------------------------------------------------
/**
 *
 *     S C R O W F : Row Scaling of a (M,N)-matrix in full storage
 *                   mode
 *
 *    Input parameters (* marks inout parameters)
 *     ===========================================
 *
 *       M           Int    Number of rows of the matrix
 *       N           Int    Number of columns of the matrix
 *     * A(M,N)      Dble   Matrix to be scaled
 *
 *    Output parameters
 *    =================
 *
 *       FW(M)       Dble   Row scaling factors - FW[I] contains
 *                          the factor by which the i-th row of A
 *                          has been multiplied
 *
 */
void NonLinearEquationSolver::N1SCRF(int M, int N, double *A, double *FW) {
    #define A(irow, jcol) A[irow * M + jcol]

    for (int krow = 0; krow < M; ++krow) {
        // find the biggest row element in absolute value
        double S1 = ZERO;
        for (int jcol = 0; jcol < N; ++jcol) {
            double S2 = fabs(A(krow, jcol));
            if (S2 > S1) S1 = S2;
        }

        // scale FW
        if (S1 > ZERO) {
            S1 = ONE / S1;
            FW[krow] = S1;
            for (int jcol = 0; jcol < N; ++jcol) {
                A(krow, jcol) = A(krow, jcol) * S1;
            }
        } else {
            FW[krow] = ONE;
        }
    }

    #undef A
}

//  ------------------------------------------------------------
/*
 *     S C R O W B : Row Scaling of a (N,N)-matrix in band storage
 *                   mode
 *
 *    Input parameters (* marks inout parameters)
 *    ===========================================
 *
 *       N           Int    Number of rows and columns of the matrix
 *       LDA         Int    Leading dimension of the matrix array
 *       ML          Int    Lower bandwidth of the matrix (see IOPT)
 *       MU          Int    Upper bandwidth of the matrix (see IOPT)
 *     * A(LDA,N)    Dble   Matrix to be scaled (see Note in routine
 *                          DGBFA for details on storage technique)
 *
 *    Output parameters
 *    =================
 *
 *       FW(N)       Dble   Row scaling factors - FW[I] contains
 *                          the factor by which the i-th row of
 *                          the matrix has been multiplied
 *
 */
void NonLinearEquationSolver::N1SCRB(int N, int LDA, int ML, int MU, double *A, double *FW) {
    #define A(irow, jcol) A[irow * LDA + jcol]

    int M2 = ML + MU + 1;
    for (int K = 0; K < N; ++K) {
        double S1 = ZERO;
        int L2 = std::max(1, K - ML);
        int L3 = std::min(N, K + MU);
        int K1 = M2 + K;
        for (int L1=L2; L1 < L3; ++L1) {
            double S2 = fabs(A(K1 - L1, L1));
            if (S2 > S1) S1 = S2;
        }

        if (S1 > ZERO) {
            S1 = ONE / S1;
            FW[K] = S1;
            for (int L1 = L2; L1 < L3; ++L1) {
                A(K1 - L1, L1) = A(K1 - L1, L1) * S1;
            }
        } else {
            FW[K] = ONE;
        }
    }

    #undef A
}

//  ------------------------------------------------------------
/**
 *     F A C T : Call linear algebra subprogram for factorization of
 *              a (N,N)-matrix
 *
 *    Input parameters (* marks inout parameters)
 *     ===========================================
 *
 *     N             Int    Order of the linear system
 *     LDA           Int    Leading dimension of the matrix array A
 *     ML            Int    Lower bandwidth of the matrix (only for
 *                         banded systems)
 *    MU            Int    Upper bandwidth of the matrix (only for
 *                          banded systems)
 *  * A(LDA,N)      Dble   Matrix storage. See main routine NLEQ1,
 *                          note 4 for how to store a banded Jacobian
 *     IOPT(50)      Int    Option vector passed from NLEQ1
 *
 *    Output parameters
 *     =================
 *
 *     IFAIL         Int    Error indicator returned by this routine:
 *                          = 0 matrix decomposition successfull
 *                          = 1 decomposition failed -
 *                              matrix is numerically singular
 *                          =10 supplied (integer) workspace too small
 *
 *    Workspace parameters
 *     ====================
 *
 *     LIWK          Int    Length of integer workspace passed to this
 *                          routine (In)
 *     IWK(LIWK)     Int    Integer Workspace supplied for this routine
 *     LAIWK         Int    Length of integer Workspace used by this
 *                         routine (out)
 *     LRWK          Int    Length of real workspace passed to this
 *                          routine (In)
 *     RWK(LRWK)     Dble   Real Workspace supplied for this routine
 *     LARWK         Int    Length of real Workspace used by this
 *                          routine (out)
 *
 */
void NonLinearEquationSolver::N1FACT(int N, int LDA, int ML, int MU, double *A,
        long int *IOPT, long int &IFAIL, int LIWK, long int *IWK,
        int &LAIWK, int LRWK,
        double *RWK, int &LARWK) {
    #define A(irow, jcol) A[irow * LDA + jcol]

    LAIWK = N;
    LARWK = 0;

    //( Real workspace starts at RWK(NRWKFR) )
    if (LIWK >= LAIWK) {
        int MSTOR = IOPT[4];
        if (MSTOR ==0 ) {
            //DGEFA(A,LDA,N,IWK,&IFAIL);
            // in this case, we will factor later to solve A*X=B
            // with LU decomposition using Eigen
            /*
            MATRIXVAR mA(N,N);
            for (int i=0; i<N; ++i) {
                for (int j=0; j<N; ++j) {
                    mA(i,j) = A(i,j);
                }
            }

            Eigen::PartialPivLU<MATRIXVAR> lu(mA);
            // L+U-I
            MATRIXVAR LUA = lu.matrixLU();

            for (int i=0; i<N; ++i) {
                for (int j=0; j<N; ++j) {
                    A(i,j) = LUA(i,j);
                }
            }
            */
        } else if (MSTOR == 1) {
            DGBFA(A, LDA, N, ML, MU, IWK, &IFAIL);
        } else {
            // should not happen because N1PCheck has been called
        }

        if (IFAIL != 0) {
          IFAIL = 1;
        }
    } else {
        IFAIL = 10;
        int MPRERR = IOPT[11];
        if ((LIWK < LAIWK) && (MPRERR > 0))
            printf(" Insuffient workspace for linear solver. at least needed more needed :  Integer workspace : %4d\n", LAIWK-LIWK);
    }

    #undef A
}
//  ------------------------------------------------------------
/**
 *    Summary :
 *
 *     S O L V E : Call linear algebra subprogram for solution of
 *                 the linear system A*Z = B
 *
 *    Parameters
 *   ==========
 *
 *    N,LDA,ML,MU,A,IOPT,IFAIL,LIWK,IWK,LAIWK,LRWK,RWK,LARWK :
 *                       See description for subroutine N1FACT.
 *    B          Dble    In:  Right hand side of the linear system
 *                       Out: Solution of the linear system
 *
 *    Subroutines called: DGESL, DGBSL
 *
 */
void NonLinearEquationSolver::N1SOLV(int N, int LDA, int ML, int MU, double *A, double *B,
        long int *IOPT,
        long int &IFAIL, int LIWK, long int *IWK, int LAIWK,
        int LRWK, double *RWK, int LARWK) {
    #define A(irow, jcol) A[irow * LDA + jcol]

    int MSTOR = IOPT[4];
    if (MSTOR == 0) {
        //DGESL(A,LDA,N,IWK,B,0);

        MATRIXVAR mA(N, N);
        MATRIXVAR mB(N, 1);
        for (int i = 0; i < N; ++i) {
            mB(i) = B[i];
            for (int j = 0; j < N; ++j) {
                mA(i, j) = A(i, j);
            }
        }

        MATRIXVAR mX = mA.lu().solve(mB);

        for (int i = 0; i < N; ++i) {
            B[i] = mX(i, 0);
        }
    } else if (MSTOR == 1) {
        DGBSL(A, LDA, N, ML, MU, IWK, B, 0);
    }
    IFAIL = 0;

    #undef A
}

//  ------------------------------------------------------------
/**
 *    L E V E L S : To be used in connection with NLEQ1 .
 *    provides descaled solution, error norm and level functions
 *
 *    Input parameters (* marks inout parameters)
 *    ===========================================
 *
 *      N              Int    Number of parameters to be estimated
 *      DX1(N)         Dble   array containing the scaled Newton
 *                            correction
 *      XW(N)          Dble   Array containing the scaling values
 *      F(N)           Dble   Array containing the residuum
 *
 *    Output parameters
 *    =================
 *
 *      DXQ(N)         Dble   Array containing the descaled Newton
 *                           correction
 *      CONV           Dble   Scaled maximum norm of the Newton
 *                             correction
 *      SUMX           Dble   Scaled natural level function value
 *      DLEVF          Dble   Standard level function value (only
 *                           if needed for print)
 *       MPRMON         Int    Print information parameter (see
 *                            driver routine NLEQ1 )
 *      QDSCAL         Logic  true, if descaling of DX1 required,
 *                            else false
 *
 */
void NonLinearEquationSolver::N1LVLS(int N, const double *DX1, const double *XW, const double *F,
            double *DXQ,
            double &CONV, double &SUMX, double &DLEVF, int MPRMON, bool QDSCAL) {

    if (QDSCAL) {
        // 1.2 Descaling of solution DX1 ( stored to DXQ )
        for (int L1 = 0; L1 < N; ++L1) {
          DXQ[L1] = DX1[L1] * XW[L1];
        }
    }

    // ------------------------------------------------------------
    // 2 Evaluation of scaled natural level function SUMX and scaled maximum error norm CONV
    CONV = ZERO;
    for (int L1 = 0; L1 < N; ++L1) {
        double S1 = fabs(DX1[L1]);
        if (S1 > CONV) CONV = S1;
    }

    SUMX = ZERO;
    for (int L1 = 0; L1 < N; ++L1) {
        SUMX = SUMX + DX1[L1] * DX1[L1];
    }

    // ------------------------------------------------------------
    // 3 Evaluation of (scaled) standard level function DLEVF (only
    if (MPRMON >= 2) {
        DLEVF = ZERO;
        for (int L1 = 0; L1 < N; ++L1) {
          DLEVF = DLEVF + F[L1] * F[L1];
        }
        DLEVF = sqrt(DLEVF / double(N));
    }
}

//  ------------------------------------------------------------
/**
 *  Evaluation of a dense Jacobian matrix using finite difference
 *  approximation adapted for use in nonlinear systems solver NLEQ1
 *
 * Environment       Fortran 77
 *                    Double Precision
 *                    Sun 3/60, Sun OS
 * Latest Revision   May 1990
 *
 *
 * Parameter list description
 *  --------------------------
 *
 * External subroutines (to be supplied by the user)
 *  -------------------------------------------------
 *
 * FCN        Ext     FCN (N, X, FX, IFAIL)
 *                    Subroutine in order to provide right-hand
 *                     side of first-order differential equations
 *    N        Int     Number of rows and columns of the Jacobian
 *   X(N)     Dble    The current scaled iterates
 *   FX(N)    Dble    Array containing FCN(X)
 *   IFAIL    Int     Return code
 *                    Whenever a negative value is returned by FCN
 *                    routine N1JAC is terminated immediately.
 *
 *
 * Input parameters (* marks inout parameters)
 *  ----------------
 *
 *  N          Int     Number of rows and columns of the Jacobian
 *  LDA        Int     Leading Dimension of array A
 *  X(N)       Dble    Array containing the current scaled
 *                     iterate
 *  FX(N)      Dble    Array containing FCN(X)
 *  YSCAL(N)   Dble    Array containing the scaling factors
 *  AJDEL      Dble    Perturbation of component k: abs(Y(k))*AJDEL
 *  AJMIN      Dble    Minimum perturbation is AJMIN*AJDEL
 *  NFCN       Int  *  FCN - evaluation count
 *
 * Output parameters (* marks inout parameters)
 *  -----------------
 *
 * A(LDA,N)   Dble    Array to contain the approximated
 *                    Jacobian matrix ( dF[I]/dx(j)in A(i,j))
 *  NFCN       Int  *  FCN - evaluation count adjusted
 *  IFAIL      Int     Return code non-zero if Jacobian could not
 *                    be computed.
 *
 * Workspace parameters
 * --------------------
 *
 * FU(N)      Dble    Array to contain FCN(x+dx) for evaluation of
 *                    the numerator differences
 *
 */
void NonLinearEquationSolver::N1JAC(/*void (*FCN)(), */
                int N, int LDA, double *X, const double *FX, double *A, const double *YSCAL,
                       double AJDEL, double AJMIN, long int &NFCN, double *FU,
                       long int &IFAIL)
{
    #define A(irow, jcol) A[irow * LDA + jcol]

    IFAIL = 0;
    for (int K = 0; K < N; ++K) {
        double W = X[K];
        double U = sign(std::max(fabs(X[K]), std::max(AJMIN, YSCAL[K])) * AJDEL, X[K]);
        X[K] = W + U;

        // evaluate function
        IFAIL = Function(N, X, FU);
        NFCN = NFCN + 1;
        if (IFAIL != 0) break;

#ifdef DEBUG_FINITE_DIFFERENCES
    printf("U = %g\n", U);
    printf("X = ");
    for (int I = 0; I < N; ++I) {
        printf("%12.10g ", X[I]);
    }
    printf("\n");
    printf("FU = ");
    for (int I = 0; I < N; ++I) {
        printf("%12.10g ", FU[I]);
    }
    printf("\n");
#endif

        X[K] = W;
        for (int I = 0; I < N; ++I) {
            A(I, K) = (FU[I] - FX[I]) / U;
        }
    }
#ifdef DEBUG
    printf("A = ");
    for (int K = 0; K < N; ++K) {
        for (int I = 0; I < N; ++I) {
            printf("%12.10g ", A(I, K));
        }
        printf("\n");
    }
#endif

    #undef A
}

//  ------------------------------------------------------------
/**
C  Evaluation of a banded Jacobian matrix using finite difference
C  approximation adapted for use in nonlinear systems solver NLEQ1
C
C* Environment       Fortran 77
C                    Double Precision
C                    Sun 3/60, Sun OS
C* Latest Revision   May 1990
C
C
C* Parameter list description
C  --------------------------
C
C* External subroutines (to be supplied by the user)
C  -------------------------------------------------
C
C  FCN        Ext     FCN (N, X, FX, IFAIL)
C                     Subroutine in order to provide right-hand
C                     side of first-order differential equations
C    N        Int     Number of rows and columns of the Jacobian
C    X(N)     Dble    The current scaled iterates
C    FX(N)    Dble    Array containing FCN(X)
C    IFAIL    Int     Return code
C                     Whenever a negative value is returned by FCN
C                     routine N1JACB is terminated immediately.
C
C
C* Input parameters (* marks inout parameters)
C  ----------------
C
C  N          Int     Number of rows and columns of the Jacobian
C  LDA        Int     Leading Dimension of array A
C  ML         Int     Lower bandwidth of the Jacobian matrix
C  X(N)       Dble    Array containing the current scaled
C                     iterate
C  FX(N)      Dble    Array containing FCN(X)
C  YSCAL(N)   Dble    Array containing the scaling factors
C  AJDEL      Dble    Perturbation of component k: abs(Y(k))*AJDEL
C  AJMIN      Dble    Minimum perturbation is AJMIN*AJDEL
C  NFCN       Int  *  FCN - evaluation count
C
C* Output parameters (* marks inout parameters)
C  -----------------
C
C  A(LDA,N)   Dble    Array to contain the approximated
C                     Jacobian matrix ( dF[I]/dx(j)in A(i,j))
C  NFCN       Int  *  FCN - evaluation count adjusted
C  IFAIL      Int     Return code non-zero if Jacobian could not
C                     be computed.
C
C* Workspace parameters
C  --------------------
C
C  FU(N)      Dble    Array to contain FCN(x+dx) for evaluation of
C                     the numerator differences
C  U(N)       Dble    Array to contain dx[I]
C  W(N)       Dble    Array to save original components of X
C
*/
void NonLinearEquationSolver::N1JACB(/*void (*FCN)(), */
                    int N, int LDA, int ML, double *X, double *FX, double *A,
                    double *YSCAL, double AJDEL, double AJMIN,
                    long int &NFCN, double *FU, double *U, double *W,
                    long int &IFAIL)
{
    #define A(irow, jcol) A[irow * LDA + jcol]

    IFAIL = 0;
    int MU = LDA - 2 * ML - 1;
    int LDAB = ML + MU + 1;
    for (int I = 0; I < LDAB; ++I) {
        for (int K = 0; K < N; ++K) {
            A(I, K) = ZERO;
        }
    }

    for (int JJ = 0; JJ < LDAB; ++JJ) {
         for (int K = JJ; K < N; K+=LDAB) {
            W[K] = X[K];
            U[K] = sign(std::max(fabs(X[K]), std::max(AJMIN, YSCAL[K])) * AJDEL, X[K]);
            X[K] = W[K] + U[K];
         }

         IFAIL = Function(N, X, FU);
         NFCN = NFCN + 1;

         if (IFAIL != 0) break;

         for (int K = JJ; K < N; K += LDAB) {
            X[K] = W[K];
            int I1 = std::min(1, K - MU);
            int I2 = std::min(N, K + ML);
            int MH = MU + 1 - K;
            for (int I = I1; I < I2; ++I) {
               A(MH + I, K) = (FU[I] - FX[I]) / U[K];
            }
        }
    }

    #undef A
}

//  ------------------------------------------------------------
/**
C* Title
C
C  Approximation of dense Jacobian matrix for nonlinear systems solver
C  NLEQ1 with feed-back control of discretization and rounding errors
C
C* Environment       Fortran 77
C                    Double Precision
C                    Sun 3/60, Sun OS
C* Latest Revision   May 1990
C
C
C* Parameter list description
C  --------------------------
C
C* External subroutines (to be supplied by the user)
C  -------------------------------------------------
C
C  FCN        Ext     FCN (N, X, FX, IFAIL)
C                     Subroutine in order to provide right-hand
C                     side of first-order differential equations
C    N        Int     Number of rows and columns of the Jacobian
C    X(N)     Dble    The current scaled iterates
C    FX(N)    Dble    Array containing FCN(X)
C    IFAIL    Int     Return code
C                     Whenever a negative value is returned by FCN
C                     routine N1JCF is terminated immediately.
C
C
C* Input parameters (* marks inout parameters)
C  ----------------
C
C  N          Int     Number of rows and columns of the Jacobian
C  LDA        Int     Leading dimension of A (LDA >= N)
C  X(N)       Dble    Array containing the current scaled
C                     iterate
C  FX(N)      Dble    Array containing FCN(X)
C  YSCAL(N)   Dble    Array containing the scaling factors
C  ETA(N)     Dble *  Array containing the scaled denominator
C                     differences
C  ETAMIN     Dble    Minimum allowed scaled denominator
C  ETAMAX     Dble    Maximum allowed scaled denominator
C  ETADIF     Dble    sqrt (1.1*EPMACH)
C                     EPMACH = machine precision
C  CONV       Dble    Maximum norm of last (unrelaxed) Newton correction
C  NFCN       Int  *  FCN - evaluation count
C
C* Output parameters (* marks inout parameters)
C  -----------------
C
C  A(LDA,N)   Dble    Array to contain the approximated
C                     Jacobian matrix ( dF[I]/dx(j)in A(i,j))
C  ETA(N)     Dble *  Scaled denominator differences adjusted
C  NFCN       Int  *  FCN - evaluation count adjusted
C  IFAIL      Int     Return code non-zero if Jacobian could not
C                     be computed.
C
C* Workspace parameters
C  --------------------
C
C  FU(N)      Dble    Array to contain FCN(x+dx) for evaluation of
C                     the numerator differences
C
C* Called
C  ------
*/
void NonLinearEquationSolver::N1JCF(/*void (*FCN)(), */
                int N, int LDA, double *X, double *FX, double *A, double *YSCAL,
                double *ETA, double ETAMIN, double ETAMAX, double ETADIF,
                double &CONV, long int &NFCN, double *FU,
                long int &IFAIL) {
    #define A(i, j) A[i * LDA + j]

    double SMALL2 = 0.1;

    for (int K = 0; K < N; ++K) {
        int IS = 0;
        bool QFINE = false;
        while (!QFINE) {
            double W = X[K];
            double U = sign(ETA[K]*YSCAL[K], X[K]);
            X[K] = W + U;
            IFAIL = Function(N, X, FU);
            NFCN = NFCN + 1;

            // Exit, If ...
            if (IFAIL != 0) break;

            X[K] = W;
            double SUMD = ZERO;
            for (int I = 0; I < N; ++I) {
               double HG = std::max(fabs(FX[I]), fabs(FU[I]));
               double FHI = FU[I] - FX[I];
               if (fabs(HG) > ZERO) SUMD = SUMD + (FHI/HG) * (FHI/HG);
               A(I, K) = FHI / U;
            }
            SUMD = sqrt(SUMD / static_cast<double>(N));

            if ((fabs(SUMD) > ZERO) && (IS == 0)) {
                ETA[K] = std::min(ETAMAX,
                                std::max(ETAMIN, sqrt(ETADIF / SUMD) * ETA[K]));
                IS = 1;
                QFINE = (CONV < SMALL2) || (SUMD >= ETAMIN);
            }
        }
    }

    #undef A
}

//  ------------------------------------------------------------
/**
C* Title
C
C  Approximation of banded Jacobian matrix for nonlinear systems solver
C  NLEQ1 with feed-back control of discretization and rounding errors
C
C* Environment       Fortran 77
C                    Double Precision
C                    Sun 3/60, Sun OS
C* Latest Revision   May 1990
C
C
C* Parameter list description
C  --------------------------
C
C* External subroutines (to be supplied by the user)
C  -------------------------------------------------
C
C  FCN        Ext     FCN (N, X, FX, IFAIL)
C                     Subroutine in order to provide right-hand
C                     side of first-order differential equations
C    N        Int     Number of rows and columns of the Jacobian
C    X(N)     Dble    The current scaled iterates
C    FX(N)    Dble    Array containing FCN(X)
C    IFAIL    Int     Return code
C                     Whenever a negative value is returned by FCN
C                     routine N1JCFB is terminated immediately.
C
C
C* Input parameters (* marks inout parameters)
C  ----------------
C
C  N          Int     Number of rows and columns of the Jacobian
C  LDA        Int     Leading dimension of A (LDA >= ML+MU+1)
C  ML         Int     Lower bandwidth of Jacobian matrix
C  X(N)       Dble    Array containing the current scaled
C                     iterate
C  FX(N)      Dble    Array containing FCN(X)
C  YSCAL(N)   Dble    Array containing the scaling factors
C  ETA(N)     Dble *  Array containing the scaled denominator
C                     differences
C  ETAMIN     Dble    Minimum allowed scaled denominator
C  ETAMAX     Dble    Maximum allowed scaled denominator
C  ETADIF     Dble    sqrt (1.1*EPMACH)
C                     EPMACH = machine precision
C  CONV       Dble    Maximum norm of last (unrelaxed) Newton correction
C  NFCN       Int  *  FCN - evaluation count
C
C* Output parameters (* marks inout parameters)
C  -----------------
C
C  A(LDA,N)   Dble    Array to contain the approximated
C                     Jacobian matrix ( dF[I]/dx(j)in A(i,j))
C  ETA(N)     Dble *  Scaled denominator differences adjusted
C  NFCN       Int  *  FCN - evaluation count adjusted
C  IFAIL      Int     Return code non-zero if Jacobian could not
C                     be computed.
C
C* Workspace parameters
C  --------------------
C
C  FU(N)      Dble    Array to contain FCN(x+dx) for evaluation of
C                     the numerator differences
C  U(N)       Dble    Array to contain dx[I]
C  W(N)       Dble    Array to save original components of X
C
C* Called
*/
void NonLinearEquationSolver::N1JCFB(/*void (*FCN)(), */
            int N, int LDA, int ML, double *X, double *FX, double *A,
            double *YSCAL, double *ETA,
            double ETAMIN, double ETAMAX, double ETADIF, double &CONV,
            long int& NFCN, double *FU, double *U, double *W,
            long int& IFAIL) {
    #define A(i, j) A[i * LDA + j]

    double SMALL2 = 0.1;

    int MU = LDA - 2 * ML - 1;
    int LDAB = ML + MU + 1;
    for (int I = 0; I < LDAB; ++I) {
         for (int K = 0; K < N; ++K) {
            A(I, K) = ZERO;
         }
    }

    for (int JJ = 0; JJ < LDAB; ++JJ) {
        int IS = 0;
        bool QFINE = false;
        while (!QFINE) {
            for (int K = JJ; K < N; K+=LDAB) {
               W[K] = X[K];
               U[K] = sign(ETA[K] * YSCAL[K], X[K]);
               X[K] = W[K] + U[K];
            }

            IFAIL = Function(N, X, FU);
            NFCN = NFCN + 1;

            // Exit, If ...
            if (IFAIL != 0) break;

            for (int K = JJ; K < N; K+=LDAB) {
                X[K] = W[K];
                double SUMD = ZERO;
                int I1 = std::max(1, K - MU);
                int I2 = std::min(N, K + ML);
                int MH = MU + 1 - K;
                for (int I = I1; I < I2; ++I) {
                    double HG = std::max(fabs(FX[I]), fabs(FU[I]));
                    double FHI = FU[I] - FX[I];
                    if (fabs(HG) > ZERO) SUMD = SUMD + (FHI/HG) * (FHI/HG);
                    A(MH+I, K) = FHI / U[K];
                }
                SUMD = sqrt(SUMD / double(N));

                if ((fabs(SUMD) > ZERO) && (IS == 0)) {
                    ETA[K] = std::min(ETAMAX,
                                        std::max(ETAMIN, sqrt (ETADIF / SUMD)*ETA[K]));
                    IS = 1;
                    QFINE = (CONV < SMALL2) || (SUMD >= ETAMIN);
                }
            }
        } // end while
    } // end for

    #undef A
}  // End of subroutine N1JCFB


//  ------------------------------------------------------------
/**
 *    Summary :
 *
 *     N 1 P R V 1 : Printing of intermediate values (Type 1 routine)
 *
 *    Parameters
 *    ==========
 *
 *    DLEVF, DLEVX   See descr. of internal double variables of N1INT
 *    FC,NITER,NEW,MPRMON,LUMON
 *                 See parameter descr. of subroutine N1INT
 *    QMIXIO Logical  = true  , if LUMON==LUSOL
 *                    = false , if LUMON!=LUSOL
 *
 */
void NonLinearEquationSolver::N1PRV1(double DLEVF, double DLEVX, double FC, int NITER, int NEW, int MPRMON,
                                    int LUMON, bool QMIXIO) {
    // Print Standard - and natural level
    if (QMIXIO) {
        printf("****************************************************************\n");

        if (MPRMON >= 3)
            printf("        It       Normf          Normx                    New\n");

        if (MPRMON == 2)
            printf("        It       Normf          Normx         Damp.Fct.   New\n");
    }

    if ((MPRMON >= 3) || (NITER == 0))
        printf("      %4d     %10.3g      %10.3g                 %2d\n", NITER, DLEVF, DLEVX, NEW);

    if ((MPRMON == 2) && (NITER != 0))
        printf("      %4d     %10.3g      %10.3g      %7.5f    %2d\n", NITER, DLEVF, DLEVX, FC, NEW);

    if (QMIXIO) {
        printf("****************************************************************\n");
    }
}
//  ------------------------------------------------------------
/**
 *    Summary :
 *
 *     N 1 P R V 2 : Printing of intermediate values (Type 2 routine)
 *
 *    Parameters
 *     ==========
 *
 *     DLEVF, DLEVX   See descr. of internal double variables of N2INT
 *     FC,NITER,MPRMON,LUMON
 *                 See parameter descr. of subroutine N2INT
 *    QMIXIO Logical  = true  , if LUMON==LUSOL
 *                     = false , if LUMON!=LUSOL
 *     CMARK Char*1    Marker character to be printed before DLEVX
 *
 *     ------------------------------------------------------------
 *    End Prologue
 *    Print Standard - and natural level, and damping
 *     factor
 */
void NonLinearEquationSolver::N1PRV2(double DLEVF, double DLEVX, double FC, int NITER, int MPRMON,
                                    int LUMON, bool QMIXIO, char CMARK) {
    if (QMIXIO) {
        printf("****************************************************************\n");
        printf("        It       Normf          Normx        Damp.Fct.\n");
    }

    printf("      %4d     %10.3g    %c %10.3g      %7.5f\n", NITER, DLEVF, CMARK, DLEVX, FC);

    if (QMIXIO) {
        printf("****************************************************************\n");
    }
}
//  ------------------------------------------------------------
/**
 *
 *    Summary :
 *
 *    S O L O U T : Printing of iterate (user customizable routine)
 *
 *    Input parameters
 *     ================
 *
 *     N         Int Number of equations/unknowns
 *     X(N)   Double iterate vector
 *     MODE          =1 This routine is called before the first
 *                      Newton iteration step
 *                   =2 This routine is called with an intermedi-
 *                      ate iterate X(N)
 *                   =3 This is the last call with the solution
 *                      vector X(N)
 *                  =4 This is the last call with the final, but
 *                      not solution vector X(N)
 *     IOPT(50)  Int The option array as passed to the driver
 *                   routine(elements 46 to 50 may be used
 *                   for user options)
 *     MPRINT    Int Solution print level
 *                   (see description of IOPT-field MPRINT)
 *     LUOUT     Int the solution print unit
 *                   (see description of see IOPT-field LUSOL)
 *
 *
 *    Workspace parameters
 *    ====================
 *
 *     NRW, RWK, NIW, IWK    see description in driver routine
 *
 *    Use of IOPT by this routine
 *    ===========================
 *
 *     Field 46:       =0 Standard output
 *                     =1 GRAZIL suitable output
*/
// ----------------------------------------------------------------
void NonLinearEquationSolver::N1SOUT(int N, const double *X, int MODE,
                const long int *IOPT,
                const double *RWK, int NRW, const long int *IWK, int NIW, int MPRINT, int &LUOUT) {
    bool QNORM = IOPT[46] == 0;
    bool QGRAZ = IOPT[46] == 1;
    if (QNORM) {
        if (MODE == 1) {
            printf("  Start data:\n  N = %5d\n", N);
            printf("  Format: iteration-number, (x[I],i=1,...N)  Normf , Normx \n");
            printf("  Initial data:\n");
         } else if (MODE == 3) {
            printf("  Solution data:\n");
         } else if (MODE == 4) {
            printf("  Final data:\n");
         }

         printf(" %5ld\n", IWK[1]);

         printf("            ");
         for (int L1 = 0; L1 < N; ++L1) {
            printf("%18.10f ", X[L1]);
         }

         printf("\n            %18.10g %18.10g\n", RWK[19], sqrt(RWK[18] / double(N)));

        if ((MODE == 1) && (MPRINT >= 2)) {
            printf("  Intermediate data:\n");
        } else if (MODE >= 3) {
            printf("  End data:\n");
        }
    }

    if (QGRAZ) {
        if (MODE == 1) {
            /*
10        FORMAT('&name com',I3.3,:,255(7(', com',I3.3,:),/))
          WRITE(LUOUT,10)(I,I=1,N+2)
15        FORMAT('&def  com',I3.3,:,255(7(', com',I3.3,:),/))
          WRITE(LUOUT,15)(I,I=1,N+2)
16        FORMAT(6X,': X=1, Y=',I3)
          WRITE(LUOUT,16) N+2
           * */
        }

        printf("&data %5ld\n", IWK[1]);

        printf("      ");
        for (int L1 = 0; L1 < N; ++L1) {
            printf("%18.10f \n", X[L1]);
        }

        printf("%18.10g %18.10g", RWK[19], sqrt(RWK[18] / double(N)));

        if (MODE >= 3) {
            /*
30        FORMAT('&wktype 3111',/,'&atext x ''iter''')
          WRITE(LUOUT,30)
35        FORMAT('&vars = com',I3.3,/,'&atext y ''x',I3,'''',
     $           /,'&run')
          WRITE(LUOUT,35) (I,I,I=1,N)
36        FORMAT('&vars = com',I3.3,/,'&atext y ''',A,'''',
     $           /,'&run')
          WRITE(LUOUT,36) N+1,'Normf ',N+2,'Normx '
C39       FORMAT('&stop')
C         WRITE(LUOUT,39)
*/
        }
    }
}

//  ------------------------------------------------------------
/**
 *    Summary :
 *
 *     E N O R M : Return the norm to be used in exit (termination)
 *                 criteria
 *
 *     Input parameters
 *     ================
 *
 *     N         Int Number of equations/unknowns
 *     Z(N)     Dble  The vector, of which the norm is to be computed
 *     XW(N)    Dble  The scaling values of Z(N)
 *
 *    Output
 *    ======
 *
 *     WNORM(N,Z,XW)  Dble  The mean square root norm of Z(N) subject
 *                          to the scaling values in XW(N):
 *                          = Sqrt( Sum(1,...N)((Z[I]/XW[I])**2) / N )
*/
// ------------------------------------------------------------
double NonLinearEquationSolver::WNORM(int N, double *Z, double *XW) {
    double S = 0.0;
    for (int i = 0; i < N; ++i) {
        S = S + (Z[i] / XW[i]) * (Z[i] / XW[i]);
    }
    return sqrt( S / double(N) );
} // end package
