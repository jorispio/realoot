// $Id$
// ---------------------------------------------------------------------------------
/*
 *   This file is part of SpaceMechanicsToolBox.
 *
 * Copyright (C) 2016 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#ifndef NON_LINEAR_EQUATION_SOLVER_HPP
#define NON_LINEAR_EQUATION_SOLVER_HPP


/** c++11 enum class */
enum class NonLinearSolverExitCode : int  {
    Ok = 0,
    JacobianMatrixSingular = 1,  // 9.2.1 Termination, since jacobian matrix became singular
    MaxIterationReached = 2,    // 9.2.2 Termination after more than NITMAX iterations
    DampingFactorTooSmall = 3,  // 9.2.3 Damping factor FC became too small
    SuperLinearConvergenceSlowed = 4,// 9.2.4.1 Superlinear convergence slowed down
    PrematureConvergence = 5,  // 9.2.4.2 Convergence criterion satisfied before superlinear convergence has been established
    WorkspaceError = 10,      // workspace Error
    BadDimension = 20,        // dimensional parameter N is negative
    NonPositiveRtol = 21,  // Nonpositive RTOL supplied
    NegativeScaling = 22,  // Negative value in XSCAL
    BadMStor = 23,  // Bad parameter for MSTOR
    InvalidOptionValue = 30,  // Invalid option specified
    ErrorN1Fact = 80,   // 9.2.5 Error exit due to linear solver routine N1FACT
    ErrorN1Solve = 81,  // 9.2.6 Error exit due to linear solver routine N1SOLV
    ErrorUserFcn = 82,  // 9.2.7 Error exit due to fail of user function FCN
    ErrorUserJac = 83  // 9.2.8 Error exit due to fail of user function JAC
};

/** */
#include "NonLinearEquationSolverOptions.hpp"

// ---------------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------------
class NonLinearEquationSolver {
 protected:
    virtual long int Function(int N, const double *X, double *FU) = 0;
    virtual long int Jacobian(int N, int M1, const double *X, double *A) = 0;

    double EPMACH;  // D1MACH(3)
    double SMALL;  // D1MACH(6)
    double GREAT;  // sqrt(D1MACH(2)/TEN);

    /** Solver options */
    NonLinearEquationSolverOptions Options;

 private:
    /** */
    double SUMXA;
    /** Holds the previous value of CONV */
    double CONVA;

    /** */
    void NLEQ1(int N,
                /*void (*FCN)(), void (*JAC)(), */
                double *X, double *XSCAL,
                double RELTOL, long int IOPT[],
                int &IERR, int LIWK, long int IWK[], int LRWK, double RWK[]);

    /** */
    void N1INT(int N,
        /*void (*FCN)(), void (*JAC)(), */
        double* X, double *XSCAL, double RELTOL,
        int &NITMAX, int NONLIN,
        long int *IOPT, int &IERR,
        int LRWK, double* RWK, int NRWKFR, int LIWK, long int *IWK, int NIWKFR, int M1, int M2, int NBROY,
        double* A, double *DXSAVE, double *DX, double *DXQ, double *XA, double *XWA, double* F, double* FA, double* ETA,
        double* XW, double* FW, double* DXQA, double* T1, double* T2, double* T3,
        double FC, double FCMIN,
        double SIGMA, double SIGMA2, double FCA, double FCKEEP,
        double FCPRI, double DMYCOR,
        double &CONV, double &SUMX, double &DLEVF, int &MSTOR, int &MPRERR,
        int MPRMON, int MPRSOL,
        int LUERR, int LUMON, int LUSOL,
        long int &NITER, long int &NCORR, long int &NFCN, long int &NJAC, long int &NFCNJ,
        long int &NREJR1, long int &NEW,
        bool QBDAMP);

    /** Check options */
    int N1PCheck(int N, double *X, double *XSCAL, double RELTOL,
        long int *IOPT,
        int &LIWK, long int *IWK, int &LRWK, double *RWK);

    /** */
    void N1SCAL(int N, const double *X, const double *XA, const double *XSCAL,
                    double *XW, int ISCAL, bool QINISC,
                    long int *IOPT, int LRWK, double *RWK);

    /** */
    void N1SCRF(int M, int N, double *A, double *FW);

    /** */
    void N1SCRB(int N, int LDA, int ML, int MU, double *A, double *FW);

    /** */
    void N1FACT(int N, int LDA, int ML, int MU, double *A, long int *IOPT,
            long int &IFAIL, int LIWK, long int *IWK,
            int &LAIWK, int LRWK,
            double *RWK, int &LARWK);

    /** */
    void N1SOLV(int N, int LDA, int ML, int MU, double *A, double *B, long int *IOPT,
            long int &IFAIL, int LIWK, long int *IWK, int LAIWK,
            int LRWK, double *RWK, int LARWK);

     /** */
    void N1LVLS(int N, const double *DX1, const double *XW, const double *F,
            double *DXQ,
            double &CONV, double &SUMX, double &DLEVF, int MPRMON, bool QDSCAL);

    /** */
    void N1JAC(/*void (*FCN)(), */
                int N, int LDA, double *X, const double *FX, double *A, const double *YSCAL,
                double AJDEL, double AJMIN, long int &NFCN, double *FU, long int &IFAIL);

    /** */
    void N1JACB(/*void (*FCN)(), */
                    int N, int LDA, int ML, double *X, double *FX, double *A,
                    double *YSCAL, double AJDEL, double AJMIN, long int &NFCN, double *FU, double *U, double *W,
                    long int &IFAIL);

    /** */
    void N1JCF(/*void (*FCN)(), */
                int N, int LDA, double *X, double *FX, double *A, double *YSCAL,
                double *ETA, double ETAMIN, double ETAMAX, double ETADIF,
                double &CONV, long int &NFCN, double *FU,
                long int &IFAIL);

    /** */
    void N1JCFB(/*void (*FCN)(),*/
                int N, int LDA, int ML, double *X, double *FX, double *A,
                double *YSCAL, double *ETA,
                double ETAMIN, double ETAMAX, double ETADIF, double &CONV, long int &NFCN,
                double *FU, double *U, double *W,
                long int &IFAIL);

    /** */
    void N1PRV1(double DLEVF, double DLEVX, double FC, int NITER, int NEW, int MPRMON, int LUMON, bool QMIXIO);

    /** */
    void N1PRV2(double DLEVF, double DLEVX, double FC, int NITER, int MPRMON, int LUMON, bool QMIXIO, char CMARK);

    /** */
    void N1SOUT(int N, const double *X, int MODE, const long int *IOPT,
                const double *RWK, int NRW,
                const long int *IWK, int NIW, int MPRINT, int &LUOUT);

    /** */
    double WNORM(int N, double *Z, double *XW);

    void PrintErrorMessage(int IERR, int MPRERR, int NITMAX, int NITER, double FC, bool QMSTOP, int ICONV, long int IFAIL);

 public:
    /** */
    NonLinearEquationSolver();

    /** */
    virtual ~NonLinearEquationSolver();

    /** */
    int solve(int N, double *X, double *XSCAL, double rtol,
            NonLinearEquationSolverOptions options = NonLinearEquationSolverOptions());
};

// ---------------------------------------------------------------------------------
#endif  // NON_LINEAR_EQUATION_SOLVER_HPP
