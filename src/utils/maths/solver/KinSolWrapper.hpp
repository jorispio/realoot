// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2020 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __REALOOT_UTILS_MATHS_SOLVER_SUNDIALS_KINSOLWRAPPER_HPP__
#define __REALOOT_UTILS_MATHS_SOLVER_SUNDIALS_KINSOLWRAPPER_HPP__


#include "kinsol/kinsol.h"           /* access to KINSOL func., consts. */
#include "nvector/nvector_serial.h"  /* access to serial N_Vector       */
#include "NonLinearEquationSolverOptions.hpp"


class KinsolWrapper {
 protected:	
	virtual long int Function(int N, const double* X, double* FU) = 0;
	virtual long int Jacobian(int N, int M1, const double* X, double* A) = 0;

 private:
	 int print_level;
	 N_Vector  u = NULL;
	 N_Vector  scale = NULL;
	 void* kmem = NULL;
	 long int  nni, nfe;

	 /* Check function return values */
	 int check_retval(void* returnvalue, const char* funcname, int opt);

 public:
	KinsolWrapper() {};
	virtual ~KinsolWrapper();

	int init(int n_equations, NonLinearEquationSolverOptions options);

	int solve(int n, double* x);

	long int KsFunction(N_Vector x, N_Vector f, void* user_data);
};

#endif  // __REALOOT_UTILS_MATHS_SOLVER_SUNDIALS_KINSOLWRAPPER_HPP__
