// $Id$
// -----------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 * Copyright (C) 2016 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#ifndef NLP_SOLVER_OPTIONS_HPP
#define NLP_SOLVER_OPTIONS_HPP

/** */
struct NonLinearEquationSolverOptions {
/*
 *      1  QSUCC  0        =0 (.FALSE.) initial call:
 *                            NLEQ1 is not yet initialized, i.e. this is
 *                            the first call for this nonlinear system.
 *                            At successfull return with MODE=1,
 *                            QSUCC is set to 1.
 *                         =1 (.TRUE.) successive call:
 *                            NLEQ1 is initialized already and is now
 *                            called to perform one or more following
 *                            Newton-iteration steps.
 *                            ATTENTION:
 *                               Don't destroy the contents of
 *                               IOPT(i) for 1 <= i <= 50 ,
 *                               IWK(j)  for 1 <= j < NIWKFR and
 *                               RWK(k)  for 1 <= k < NRWKFR.
 *                               (Nevertheless, some of the options, e.g.
 *                                FCMIN, SIGMA, MPR..., can be modified
 *                                before successive calls.)
*/
    bool QSUCC;

/*       2  MODE   0        =0 Standard mode initial call:
 *                            Return when the required accuracy for the
 *                            iteration vector is reached. User defined
 *                            parameters are evaluated and checked.
 *                            Standard mode successive call:
 *                            If NLEQ1 was called previously with MODE=1,
 *                            it performs all remaining iteration steps.
 *                         =1 Stepwise mode:
 *                            Return after one Newton iteration step.
*/
    int MODE;

/*       3  JACGEN 0        Method of Jacobian generation
 *                         =0 Standard method is JACGEN=2
 *                         =1 User supplied subroutine JAC will be
 *                            called to generate Jacobian matrix
 *                         =2 Jacobian approximation by numerical
 *                            differentation (see subroutines N1JAC
 *                            and N1JACB)
 *                         =3 Jacobian approximation by numerical
 *                            differentation with feedback control
 *                            (see subroutines N1JCF and N1JCFB)
*/
    int JACGEN;

/*       4  MSTOR  0        =0 The Jacobian A is a dense matrix
 *                         =1 A is a band matrix
*/
    int MSTOR;

/*       6  ML     0        Lower bandwidth of A (excluding the
 *                         diagonal);
 *                         IOPT(6) ignored, if IOPT(4).NE.1
*/
    int ML;
/*       7  MU     0        Upper bandwidth of A (excluding the
 *                         diagonal);
 *                         IOPT(7) ignored, if IOPT(4).NE.1
*/
    int MU;

/*       9  ISCAL  0        Determines how to scale the iterate-vector:
 *                         =0 The user supplied scaling vector XSCAL is
 *                            used as a (componentwise) lower threshold
 *                            of the current scaling vector
 *                         =1 The vector XSCAL is always used as the
 *                            current scaling vector
*/
    int ISCAL;

/*
 *     11  MPRERR 0        Print error messages
 *                         =0 No output
 *                         =1 Error messages
 *                         =2 Warnings additionally
 *                         =3 Informal messages additionally
*/
    int MPRERR;

/*      12  LUERR  6        Logical unit number for error messages
 */
/*      13  MPRMON 0        Print iteration Monitor
 *                         =0 No output
 *                         =1 Standard output
 *                         =2 Summary iteration monitor additionally
 *                         =3 Detailed iteration monitor additionally
 *                         =4,5,6 Outputs with increasing level addi-
 *                            tional increasing information for code
 *                            testing purposes. Level 6 produces
 *                            in general extremely large output!
 */
    int MPRMON;
/*      14  LUMON  6        Logical unit number for iteration monitor
 */
/*      15  MPRSOL 0        Print solutions
 *                         =0 No output
 *                         =1 Initial values and solution values
 *                         =2 Intermediate iterates additionally
 */
    int MPRSOL;

/*      16  LUSOL  6        Logical unit number for solutions
 */

/*      19  MPRTIM 0        Output level for the time monitor
 *                         = 0 : no time measurement and no output
 *                         = 1 : time measurement will be done and
 *                               summary output will be written -
 *                               regard note 5a.
*/
/*      20  LUTIM  6        Logical output unit for time monitor
 */

/*      31  NONLIN 3        Problem type specification
 *                         =1 Linear problem
 *                            Warning: If specified, no check will be
 *                            done, if the problem is really linear, and
 *                            NLEQ1 terminates unconditionally after one
 *                            Newton-iteration step.
 *                         =2 Mildly nonlinear problem
 *                         =3 Highly nonlinear problem
 *                         =4 Extremely nonlinear problem
 */
    int NONLIN;

/*      32  QRANK1 0        =0 (.FALSE.) Rank-1 updates by Broyden-
 *                            approximation are inhibited.
 *                         =1 (.TRUE.) Rank-1 updates by Broyden-
 *                            approximation are allowed.
 */
    bool QRANK1;

/*      33  QORDI  0        =0 (.FALSE.) Standard program mode
 *                         =1 (.TRUE.)  Special program mode:
 *                            Ordinary Newton iteration is done, e.g.:
 *                            No damping strategy and no monotonicity
 *                            test is applied
 */
    bool QORDI;
/*      34  QSIMPL 0        =0 (.FALSE.) Standard program mode
 *                         =1 (.TRUE.)  Special program mode:
 *                            Simplified Newton iteration is done, e.g.:
 *                            The Jacobian computed at the starting
 *                            point is fixed for all subsequent
 *                            iteration steps, and
 *                            no damping strategy and no monotonicity
 *                            test is applied.
 */
    bool QSIMPL;
/*      35  QNSCAL 0        Inhibit automatic row scaling:
 *                         =0 (.FALSE.) Automatic row scaling of
 *                            the linear system is activ:
 *                            Rows i=1,...,N will be divided by
 *                            max j=1,...,N (abs(a(i,j)))
 *                         =1 (.TRUE.) No row scaling of the linear
 *                            system. Recommended only for well row-
 *                            scaled nonlinear systems.
*/
    bool QNSCAL;

/*      38  IBDAMP          Bounded damping strategy switch:
 *                         =0 The default switch takes place, dependent
 *                            on the setting of NONLIN (=IOPT(31)):
 *                            NONLIN = 0,1,2,3 -> IBDAMP = off ,
 *                            NONLIN = 4 -> IBDAMP = on
 *                         =1 means always IBDAMP = on
 *                         =2 means always IBDAMP = off
*/
    int IBDAMP;

/*      39  IORMON          Convergence order monitor
 *                         =0 Standard option is IORMON=2
 *                         =1 Convergence order is not checked,
 *                            the iteration will be always proceeded
 *                            until the solution has the required
 *                            precision RTOL (or some error condition
 *                            occured)
 *                         =2 Use additional 'weak stop' criterion:
 *                            Convergence order is monitored
 *                            and termination due to slowdown of the
 *                            convergence may occur.
 *                         =3 Use additional 'hard stop' criterion:
 *                            Convergence order is monitored
 *                            and termination due to superlinear
 *                            convergence slowdown may occur.
 *                         In case of termination due to convergence
 *                         slowdown, the warning code IERR=4 will be
 *                         set.
 *                         In cases, where the Newton iteration con-
 *                         verges but superlinear convergence order has
 *                         never been detected, the warning code IERR=5
 *                         is returned.
*/
    int IORMON;


/* *    31   NITMAX IN     Maximum number of permitted iteration
 *                       steps (default: 50)
 */
    int NITMAX;

/*     36   NBROY  IN     Maximum number of possible consecutive
 *                       iterative Broyden steps. The total real
 *                       workspace needed (RWK) depends on this value
 *                       (see LRWK above).
 *                       Default is N (see parameter N),
 *                       if MSTOR=0 (=IOPT(4)),
 *                       and ML+MU+1 (=IOPT(6)+IOPT(7)+1), if MSTOR=1
 *                       (but minimum is always 10) -
 *                       provided that Broyden is allowed.
 *                       If Broyden is inhibited, NBROY is always set to
 *                       zero.
*/
    int NBROY;

/*     20   FCBND  IN     Bounded damping strategy restriction factor
 *                       (Default is 10)
*/

    double FCBND;
/*     21   FCSTRT IN     Damping factor for first Newton iteration -
 *                       overrides option NONLIN, if set (see note 6)
*/
    double FCSTRT;
/*     22   FCMIN  IN     Minimal allowed damping factor (see note 6)*/
    double FCMIN;
/*     23   SIGMA  IN     Broyden-approximation decision parameter
 *                       Required choice: SIGMA.GE.1. Increasing this
 *                       parameter make it less probable that the algo-
 *                       rith performs rank-1 updates.
 *                       Rank-1 updates are inhibited, if
 *                       SIGMA.GT.1/FCMIN is set. (see note 6)
*/
    double SIGMA;

/*     24   SIGMA2 IN     Decision parameter about increasing damping
 *                       factor to corrector if predictor is small.
 *                       Required choice: SIGMA2.GE.1. Increasing this
 *                       parameter make it less probable that the algo-
 *                       rith performs rank-1 updates.
*/
    double SIGMA2;
/*     26   AJDEL  IN     Jacobian approximation without feedback:
 *                       Relative pertubation for components
 *                       (Default: sqrt(epmach*10), epmach: relative
 *                        machine precision)
*/
    double AJDEL;

/*     27   AJMIN  IN     Jacobian approximation without feedback:
 *                       Threshold value (Default: 0.0d0)
 *                         The absolute pertubation for component k is
 *                         computed by
 *                         DELX := AJDEL*max(abs(Xk),AJMIN)
*/
    double AJMIN;
/*     28  ETADIF  IN     Jacobian approximation with feedback:
 *                       Target value for relative pertubation ETA of X
 *                       (Default: 1.0d-6)
*/
    double ETADIF;
/*     29  ETAINI  IN     Jacobian approximation with feedback:
 *                       Initial value for denominator differences
 *                       (Default: 1.0d-6)
*/
    double ETAINI;

    NonLinearEquationSolverOptions();
};

#endif  // NLP_SOLVER_OPTIONS_HPP
