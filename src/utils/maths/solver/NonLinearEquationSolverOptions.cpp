// $Id$
// -----------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 * Copyright (C) 2016 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#include "NonLinearEquationSolverOptions.hpp"

NonLinearEquationSolverOptions::NonLinearEquationSolverOptions() {
    QSUCC = false;  // the first call for this nonlinear system.
    MODE = 0;       // Standard mode initial call
    JACGEN = 2;     // Jacobian approximation by numerical differentation
    MSTOR = 0;      // Jacobian is a dense matrix
    ML = MU = 0;    // lower and upper band index in the Jacobian
    ISCAL = 0;      // The user supplied scaling vector XSCAL is used as a (componentwise) lower threshold of the current scaling vector
    MPRMON = 1;     // Standard output
    MPRERR = 2;     // Error messages. Warnings additionally.
    MPRSOL = 1;     // Initial values and solution values
    NONLIN = 2;     // Midly nonlinear problem
    QRANK1 = false, // Rank-1 updates by Broyden-approximation are inhibited.
    QORDI = false;  // Standard program mode
    QSIMPL = false;  // Standard program mode
    QNSCAL = false;  // Automatic row scaling of the linear system is activ
    IBDAMP = 0;     // Bounded damping strategy switch
    IORMON = 0;     // Convergence order monitor
    NITMAX = 50;    // Maximum number of permitted iteration steps
    NBROY = 0;      // Maximum number of possible consecutive  iterative Broyden steps

    FCBND = 0.;     // Bounded damping strategy restriction factor
    FCSTRT = 0.;    // Damping factor for first Newton iteration - overrides option NONLIN
    FCMIN = 0.;     // Minimal allowed damping factor
    SIGMA = 0.;     // Broyden-approximation decision parameter
    SIGMA2 = 0.;    // Decision parameter about increasing damping factor to corrector if predictor is small.
    AJDEL = 0.;     // Jacobian approximation without feedback: Relative pertubation for components
    AJMIN = 0.;     // Jacobian approximation without feedback: Threshold value
    ETADIF = 1.0e-6;  // Jacobian approximation with feedback: Target value for relative pertubation ETA of X
    ETAINI = 1.0e-6;  // Jacobian approximation with feedback: Initial value for denominator differences
}
