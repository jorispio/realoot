// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2020 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <functional>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "KinSolWrapper.hpp"

#include <sunmatrix/sunmatrix_dense.h> /* access to dense SUNMatrix       */
#include <sunlinsol/sunlinsol_dense.h> /* access to dense SUNLinearSolver */
#include <sundials/sundials_types.h>   /* defs. of realtype, sunindextype */

static KinsolWrapper* kinsolWrapperClass;

// -------------------------------------------------------------------------
// C callback solver function
int FPFunction(N_Vector x, N_Vector f, void* user_data) {
	if (kinsolWrapperClass != NULL) {
		return kinsolWrapperClass->KsFunction(x, f, user_data);
	}
	return 1; // stop
}

// -------------------------------------------------------------------------
KinsolWrapper::~KinsolWrapper() {
//	SUNLinSolFree(LS);
//	SUNMatDestroy(J);
	if (u) N_VDestroy(u);
	if (scale) N_VDestroy(scale);
	if (kmem) KINFree(&kmem);
}
// -------------------------------------------------------------------------
/**
 */
int KinsolWrapper::init(int n_equations, NonLinearEquationSolverOptions options) {

	// Create vectors for solution and scales	
	u = N_VNew_Serial(n_equations);
	if (check_retval((void*)u, "N_VNew_Serial", 0)) return(1);

	scale = N_VClone(u);
	if (check_retval((void*)scale, "N_VClone", 0)) return(1);

	// Initialize and allocate memory for KINSOL
	kmem = KINCreate();
	if (check_retval((void*)kmem, "KINCreate", 0)) return(1);
	
	/* Set number of prior residuals used in Anderson acceleration */
	long int  maa = 0;           /* no acceleration */
	int retval = KINSetMAA(kmem, maa);

	print_level = options.MPRMON;
	retval = KINSetPrintLevel(kmem, print_level);
	//if (check_retval(&retval, "KINSetPrintLevel", 1)) return(1);

	retval = KINSetFuncNormTol(kmem, 1e-10);
	//if (check_flag(&flag, "KINSetFuncNormTol", 1)) return(1);

	retval = KINSetNumMaxIters(kmem, options.NITMAX);

	//N_Vector c = N_VNew_Serial(n_equations);
	//N_VConst(0, c);
	//retval = KINSetConstraints(kmem, c);

	//auto callback_ptr = static_cast<std::function<FPFunction> *>(KsFunction);
	kinsolWrapperClass = this;
	auto callback_ptr = FPFunction;
	retval = KINInit(kmem, callback_ptr, u);
	if (check_retval(&retval, "KINInit", 1)) return(1);

	/* Create dense SUNMatrix */
	SUNMatrix J = SUNDenseMatrix(n_equations, n_equations);
	if (check_retval((void*)J, "SUNDenseMatrix", 0)) return(1);

	/* Create dense SUNLinearSolver object */
	SUNLinearSolver LS = SUNLinSol_Dense(u, J);
	if (check_retval((void*)LS, "SUNLinSol_Dense", 0)) return(1);

	/* Attach the matrix and linear solver to KINSOL */
	int flag = KINSetLinearSolver(kmem, LS, J);
	if (check_retval(&flag, "KINSetLinearSolver", 1)) return(1);

	return 0;
}
// -------------------------------------------------------------------------
// wrapping variable type
long int KinsolWrapper::KsFunction(N_Vector x, N_Vector f, void* user_data) {
	realtype* xd = N_VGetArrayPointer(x);
	realtype* fd = N_VGetArrayPointer(f);
	Function(0, xd, fd);
	return 0;
}

// -------------------------------------------------------------------------
/**
 */
int KinsolWrapper::solve(int n, double *x) {
	/* Initial guess */
	/* Get vector data array */
	realtype* data = N_VGetArrayPointer(u);
	if (check_retval((void*)data, "N_VGetArrayPointer", 0)) return(1);

	for (int i = 0; i < n; ++i) {
		data[i] = x[i];
	}

	/* No scaling used */
	N_VConst(1, scale);

	/* Call main solver */
	int retval = KINSol(kmem,         /* KINSol memory block */
						u,            /* initial guess on input; solution vector */
						KIN_LINESEARCH, /* global strategy choice: KIN_FP, KIN_LINESEARCH, KIN_NONE*/
						scale,        /* scaling vector, for the variable cc */
						scale);       /* scaling vector for function values fval */
	if (check_retval(&retval, "KINSol", 1)) return(1);

	/* get solver stats */
	retval = KINGetNumNonlinSolvIters(kmem, &nni);
	check_retval(&retval, "KINGetNumNonlinSolvIters", 1);

	retval = KINGetNumFuncEvals(kmem, &nfe);
	check_retval(&retval, "KINGetNumFuncEvals", 1);

	// solution
	if (check_retval((void*)data, "N_VGetArrayPointer", 0)) return(1);
	for (int i = 0; i < n; ++i) {
		x[i] = data[i];
	}

	if (print_level >= 1) {
		printf("\nFinal Statistics:\n");
		printf("Number of nonlinear iterations: %6ld\n", nni);
		printf("Number of function evaluations: %6ld\n", nfe);

		printf("x = [");
		for (int i = 0; i < n; ++i) {
			printf("%18.6f ", x[i]);
		}
		printf("]\n");
	}
	return 0;
}

/* -----------------------------------------------------------------------------
 * Check function return value
 *   opt == 0 check if returned NULL pointer
 *   opt == 1 check if returned a non-zero value
 * ---------------------------------------------------------------------------*/
int KinsolWrapper::check_retval(void* returnvalue, const char* funcname, int opt) {
	/* Check if the function returned a NULL pointer -- no memory allocated */
	if (opt == 0) {
		if (returnvalue == NULL) {
			fprintf(stderr, "\nERROR: %s() failed -- returned NULL\n\n", funcname);
			return(1);
		}
		else {
			return(0);
		}
	}

	/* Check if the function returned an non-zero value -- internal failure */
	if (opt == 1) {
		int* errflag = (int*)returnvalue;
		if (*errflag != 0) {
			fprintf(stderr, "\nERROR: %s() failed -- returned %d\n\n", funcname, *errflag);
			return(1);
		}
		else {
			return(0);
		}
	}

	/* if we make it here then opt was not 0 or 1 */
	fprintf(stderr, "\nERROR: check_retval failed -- Invalid opt value\n\n");
	return(1);
}

