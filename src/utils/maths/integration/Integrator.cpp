// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of SpaceMechanicsToolBox.
 *
 *   Copyright (C) 2011-2015 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
// ---------------------------------------------------------------------

/** \class Integrator
  *
  * \brief The Integrator class
  *
  * The %Integrator class is the generic class that handle the different integrator classes.
  *
  * The %Integrator allows using dynamically DOPRI5 or DOP853 just by changing the ode solver ID.
  *
 */
#include "Integrator.hpp"

#include <string>

#include "LtException.hpp"

// ---------------------------------------------------------------------
Integrator::Integrator(uint n, ODE_SOLVER_ID solver_type_)
    : INTEGRATOR(n), solver_type(solver_type_), initialised(false) {
}

// ---------------------------------------------------------------------
/** Set the maximum number of iterations over one integration step
 */
// ---------------------------------------------------------------------
void Integrator::SetNMax(uint n) {
    INTEGRATOR::nmax = n;
}
// ---------------------------------------------------------------------
/** Return the degree of the RK polynomials for interpolation.
 */
// ---------------------------------------------------------------------
real_type Integrator::cont(uint i, real_type t) {
    return INTEGRATOR::cont(i, t);
}

// ---------------------------------------------------------------------
/** Return the continuous solution using the RK polynomials for interpolation.
 */
// ---------------------------------------------------------------------
void Integrator::cont(ODE_SOLVER_ID solver_type, OdePolyData5 data, real_type t, real_type* y) {
    if (solver_type != ODE_DOPRI5) {
        throw LtException("ODE_SOLVER_ID can only be ODE_DOPRI5!");
    }
    data.cont5(t, y);
}

// ---------------------------------------------------------------------
/**
 */
// ---------------------------------------------------------------------
ODE_SOLVER_ID Integrator::getSolverId(std::string solver_name) {
    return ODE_SOLVER_ID::ODE_RK4;  // INTEGRATOR;
}

// ---------------------------------------------------------------------
/**
 */
// ---------------------------------------------------------------------
std::string Integrator::getSolveName(ODE_SOLVER_ID id) {
    return std::string("RK4");
}
