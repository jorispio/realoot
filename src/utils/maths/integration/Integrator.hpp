// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of SpaceMechanicsToolBox.
 *
 *   Copyright (C) 2011-2015 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
// ---------------------------------------------------------------------

#ifndef INTEGRATOR_HPP
#define INTEGRATOR_HPP

#include <string>

#include "core/LtException.hpp"
#include "core/DataTypes.hpp"
#include "core/SizeParameters.hpp"

#include "HelioLib/Maths.hpp"

//#define INTEGRATOR RK4_LT
#define INTEGRATOR DOPRI5

class Integrator : public INTEGRATOR {
 public:
    OdeStepperMode stepFlags;

 private:
    ODE_SOLVER_ID solver_type;
    bool initialised;

 public:
    /** Constructor. */
    Integrator(uint n, ODE_SOLVER_ID solver_type);

    /** Destructor. */
    ~Integrator(void) { };

    /** Set max dimension. */
    void SetNMax(uint n);

    /** Set solver. */
    inline void SetSolver(ODE_SOLVER_ID solver_type_) {
        solver_type = solver_type_;
    }

    /** Switch on dense output for the first N variables */
    void setmaxdense(unsigned N) {
        INTEGRATOR::setmaxdense(N);
    }

    /** Switch on dense output for variable i */
    bool setdense(unsigned i) {
        return INTEGRATOR::setdense(i);
    }

    ///
    inline long nfcnRead() const {
        return INTEGRATOR::nfcnRead();
    }

    /// Number of used steps.
    inline long nstepRead() const {
        return INTEGRATOR::nstepRead();
    }

    /// Number of accepted steps.
    inline long naccptRead() const {
        return INTEGRATOR::naccptRead();
    }

    /// Number of rejected steps.
    inline long nrejctRead() const {
        return INTEGRATOR::nrejctRead();
    }

    //
    inline int Initialise(OdeStepperMode stepflags) {
        stepFlags = stepflags;
        initialised = true;
        return INTEGRATOR::Initialise(stepflags);
    }

    //
    inline int solve(real_type& x,
                     real_type* y,
                     real_type xend,
                     real_type h,        ///< initial step size
                     real_type hmax = 0) {  ///< maximal step size
        assert(initialised);
        return INTEGRATOR::solve_normal(x, y, xend, stepFlags, hmax, h);
    }

    /// Set the absolute and relative tolerance globally for all indices (1E-10 by default)
    inline void setTolerances(real_type abstol, real_type reltol) {
        INTEGRATOR::settol(abstol, reltol);
    }

    /// Set the absolute and relative tolerance for each index
    inline void setTolerances(const real_type* abstol, const real_type* reltol) {
        INTEGRATOR::settol(abstol, reltol);
    }

    /** */
    real_type cont(uint i, real_type t);

    /** */
    static void cont(ODE_SOLVER_ID solver_type, OdePolyData5 data, real_type t, real_type* y);

    /** */
    static ODE_SOLVER_ID getSolverId(std::string solver_name);

    /** */
    static std::string getSolveName(ODE_SOLVER_ID id);
};

// ----------------------------------------------------------------
#endif  // INTEGRATOR_HPP
