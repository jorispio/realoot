// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UtilsEnum.hpp"

#include <stdio.h>
#include <string>

// -------------------------------------------------------------------------
DYNAMICALMODEL_TYPE getOrbitEnum(const char* orbitParameterTypeStr) {
    if (strcasecmp(orbitParameterTypeStr, "CARTESIAN") == 0) {
        return D_CARTESIAN;
    } else if (strcasecmp(orbitParameterTypeStr, "EQUINOCTIAL") == 0) {
        return D_EQUINOCTIAL;
    } else if (strcasecmp(orbitParameterTypeStr, "KEPLERIAN") == 0) {
        return D_KEPLERIAN;
    }

    char msg[1024];
    snprintf(msg, 1024, "Orbit type is not properly defined: %s {CARTESIAN, EQUINOCTIAL, KEPLERIAN}\n", orbitParameterTypeStr);
    throw LtException(msg);
}
// -------------------------------------------------------------------------
TerminalConstraintType getConstraintEnum(const char* terminalConstraintTypeStr) {
    if (strcasecmp(terminalConstraintTypeStr, "ORBIT") == 0) {
        return C_ORBIT;
    } else if (strcasecmp(terminalConstraintTypeStr, "ENERGY") == 0) {
        return C_ENERGY;
    } else if (strcasecmp(terminalConstraintTypeStr, "SMA_ECC_INC") == 0) {
        return C_SMA_ECC_INC;
    } else if (strcasecmp(terminalConstraintTypeStr, "SMA_ECC_INC_AOP") == 0) {
        return C_SMA_ECC_INC_AOP;
    } else if (strcasecmp(terminalConstraintTypeStr, "SMA_ECC_INC_RAAN") == 0) {
        return C_SMA_ECC_INC_RAAN;
    } 

    // error
    char msg[1024];
    snprintf(msg, 1024, "Target/type type is not properly defined: %s {ORBIT, ENERGY, SMA_ECC_INC, C_SMA_ECC_INC_AOP, SMA_ECC_INC_RAAN}\n", terminalConstraintTypeStr);
    throw LtException(msg);
}

// -------------------------------------------------------------------------
ContinuationVariable getContinuationVariableEnum(const char* variableNameStr) {
    if (strcasecmp(variableNameStr, "INIT_MASS") == 0) {
        return ContinuationVariable::MASS0;
    }
    else if (strcasecmp(variableNameStr, "THRUST") == 0) {
        return ContinuationVariable::FTH;
    }
    else if (strcasecmp(variableNameStr, "J2") == 0) {
        return ContinuationVariable::J2;
    }
    else if (strcasecmp(variableNameStr, "NOT_SET") == 0) {
        return ContinuationVariable::NOT_SET;
    }
    
    char msg[1024];
    snprintf(msg, 1024, "Continuation variable name is unknown: %s\nAllowed values: {INIT_MASS, THRUST, J2, NOT_SET}\n", variableNameStr);
    throw LtException(msg);
}

// -------------------------------------------------------------------------
SolverType getSolverTypeEnum(const char* solverTypeStr) {
    if (strcasecmp(solverTypeStr, "NLEQ") == 0) {
        return SolverType::SOLVER_NLEQ;
    }
    else if (strcasecmp(solverTypeStr, "NLEQ2") == 0) {
        return SolverType::SOLVER_NLEQ2;
    }
    else if (strcasecmp(solverTypeStr, "KINSOL") == 0) {
        return SolverType::SOLVER_KINSOL;
    }
    
    char msg[1024];
    snprintf(msg, 1024, "SolverType name is unknown: %s\nAllowed values: {NLEQ, NLEQ2, KINSOL}\n", solverTypeStr);
    throw LtException(msg);
}
