// $Id$
// ---------------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __ABSTRACT_INPUT_SCRIPT_READER_HPP__
#define __ABSTRACT_INPUT_SCRIPT_READER_HPP__

#include "Enumerations.hpp"
#include "LtSpacecraftData.hpp"
#include "LtProblemContext.hpp"
#include "LtProblemDefinition.hpp"
#include "StateNumericalPropagator.hpp"
#include "xml/UtilsStringXml.hpp"

class AbstractInputScriptReader {
 protected:
    int set_mode(const char* modeParameterTypeStr, LtProblemContext& problemContext);
    int set_objective(const char* objective, LtProblemDefinition& problemDefinition);
    void set_format(const char* strAttrData, LtProblemContext& problemContext);

 public:
     AbstractInputScriptReader() {}
    ~AbstractInputScriptReader() {}

    // -------------------------------------------------------------------------
    void Default(LtProblemDefinition& problemDefinition, LtProblemContext& problemContext) {
        // set a default value for optional parameters
        problemContext.reportFilename = "";
        problemContext.outputFilename = "";
        problemContext.outputFormat = D_EQUINOCTIAL;
        problemContext.outputNbOfPoints = -1;
    }
};

#endif // __ABSTRACT_INPUT_SCRIPT_READER_HPP__
