// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ctime>
#include <random>

#include "LtAvgOcpMainInterface.hpp"
#include "core/SizeParameters.hpp"// IDX_***
#include "ocp/MinTimeOptimalControl.hpp"
#include "ode/TDynamicsAveragedEquinoctial.hpp"
#include "ProblemSolver.hpp"
#include "utils/report/EphemerisReport.hpp"
#include "utils/report/SolutionReport.hpp"


void getInitialConditions(double* S0, LtProblemDefinition& problemDefinition, LtProblemContext& problemContext) {
    memcpy(S0, problemDefinition.state0.equiAdapt, 6 * sizeof(double));
    S0[IDX_L] = 0;
    S0[IDX_MASS] = problemDefinition.state0.mass;

    S0[STATE_SIZE] = problemContext.initguess[0];
    S0[STATE_SIZE + 1] = problemContext.initguess[1];
    S0[STATE_SIZE + 2] = problemContext.initguess[2];
    S0[STATE_SIZE + 3] = problemContext.initguess[3];
    S0[STATE_SIZE + 4] = problemContext.initguess[4];
    S0[STATE_SIZE + 5] = 0;
    S0[STATE_SIZE + 6] = problemContext.initguess[5];
    S0[IDX_L1] = problemContext.initguess_duration;  // final longitude
    S0[IDX_T] = 0;  // t
}

// -------------------------------------------------------------------------
int processPropagation(LtProblemDefinition &problemDefinition, LtProblemContext &problemContext, bool verbose, double *lastPoint, ProblemScaling &scaling) {
    real_type S0[ODE_STATE_SIZE];
    getInitialConditions(S0, problemDefinition, problemContext);

    LtThrusterData thrusterDef = problemDefinition.spacecraft.propulsion;
    thrusterDef.PropType = PROP_NEP;

    // dynamical model
    DYNAMICALMODEL_TYPE outputFormat = problemContext.outputFormat;
    TDynamicsAveragedEquinoctial *dynamicalModel = new TDynamicsAveragedEquinoctial(problemDefinition.mu, problemDefinition.cj2, problemDefinition.radius, 
                                thrusterDef,
                                problemDefinition.eclipse);

    // set scaling for averaging
    dynamicalModel->setScalingCoefficients(scaling);
    S0[IDX_SMA] /= scaling.distance;
    S0[IDX_MASS] /= scaling.mass;

    MinTimeOptimalControl *optimalControl = new MinTimeOptimalControl(dynamicalModel, OBJECTIVE_TYPE::C_TIME);

    StateNumericalPropagator *stateNumericalPropagator = new StateNumericalPropagator(dynamicalModel, optimalControl, ODE_SOLVER_ID::ODE_DOPRI5);
    stateNumericalPropagator->setNumberOfPointsPerOrbit(problemContext.outputNbOfPoints);

    if (verbose) {
        printf("Propagating: theta0=0. thetaf=%f, format=%d\n",  problemContext.initguess_duration, int(outputFormat));
        printf("S0 = [");
        for (int idx = 0; idx < ODE_STATE_SIZE; ++idx) printf("%18.12f ", S0[idx]);
        printf("]\n");
    }

    int res = 0;
    try {
        res = stateNumericalPropagator->solve(0., S0, 1., outputFormat);
    } 
    catch (std::exception &e) {
        //printf("Exception during propagation: %s\n", e.what());
        printf("Exception during propagation\n");
    }

    if (verbose) {
        printf("Return code: %d\n", res);
    }

    if (lastPoint) {
        stateNumericalPropagator->getLastPoint(lastPoint);
        lastPoint[IDX_SMA] *= scaling.distance;
        lastPoint[IDX_MASS] *= scaling.mass;
    }

    // write to file
    if (res > 0) {
        printEphemeris(stateNumericalPropagator, problemDefinition, problemContext, scaling, verbose);
    }

    delete stateNumericalPropagator;
    delete optimalControl;
    delete dynamicalModel;
    return res;
}

/* ------------------------------------------------------------------------- */
int processSolving(LtProblemDefinition &problemDefinition, LtProblemContext &problemContext, LtProblemSolution &solution, bool verbose) {

    ProblemSolver* problemSolver = new ProblemSolver(problemDefinition);

    int res = problemSolver->solve(problemContext, solution, verbose);

    int return_code = -1;
    if ((res == 0) || (res == 4)) {        
        // propagate and return last point
        double pts[ODE_STATE_SIZE];
        processPropagation(problemDefinition, problemContext, verbose, pts, solution.scaling);

        double m0 = problemDefinition.state0.mass;
        double mf = pts[IDX_MASS];
        double dm = m0 - mf;
        
        solution.dm = dm;
        solution.computeDeltaV(problemDefinition);
        solution.computeThrustDuration(problemDefinition);
        memcpy(solution.finalState, pts, STATE_SIZE * sizeof(double));

        if (verbose) {
            printf("Solution:\n");
            printf("   lambda    = %f %f %f %f %f %f,  theta=%f\n", solution.solution[0], solution.solution[1], solution.solution[2], solution.solution[3], solution.solution[4], solution.solution[5], solution.solution[6]);
            printf("   longitude = %f rad (%.1f revs)\n", solution.solution[6] / solution.scaling.longitude, solution.nbRevolutions);
            printf("   tof       = %.3f days\n", solution.transferDuration);
            printf("   dm        = %.3f kg\n", dm);
            printf("   dv        = %.3f m/s\n", solution.dvcost);
        }


        printSolutionFile(problemContext, problemDefinition, solution.scaling, solution, verbose);

        return_code  = 0;
    } 
    else {
        if (verbose) {
            printf("No solution found (res=%d)!\n", res);
        }
    }

    // free resource
    delete problemSolver;

    return return_code;
}
/* ------------------------------------------------------------------------- */
void printSolutionFile(LtProblemContext &problemContext, LtProblemDefinition &problemDefinition,
        ProblemScaling &scaling, LtProblemSolution &solution, bool verbose) {
    if (!problemContext.reportFilename.empty()) {
        if (verbose) {
            printf("Writing solution file\n");
        }

        SolutionReport *report = new SolutionReport();

        if (!report->printSolutionToFile(problemContext, problemDefinition, scaling, solution)) {
            printf("   Failed writing solution to file!\n");
        }

        delete report;
    }
}
/* ------------------------------------------------------------------------- */
void printEphemeris(StateNumericalPropagator* stateNumericalPropagator, LtProblemDefinition& problemDefinition, LtProblemContext& problemContext, ProblemScaling& scaling, bool verbose) {
    if (!problemContext.outputFilename.empty()) {
        if (verbose) {
            printf("Writing trajectory to file\n");
        }

        EphemerisReport *report = new EphemerisReport(stateNumericalPropagator, problemDefinition, problemContext);
        double a_m = scaling.distance;
        double a_s = scaling.time;
        double a_kg = scaling.mass;
        double a_longitude = 1. / scaling.longitude;

        if (!report->printSolutionToFile(problemContext.outputFilename.c_str(), problemContext.outputCostates, a_m, a_s, a_kg, a_longitude)) {
            printf("   Failed writing solution to file!\n");
        }

        delete report;
    }
}

/* ------------------------------------------------------------------------- */
void process(LtProblemDefinition &problemDefinition, LtProblemContext &problemContext, bool verbose) {
    // optimisation mode
    if (problemContext.performOptimisation) {
        LtProblemSolution problemSolution;  // not used
        processSolving(problemDefinition, problemContext, problemSolution, verbose);
    }

    // simulation mode
    if (problemContext.performSimulation) {
        ProblemScaling scaling = ProblemScaling();

        scaling.mass = problemDefinition.state0.mass;
        scaling.distance = problemDefinition.statef.getSma();
        scaling.gravParameter = problemDefinition.mu;
        scaling.velocity = sqrt(scaling.gravParameter / scaling.distance);  // normalisation for m/s
        scaling.longitude = ((scaling.distance * scaling.distance) / scaling.gravParameter) * (problemDefinition.spacecraft.propulsion.Fth / scaling.mass);  // scale for gravitational constant, rad
        double g0Isp = problemDefinition.spacecraft.propulsion.g0Isp;
        double qFuel = problemDefinition.spacecraft.propulsion.Fth / problemDefinition.spacecraft.propulsion.g0Isp;
        scaling.time = scaling.mass / qFuel * scaling.velocity / g0Isp;
        processPropagation(problemDefinition, problemContext, verbose, NULL, scaling);
    }
}
