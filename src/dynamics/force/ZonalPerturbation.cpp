/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of SpaceMechanicsToolBox.
 *
 *   Copyright (C) 2010 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#include <math.h>
#include <stdlib.h>

#include "ZonalPerturbation.hpp"
#include "core/LtException.hpp"
#include "core/SizeParameters.hpp"// IDX_***


// ---------------------------------------------------------------------
ZonalPerturbation::ZonalPerturbation(double jzonal, int deg, double re, double mup)
    : J0(jzonal * pow(re, deg)), degree(deg), mu(mup) {
    if ((deg < 2) || (deg > 6)) {
        throw LtException("Invalid order for zonal perturbation. 2 <= order <= 6.\n");
    }
}

// ---------------------------------------------------------------------
void ZonalPerturbation::setScaling(const double distScale, const double gravScale_) {
    J0.scale(1. / pow(distScale, degree));
    mu.scale(1. / gravScale_);
}
// ---------------------------------------------------------------------
void ZonalPerturbation::getLongitudeRate(const double *Oe, double &longiRate, double *longiRateDerivatives) {

    double sma = Oe[0];
    double ex = Oe[1];     // ex=e*cos(w+W), ey=e*sin(w+W)
    double ey = Oe[2];
    double eccSq = Oe[1] * Oe[1] + Oe[2] * Oe[2];
    double ecc = sqrt(eccSq);
    double hx = Oe[3];         // hx=tan(i/2)*cos(W),hy=tan(i/2)*sin(W))
    double hy = Oe[4];
    double inc = 2 * atan(sqrt((hx * hx + hy * hy)));

    if (fabs(inc) < epsilon_double) return;
    if (J0.isZero()) return;

    // common coefficients
    double K = 0.75 * J0.value() * sqrt(mu.value());
    double cosi = cos(inc);
    double f2 = 1. - eccSq;
    double f = sqrt(f2);
    double D = sqrt(sma * sma * sma) * sma * sma * f2 * f2;  // denominator in following expressions

    // secular drift
    longiRate = K * (3. * cosi * cosi - 1.) * f / D;   // mean anomaly: effect of J2

    if (longiRateDerivatives != NULL) {
        double sini = sin(inc);
        double didhx = 2. / (1 + hx * hx + hy * hy) * hx / sqrt(hx * hx + hy * hy);
        double didhy = 2. / (1 + hx * hx + hy * hy) * hy / sqrt(hx * hx + hy * hy);
        double f3 = f2 * f;
        double f4 = f2 * f2;
        double dedex = ex / ecc;
        double dedey = ey / ecc;
        double dfdex =-ecc / f * dedex;
        double dfdey =-ecc / f * dedey;

        longiRateDerivatives[0] =-7./2. * longiRate / sma;
        longiRateDerivatives[1] = K * (3 * cosi * cosi - 1) * (dfdex / D - f / D * (4 * f3 * dfdex / (f4)));
        longiRateDerivatives[2] = K * (3 * cosi * cosi - 1) * (dfdey / D - f / D * (4 * f3 * dfdey / (f4)));
        longiRateDerivatives[3] =-K * (6 * cosi * sini * didhx) * f / D;
        longiRateDerivatives[4] =-K * (6 * cosi * sini * didhy) * f / D;
    }
}
// ---------------------------------------------------------------------
void ZonalPerturbation::getRate(const double *Oe, double &pomdot, double &gomdot,
                        double *pomdotDerivatives,
                        double *gomdotDerivatives) {
    double sma = Oe[0];
    double ex = Oe[1];     // ex=e*cos(w+W), ey=e*sin(w+W)
    double ey = Oe[2];
    double eccSq = ex * ex + ey * ey;
    double ecc = sqrt(eccSq);
    double hx = Oe[3];         // hx=tan(i/2)*cos(W),hy=tan(i/2)*sin(W))
    double hy = Oe[4];
    double inc = 2 * atan(sqrt((hx * hx + hy * hy)));

    if (fabs(inc) < epsilon_double) {
        pomdot = 0;
        gomdot = 0;
        return;
    }
    if (J0.isZero()) return;

    // common coefficient
    double K = 0.75 * J0.value() * sqrt(mu.value());
    double cosi = cos(inc);
    double sini = sin(inc);
    double f2 = 1. - eccSq;
    double D = sqrt(sma * sma * sma) * sma * sma * f2 * f2;  // denominator in following expressions

    // secular drifts
    pomdot = K * (5. * cosi * cosi - 1.) / D;        // arg of perigee
    gomdot = -2. * K * cosi / D;                // RAAN

    if (pomdotDerivatives || gomdotDerivatives) {
        // Compute df/dx, where f = [dsma/dtheta, dex/dtheta, dey/dtheta, dhx/dtheta, dhy/dtheta]
        double dedex = ex / ecc;
        double dedey = ey / ecc;
        double didhx = 2. / (1 + hx * hx + hy * hy) * hx / sqrt(hx * hx + hy * hy);
        double didhy = 2. / (1 + hx * hx + hy * hy) * hy / sqrt(hx * hx + hy * hy);
        double f = sqrt(f2);
        double dfde =-ecc / f;

        if (pomdotDerivatives) {
            double dpomde =-4. * pomdot * dfde / f;           // d(pomdot)/d(exc)

            pomdotDerivatives[0] = -3.5 * pomdot / sma;             // d(pomdot)/d(sma)
            pomdotDerivatives[1] = dpomde * dedex;
            pomdotDerivatives[2] = dpomde * dedey;
            pomdotDerivatives[3] = -K * (10 * cosi * sini) * didhx / D;
            pomdotDerivatives[4] = -K * (10 * cosi * sini) * didhy / D;
        }

        if (gomdotDerivatives) {
            double dgomde =-4. * gomdot * dfde / f;           // d(gomdot)/d(exc)

            gomdotDerivatives[0] = -3.5 * gomdot / sma;             // d(gomdot)/d(sma)
            gomdotDerivatives[1] = dgomde * dedex;
            gomdotDerivatives[2] = dgomde * dedey;
            gomdotDerivatives[3] = 2. * K * sini * didhx / D;
            gomdotDerivatives[4] = 2. * K * sini * didhy / D;
        }
    }
}
// ---------------------------------------------------------------------
/** Get instantaneous ODE of zonal perturbation for Gauss equations
 *
 * @param theta longitude
 * @param Oe     Equinoctial orbital elements (state vector)
 * @param lambda costate vector
 * @return state and costate time derivative
 */
// ---------------------------------------------------------------------
bool ZonalPerturbation::getInstantaneousSensitivity(double theta, const double *Oe, const double *lambda, double *dOedt) {
    if (dOedt) {
        for (int i=0; i < ODE_STATE_SIZE; ++i) {
            dOedt[i] = 0.;
        }
    }
    return false;
}
// ---------------------------------------------------------------------
/** Longitude-averaged ODE for zonal perturbation in Gauss equations
 *
 * @param theta longitude
 * @param Oe     Equinoctial orbital elements (state vector)
 * @param lambda costate vector
 * @return state and costate time derivative
 */
// ---------------------------------------------------------------------
bool ZonalPerturbation::getAverageSensitivity(double theta, const double *Oe, const double *lambda, double *dOedt) {
    if (J0.isZero()) return false;

    //double sma = Oe[0];
    double ex = Oe[1];     // ex=e*cos(w+W), ey=e*sin(w+W)
    double ey = Oe[2];
    double hx = Oe[3];         // hx=tan(i/2)*cos(W),hy=tan(i/2)*sin(W))
    double hy = Oe[4];
    double inc = 2*atan(sqrt((hx * hx + hy * hy)));
    if (fabs(inc) < epsilon_double) return false;

    double pomdot, gomdot;
    double pomdotDerivatives[5], gomdotDerivatives[5];
    getRate(Oe, pomdot, gomdot, pomdotDerivatives, gomdotDerivatives);
    double longiRate, longiRateDerivatives[5];
    getLongitudeRate(Oe, longiRate, longiRateDerivatives);

    dOedt[0] = 0;
    dOedt[1] =-(pomdot + gomdot) * ey;  // dexdt
    dOedt[2] = (pomdot + gomdot) * ex;  // deydt
    dOedt[3] =-gomdot * hy;  // dhxdt
    dOedt[4] = gomdot * hx;  // dhydt
    dOedt[5] = 0;  //dthetadt
    dOedt[6] = 0;  // dmdt

    //
    double dexda =-(pomdotDerivatives[0] + gomdotDerivatives[0]) * ey;
    double dexdex =-(pomdotDerivatives[1] + gomdotDerivatives[1]) * ey;
    double dexdey =-(pomdot + gomdot) - (pomdotDerivatives[2] + gomdotDerivatives[2]) * ey;
    double dexdhx =-(pomdotDerivatives[3] + gomdotDerivatives[3]) * ey;
    double dexdhy =-(pomdotDerivatives[4] + gomdotDerivatives[4]) * ey;
    //
    double deyda = (pomdotDerivatives[0] + gomdotDerivatives[0]) * ex;
    double deydex = (pomdot + gomdot) + (pomdotDerivatives[1] + gomdotDerivatives[1]) * ex;
    double deydey = (pomdotDerivatives[2] + gomdotDerivatives[2]) * ex;
    double deydhx = (pomdotDerivatives[3] + gomdotDerivatives[3]) * ex;
    double deydhy = (pomdotDerivatives[2] + gomdotDerivatives[4]) * ex;
    //
    double dhxda =-gomdotDerivatives[0] * hy;
    double dhxdex =-gomdotDerivatives[1] * hy;
    double dhxdey =-gomdotDerivatives[2] * hy;
    double dhxdhx =-gomdotDerivatives[3] * hy;
    double dhxdhy = -gomdot + -gomdotDerivatives[4] * hy;
    //
    double dhyda = gomdotDerivatives[0] * hx;
    double dhydex = gomdotDerivatives[1] * hx;
    double dhydey = gomdotDerivatives[2] * hx;
    double dhydhx = gomdot + gomdotDerivatives[3] * hx;
    double dhydhy = gomdotDerivatives[4] * hx;
    //
    double dLdtda = longiRateDerivatives[0];
    double dLdtdex = longiRateDerivatives[1];
    double dLdtdey = longiRateDerivatives[2];
    double dLdtdhx = longiRateDerivatives[3];
    double dLdtdhy = longiRateDerivatives[4];

    // dlambda/dt = -lambda' * df/dx
    dOedt[7] =-(lambda[1] * dexda  + lambda[2] * deyda  + lambda[3] * dhxda  + lambda[4] * dhyda  + lambda[5] * dLdtda);  // dpa
    dOedt[8] =-(lambda[1] * dexdex + lambda[2] * deydex + lambda[3] * dhxdex + lambda[4] * dhydex + lambda[5] * dLdtdex);  // dpex
    dOedt[9] =-(lambda[1] * dexdey + lambda[2] * deydey + lambda[3] * dhxdey + lambda[4] * dhydey + lambda[5] * dLdtdey);  // dpey
    dOedt[10] =-(lambda[1] * dexdhx + lambda[2] * deydhx + lambda[3] * dhxdhx + lambda[4] * dhydhx + lambda[5] * dLdtdhx);  // dhx
    dOedt[11] =-(lambda[1] * dexdhy + lambda[2] * deydhy + lambda[3] * dhxdhy + lambda[4] * dhydhy + lambda[5] * dLdtdhy);  // dhy
    dOedt[12] = 0;  // ptheta/dt
    dOedt[13] = 0;  // pmass/dt

    dOedt[IDX_L1] = 0;  // theta1
    dOedt[IDX_T] = 0;  // time

    return true;
}
