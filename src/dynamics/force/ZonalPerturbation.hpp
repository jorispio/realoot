/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of SpaceMechanicsToolBox.
 *
 *   Copyright (C) 2010 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#ifndef J2_ZONAL_PERTURBATION_HPP
#define J2_ZONAL_PERTURBATION_HPP

#define EARTH_MU 3.9860064e+14
#define EARTH_RADIUS 6378137
#define EARTH_J2 0.00108263

#include "ScaledConstant.hpp"

/** @class ZonalPerturbation
 * @brief Gravitational Zonal Perturbation class
 */
class ZonalPerturbation {
 private:
    ScaledConstant J0;
    int degree;
    ScaledConstant mu;

 public:
    /** Creator. */
    ZonalPerturbation(double jzonal, int degree, double re = EARTH_RADIUS, double mu = EARTH_MU);

    /** Destructor */
    ~ZonalPerturbation() { };

    /** */
    void setScaling(const double distScale, const double gravScale_);

    /** */
    void getLongitudeRate(const double *Oe, double &longiRate, double *longiRateDerivatives);

    /** */
    void getRate(const double *Oe, double &pomdot, double &gomdot, double *pomdotDerivatives, double *gomdotDerivatives);

    /** */
    bool getInstantaneousSensitivity(double theta, const double *Oe, const double *lambda, double *dOedt);

    /** */
    bool getAverageSensitivity(double theta, const double *Oe, const double *lambda, double *dOedt);
};

/** @class J2ZonalPerturbation
 * @brief Gravitational J2 Zonal Perturbation class
 */
class J2ZonalPerturbation : public ZonalPerturbation {
 public:
    /** */
    J2ZonalPerturbation(double j2 = EARTH_J2, double re = EARTH_RADIUS, double mu = EARTH_MU)
        : ZonalPerturbation(j2, 2, re, mu) { };
    /** */
    ~J2ZonalPerturbation(){};
};

// ---------------------------------------------------------------------
#endif  // J2_ZONAL_PERTURBATION_HPP

