// $Id$
// ---------------------------------------------------------------------------
/* ---------------------------------------------------------------------------
 *   This file is part of LT-IPOPT.

 *   Copyright (C) 2010-2015 Joris Olympio

 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.

 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef STATE_NUMERICAL_PROPAGATOR_HPP
#define STATE_NUMERICAL_PROPAGATOR_HPP

#include <vector>

#include "core/SizeParameters.hpp"// IDX_***
#include "core/DataTypes.hpp"
#include "core/Enumerations.hpp"
#include "core/LtProblemContext.hpp"
#include "core/LtSpacecraftData.hpp"
#include "dynamics/NumericalPropagator.hpp"
#include "ode/TDynamicsEquinoctial.hpp"
#include "ocp/OptimalControlBase.hpp"
#include "ocp/Scaling.hpp"
#include "HelioLib/Propagation.hpp"

/* ------------------------------------------------------------------------------- */
class StateNumericalPropagator : public NumericalPropagator {
 private:
    TOptimalControlBase *optimalControl;

    /** working structure with commonly computed variables. */
    DynamicalContextVariables commonVariables;

    ProblemScaling scaling;

    double nbPointsPerOrbit;
    real_type tIntegrationInterval[2];

    /* working areas for trajectory storage. */
    std::vector<double> solTrajectory_t;
    std::vector<double> solTrajectory_theta;
    std::vector<S14VECTOR> solTrajectory_y;
    std::vector<double> solTrajectory_u;
    std::vector<double> solTrajectory_aux;
    DYNAMICALMODEL_TYPE TrajectoryOutputFormat;
    SimulationEventManager<DOPRI5, OdePolyData5>* mEventsHandler;

    double cpu_time_callfcn;

    /** Differential equations. */
    void DiffEqn(real_type x, const real_type* y, real_type* f);

    /** Step handler. */
    int AtStep(long nr, real_type xold, real_type x, const real_type* y);

    /** Store a trajectory data point/ */
    void storePoint(double x, const double* y);

 public:
    /** Constructor. */
    StateNumericalPropagator(TDynamicsEquinoctial* dynamicalModel,
                             TOptimalControlBase *optimalControl,
                             ODE_SOLVER_ID solver_type);

    /** Destructor. */
    ~StateNumericalPropagator(void) { };

    /* */
    bool computeCommonVariables(double t, const double* StateLagranges, DynamicalContextVariables& common);

    TOptimalControlBase * getOptimalControl() const {
        return optimalControl;
    }

    /** Add event handler. */
    void setEventHandler(SimulationEventManager<DOPRI5, OdePolyData5>* handler);

    /** Clear event handler list. */
    void clearEventHandler();

    /* Solve the ODE problem. */
    int solve(double t0, const real_type* S0, double tf, DYNAMICALMODEL_TYPE format);

    /* Set output samping period */
    void setNumberOfPointsPerOrbit(int nbPointsPerOrbit);

    /* State dynamics */
    void dynamics(double t, const double* StateLagranges, double* StateLagranges_f);

    /** Thrust amplitude, N */
    void getThrustAmplitude(real_type t, const double* x, const double* lambda, real_type& F, real_type& P);

    /** Clear trajectory arrays */
    void clearTrajectoryCache(void);

    /** */
    int getTrajectoryPoints(std::vector<double>& tout, std::vector<real_type> &thetaOut, std::vector<S14VECTOR>& xout);

    /** */
    int getControlProfile(std::vector<double>& uOut);

    /** */
    int getAuxiliaryVariableHistory(std::vector<double>& auxOut);

    /* Return the last point of the trajectory. */
    void getLastPoint(double* pts);

    /* Return the CPU computational duration */
    double getCPUTime(void);
};

/* --------------------------------------------------------------------- */
#endif  // STATE_NUMERICAL_PROPAGATOR_HPP

