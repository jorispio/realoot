// $Id$
// ---------------------------------------------------------------------------
/* ---------------------------------------------------------------------------
 *   This file is part of LT-IPOPT.

 *   Copyright (C) 2010-2015 Joris Olympio

 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.

 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "NumericalPropagator.hpp"

// ---------------------------------------------------------------------
NumericalPropagator::NumericalPropagator(int node, TDynamicsEquinoctial* dynamics, ODE_SOLVER_ID solver_type)
    : Integrator(node, solver_type), node(node) , dynamicalModel(dynamics), timeStep(0.),
      objectiveFunctionEnum(C_TIME) {
    constMu = dynamicalModel->getGravitationalConstant().value();
    objectiveFunctionEnum = dynamicalModel->getObjectiveFunction();
    setTolerances(ATOL, RTOL);
    y_final = new real_type[node];
}
// ---------------------------------------------------------------------
NumericalPropagator::~NumericalPropagator() {
    if (y_final) {
        delete[] y_final;
    }
}
// ---------------------------------------------------------------------
/*
 * Set ODE integration tolerances.

 * @param atol absolute tolerance
 * @param rtol relative tolerance
 */
/* --------------------------------------------------------------------- */
void NumericalPropagator::setOptions(real_type atol_, real_type rtol_, real_type step) {
    setTolerances(atol_, rtol_);
    timeStep = step;
}
