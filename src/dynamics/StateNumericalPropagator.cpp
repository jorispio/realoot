// $Id$
// ---------------------------------------------------------------------
/* ---------------------------------------------------------------------------
 *   This file is part of LT-IPOPT.

 *   Copyright (C) 2010-2015 Joris Olympio

 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.

 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "StateNumericalPropagator.hpp"
#include "core/LtException.hpp"
#include "maths/integration/OdePolyData.hpp"
#include "ode/TDynamicsAveragedEquinoctial.hpp"

// ---------------------------------------------------------------------
StateNumericalPropagator::StateNumericalPropagator(
                     TDynamicsEquinoctial* dynamicalModel,
                     TOptimalControlBase *optimalControl_,
                     ODE_SOLVER_ID solver_type)
    : NumericalPropagator(ODE_STATE_SIZE, dynamicalModel, solver_type),
      optimalControl(optimalControl_),
      mEventsHandler(NULL) {
    nbPointsPerOrbit = 0;
    cpu_time_callfcn = 0;
    setmaxdense(ODE_STATE_SIZE);
    Integrator::SetNMax(1000000);
    Integrator::Initialise(call_for_dense_output);
    scaling = ProblemScaling();
}
// ---------------------------------------------------------------------
/** Set output sampling period.

 * @param dt  approximate number of points for output trajectory file
 */
// ---------------------------------------------------------------------
void StateNumericalPropagator::setNumberOfPointsPerOrbit(int nbPointsPerOrbit_) {
    nbPointsPerOrbit = static_cast<double>(nbPointsPerOrbit_);
}

// ---------------------------------------------------------------------
/*
 * @param t current time
 * @param x current state/costate vector
 * @param x_dot
 * @param common common variable structure
 */
// ---------------------------------------------------------------------
bool StateNumericalPropagator::computeCommonVariables(double s,
                                       const double* state_costate,
                                       DynamicalContextVariables& common) {
    return dynamicalModel->computeCommonVariables(s, state_costate, common);
}
// ---------------------------------------------------------------------
/*
 * @return CPU computational time
 */
// ---------------------------------------------------------------------
real_type StateNumericalPropagator::getCPUTime(void) {
    return cpu_time_callfcn / 1000.;
}
// ---------------------------------------------------------------------
/** Set event handler.
 */
// ---------------------------------------------------------------------
void StateNumericalPropagator::setEventHandler(SimulationEventManager<DOPRI5, OdePolyData5>* handler) {
    mEventsHandler = handler;
    if (handler != NULL) {
        setmaxdense(ODE_STATE_SIZE);
    }
}

// ---------------------------------------------------------------------
/** Clear event handler.
 */
void StateNumericalPropagator::clearEventHandler() {
    mEventsHandler = NULL;
}
// ---------------------------------------------------------------------
/*
 * State+CoState dynamics
 * @param t   current time
 * @param s0  current state+costate vector
 * @param ds  state+costate time derivative at s0.
 */
// ---------------------------------------------------------------------
void StateNumericalPropagator::DiffEqn(real_type t, const real_type* s0, real_type* ds) {
    dynamics(t, s0, ds);
}

// ---------------------------------------------------------------------
/*
 * Called at each integration step.
 * Function which is called during integration if output is activated.
 * While this function is called, dense output (member function cont() ) is
 * available within the range [xold, x].

 * @param nr The call number (automatically incremented integer)
 * @param xold The last value of the affine parameter
 * @param x The current value of the affine parameter
 * @param y The solutions y(x) for the most recent affine parameter x
 */
// ---------------------------------------------------------------------
int StateNumericalPropagator::AtStep(long nr, real_type s_old, real_type s, const real_type* y) {
    bool isStoppingEvent = false;
    if (mEventsHandler != NULL) {
        OdePolyData5 odedata = NumericalPropagator::getContinuousPolyData();
        isStoppingEvent |= mEventsHandler->AtStep(nr, s_old, s, y, odedata);

        if ((isStoppingEvent) && (mEventsHandler->getEvents().size() > 0)) {
            // TODO(joris) find the earliest stopping event
            s = mEventsHandler->getEvents().at(0).date;
        }
    }

    if (nbPointsPerOrbit <= 0) {
        storePoint(s, y);
    } else {
        // get last recorded point to be on the period-grid
        real_type theta_old = solTrajectory_theta.at(solTrajectory_theta.size() - 1);
        real_type theta_new = y[IDX_L];
        real_type n = (theta_new - theta_old) / scaling.longitude / (2.0 * M_PI) * nbPointsPerOrbit;
        real_type p = (s - s_old) / n;

        // interpolate to get intermediate points in [s_old, s]
        // s_old is on the period-grid.
        real_type yt[ODE_STATE_SIZE];
        for (real_type st = s_old + p; st < s; st += p) {
            // interpolation with ODE embedded polynomial
            for (int i = 0; i < ODE_STATE_SIZE; ++i) {
                yt[i] = cont(i, st);
            }
            storePoint(st, yt);
        }

        // last point of the interval, if not on the grid
        if (fabs(s - tIntegrationInterval[1]) < p) {
            storePoint(s, y);
        }
    }

    // update value of final point
    memcpy(y_final, y, ODE_STATE_SIZE * sizeof(real_type));

    return (isStoppingEvent)? -1 : 0;
}
// ---------------------------------------------------------------------
/*
 * Store a point of the solution trajectory.
 * @param x current time
 * @param y current state
 */
// ---------------------------------------------------------------------
void StateNumericalPropagator::storePoint(double x, const double* y) {
    if (TrajectoryOutputFormat == D_NOT_SET) {
        return;
    }

    S14VECTOR vec(14);
    vec << y[0], y[1], y[2], y[3], y[4], y[5], y[6], /* costate */y[7], y[8], y[9], y[10], y[11], y[12], y[13];
    solTrajectory_y.push_back(vec);

    double fThrustAmplitude = 0;
    double fSolarPowerRatio = 1;
    getThrustAmplitude(x, y, y + STATE_SIZE, fThrustAmplitude, fSolarPowerRatio);

    solTrajectory_u.push_back(fThrustAmplitude);
    solTrajectory_aux.push_back(fSolarPowerRatio);

    // we could have used mass to estimate time: (1 - y[12])
    solTrajectory_t.push_back(y[IDX_T]);
    solTrajectory_theta.push_back(y[IDX_L]);
}
// ---------------------------------------------------------------------
/*
 * Solve the ODE system.

 * @param theta0  initial date
 * @param S0   initial state/costate vector
 * @param thetaf  final date
 * @param format output format (cartesian or equinoctial)

 * @return the exitflag.
 */
// ---------------------------------------------------------------------
int StateNumericalPropagator::solve(real_type theta0,
                     const real_type* S0,
                     real_type thetaf,
                     DYNAMICALMODEL_TYPE format) {
    dynamicalModel->setObjectiveFunction(objectiveFunctionEnum);
    dynamicalModel->getScalingCoefficients(scaling);

    // output format: Cartesian, Keplerian, Equinoctial
    TrajectoryOutputFormat = format;

    if (S0 == NULL) {
        throw LtException("*** Initial condition vector is NULL\n");
    }

    real_type S0_[ODE_STATE_SIZE];
    memcpy(S0_, S0, ODE_STATE_SIZE * sizeof(real_type));

    // start integration
    clock_t cpu_t1 = clock();
    tIntegrationInterval[0] = theta0;
    tIntegrationInterval[1] = thetaf;
    storePoint(tIntegrationInterval[0], S0_);
    int res = Integrator::solve(tIntegrationInterval[0], S0_, tIntegrationInterval[1], timeStep);
    if (res < 0) {
        char str[1024];
        snprintf(str, 1024, "*** Integration error (%d) at t=%f in [%f, %f] in StateNumericalPropagator.\n*** timeStep=%12.6g\n*** scaling=%12.6g %12.6g %12.6g %12.6g %12.6g %12.6g\n",
                   res, tIntegrationInterval[0], theta0, thetaf, 
                   timeStep,
                   scaling.distance, scaling.gravParameter, scaling.mass, scaling.longitude, scaling.velocity, scaling.time);
        throw LtException(str);
    }

    clock_t cpu_t2 = clock();
    cpu_time_callfcn += difftime(cpu_t2, cpu_t1);

    return res;
}

// ---------------------------------------------------------------------
void StateNumericalPropagator::clearTrajectoryCache(void) {
    solTrajectory_t.clear();
    solTrajectory_theta.clear();
    solTrajectory_y.clear();
    solTrajectory_u.clear();
    solTrajectory_aux.clear();
}

// ---------------------------------------------------------------------
int StateNumericalPropagator::getTrajectoryPoints(std::vector<double>& sout, std::vector<real_type> &thetaOut, std::vector<S14VECTOR>& xout) {
    sout.insert(sout.end(), solTrajectory_t.begin(), solTrajectory_t.end());
    thetaOut.insert(thetaOut.end(), solTrajectory_theta.begin(), solTrajectory_theta.end());
    xout.insert(xout.end(), solTrajectory_y.begin(), solTrajectory_y.end());
    return static_cast<int>(solTrajectory_t.size());
}

// ---------------------------------------------------------------------
int StateNumericalPropagator::getControlProfile(std::vector<double>& uOut) {
    uOut.insert(uOut.end(), solTrajectory_u.begin(), solTrajectory_u.end());
    return static_cast<int>(solTrajectory_t.size());
}

// ---------------------------------------------------------------------
int StateNumericalPropagator::getAuxiliaryVariableHistory(std::vector<double>& auxOut) {
    auxOut.insert(auxOut.end(), solTrajectory_aux.begin(), solTrajectory_aux.end());
    return static_cast<int>(solTrajectory_t.size());
}

// ---------------------------------------------------------------------
void StateNumericalPropagator::getLastPoint(real_type* y) {
    if (y) {
        memcpy(y, y_final, ODE_STATE_SIZE * sizeof(real_type));
    }
}
// ---------------------------------------------------------------------
/*
 * State+CoState Dynamics

 * @param s     current integration variable
 * @param x     current state-costate vector
 * @param dSdt  state-costate time derivative
 */
// ---------------------------------------------------------------------
void StateNumericalPropagator::dynamics(real_type s,
                         const real_type* x,
                         /* OUTPUTS */ real_type* dxds) {
    computeCommonVariables(s, x, commonVariables);
    dynamicalModel->getDynamics(s, commonVariables, dxds);
}

// ---------------------------------------------------------------------
/*
 * @desc Returns the thrust amplitude
 */
// ---------------------------------------------------------------------
void StateNumericalPropagator::getThrustAmplitude(real_type s, const real_type* state,
                                                    const real_type* lambda,
                                                    real_type& F, real_type& P) {
    dynamicalModel->getThrustAmplitude(s, state, lambda, F, P);
}

