// $Id$
/* ---------------------------------------------------------------------------
 *   This file is part of LT-IPOPT.

 *   Copyright (C) 2010-2015 Joris Olympio

 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.

 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef NUMERICAL_PROPAGATOR_HPP
#define NUMERICAL_PROPAGATOR_HPP

#include "core/SizeParameters.hpp"// IDX_***
#include "core/DataTypes.hpp"
#include "ode/TDynamicsEquinoctial.hpp"

#include "Config.hpp" // ODESOLVER_TRAJECTORY
#include "maths/integration/Integrator.hpp"

// ---------------------------------------------------------------------
class NumericalPropagator : public Integrator {
 protected:
    /** ODE system size. */
    int node;

    /** Dynamical model */
    TDynamicsEquinoctial* dynamicalModel;

    /** */
    real_type timeStep;

    /* central body gravitational constant. */
    double constMu;

    /** objective function type. */
    OBJECTIVE_TYPE objectiveFunctionEnum;

    /* working areas */
    // declare everything necessary as dynamical to allow vectorization
    /** final point. */
    real_type* y_final;
    real_type StateLagrangesDot[2 * STATE_SIZE];

    /** Differential equation. */
    virtual void DiffEqn(real_type x, const real_type* y, real_type* f) = 0;

    /** Step handler. */
    virtual int AtStep(long nr, real_type xold, real_type x, const real_type* y) = 0;

 public:
    /** Constructor. */
    NumericalPropagator(int node, TDynamicsEquinoctial* dynamics, ODE_SOLVER_ID solver_type);

    /** Destructor. */
    virtual ~NumericalPropagator();

    /** Set tolerances and integration step. */
    void setOptions(real_type atol, real_type rtol, real_type step);

    /** Set the dynamical instance. */
    void setDynamics(TDynamicsEquinoctial* dynamics) {
        dynamicalModel = dynamics;
    }

    /** Get dynamical instance. */
    TDynamicsEquinoctial* getDynamics() {
        return dynamicalModel;
    }

    /* Set objective function. */
    void setObjective(OBJECTIVE_TYPE objtype) {
        objectiveFunctionEnum = objtype;
    }

    /* Solve the ODE problem. */
    int solve(double t0, double* S0, double tf, double tcoast0, double tcoastf, DYNAMICALMODEL_TYPE format);
};

// ---------------------------------------------------------------------
#endif  // NUMERICAL_PROPAGATOR_HPP
