// $Id$
/* ---------------------------------------------------------------------------
 *   This file is part of LT-IPOPT.

 *   Copyright (C) 2010-2020 Joris Olympio

 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.

 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __DRY_MASS_DETECTOR_HPP_
#define __DRY_MASS_DETECTOR_HPP_

#include "propagation/numerical/events/BaseSimulationEventHandler.hpp"

class DryMassEventDetector : public BaseSimulationEventHandler {
 private:
    double target_value;

 public:
    DryMassEventDetector(double drymass) : BaseSimulationEventHandler("dry-mass-detector", ""), target_value(drymass) { }
    ~DryMassEventDetector() { }

    void set_target_value(double value) { target_value = value; }

    real_type switching_function(real_type , real_type* x) {
        return x[IDX_MASS] - target_value;
    }
};

// ------------------------------------------------------------------------
#endif  // __DRY_MASS_DETECTOR_HPP_
