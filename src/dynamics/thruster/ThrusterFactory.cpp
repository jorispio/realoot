// $Id$
// ---------------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2015 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ThrusterFactory.hpp"

//#include "ThrusterSep.hpp"
#include "ThrusterNep.hpp"

#include "core/LtException.hpp"

/* ----------------------------------------------------------------------- */
/** Thruster creator.
 *
 * @param model
 * @param type parameters
 */
/* ----------------------------------------------------------------------- */
Thruster *createThruster(const LtThrusterData& ThrusterDef, const LtEclipseParameters& eclipseParameters) {
    PROPULSION_TYPE type = ThrusterDef.PropType;
    if (type == PROP_NEP) {
        return new THRUSTER_NEP(ThrusterDef);
    } else if (type == PROP_SEP) {
        // return new THRUSTER_SEP(ThrusterDef);
        throw LtException("SEP thruster is not implemented!\n");
    } else {
        throw LtException("Unknown thruster type!\n");
    }
}
