// $Id$
// ---------------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ThrusterNep.hpp"
#include "core/LtException.hpp"

// ---------------------------------------------------------------------------
THRUSTER_NEP::THRUSTER_NEP(const LtThrusterData& thrusterDefinition)
    : Thruster(thrusterDefinition) {
    if (thrusterDefinition.PropType != PROP_NEP) {
        throw LtException("Invalid thrust propulsion system!");
    }

    P = getNominalPower();
}
// ---------------------------------------------------------------------------
THRUSTER_NEP::~THRUSTER_NEP() {
    // if (pl) delete pl;
}
// ---------------------------------------------------------------------------
/** Set the thruster caracteristics
 * Fth is the nominal thrust amplitude.
 * Isp is the constant specific impulse.
 */
// --------------------------------------------------------------------------- */
void THRUSTER_NEP::setThruster(const LtThrusterData& thrusterDefinition) {
    Thruster::setThruster(thrusterDefinition);

    Fth = thrusterDefinition.Fth;
    g0Isp = thrusterDefinition.g0Isp;
    eff = thrusterDefinition.eff;
    q = Fth / g0Isp;
    P = getNominalPower();
    type = thrusterDefinition.PropType;
}

// ---------------------------------------------------------------------------
/** Set the thruster caracteristics
 * P0 is the power at 1AU, in the case of the SEP system.
 */
// --------------------------------------------------------------------------- */
void THRUSTER_NEP::setPower(double P0_, double g0Isp_, double nu_) {
    g0Isp = g0Isp_;
    P = P0_;
    eff = nu_;
    Fth = P * (2 * eff) / g0Isp;
}

// ---------------------------------------------------------------------------
/** Returns the thrust amplitude, for a given throttle, and spacecraft position
 * with respect to the Sun
 *
 * F_NEP = F0
 * F_SEP = F0 * (r0/r)^2
 *
 * @param t     current date, MJD2000
 * @param R_Sc_Cb position vector of the S/C wrt the central body (e.g. Sun, Earth)
 * @return thrust amplitude
 */
// ---------------------------------------------------------------------------
double THRUSTER_NEP::getUnperturbedThrust(double, const Vector3&) {
    return Fth.value();
}
// ---------------------------------------------------------------------------
/**
 * @param t     current date, MJD2000
 * @param R_Sc_Cb position vector of the S/C wrt the central body (e.g. Sun, Earth)
 * @return thrust power
 */
// ---------------------------------------------------------------------------
double THRUSTER_NEP::getAvailablePower(double , const Vector3&) {
    return P;
}

// ---------------------------------------------------------------------------
/** Returns the thrust amplitude, for a given throttle, and spacecraft position
 * with respect to the Sun
 *
 * F_NEP = F0
 * F_SEP = F0 * (r0/r)^2
 *
 * @param t     current date, MJD2000
 * @param R_Sc_Cb position vector of the S/C wrt the central body (e.g. Sun, Earth)
 * @return thrust amplitude
 */
// ---------------------------------------------------------------------------
double THRUSTER_NEP::getThrust(double t, const Vector3& R_Sc_Cb) {
    return getUnperturbedThrust(t, R_Sc_Cb);
}
// ---------------------------------------------------------------------------
/** Returns the derivative of thrust amplitude with respect to the position
 * for a given throttle, and spacecraft position
 * with respect to the Sun
 *
 * @param t  current date, MJD2000
 * @param R_Sc_Cb position vector of the S/C wrt the central body (e.g. Sun, Earth)
 * @param dFr
 * @param dQr
 * @return the thrust force
 *
 */
// ---------------------------------------------------------------------------
double THRUSTER_NEP::getDerivatives(real_type, const Vector3&, Vector3& dFr, Vector3& dQr, bool useSmoothMin) {
    double F = Fth.value();
    dFr = Vector3::Zero();
    dQr = Vector3::Zero();

    return F;
}
// ---------------------------------------------------------------------------
/** Returns the time derivative of thrust amplitude
 * for a given throttle, and spacecraft position
 * with respect to the Sun
 *
 * @param t  current date, MJD2000
 * @param R_Sc_Cb position vector of the S/C wrt the central body (e.g. Sun, Earth)
 * @return the thrust force time-derivative
 */
// ---------------------------------------------------------------------------
double THRUSTER_NEP::getTimeDerivative(real_type t, const Vector3& ) {
    return 0;
}
// ---------------------------------------------------------------------------
/** Returns the 2nd derivative of thrust amplitude with respect to the position
 * for a given throttle, and spacecraft position
 * with respect to the Sun
 *
 * @param t  current date, MJD2000
 * @param R_Sc_Cb distance to sun
 * @param d2Fr
 * @param d2Qr
 *
 */
// ---------------------------------------------------------------------------
void THRUSTER_NEP::getSecondDerivatives(real_type, const Vector3&, Matrix3& d2Fr, Matrix3& d2Qr) {
    d2Fr = Matrix3::Zero();
    d2Qr = Matrix3::Zero();
}
