// $Id$
// ---------------------------------------------------------------------------
/** ---------------------------------------------------------------------------
 * Low-Thrust Thruster model
 *
 * Thruster class to model NEP and SEP systems.
 *
 * NEP systems are characterized with a constant thrust amplitude and fuel mass
 * flow rate. The input and output powers are also fixed.
 *
 * SEP systems performance depends on the distance to the Sun. Those systems
 * explicitely depends on the input power, coming from solar panels. The input
 * and output powers, the thrust amplitude, and the mass flow rate thus vary
 * with the distance to the Sun. However, the thrust amplitude, and subsequent
 * system variables, is limited.
 *
 * Author: Joris Olympio
 * Date: 03/2011
 * @version $Revision$
 * @lastrevision $Date$
 * @modifiedby $LastChangedBy$
 * @lastmodified $LastChangedDate$
 * @filesource $URL: svn+ssh://joris@192.168.1.2/SVN_REPOSITORY/LT-IPOPT/src/dynamics/propulsion/Thruster.hpp $
 *
 * --------------------------------------------------------------------------- */
#ifndef LTIPOPT_THRUSTER_NEP_HPP
#define LTIPOPT_THRUSTER_NEP_HPP

#include <stdlib.h>
#include <math.h>

#include "Thruster.hpp"
#include "DataTypes.hpp"
#include "core/LtSpacecraftData.hpp"

// ---------------------------------------------------------------------------
class THRUSTER_NEP : public Thruster {
 private:

    /** Get unperturbed (eclipse) thrust */
    double getUnperturbedThrust(double t, const Vector3& R_Sc_Cb);

 public:
    /** Creator */
    explicit THRUSTER_NEP(const LtThrusterData& ThrusterDef);

    ~THRUSTER_NEP();

    /** Set the thruster caracteristics. */
    void setThruster(const LtThrusterData& ThrusterDef);

    /** Set the thruster caracteristics */
    void setPower(double P0_, double g0Isp_, double nu_ = 1);

    /** return the thrust amplitude */
    double getThrust(double t, const Vector3& r_sun);

    /** return available power ratio [0-1]. */
    double getAvailablePower(double t, const Vector3& R_Sc_Cb);

    /* Returns the derivative of thrust amplitude with respect to the position
 * for a given throttle, and spacecraft position
 * with respect to the Sun
 */
    double getDerivatives(real_type t, const Vector3& R_sun, Vector3& dFr, Vector3& dQ, bool useSmoothMin = true);

    /* Return the time derivative of the thrust amplitude. */
    double getTimeDerivative(real_type t, const Vector3& R_Sc_Cb);

    /* Return second-order derivative of thrust amplitude wrt position vector. */
    void getSecondDerivatives(real_type t, const Vector3& R, Matrix3& d2Fr, Matrix3& d2Q);
};

// ---------------------------------------------------------------------------
#endif  // LTIPOPT_THRUSTER_NEP_HPP
