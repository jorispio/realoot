// $Id$
// ---------------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
/** ---------------------------------------------------------------------------
 * Low-Thrust Thruster model
 *
 * Thruster class to model NEP and SEP systems.
 *
 * NEP systems are characterized with a constant thrust amplitude and fuel mass
 * flow rate. The input and output powers are also fixed.
 *
 * SEP systems performance depends on the distance to the Sun. Those systems
 * explicitely depends on the input power, coming from solar panels. The input
 * and output powers, the thrust amplitude, and the mass flow rate thus vary
 * with the distance to the Sun. However, the thrust amplitude, and subsequent
 * system variables, is limited.
 *
 * Author: Joris Olympio
 *
 * --------------------------------------------------------------------------- */
#ifndef LTIPOPT_THRUSTER_HPP
#define LTIPOPT_THRUSTER_HPP

#include <stdlib.h>
#include <math.h>

#include "DataTypes.hpp"
#include "LtSpacecraftData.hpp"
#include "ScaledConstant.hpp"

// ---------------------------------------------------------------------------
class Thruster {
 private:

 protected:
    /** Thruster data */
    LtThrusterData mThrusterData;

    /** SEP or NEP */
    PROPULSION_TYPE type;

    /** thruster nominal thrust amplitude. */
    ScaledConstant Fth;
    /** thruster exhaust velocity. */
    ScaledConstant g0Isp;
    /** thruster mass flow rate */
    double q;
    /** thruster power */
    double P;
    /** thruster efficiency (power conversion electrical->jet) */
    double eff;
    /** */
    bool constantAcceleration;

 public:
    /** Creator */
    Thruster(const LtThrusterData& ThrusterDef);

    /** Destructor */
    virtual ~Thruster();

    /** return Thruster type */
    PROPULSION_TYPE getType(void) {
        return type;
    };

    /** Set the thruster caracteristics. */
    void setThruster(const LtThrusterData& ThrusterDef);

    /** Get thruster data. */
    LtThrusterData getThrusterData() const;

    /** Set the thruster caracteristics */
    void setPower(double P0_, double g0Isp_, double nu_ = 1);

    /** return the nominal thrust amplitude at 1AU*/
    inline ScaledConstant getNominalThrust() {
        return Fth;
    }
    inline void setNominalThrust(double f) {
        Fth.setValue(f);
    }
    inline void scaleNominalThrust(double s) {
        Fth.scale(s);
    }

    /** return the exhaust velocity g0*Isp */
    //inline double GetExhaustVelocity() {
    //    return g0Isp.value();
    //}
    inline ScaledConstant getExhaustVelocity() {
        return g0Isp;
    }
    inline void setExhaustVelocity(double v) {
        g0Isp.setValue(v);
    }
    inline void scaleExhaustVelocity(double s) {
        g0Isp.scale(s);
    }

    /** return the thrust amplitude */
    virtual double getThrust(double t, const Vector3& r_sun) = 0;

    /** return the thrust throttle (thrust amplitude/norminal thrust amplitude) */
    double getThrottle(double t, const Vector3& r_sun);

    /* return the fuel mass flow rate */
    virtual double getMassFlowRate(double t, const Vector3& r_sun);

    /* returns the jet power */
    inline double getNominalPower(void) {
        return (Fth.getValue() * g0Isp.getValue()) / (2 * eff);
    }

    /* returns the jet power */
    inline double getPower(double t, const Vector3& rsun) {
        return (getThrust(t, rsun) * g0Isp.value()) / (2 * eff);
    }

    /** is constant acceleration (constant mass) required. */
    bool isConstantAcceleration() {
        return constantAcceleration;
    }
    
    /** return available power ratio [0-1]. */
    double getAvailablePower(double t, const Vector3& R_Sc_Cb);

    /* Returns the derivative of thrust amplitude with respect to the position
     * for a given throttle, and spacecraft position
     * with respect to the Sun
     */
    virtual double getDerivatives(real_type t, const Vector3& R_sun, Vector3& dFr, Vector3& dQ, bool useSmoothMin = true) = 0;

    /* Return the time derivative of the thrust amplitude. */
    virtual double getTimeDerivative(real_type t, const Vector3& R_Sc_Cb) = 0;

    /* Return second-order derivative of thrust amplitude wrt position vector. */
    virtual void getSecondDerivatives(real_type t, const Vector3& R, Matrix3& d2Fr, Matrix3& d2Q) = 0;

    /* */
    int CheckDerivatives(real_type t, const Vector3& R0, const Vector3& V0, const real_type m0, const real_type* X);
};

// ---------------------------------------------------------------------------
#endif  // LTIPOPT_THRUSTER_HPP
