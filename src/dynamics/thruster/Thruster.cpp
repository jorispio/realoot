// $Id$
// ---------------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <vector>

#include "core/FDError.hpp"
#include "Thruster.hpp"

// ---------------------------------------------------------------------------
Thruster::Thruster(const LtThrusterData& thrusterDefinition)
    : mThrusterData (thrusterDefinition),
    type(thrusterDefinition.PropType), Fth(thrusterDefinition.Fth), g0Isp(thrusterDefinition.g0Isp), q(Fth.value() / g0Isp.value()), P(0),
    eff(thrusterDefinition.eff) {
    P = getNominalPower();
    constantAcceleration = thrusterDefinition.constantAcceleration;
}
// ---------------------------------------------------------------------------
Thruster::~Thruster() {
    // if (pl) delete pl;
}
// ---------------------------------------------------------------------------
/** Set the thruster caracteristics
 * Fth is the nominal thrust amplitude.
 * Isp is the constant specific impulse.
 */
// --------------------------------------------------------------------------- */
void Thruster::setThruster(const LtThrusterData& thrusterDefinition) {
    mThrusterData = thrusterDefinition;
    Fth.setValue(thrusterDefinition.Fth);
    g0Isp.setValue(thrusterDefinition.g0Isp);
    eff = thrusterDefinition.eff;
    q = Fth.value() / g0Isp.value();
    P = getNominalPower();
    type = thrusterDefinition.PropType;
    constantAcceleration = thrusterDefinition.constantAcceleration;
}
// ---------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------
LtThrusterData Thruster::getThrusterData() const {
    return mThrusterData;
}

// ---------------------------------------------------------------------------
/** Set the thruster caracteristics
 * P0 is the power at 1AU, in the case of the SEP system.
 */
// --------------------------------------------------------------------------- */
void Thruster::setPower(double P0_, double g0Isp_, double nu_) {
    g0Isp.setValue(g0Isp_);
    P = P0_;
    eff = nu_;
    Fth.setValue(P * (2 * eff) / g0Isp.value());
}

// ---------------------------------------------------------------------------
/**
 * @param t     current date, MJD2000
 * @param R_Sc_Cb position vector of the S/C wrt the central body (e.g. Sun, Earth)
 * @return thrust amplitude
 */
// ---------------------------------------------------------------------------
double Thruster::getAvailablePower(double, const Vector3&) {
    return 1.;
}
// ---------------------------------------------------------------------------
/**
 * @param r_sun  spacecraft-sun position vector.
 * @return the thrust throttle (thrust amplitude/norminal thrust amplitude).
 */
// ---------------------------------------------------------------------------
double Thruster::getThrottle(double, const Vector3& ) {
    return 1;
}
// ---------------------------------------------------------------------------
/*
 * @return the fuel mass flow rate
 */
// ---------------------------------------------------------------------------
double Thruster::getMassFlowRate(double t, const Vector3& r_sun) {
    return getThrust(t, r_sun) / g0Isp.value();
}

// ---------------------------------------------------------------------------
/** Check derivative. state/costate. No time.
 *
 * @param t  current date, MJD2000
 * @param R0 distance to sun
 * @param V0
 * @param m0
 * @param X
 *
 */
// ---------------------------------------------------------------------------
int
Thruster::CheckDerivatives(real_type t, const Vector3& R0, const Vector3& V0, const real_type m0, const real_type* X) {
    real_type derivative_test_tol = 1e-5;
    real_type h = 1e-8;
    int nerrors = 0;

    SVECTOR R = R0;
    // R /= R.norm();  // to avoid round-off errors

    // All we check is the thruster derivatives
    Vector3 dThrustdRref, dQfueldRref;
    getDerivatives(t, R, dThrustdRref, dQfueldRref);

    SVECTOR dThrustdR, dQfueldR;
    std::vector<FDError> fderror1, fderror2;
    for (int irow = 0; irow < 3; ++irow) {
        Vector3 Rdh = R;  // perturb spacecraft position
        Rdh(irow) += h;
        real_type ThrustRight = getThrust(t, Rdh);
        real_type qfuelRight = getMassFlowRate(t, Rdh);

        Rdh(irow) -= 2 * h;
        real_type ThrustLeft = getThrust(t, Rdh);
        real_type qfuelLeft = getMassFlowRate(t, Rdh);

        // centred finite difference
        dThrustdR(irow) = (ThrustRight - ThrustLeft) / (2 * h);
        dQfueldR(irow) = (qfuelRight - qfuelLeft) / (2 * h);

        // check derivatives for dF/dR
        real_type diffErrorT = FDError::getRelativeError(dThrustdRref(irow), dThrustdR(irow));
        if ((fabs(diffErrorT) > derivative_test_tol) || std::isnan(diffErrorT)) {
            fderror1.push_back(FDError(irow, 0, dThrustdRref(irow), dThrustdR(irow), diffErrorT));
        }

        // check derivatives for dQ/dR
        real_type diffErrorQ = FDError::getRelativeError(dQfueldRref(irow), dQfueldR(irow));
        if ((fabs(diffErrorQ) > derivative_test_tol) || std::isnan(diffErrorQ)) {
            fderror2.push_back(FDError(irow, 0, dQfueldRref(irow), dQfueldR(irow), diffErrorQ));
        }
    }

    if (fderror1.size() == 0) {
        printf("   All derivatives seem OK. (dThrust/d[r,v])   (TDYNAMICS)\n");
    } else {
        printf("dThrust/d[r,v]\n");
        printf("  T = %8.3f. R = %8.3f %8.3f %8.3f\n", t, R0(0), R0(1), R0(2));
        double eclRatio = getAvailablePower(t, R0);
        printf("  EclipseRatio = %8.5f\n", eclRatio);
        FDError::printHeader();
        for (uint i = 0; i < fderror1.size(); ++i) {
            fderror1.at(i).print();
        }
        nerrors += fderror1.size();
    }

    if (fderror2.size() == 0) {
        printf("   All derivatives seem OK. (dQfuel/d[r,v])   (TDYNAMICS)\n");
    } else {
        printf("dQfuel/d[r,v]\n");
        printf("  R = %8.3f %8.3f %8.3f\n", R0(0), R0(1), R0(2));
        FDError::printHeader();
        for (uint i = 0; i < fderror2.size(); ++i) {
            fderror2.at(i).print();
        }
        nerrors += fderror2.size();
    }

    // second derivatives
    Matrix3 d2FrRef, d2QRef;
    getSecondDerivatives(t, R, d2FrRef, d2QRef);
    // compute by finite-differences
    // double Sf[2*STATE_SIZE+1];
    Matrix3 dT2dR2, dQ2dR2;
    std::vector<FDError> fderror3, fderror4;
    for (int jcol = 0; jcol < 3; ++jcol) {
        Vector3 dThrustdRRight, dQfueldRRight;
        Vector3 Rdh = R;
        Rdh(jcol) += h;
        getDerivatives(t, Rdh, dThrustdRRight, dQfueldRRight);

        Vector3 dThrustdRLeft, dQfueldRLeft;
        Rdh(jcol) -= 2 * h;
        getDerivatives(t, Rdh, dThrustdRLeft, dQfueldRLeft);

        for (int irow = 0; irow < 3; ++irow) {
            // centred finite differences
            dT2dR2(irow, jcol) = (dThrustdRRight[irow] - dThrustdRLeft[irow]) / (2 * h);
            dQ2dR2(irow, jcol) = (dQfueldRRight[irow] - dQfueldRLeft[irow]) / (2 * h);

            // check derivatives for d2F/dR2
            real_type ref = d2FrRef(irow, jcol);
            real_type approx = dT2dR2(irow, jcol);
            real_type diffError = FDError::getRelativeError(ref, approx);
            if ((fabs(diffError) > derivative_test_tol) || std::isnan(diffError))
                fderror3.push_back(FDError(irow, jcol, ref, approx, diffError));

            // check derivatives for d2Q/dR2
            ref = d2QRef(irow, jcol);
            approx = dQ2dR2(irow, jcol);
            diffError = FDError::getRelativeError(ref, approx);
            if ((fabs(diffError) > derivative_test_tol) || std::isnan(diffError))
                fderror4.push_back(FDError(irow, jcol, ref, approx, diffError));
        }
    }

    if (fderror3.size() == 0) {
        printf("   All derivatives seem OK. (d2Thrust/d[r2,v2]) (TDYNAMICS)\n");
    } else {
        printf("d2Thrust/d[r2,v2]\n");
        printf("  R = %8.3f %8.3f %8.3f\n", R0(0), R0(1), R0(2));
        FDError::printHeader();
        for (uint i = 0; i < fderror3.size(); ++i) {
            fderror3.at(i).print();
        }
        nerrors += fderror3.size();
    }
    if (fderror4.size() == 0) {
        printf("   All derivatives seem OK. (d2Qfuel/d[r2,v2]) (TDYNAMICS)\n");
    } else {
        printf("d2Qfuel/d[r2,v2]\n");
        printf("  R = %8.3f %8.3f %8.3f\n", R0(0), R0(1), R0(2));
        FDError::printHeader();
        for (uint i = 0; i < fderror4.size(); ++i) {
            fderror4.at(i).print();
        }
        nerrors += fderror4.size();
    }
    return nerrors;
}
