// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018-2020 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ctime>
#include <random>

#include "ProblemSolver.hpp"

#include "core/LtSpacecraftData.hpp"

#include "ode/TDynamicsAveragedEquinoctial.hpp"
#include "constraints/ConstraintOrbit.hpp"
#include "constraints/ConstraintEnergy.hpp"
#include "constraints/ConstraintSmaEccInc.hpp"
#include "constraints/ConstraintSmaEccIncRaan.hpp"
#include "constraints/ConstraintSmaExEyInc.hpp"
#include "continuation/ContinuationSolver.hpp"

// -------------------------------------------------------------------------
ProblemSolver::ProblemSolver(LtProblemDefinition& problemDefinition)
    : problemDefinition(problemDefinition), dynamicalModel(NULL), nlp(NULL), constraint(NULL) {
    
}

// -------------------------------------------------------------------------
ProblemSolver::~ProblemSolver() {
    if (dynamicalModel) delete dynamicalModel;
}
// -------------------------------------------------------------------------
void ProblemSolver::getInitialConditions(double* S0, LtProblemDefinition& problemDefinition, LtProblemContext& problemContext) {
    memcpy(S0, problemDefinition.state0.equiAdapt, 6 * sizeof(double));
    S0[IDX_L] = 0;
    S0[IDX_MASS] = problemDefinition.state0.mass;

    S0[STATE_SIZE] = problemContext.initguess[0];
    S0[STATE_SIZE + 1] = problemContext.initguess[1];
    S0[STATE_SIZE + 2] = problemContext.initguess[2];
    S0[STATE_SIZE + 3] = problemContext.initguess[3];
    S0[STATE_SIZE + 4] = problemContext.initguess[4];
    S0[STATE_SIZE + 5] = 0;
    S0[STATE_SIZE + 6] = problemContext.initguess[5];
    S0[IDX_L1] = problemContext.initguess_duration;  // final longitude
    S0[IDX_T] = 0;  // t
}
// -------------------------------------------------------------------------
/** Compute boundary value of the costate vector
 * @see AveragedProblemNlp::setInitialBoundaryConditions
 */
void ProblemSolver::computeBounds(LtProblemDefinition& definition, double* lower_bounds, double* upper_bounds) {
    // p_sma
    upper_bounds[P_IDX_PSMA] = std::max(definition.state0.getSma(), definition.statef.getSma()) / std::min(definition.state0.getSma(), definition.statef.getSma());
    lower_bounds[P_IDX_PSMA] =-upper_bounds[P_IDX_PSMA];

    // p_ex
    lower_bounds[P_IDX_PEX] = -1;
    upper_bounds[P_IDX_PEX] = 1;

    // p_ey
    lower_bounds[P_IDX_PEY] = -1;
    upper_bounds[P_IDX_PEY] = 1;

    // p_hx
    lower_bounds[P_IDX_PHX] = -1;
    upper_bounds[P_IDX_PHX] = 1;

    // p_hy
    lower_bounds[P_IDX_PHY] = -1;
    upper_bounds[P_IDX_PHY] = 1;

    // p_mass
    lower_bounds[P_IDX_PM] = -1;
    upper_bounds[P_IDX_PM] = 0;

    // t not longitude - it will be converted
    double maxTof = definition.spacecraft.mass / definition.spacecraft.propulsion.getFlowRate();
    lower_bounds[P_IDX_L1] = 1e-6;
    upper_bounds[P_IDX_L1] = maxTof;
}
/* ------------------------------------------------------------------------- */
void ProblemSolver::copyInitialGuess(LtProblemContext &problemContext, double *xInitguess) {
    // unknowns: la, lex, ley, lhx, lhy, lm,
    for (int i = 0; i < NB_UNKNOWN; ++i) {
        xInitguess[i] = problemContext.initguess[i];
    }    
    xInitguess[NB_UNKNOWN - 1] = problemContext.initguess_duration;  // unknown: theta
}

/* ------------------------------------------------------------------------- */
void ProblemSolver::copySolution(LtProblemContext& problemContext, const double* xInitguess) {
    for (int i = 0; i < NB_UNKNOWN; ++i) {
        problemContext.initguess[i] = xInitguess[i];     // unknowns: la, lex, ley, lhx, lhy, lm,
    }
    problemContext.initguess_duration = xInitguess[NB_UNKNOWN - 1];  // unknown: theta
}
// -------------------------------------------------------------------------
bool ProblemSolver::getScalingCoefficients(ProblemScaling& scaling) {
    if (dynamicalModel) {
        dynamicalModel->getScalingCoefficients(scaling);
        return true;
    }
    return false;
}
// -------------------------------------------------------------------------
void ProblemSolver::generateInitialGuess(LtProblemDefinition& problemDefinition, 
    std::mt19937& generator, 
    std::normal_distribution<double> &distribution, double * xInitguess, bool verbose) {
    xInitguess[P_IDX_PSMA] = std::max(1., (problemDefinition.statef.getSma() - problemDefinition.state0.getSma()) / problemDefinition.state0.getSma()) * (1 + distribution(generator)) / 2.; // lsma
    for (int i = 1; i < NB_UNKNOWN - 2; ++i) {
        xInitguess[i] = distribution(generator);
    }
    xInitguess[P_IDX_PM] = -(1 + distribution(generator)) / 2.; // lm < 0
    xInitguess[P_IDX_L1] = (1 + distribution(generator)) / 2.;  // ll > 0

    if (verbose) {
        printf("xInitguess = ");
        for (int i = 0; i < NB_UNKNOWN; ++i) {
            printf("%f ", xInitguess[i]);
        }
        printf("\n");
    }
}
// -------------------------------------------------------------------------
void ProblemSolver::prepareNlp(LtProblemDefinition& problemDefinition, LtProblemContext& problemContext, bool verbose) {
    if (verbose) {
        printf("Initializing Constraints...\n");
    }
    constraint = NULL;
    switch (problemDefinition.constraintType) {
    case C_ORBIT:
        constraint = new ConstraintOrbit();
        break;
    case C_ENERGY:
        constraint = new ConstraintEnergy();
        break;
    case C_SMA_ECC_INC:
        constraint = new ConstraintSmaEccInc();
        break;
    case C_SMA_ECC_INC_AOP:
        constraint = new ConstraintSmaExEyInc();
        break;
    case C_SMA_ECC_INC_RAAN:
        constraint = new ConstraintSmaEccIncRaan();
        break;
    default:
        throw LtException("Unknown constraint type!");
    }

    real_type S0[ODE_STATE_SIZE];
    getInitialConditions(S0, problemDefinition, problemContext);

    real_type lower_bounds[NB_UNKNOWN], upper_bounds[NB_UNKNOWN];
    computeBounds(problemDefinition, lower_bounds, upper_bounds);

    if (verbose) {
        printf("Initializing NLP...\n");
    }
    nlp = new AveragedProblemNlp(dynamicalModel, constraint);
    nlp->setSolverType(problemContext.solverType);
    
    nlp->setInitialState(S0);
    nlp->setFinalState(problemDefinition.statef);
    nlp->setBounds(lower_bounds, upper_bounds);

    if (problemDefinition.spacecraft.dry_mass > 0) {
        nlp->setDryMass(problemDefinition.spacecraft.dry_mass);
    }
}

// -------------------------------------------------------------------------
int ProblemSolver::solve(LtProblemContext& problemContext,
            LtProblemSolution& solution, bool verbose) {

    // first run (in multistart if activated). Can be used to start the continuation.
    if (!problemContext.continuation.enable) {
        return solve(problemDefinition, problemContext, solution, verbose);
    }

    ContinuationSolver* continuation = new ContinuationSolver(problemContext.continuation, problemDefinition);

    // finding continuation starting point
    LtProblemDefinition currentProblemDefinition = continuation->get_initial_problem();
    int res = solve(currentProblemDefinition, problemContext, solution, verbose);
    if ((res != 0) && (res != 4)) {
        // cannot start the continuation from an unconverged point.
        if (verbose) {
            printf("Continuation cannot start: could not find a converged point to start from\n");
        }
        delete continuation;
        return res;
    }

    // update problem and start stepping
    currentProblemDefinition = continuation->step(true, solution);

    problemContext.initGuessMultiStart = false; // disable multi-start, once we do continuation, this shall not be necessary

    int iter = 0;
    int max_iter = 100;
    while ((iter < max_iter) && (continuation->can_continue()) && (!continuation->is_completed())) {
        if (verbose) {
            printf("Continuation %d, progress=%.0f %%, value=%10g\n", iter, continuation->get_progress() * 100., continuation->get_current_value());
        }
        res = solve(currentProblemDefinition, problemContext, solution, verbose);

        currentProblemDefinition = continuation->step((res == 0) || (res == 4), solution);

        ++iter;
    }

    if (!problemContext.continuation.report_name.empty()) {
        continuation->to_report(problemContext.continuation.report_name);
    }

    delete continuation;
    return res;
}
// -------------------------------------------------------------------------
int ProblemSolver::solve(LtProblemDefinition& problemDefinition, 
            LtProblemContext& problemContext, 
            LtProblemSolution & solution, bool verbose) {

    real_type mu_ = problemDefinition.mu;
    double cj2_ = problemDefinition.cj2;
    double re_ = problemDefinition.radius;
    LtThrusterData thrusterDef = problemDefinition.spacecraft.propulsion;
    thrusterDef.PropType = PROP_NEP;
    if (dynamicalModel) {
        delete dynamicalModel;
    }
    dynamicalModel = new TDynamicsAveragedEquinoctial(mu_, cj2_, re_, thrusterDef);

    prepareNlp(problemDefinition, problemContext, verbose);

    double xInitguess[NB_UNKNOWN];
    copyInitialGuess(problemContext, xInitguess);

    if (verbose) {
        printf("Solving...\n");
    }
    int nUnknown = STATE_SIZE;
    double XSCAL[NB_UNKNOWN] = { 1, 1, 1, 1, 1, 1, 1 };
    double rtol = problemContext.rTol;
    NonLinearEquationSolverOptions options = NonLinearEquationSolverOptions();
    options.NITMAX = problemContext.maxIter;
    options.FCMIN = 1e-6;
    options.MPRSOL = 0;
    options.MPRERR = 1;
    options.MPRMON = problemContext.verbose;

    bool useMultiStart = false;
    if (problemContext.initGuessMultiStart && (problemContext.initGuessMultiStartMaxAttempts > 1)) {
        useMultiStart = true;
    }

    std::random_device rd{};
    std::mt19937 generator{ rd() };
    //std::default_random_engine generator;
    std::normal_distribution<double> distribution(0.0, 1.0);

    int iter = 0;
    int res = 1;
    while ((res != 0) && (res != 4)) {
        if (useMultiStart && verbose) {
            printf("MULTISTART #%d\n", iter);
        }

        try {            
            res = nlp->solve(nUnknown, xInitguess, XSCAL, rtol, options);
        }
        catch (std::exception& e) {
            //printf("no solution found due to exception: %s\n", e.what());
            printf("no solution found due to exception\n");
        }

        ++iter;
        if (iter >= problemContext.initGuessMultiStartMaxAttempts) break;

        if (!problemContext.initGuessMultiStart) break;

        if (useMultiStart && (res != 0) && (res != 4)) {
            generateInitialGuess(problemDefinition, generator, distribution, xInitguess, verbose);
        }
    }

    solution.res = res;

    if ((res == 0) || (res == 4)) {
        if (verbose) {
            printf("Producing solution (res=%d)\n", res);
        }
        copySolution(problemContext, xInitguess);

        solution.scaling = ProblemScaling();
        getScalingCoefficients(solution.scaling);
        solution.scaling.time = (solution.scaling.mass / problemDefinition.spacecraft.propulsion.Fth) * solution.scaling.velocity;

        double pts[ODE_STATE_SIZE];
        if (nlp->getLastPoint(xInitguess, pts)) {

            double m0 = problemDefinition.state0.mass;
            double mf = pts[IDX_MASS] * solution.scaling.mass;
            double dm = m0 - mf;
            solution.dm = dm;
            solution.computeDeltaV(problemDefinition);
            solution.computeThrustDuration(problemDefinition);
            solution.setTransferDuration(pts[IDX_T] * solution.scaling.time * SEC2DAY);
        } 
        else {
            if (verbose) {
                printf("Could not get last solution point!\n");
            }
        }

        solution.setSolutionVector(xInitguess);
        solution.setSolutionLongitude(xInitguess[NB_UNKNOWN - 1]);
        solution.nbRevolutions = xInitguess[6] / solution.scaling.longitude / (2 * M_PI);
    }

    // free resource
    delete nlp;
    delete constraint;
    return res;
}
