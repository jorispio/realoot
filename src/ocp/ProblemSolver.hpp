// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018-2020 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __REALOOT_PROBLEM_SOLVER_HPP__
#define __REALOOT_PROBLEM_SOLVER_HPP__

#include <random>

#include "ocp/AveragedProblemNlp.hpp"
#include "core/LtProblemContext.hpp"
#include "core/LtProblemDefinition.hpp"
#include "core/LtProblemSolution.hpp"
#include "ode/TDynamicsAveragedEquinoctial.hpp"
#include "constraints/TerminalConstraintBase.hpp"


class ProblemSolver {
 private:
	 LtProblemDefinition problemDefinition;
	 TDynamicsAveragedEquinoctial* dynamicalModel;
	 AveragedProblemNlp* nlp;
	 TerminalConstraintBase* constraint;

	 void computeBounds(LtProblemDefinition& problemDefinition, double* lower_bounds, double* upper_bounds);
	 void getInitialConditions(double* S0, LtProblemDefinition& problemDefinition, LtProblemContext& problemContext);

	 void generateInitialGuess(LtProblemDefinition& problemDefinition, 
		 std::mt19937& generator, 
		 std::normal_distribution<double>& distribution,
		 double* xInitguess, bool verbose);

	 void copySolution(LtProblemContext& problemContext, const double* xInitguess);
	 void copyInitialGuess(LtProblemContext& problemContext, double* xInitguess);

	 void prepareNlp(LtProblemDefinition& problemDefinition, LtProblemContext& problemContext, bool verbose);

	 int solve(LtProblemDefinition& problemDefinition, LtProblemContext& problemContext, LtProblemSolution& solution, bool verbose);
 public:
	 ProblemSolver(LtProblemDefinition& problemDefinition);
	 ~ProblemSolver();

	 bool getScalingCoefficients(ProblemScaling& scaling);

	 int solve(LtProblemContext& problemContext, LtProblemSolution& solution, bool verbose);
};

// -------------------------------------------------------------------------
#endif  // __REALOOT_PROBLEM_SOLVER_HPP__
