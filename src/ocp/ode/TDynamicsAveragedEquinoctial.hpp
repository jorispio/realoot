// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef REALOOT_AVERAGED_DYNAMICS_HPP
#define REALOOT_AVERAGED_DYNAMICS_HPP

#include "core/DataTypes.hpp"
#include "core/SizeParameters.hpp"
#include "LtEclipseParameters.hpp"
#include "TDynamicsEquinoctial.hpp"

#include "HelioLib/Maths.hpp"


#ifndef QUAD_ORDER
    #define Integral GaussLegendreQuad24
#else
    #if QUAD_ORDER == 12
        #define Integral GaussLegendreQuad12
    #elif QUAD_ORDER == 24
        #define Integral GaussLegendreQuad24
    #elif QUAD_ORDER == 36
        #define Integral GaussLegendreQuad36
    #else
        #error "Gauss quadrature order is not handled. Please set QUAD_ORDER to any of {12, 24, 36}"
    #endif
#endif

#undef G

// ---------------------------------------------------------------------
class TDynamicsAveragedEquinoctial : public TDynamicsEquinoctial, private Integral {
 private:
    /** component for cartesian<->equinoctial transformation */
    //OrbitParameterJacobian* orbitJacobian;

    real_type delta;

    DynamicalContextVariables commonVars;

    /** */
    double getJacobean(double l, const DynamicalContextVariables& cVars);

    /** */
    void averageKeplerianOde(double l, const DynamicalContextVariables& cV, const Vector3 &uTnw, double *F);

    /** */
    void integrand(double theta, double *dx);

 public:
    TDynamicsAveragedEquinoctial(real_type mu_,
                         const double cj2_, const double re_,
                         const LtThrusterData& ThrusterDef,
                         const LtEclipseParameters& eclipseParameters = LtEclipseParameters());

    /** */
    ~TDynamicsAveragedEquinoctial(void) { };

    /** */
    int getControlProfile(double* u, int nmax);

    /* */
    virtual void getDynamics(double t,
                     const DynamicalContextVariables& cv,
                     /* OUTPUTS */ double* StateLagranges_f);

};

// ---------------------------------------------------------------------
#endif  // REALOOT_AVERAGED_DYNAMICS_HPP
