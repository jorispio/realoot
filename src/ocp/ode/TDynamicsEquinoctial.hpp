// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010-2016 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LTIPOPT_DYNAMICS_HPP
#define LTIPOPT_DYNAMICS_HPP


#include <vector>

#include "core/SizeParameters.hpp"
#include "core/DataTypes.hpp"
#include "force/ZonalPerturbation.hpp"
#include "LtEclipseParameters.hpp"
#include "thruster/Thruster.hpp"
#include "Scaling.hpp"
#include "DynamicalDataContext.hpp"
#include "OptimalControlBase.hpp"
#include "ScaledConstant.hpp"

#include "HelioLib/Physics.hpp"

#undef G

class TOptimalControlBase;


// ---------------------------------------------------------------------
class TDynamicsEquinoctial {
 private:
    /** optimal control class */
    TOptimalControlBase* optimalControl;

    /** */
    J2ZonalPerturbation *perturbation;

    /** */
    void getKeplerianOde(double l, const DynamicalContextVariables& cV, const Vector3 &uTnw, double *F);

 protected:
    DYNAMICALMODEL_TYPE model;
    /** Frame */
    Frame* frame;
    /* internal constants */
    /** Central body gravitational constant. */
    ScaledConstant constMu;

    /** dynamical forces */
    std::vector<Force*> forces;

    /** Propulsion method flag. */
    PROPULSION_TYPE propType;

    /** Thruster. */
    Thruster* mThruster;
    bool constantAcceleration;

    /* initial state/costate vector. */
    double S0[2 * STATE_SIZE + 2];

    /** Objective function type. */
    OBJECTIVE_TYPE critere;

    /** initial date. */
    double t0;

    /** final date. */
    double tf;

    /** scaling constant */
    real_type distScale;
    real_type gravScale;
    real_type massScale;
    real_type longiScale;
    real_type velScale;

    real_type delta;

    /** Compute and return the optimal control */
    Vector3 getOptimalControlDirection(const DynamicalContextVariables& common);

    void applyScaling();

 public:
    TDynamicsEquinoctial(real_type mu_,
                         const double cj2_, const double re_,
                         const LtThrusterData& ThrusterDef,
                         const LtEclipseParameters& eclipseParameters);

    /** */
    virtual ~TDynamicsEquinoctial(void);

    /** Return the model type */
    DYNAMICALMODEL_TYPE getModelType() {
      return model;       
    }

    /** Return central body gravitational constant. */
    ScaledConstant getMu() {
       return constMu;
    }

    /** Set initial state conditions */
    void setInitialConditions(const double* S0_);

    /** Initialise the instance */
    void Init(const real_type* S0_, const LtThrusterData& ThrusterDef);

    /** Set hyperbolic initial velocity vector. */
    void setVinf(double* Vinf_);

    /** Set thruster. */
    void setThrusterData(const LtThrusterData& ThrusterDef);

    /** Set thruster. */
    void setThruster(Thruster* ThrusterDef);

    /** Get thruster data. */
    Thruster* getThruster() const {
        return mThruster;
    }

    /** Set objective function type. */
    void setObjectiveFunction(OBJECTIVE_TYPE objtype_) {
        critere = objtype_;
    }

    /** Get objective function type. */
    OBJECTIVE_TYPE getObjectiveFunction() {
        return critere; 
    }

    /** Get the gravitational constant. */
    ScaledConstant getGravitationalConstant() const {
        return constMu;       
    }

    void scaleGravitationalConstant(double scale);

    void setGravitationalConstant(double mu);

    /** Add a force to the dynamical model */
    void addForce(Force *force);

    /** remove dynamical forces */
    void clearForces(void);

    /** */
    std::vector<Force*> getForces() const {
        return forces;       
    }

    /** Set implicit integration. No continuation. */
    void setImplicitIntegration(bool impl);

    /** Get the Hamiltonian function value.
     * @param x state vector
     * @param lambda costate vector
     * @param u control vector
     *
     * @return the hamiltonian value
     * */
    real_type getHamiltonian(const real_type* x, const real_type* lambda, const Vector3& u);

    /** Get the thrust amplitude.
     *
     * @param t  time
     * @param state state vector
     * @param lambda costate vector
     * @param F the thrust amplitude
     * @param P available solar power
     * */
    void getThrustAmplitude(real_type t, const real_type* state, const real_type* lambda, real_type& F, real_type& P);

    /** */
    void setScaling(real_type xSma0Ref, real_type xSmaEndRef, real_type xMass0Ref);

    /** */
    void setScalingCoefficients(const ProblemScaling &scaling);

    /** */
    void getScalingCoefficients(ProblemScaling &S);

    /** Compute all variables common to the methods of the class.
     *
     * @param t  time
     * @param StateLagranges state/costate vector
     * @param common common variables
     * */
    bool computeCommonVariables(double t,
                                const double* StateLagranges,
                                /* OUTPUTS */ DynamicalContextVariables& common);

    /** Get the optimal control class.
     * @return the optimal control class instance
     * */
    TOptimalControlBase* getOptimalControlInstance();

    /** Get dynamical perturbation. */
    J2ZonalPerturbation* getDynamicalPerturbation() { return perturbation; }

    /** */
    int getControlProfile(double* u, int nmax);

    /** Get the dynamics.
     *
     * @param t  time
     * @param cV common variables
     * @param x_dot state time derivative
     * */
    virtual void getDynamics(double t,
                     const DynamicalContextVariables& cv,
                     /* OUTPUTS */ double* StateLagranges_f);
};

// ---------------------------------------------------------------------
#endif  // LTIPOPT_DYNAMICS_HPP
