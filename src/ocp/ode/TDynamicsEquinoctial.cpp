// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010-2016 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TDynamicsEquinoctial.hpp"

#include <algorithm>

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "core/FDError.hpp"
#include "core/LtException.hpp"
#include "thruster/ThrusterFactory.hpp"
#include "MinTimeOptimalControl.hpp"

// ---------------------------------------------------------------------
/**
 * Constructor
 *
 * @param mu_ central body gravitational constant
 * @param cj2_ central body zonal coefficient (0 if not)
 * @param re_  central body radius
 * @param ThrusterDef Thruster definition
 */
TDynamicsEquinoctial::TDynamicsEquinoctial(real_type mu_,
                                            const double cj2_, const double re_,
                                            const LtThrusterData& thrusterDefinition,
                                            const LtEclipseParameters& eclipseParameters)
    : model(D_EQUINOCTIAL),
      frame (NULL),
      constMu(mu_),
      propType(thrusterDefinition.PropType), critere(C_TIME), t0(0.), tf(0.) {
    forces = std::vector<Force*>();          
    mThruster = createThruster(thrusterDefinition, eclipseParameters);
    constantAcceleration = thrusterDefinition.constantAcceleration;
    optimalControl = new MinTimeOptimalControl(this, C_TIME);
    perturbation = new J2ZonalPerturbation(cj2_, re_, mu_);
    frame = (Frame*)FramesFactory::EME2000;
    
    S0[0] = 0;
    distScale = 1;
    massScale = 1;
    longiScale = 1;
    gravScale = 1;
    velScale = 1;
    applyScaling();
}
// ---------------------------------------------------------------------
TDynamicsEquinoctial::~TDynamicsEquinoctial() {
    if (mThruster) {
        delete mThruster;
    }
    if (optimalControl) {
        delete optimalControl;
    }
    if (perturbation) {
        delete perturbation;
    }
}
// ---------------------------------------------------------------------
void TDynamicsEquinoctial::scaleGravitationalConstant(double scale) {
    constMu.scale(scale);
}
/* --------------------------------------------------------------------- */
/** Add a force to the dynamical model
 * @param force Force model
 */
void TDynamicsEquinoctial::addForce(Force *force) {
    forces.push_back(force);
}
/* --------------------------------------------------------------------- */
/** remove dynamical forces
 */
void TDynamicsEquinoctial::clearForces(void) {
    forces.clear();
}

// ---------------------------------------------------------------------
/** Set thruster data.
 * @param thrusterDef  thruster definition
 */
void TDynamicsEquinoctial::setThrusterData(const LtThrusterData& thrusterDef) {
    mThruster->setThruster(thrusterDef);
    propType = mThruster->getType();
}

// ---------------------------------------------------------------------
/** Set thruster data.
 * @param thrusterDef  thruster definition
 */
/* --------------------------------------------------------------------- */
void TDynamicsEquinoctial::setThruster(Thruster* thruster_) {
    mThruster = thruster_;
    propType = mThruster->getType();
}

// ---------------------------------------------------------------------
/** Set the constant mass flag.
 */
void TDynamicsEquinoctial::setGravitationalConstant(double mu) {
    constMu.setValue(mu);
}

// ---------------------------------------------------------------------
/** Initialize.
 *
 * @param S0_  state/costate initial condition vector.
 * @param ThrusterDef  thruster definition
 * @param mu central body gravitational constant
 */
void TDynamicsEquinoctial::Init(const real_type* S0_, const LtThrusterData& ThrusterDef) {
    setInitialConditions(S0_);
    mThruster->setThruster(ThrusterDef);
}
// ---------------------------------------------------------------------
/**
 * Set initial conditions.
 *
 * @param S0_  state/costate initial condition vector.
 */
void TDynamicsEquinoctial::setInitialConditions(const real_type* S0_) {
    critere = C_TIME;
    memcpy(S0, S0_, ODE_STATE_SIZE * sizeof(double));
    S0[IDX_L] = 0;
    S0[IDX_T] = 0;

    if (S0[IDX_MASS] < 1e-14) {
        throw LtException("TDynamicsEquinoctial: mass is zero in setInitialConditions()!");
    }
}
// ----------------------------------------------------------------
void TDynamicsEquinoctial::getScalingCoefficients(ProblemScaling &scaling) {
    scaling.distance = distScale;
    scaling.gravParameter = gravScale;
    scaling.mass = massScale;
    scaling.longitude = longiScale;
    scaling.velocity = velScale;
}
// ----------------------------------------------------------------
void TDynamicsEquinoctial::setScalingCoefficients(const ProblemScaling &scaling) {
    distScale = scaling.distance;
    gravScale = scaling.gravParameter;
    massScale = scaling.mass;
    longiScale = scaling.longitude;
    velScale = scaling.velocity;

    applyScaling();
    if (perturbation) {
        perturbation->setScaling(distScale, gravScale);
    }
}
// ---------------------------------------------------------------------
void TDynamicsEquinoctial::setScaling(real_type xSma0Ref, real_type xSmaEndRef, real_type xMass0Ref) {
    double Fmax = getThruster()->getNominalThrust().getUnscaledValue();  // m/s^2

    //     Scaling
    // -> mu = 1
    // -> a(tf) = 1
    distScale = std::max(xSma0Ref, xSmaEndRef);  // m
    massScale = xMass0Ref;
    gravScale = getGravitationalConstant().getUnscaledValue();  // scale for gravitational constant
    velScale  = sqrt(gravScale / distScale);  // normalisation for m/s
    longiScale = ((distScale * distScale) / gravScale) * (Fmax / massScale);  // scale for gravitational constant, rad

    if (longiScale == 0) {
        throw LtException("TDynamicsEquinoctial: longitude scale is zero!");
    }

    // scaled final longitude
    //Theta1  = gravScale * L1;
    applyScaling();
}
// ----------------------------------------------------------------
void TDynamicsEquinoctial::applyScaling() {
    delta = velScale / getThruster()->getExhaustVelocity().getUnscaledValue();

    scaleGravitationalConstant(1. / gravScale);

    getThruster()->scaleNominalThrust(1. / (massScale * velScale * velScale / distScale));
    getThruster()->scaleExhaustVelocity(1. / velScale);

    if (perturbation) {
        perturbation->setScaling(distScale, gravScale);
    }
}

// ---------------------------------------------------------------------
/** Compute common variables for the State+CoState Dynamics
 * This function can be used externally.
 * The state vector describes the equinoctial coordinates:
 *  p, ex, ey, hx, hy, L
 * where L is the true longitude.
 *
 * @param t  time
 * @param StateLagranges  state/costate vector
 * @param common common variables
 *
 * @return true
 */
bool TDynamicsEquinoctial::computeCommonVariables(real_type theta,
                                                  const real_type* StateLagranges,
                                                  /* OUTPUTS */ DynamicalContextVariables& common) {
    common.theta = theta;
    common.time = StateLagranges[IDX_T];

    for (int i = 0; i < STATE_SIZE; ++i) {
        common.state(i) = StateLagranges[i];
        common.lambda(i) = StateLagranges[STATE_SIZE + i];
    }

    common.sma = common.state(0);
    common.ex = common.state(1);
    common.ey = common.state(2);
    common.hx = common.state(3);
    common.hy = common.state(4);
    common.Lm = common.state(5);
    common.mass = common.state(6);

    if (common.mass < 1e-14) {
        throw LtException("TDynamicsEquinoctial: mass is zero!");
        //printf("TDynamicsEquinoctial: mass is zero (mass = %f)!\n", common.mass);
    }

    // costates
    common.lambda_sma = common.lambda(0);
    common.lambda_ex = common.lambda(1);
    common.lambda_ey = common.lambda(2);
    common.lambda_hx = common.lambda(3);
    common.lambda_hy = common.lambda(4);
    common.lambda_L = common.lambda(5);
    common.lambda_m = common.lambda(6);

    // parameter: scaled true longitude
    common.finalLongitude = StateLagranges[IDX_L1];

    // update thrust level
    common.Thrust = mThruster->getThrust(theta, common.R);

    // update fuel consumption rate (depend on eclipsing, thruster type, etc...)
    common.qfuel = mThruster->getMassFlowRate(theta, common.R);
    common.g0Isp = mThruster->getExhaustVelocity().value();

#ifdef DEBUG_DYNAMICS_EQUINOCTIAL_AVERAGED
    std::cout << "state = " << common.state.transpose() << std::endl;
    std::cout << "lambda = " << common.lambda.transpose() << std::endl;
#endif

    return true;
}
/* --------------------------------------------------------------------- */
/** Determination of the optimal control, with adjoint vector.
 *
 * @param common common variables
 */
/* --------------------------------------------------------------------- */
Vector3 TDynamicsEquinoctial::getOptimalControlDirection(const DynamicalContextVariables& common) {
    return optimalControl->getOptimalControlDirection(common.state.data(), common.lambda.data());
}

/* --------------------------------------------------------------------- */
TOptimalControlBase* TDynamicsEquinoctial::getOptimalControlInstance() {
    return optimalControl;
}

/* --------------------------------------------------------------------- */
/** State+CoState Dynamics.
 *
 * @param t time
 * @param cv common variables
 * @param dx dynamics state/costate vector
 */
/* --------------------------------------------------------------------- */
void TDynamicsEquinoctial::getDynamics(real_type longitude,
                                        const DynamicalContextVariables& cVars,
                                        real_type* dx) {
    DynamicalContextVariables commonVars = DynamicalContextVariables(cVars);

    real_type Fp[ODE_STATE_SIZE];

    //real_type theta1 = commonVars.finalLongitude;  // scaled true longitude parameter

    commonVars.state(IDX_L) = longitude;
    Vector3 uOptTnw = getOptimalControlDirection(commonVars);
    getKeplerianOde(longitude, commonVars, uOptTnw, dx);
    if (perturbation) {
        if (perturbation->getInstantaneousSensitivity(longitude, commonVars.state.data(), commonVars.lambda.data(), Fp)) {
            for (int j = 0; j < ODE_STATE_SIZE; ++j) {
                dx[j] += Fp[j];
            }
        }
    }
}

/* --------------------------------------------------------------------- */
/**
 * Compute ODE: f(longitude) = dx(longitude,t)/dt
 */
/* --------------------------------------------------------------------- */
void TDynamicsEquinoctial::getKeplerianOde(double l, const DynamicalContextVariables& cVars, const Vector3 &uTnw, double *f) {
    real_type sma = cVars.sma;
    real_type ex = cVars.ex;  // ex = e cos(pom)
    real_type ey = cVars.ey;  // ey = e sin(pom)
    real_type hx = cVars.hx;
    real_type hy = cVars.hy;
    real_type mass = cVars.mass;  // mass

    real_type lsma = cVars.lambda_sma;
    real_type lex = cVars.lambda_ex;
    real_type ley = cVars.lambda_ey;
    real_type lhx = cVars.lambda_hx;
    real_type lhy = cVars.lambda_hy;
    real_type pm = cVars.lambda_m;  // lambda_mass

    real_type theta1 = cVars.finalLongitude;  // scaled true longitude parameter

    real_type cl = cos(l);
    real_type sl = sin(l);
    real_type clSq = cl * cl;
    real_type slSq = sl * sl;

    real_type smaPow2 = sma * sma;
    real_type sqrtSma = sqrt(sma);
    real_type smaPow3_2 = sqrtSma * sma;
    real_type invSmaPow3 = 1.0 / (smaPow2 * sma);  // t3
    real_type invSmaPow3_2 = 1 / smaPow3_2;

    real_type ex_cos = ex * cl;
    real_type ey_sin = ey * sl;
    real_type ey_cos = ey * cl;
    real_type ex_sin = ex * sl;
    real_type D = 1.0 + ex_cos + ey_sin;
    real_type DSq = D * D;
    real_type invDSq = 1.0/ DSq;
    real_type exPow2 = ex * ex;
    real_type eyPow2 = ey * ey;
    real_type eccPow2 = exPow2 + eyPow2;
    real_type ASq = 1.0 - eccPow2;
    real_type A = sqrt(ASq);
    real_type invA = 1.0 / A;
    real_type BSq = 1.0 + 2.0 * ex_cos + 2.0 * ey_sin + eccPow2;
    real_type B = sqrt(BSq);
    real_type B_A = B / A;
    real_type invB = 1.0 / B;
    real_type exey = ex * ey;
    real_type C = 2.0 * exey * cl - sl * (exPow2 - eyPow2) + 2.0 * ey + sl;
    real_type E = 2.0 * exey * sl + cl * (exPow2 - eyPow2) + 2.0 * ex + cl;

    // dsma/dt = 2 * sma^(3/2) * B / (A * sqrt(mu)) * uT
    real_type ex_cosl = ex + cl;
    real_type ey_sinl = ey + sl;
    real_type invD = 1.0 / D;
    real_type C_B = C * invB;
    real_type E_B = E * invB;
    real_type hx_sin = hx * sl;
    real_type hy_cos = hy * cl;
    real_type H = 1.0 + hx * hx + hy * hy;

    real_type invMass = 1.0 / mass;
    real_type uT = uTnw(0);
    real_type uN = uTnw(1);
    real_type uW = uTnw(2);
    real_type rho = ((MinTimeOptimalControl*)optimalControl)->rho;
    real_type ham = -rho*invMass + delta*pm + 1.0;

    // jacobian dt/dL
    real_type Apow3 = A * ASq;
    real_type jacLonT = Apow3 * invDSq * smaPow3_2;

    f[IDX_SMA] = (2.0 * smaPow3_2 * B_A * uT * invMass);  // dsma/dt
    f[IDX_EX] = sqrtSma * A * (2.0 * ex_cosl * invB * uT - invD * (C_B * uN + ey * (hx_sin - hy_cos) * uW)) * invMass;  // dex/dt
    f[IDX_EY] = sqrtSma * A * (2.0 * ey_sinl * invB * uT + invD * (E_B * uN + ex * (hx_sin - hy_cos) * uW)) * invMass;  // dey/dt
    f[IDX_HX] = sqrtSma * A * invD * 0.5 * H * cl * uW * invMass;   // dhx/dt
    f[IDX_HY] = sqrtSma * A * invD * 0.5 * H * sl * uW * invMass;   // dhy/dt
    f[IDX_L] = 1. / jacLonT;  //theta
    if (constantAcceleration) {
        f[IDX_MASS] = 0;
    }
    else {
        f[IDX_MASS] =-/*smaPow3_2 **/ delta;  // dmass/dt
    }
    
    // dlsma/dt =-dH/dsma
    real_type smaPow4 = smaPow2 * smaPow2;
    f[7] =-(
            (3.0 * sqrtSma * B_A * lsma + A/sqrtSma*(ex_cosl * invB * lex + ey_sinl * invB * ley)) * uT
            + 0.5 * sqrtSma * A * invD * (-C_B * lex + E_B * ley) * uN
            + 0.5 * sqrtSma * A * invD * ((hx_sin - hy_cos) * (-ey * lex + ex * ley) + H * (cl * lhx + sl * lhy)/2.0) * uW
            ) * invMass
          + 3.0/2.0*theta1*invDSq*Apow3/invSmaPow3_2/invSmaPow3*ham/smaPow4 / jacLonT;  // p_sma

    // dlex/dt
    real_type H_c = H * cl;
    real_type H_s = H * sl;
    real_type F = hx_sin - hy_cos;
    real_type A_D = A * invD;
    real_type G = ex_sin - ey_cos;
    real_type invBpow3 = 1.0 / (B * BSq);
    real_type invAD = invA * invD;
    real_type A_DSq = A * invDSq;
    real_type C_Bpow3 = C * invBpow3;
    real_type D_B = D * invB;
    real_type E_Bpow3 = E * invBpow3;
    real_type F_D = F * invD;
    f[8] =-2.0 * invD * ham * cl
            - 3.0 * ham * ex / (A*A)
            - invMass * (
                  uT * sqrtSma * (2.0*(sma/Apow3*B*ex + sma * invA*invB*ex_cosl)*lsma
                                + 2.0*(A*invB - invA*ex_cosl*invB*ex - A*ex_cosl*invBpow3*ex_cosl)*lex
                                + 2.0*(-A*ey_sinl*invBpow3*ex_cosl - invA*ey_sinl*invB*ex)*ley)
                + uN * sqrtSma * ((invAD*C_B*ex + A_D*(2.0*G*invB  + C_Bpow3*ex_cosl) + A_DSq*C_B*cl)*lex
                                + (-A_DSq*E_B*cl  - invAD*E_B*ex + 2.0*A_D*D_B - A_D*E_Bpow3*ex_cosl)*ley)
                + uW * sqrtSma * ((invAD * ey * F * ex + A_DSq * ey * F * cl)*lex
                                + (-A_DSq*ex * F*cl - invAD*exPow2*F + A*F_D)*ley
                                + (-invAD*H_c*ex - A_DSq*H*clSq)*lhx/2.0
                                + (-invAD*ex - A_DSq*cl)*H_s*lhy/2.0)
            );  // p_ex

    // dley/dt
    f[9] =-2.0 * invD * ham * sl
            - 3.0 * ham * ey / (A*A)
            - invMass * (
                  uT * sqrtSma * (2.0*(sma/Apow3*B*ey + sma * invA*invB*ey_sinl)*lsma
                                + 2.0*(-invA*ex_cosl*invB*ey - A*ex_cosl*invBpow3*ey_sinl)*lex
                                + 2.0*(A*invB - invA*ey_sinl*invB*ey - A*ey_sinl*invBpow3*ey_sinl)*ley)
                + uN * sqrtSma * ((A_DSq*C_B*sl + (invAD*C_B*ey - 2*A_D*D_B + A_D*C_Bpow3*ey_sinl))*lex
                                - (invAD*E_B*ey - A_D*(-2.0*G*invB + E_Bpow3*ey_sinl)
                                - A_DSq*E_B*sl)*ley)
                + uW * sqrtSma * ((A_DSq*ey * F*sl + invAD*eyPow2*F - A*F_D)*lex
                                + (-invAD*ex * F*ey - A_DSq*ex * F*sl)*ley
                                + (-invAD*ey - A_DSq*sl)*H_c*lhx/2.0
                                + (-invAD*H_s*ey - A_DSq*H*slSq)*lhy/2.0)
            );  // p_ey

    // dlhx/dt, dlhy/dt
    f[10] = -A_D * sqrtSma * (-ey_sin*lex + ex_sin*ley + hx*cl*lhx + hx_sin*lhy) * uW * invMass;  // p_hx
    f[11] = -A_D * sqrtSma * ( ey_cos*lex - ex_cos*ley + hy_cos*lhx + hy*sl*lhy) * uW * invMass;  //p_hy

    // ptheta/dt
    f[12] = ham;

    // pmass/dt
    if (constantAcceleration) {
        f[13] = 0;
    }
    else {
        f[13] = rho / (mass * mass);  // p_mass
    }

    f[IDX_L1] = 0.0;    // theta1 (unknown parameter)
    f[IDX_T] = 1;    // time
}

/* --------------------------------------------------------------------- */
/**
 * @desc Returns the thrust amplitude
 *
 * @param t time
 * @param state state vector
 * @param lambda costate vector
 *
 * @return the thrust amplitude, between 0 and 1.
 */
/* --------------------------------------------------------------------- */
void TDynamicsEquinoctial::getThrustAmplitude(real_type t,
                                       const real_type* state,
                                       const real_type* lambda,
                                       real_type& F,
                                       real_type& P) {
    real_type StateLagrangesParam[ODE_STATE_SIZE];
    memcpy(StateLagrangesParam, state, STATE_SIZE * sizeof(real_type));
    memcpy(StateLagrangesParam + STATE_SIZE, lambda, STATE_SIZE * sizeof(real_type));
    memcpy(StateLagrangesParam + 2 * STATE_SIZE, lambda, PARAMETER_SIZE * sizeof(real_type));

    DynamicalContextVariables common;
    computeCommonVariables(t, StateLagrangesParam, common);

    /* update thrust level */
    real_type A = 1;

    F = mThruster->getThrottle(t, common.R) * A;

    // available power
    P = mThruster->getAvailablePower(t, common.R);
}

/* --------------------------------------------------------------------- */
/** Get the switching function value.
 *
 * @param x state vector
 * @param lambda costate vector
 * @param uqsw control vector
 */
/* --------------------------------------------------------------------- */
real_type TDynamicsEquinoctial::getHamiltonian(const real_type* x, const real_type* lambda, const Vector3& uqsw) {
    real_type StateLagrangesParam[ODE_STATE_SIZE];
    memcpy(StateLagrangesParam, x, STATE_SIZE * sizeof(real_type));
    memcpy(StateLagrangesParam + STATE_SIZE, lambda, STATE_SIZE * sizeof(real_type));
    memcpy(StateLagrangesParam + 2 * STATE_SIZE, lambda, PARAMETER_SIZE * sizeof(real_type));

    DynamicalContextVariables cv;
    computeCommonVariables(0., StateLagrangesParam, cv);
    cv.U = uqsw;
    cv.uThrustAcceleration = cv.U * cv.Thrust / cv.mass;  // we do not put the smoothed thrust amplitude

    real_type dx[ODE_STATE_SIZE];
    getDynamics(0., cv, dx);
    Vector7 f;
    f << dx[0], dx[1], dx[2], dx[3], dx[4], dx[5], dx[6];
    Vector7 vlambda;
    vlambda << lambda[0], lambda[1], lambda[2], lambda[3], lambda[4], lambda[5], lambda[6];

    return f.dot(vlambda);
}
