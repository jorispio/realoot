// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <algorithm>

#include "TDynamicsAveragedEquinoctial.hpp"
#include "core/LtException.hpp"

//#define DEBUG_DYNAMICS_EQUINOCTIAL_AVERAGED

/* --------------------------------------------------------------------- */
/**
 * Constructor
 *
 * @param mu_ central body gravitational constant
 * @param ThrusterDef Thruster definition
 */
/* --------------------------------------------------------------------- */
TDynamicsAveragedEquinoctial::TDynamicsAveragedEquinoctial(real_type mu_,
                                            const double cj2_, const double re_,
                                            const LtThrusterData& thrusterDef,
                                            const LtEclipseParameters& eclipseParameters)
    : TDynamicsEquinoctial(mu_, cj2_, re_, thrusterDef, eclipseParameters), Integral(ODE_STATE_SIZE) {
    model = D_EQUINOCTIAL_AVERAGING;
}
// ---------------------------------------------------------------------
double TDynamicsAveragedEquinoctial::getJacobean(double l, const DynamicalContextVariables& cVars) {
    real_type sma = cVars.sma;
    real_type ex = cVars.ex;  // ex = e cos(pom)
    real_type ey = cVars.ey;  // ey = e sin(pom)
    real_type theta1 = cVars.finalLongitude;  // scaled true longitude parameter
    real_type smaPow3_2 = sma * sqrt(sma);
    real_type ASq = 1.0 - (ex * ex + ey * ey);  // 1 - ecc^2
    real_type A = sqrt(ASq);
    real_type D = 1.0 + ex * cos(l) + ey * sin(l);

    // jacobian dt/ds = dt/dL * ds/dL where L = s * (Lf - L0) = s * theta1
    return theta1 * (A * ASq) / (D * D) * smaPow3_2;
}

/* --------------------------------------------------------------------- */
/** State+CoState Dynamics.
 *
 * @param t time
 * @param cv common variables
 * @param dx dynamics state/costate vector
 */
/* --------------------------------------------------------------------- */
void TDynamicsAveragedEquinoctial::getDynamics(real_type longitude,
                                        const DynamicalContextVariables& cVars,
                                        real_type* dx) {
    commonVars = (DynamicalContextVariables)cVars;

    Integral::Solve(dx); // quadrature calling integrand()

    if (getDynamicalPerturbation() != NULL) {
        real_type F[ODE_STATE_SIZE];
        if (getDynamicalPerturbation()->getAverageSensitivity(longitude, cVars.state.data(), cVars.lambda.data(), F)) {
            real_type theta1 = cVars.finalLongitude;  // scaled true longitude parameter
            real_type meanMotion = sqrt(getGravitationalConstant() / (cVars.sma * cVars.sma * cVars.sma));
            real_type jac = theta1 * 1. / longiScale / meanMotion;
            for (int j = 0; j < ODE_STATE_SIZE; ++j) {
                dx[j] = dx[j] + jac * F[j];
            }
        }
    }
}
/* --------------------------------------------------------------------- */
// called by Integral::Solve()
void TDynamicsAveragedEquinoctial::integrand(double theta, double *dx) {
    commonVars.state(IDX_L) = theta;
    real_type F[ODE_STATE_SIZE];
    TDynamicsEquinoctial::getDynamics(theta, commonVars, F);

    //real_type jacDtDs = getJacobean(theta, commonVars);
    real_type theta1 = commonVars.finalLongitude;  // scaled true longitude parameter
    real_type jacDtDs = theta1 / F[IDX_L];
    for (int j = 0; j < ODE_STATE_SIZE; ++j) {
        dx[j] = jacDtDs * F[j];
    }
}
