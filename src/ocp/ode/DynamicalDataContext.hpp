// $Id$
// ---------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2010-2021 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __REALOOT_INTEGRATION_DYNAMICAL_DATA_CONTEXT_HPP__
#define __REALOOT_INTEGRATION_DYNAMICAL_DATA_CONTEXT_HPP__

#include "core/SizeParameters.hpp"
#include "core/DataTypes.hpp"
#include "HelioLib/Orbits.hpp"

/* ------------------------------------------------------------------------------- */
struct DynamicalContextVariables {
    /** reference time variable. */
    double ref_time;    
    /** time variable. */
    double time;
    /** Angular integration variable. */
    double theta; // 
    /** State vector. */
    S7VECTOR state;
    /** Costate vector. */
    S7VECTOR lambda;

    /** cartesian position vector. */
    SVECTOR R;
    /** cartesian velocity vector. */
    SVECTOR V;

    // equinoctial state
    double sma;
    double ex;
    double ey;
    double hx;
    double hy;
    double Lm;  // true longitude

    // costate for equinoctial state
    double lambda_sma;
    double lambda_ex;
    double lambda_ey;
    double lambda_hx;
    double lambda_hy;
    double lambda_L;
    double lambda_m;

    // mass
    double mass;

    // param
    double finalLongitude;

    /** time derivative of mass costate. */
    double lambda_m_dt;

    /** Control direction. */
    SVECTOR U;
    double uAlpha;  // [Equinoctial] control spherical angle 1
    double uBeta;  // [Equinoctial] control spherical angle 2
    double uDelta;  // [Equinoctial] control amplitude
    SVECTOR uThrustAcceleration;

    /** dynamical perturbations */
    //Vector6 fPerturbationsEquinoctial;
    //Matrix6 jacPerturbationsEquinoctial;

    /** Thruster variables and derivatives */
    /** throttle */
    real_type throttle;
    /** thrust. */
    real_type Thrust;
    /** flow rate. */
    real_type qfuel;
    /** exhaust velocity. */
    real_type g0Isp;
    //
    SVECTOR dThrustdR;
    SVECTOR dQfueldR;

    /** Default constructor. */
    DynamicalContextVariables() {
        ref_time = 0;
        time = 0;
        sma = ex = ey = hx = hy = Lm = 0;
        lambda_sma = lambda_ex = lambda_ey = lambda_hx = lambda_hy = lambda_L = 0;
        mass = 0;
        finalLongitude = 1;
        lambda_m = 0;
        lambda_m_dt = 0;
        uAlpha = uBeta = uDelta = 0;
    }

    /** */
    DynamicalContextVariables(const real_type* x, const real_type* l, real_type param) {
        ref_time = 0;
        time = 0;
        theta = 0;
        state << x[0], x[1], x[2], x[3], x[4], x[5], x[6];
        lambda << l[0], l[1], l[2], l[3], l[4], l[5], l[6];

        sma = x[0];
        ex = x[1];
        ey = x[2];
        hx = x[3];
        hy = x[4];
        Lm = x[5];
        mass = x[6];
        lambda_sma = l[0];
        lambda_ex = l[1];
        lambda_ey = l[2];
        lambda_hx = l[3];
        lambda_hy = l[4];
        lambda_L = l[5];

        lambda_m = l[6];
        lambda_m_dt = 0;

        finalLongitude = param;

        U.setZero();
        uAlpha = uBeta = uDelta = 0;

        qfuel = 0;
        Thrust = 1;
        dThrustdR.setZero();
        dQfueldR.setZero();

        uThrustAcceleration.setZero();
    }

    void computeCartesianPosition(double constMu) {
        EquinoctialOrbit orbit(
            sma, ex, ey, hx, hy, Lm, MEAN_LONGITUDE,
            (Frame*)(FramesFactory::EME2000),
            GenericDate(), constMu);
        double dt = time - ref_time;
        CartesianCoordinates pv = orbit.propagate(dt);
        R = pv.getPosition();
        V = pv.getVelocity();
    }
};

#endif  // __REALOOT_INTEGRATION_DYNAMICAL_DATA_CONTEXT_HPP__
