// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef AVERAGEDPROBLEMNLP_HPP
#define AVERAGEDPROBLEMNLP_HPP

#include <vector>

#include "core/SizeParameters.hpp"
#include "core/State.hpp"
#include "ode/TDynamicsAveragedEquinoctial.hpp"
#include "constraints/TerminalConstraintBase.hpp"
#include "StateNumericalPropagator.hpp"
#include "maths/solver/NonLinearEquationSolver.hpp"
#include "maths/solver/SolverTypeEnum.hpp"

#ifdef USE_KINSOL
#include "maths/solver/KinSolWrapper.hpp"
#endif

//---------------------------------------------------------------------------
class AveragedProblemNlp : public NonLinearEquationSolver 
#ifdef USE_KINSOL
    , public KinsolWrapper
#endif
{
 private:
    /** solver type: NLEQ, KinSol */
     SolverType solver;

    /** dynamical model */
    TDynamicsAveragedEquinoctial *mDynamics;

    /** constraint: state constraint + transversality conditions */
    TerminalConstraintBase *terminalConstraint;

    /** propagator */
    StateNumericalPropagator* propagator;

    /** initial boundary condition */
    void setInitialBoundaryConditions(double *yStart, const double *P);
    /** Final constraints */
    void setFinalBoundaryConditions(const double *yEnd, double *FC);

    void applyBounds(ProblemScaling &scaling, double *pUnknowns);

    real_type xSma0Ref, xSmaEndRef, xMass0Ref;
    real_type xSma0, xEx0, xEy0, xHx0, xHy0, xMass0;
    real_type upper_bounds[NB_UNKNOWN];
    real_type lower_bounds[NB_UNKNOWN];

 public:
    AveragedProblemNlp(TDynamicsAveragedEquinoctial *dynamics, TerminalConstraintBase *constraint);
    virtual ~AveragedProblemNlp();

    /** Set initial state */
    void setInitialState(const double* x);

    /** Set final desired state */
    void setFinalState(State &state);

    /** */
    void setBounds(const double *lower, const double *upper);

    /** */
    void setDryMass(double drymass);

    /** */
    long int Function(int n, const double *X, double *FU);

    /** Jacobian of FCN */
    long int Jacobian(int n, int m, const double *X, double *A);

    TDynamicsAveragedEquinoctial* getDynamics() { return mDynamics;}

    /** */
    void setSolverType(SolverType solver);

    /** Solve */
    int solve(int n, double *X, double *fScale, double rtol, NonLinearEquationSolverOptions options = NonLinearEquationSolverOptions());

    /** Last point */
    bool getLastPoint(const double* Xs, double* Xf);

    /** retrieve propagated trajectory */
    int getTrajectory(const double *Xs, std::vector<real_type> &tout, std::vector<S14VECTOR> &xout);
};

#endif  // AVERAGEDPROBLEMNLP_HPP
