// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef REALOOT_OPTIMAL_CONTROL_HPP
#define REALOOT_OPTIMAL_CONTROL_HPP

#include "ode/TDynamicsEquinoctial.hpp"
#include "OptimalControlBase.hpp"

class MinTimeOptimalControl : public TOptimalControlBase {
 private:

 public:
    /** Creator. */
    MinTimeOptimalControl(TDynamicsEquinoctial* dynamic, OBJECTIVE_TYPE critere);

    /** Compute and return the optimal control */
    Vector3 getOptimalControlDirection(const real_type* state_lambda);
    Vector3 getOptimalControlDirection(const real_type* state, const real_type* lambda);

    double rho;
};

// ---------------------------------------------------------------------
#endif  // REALOOT_OPTIMAL_CONTROL_HPP
