// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <cstring>

#include "AveragedProblemNlp.hpp"
#include "core/SizeParameters.hpp"  // IDX** P_IDX**
#include "dynamics/events/DryMassEvent.hpp"
#include "propagation/numerical/SimulationEventManager.hpp"

// ---------------------------------------------------------------------------
AveragedProblemNlp::AveragedProblemNlp(TDynamicsAveragedEquinoctial *dyn, TerminalConstraintBase *constraint) :
            NonLinearEquationSolver(),
#ifdef USE_KINSOL
            KinsolWrapper(),
#endif
            solver(SOLVER_NLEQ),
            mDynamics(dyn),
            terminalConstraint(constraint) {    
    if (!mDynamics) {
        throw LtException("Dynamics not set!");
    }
    if (!constraint) {
        throw LtException("Constraints not set!");
    }

    propagator = new StateNumericalPropagator(mDynamics, NULL, ODE_SOLVER_ID::ODE_DOPRI5);
    propagator->setTolerances(1e-9, 1e-9);
}

// ---------------------------------------------------------------------------
AveragedProblemNlp::~AveragedProblemNlp() {
    if (propagator) {
        delete propagator;
    }
}

// ---------------------------------------------------------------------------
void AveragedProblemNlp::setInitialState(const double* x) {
    xSma0Ref = x[0];
    xEx0 = x[1];
    xEy0 = x[2];
    xHx0 = x[3];
    xHy0 = x[4];
    xMass0Ref = x[IDX_MASS];
    mDynamics->setInitialConditions(x);
}

// ---------------------------------------------------------------------------
void AveragedProblemNlp::setFinalState(State &state) {
    xSmaEndRef = state.getSma();
    if (terminalConstraint) {
        terminalConstraint->setReferenceState(state);
    }
}
// ---------------------------------------------------------------------------
void AveragedProblemNlp::setBounds(const double *lower, const double *upper) {
    memcpy(lower_bounds, lower, NB_UNKNOWN * sizeof(double));
    memcpy(upper_bounds, upper, NB_UNKNOWN * sizeof(double));
}
// ---------------------------------------------------------------------------
void AveragedProblemNlp::setDryMass(double drymass) {
    SimulationEventManager<DOPRI5, OdePolyData5>* eventManager = new SimulationEventManager<DOPRI5, OdePolyData5>();
    DryMassEventDetector *detector = new DryMassEventDetector(drymass);
    eventManager->addEventHandler(detector);
    propagator->setEventHandler(eventManager);
}
// ---------------------------------------------------------------------------
void AveragedProblemNlp::applyBounds(ProblemScaling &scaling, double *pUnknowns) {
    for (int idx = 0; idx < NB_UNKNOWN; ++idx) {
        if (idx == P_IDX_L1) {
            double ts = scaling.getTimeScale();
            pUnknowns[P_IDX_L1] = std::max(std::min(pUnknowns[P_IDX_L1], upper_bounds[P_IDX_L1] / ts), lower_bounds[P_IDX_L1] / ts);
        }
        else {
            pUnknowns[idx] = std::max(std::min(pUnknowns[idx], upper_bounds[idx]), lower_bounds[idx]);
        }
    }
}
// ----------------------------------------------------------------
/** Initial conditions
 */
// ----------------------------------------------------------------
void AveragedProblemNlp::setInitialBoundaryConditions(double *yInitCond, const double *unknownParam) {
    yInitCond[IDX_SMA] = xSma0;
    yInitCond[IDX_EX] = xEx0;
    yInitCond[IDX_EY] = xEy0;
    yInitCond[IDX_HX] = xHx0;
    yInitCond[IDX_HY] = xHy0;
    yInitCond[IDX_L] = 0;  // initial longitude
    yInitCond[IDX_MASS] = xMass0;  // mass

    yInitCond[7] = unknownParam[P_IDX_PSMA];  // costate sma
    yInitCond[8] = unknownParam[P_IDX_PEX];  // costate ex
    yInitCond[9] = unknownParam[P_IDX_PEY];  // costate ey
    yInitCond[10] = unknownParam[P_IDX_PHX];  // costate hx
    yInitCond[11] = unknownParam[P_IDX_PHY];  // costate hy
    yInitCond[12] = 0.0;               // costate unknown final lontitude theta
    yInitCond[13] = unknownParam[P_IDX_PM];  // costate mass

    yInitCond[IDX_L1] = unknownParam[P_IDX_L1];  // unknown final longitude theta
    yInitCond[IDX_T] = 0;  // time
}
// ----------------------------------------------------------------
/** Final constraints
 */
// ----------------------------------------------------------------
void AveragedProblemNlp::setFinalBoundaryConditions(const double *yEnd, double *fFinalConstr) {
    if (terminalConstraint) {
        terminalConstraint->eval_g(1., yEnd, fFinalConstr);
    }
}

// ----------------------------------------------------------------
/** Function to solve.
 * It evaluates of final state constraint and transversality conditions.
 * Unknown variables are the initial costate vector and the transfer
 * duration.
 */
// ----------------------------------------------------------------
long int AveragedProblemNlp::Function(int n, const double *p, double *FU) {
    // initial conditions
    real_type yStart[ODE_STATE_SIZE];
    setInitialBoundaryConditions(yStart, p);

    // propagate in [0, 1] of normalised integration variable.
    //  s=1 -> l(0) = l0  and  s=1 -> l(1) = lf
    int res = propagator->solve(0, yStart, 1., DYNAMICALMODEL_TYPE::D_NOT_SET);

    // retrieve final point
    real_type yEnd[ODE_STATE_SIZE];
    propagator->getLastPoint(yEnd);

    // evaluate final constraints which give the error function to nullify
    setFinalBoundaryConditions(yEnd, FU);

    if (res == ok) {
        return 0;
    }
    return res;
}

// ----------------------------------------------------------------
long int AveragedProblemNlp::Jacobian(int n, int m, const double *X, double *A) {
    // the solver will compute the Jacobian by finite differences
    return 0;
}
// ----------------------------------------------------------------
void AveragedProblemNlp::setSolverType(SolverType solver_) {
    solver = solver_;
}
// ----------------------------------------------------------------
int AveragedProblemNlp::solve(int nUnknowns, double* pUnknowns, double* fScale, double rtol, NonLinearEquationSolverOptions options) {
    // compute scaling parameters
    ProblemScaling scaling = ProblemScaling();

    mDynamics->setScaling(xSmaEndRef, xSmaEndRef, xMass0Ref);
    mDynamics->getScalingCoefficients(scaling);

    if (terminalConstraint) {
        terminalConstraint->setScaling(scaling.distance);
    }

    // apply scaling
    xSma0 = xSma0Ref / scaling.distance;
    xMass0 = xMass0Ref / scaling.mass;

    // apply bounds to unknown
    applyBounds(scaling, pUnknowns);

    // run NLP solver    
    if (solver == SOLVER_NLEQ) {
        return NonLinearEquationSolver::solve(nUnknowns, pUnknowns, fScale, rtol, options);
    } 
    else if (solver == SOLVER_NLEQ2) {
        throw LtException("Error: NLEQ2 not implemented\n");
    }
    else if (solver == SOLVER_KINSOL) {
#ifdef USE_KINSOL
        if (KinsolWrapper::init(nUnknowns, options) == 0) {
            return KinsolWrapper::solve(nUnknowns, pUnknowns);
        }
        throw LtException("Error while initialising KINSOL\n");        
#else
        throw LtException("KINSOL is not available. It has not be compiled against REALOOT\n");
        return -1;
#endif
    } else {
        throw LtException("Error: Unknown solver\n");
    }
}
// ----------------------------------------------------------------
bool AveragedProblemNlp::getLastPoint(const double* Xs, double* Xf) {
    real_type yStart[ODE_STATE_SIZE];
    setInitialBoundaryConditions(yStart, Xs);

    // propagate
    propagator->clearTrajectoryCache();
    propagator->solve(0, yStart, 1., DYNAMICALMODEL_TYPE::D_EQUINOCTIAL_AVERAGING);

    propagator->getLastPoint(Xf);
    return true;
}
// ----------------------------------------------------------------
int AveragedProblemNlp::getTrajectory(const double *Xs, std::vector<real_type> &tout, std::vector<S14VECTOR> &xout) {
    real_type yStart[ODE_STATE_SIZE];
    setInitialBoundaryConditions(yStart, Xs);

    // propagate
    propagator->clearTrajectoryCache();
    propagator->solve(0, yStart, 1., DYNAMICALMODEL_TYPE::D_EQUINOCTIAL_AVERAGING);

    std::vector<real_type> sout;
    return propagator->getTrajectoryPoints(tout, sout, xout);
}
