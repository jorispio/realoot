// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef OPTIMAL_CONTROL_HPP
#define OPTIMAL_CONTROL_HPP

#include "core/DataTypes.hpp" // S7VECTOR
#include "core/Enumerations.hpp"
#include "core/SizeParameters.hpp"

class TDynamicsEquinoctial;

/* ------------------------------------------------------------------------------- */
class TOptimalControlBase {
 private:
 protected:
    /** Dynamics. */
    TDynamicsEquinoctial* dynamic;

    /** Objective function. */
    OBJECTIVE_TYPE critere;

 public:
    /** Creator. */
    TOptimalControlBase(TDynamicsEquinoctial* dynamic, OBJECTIVE_TYPE critere);

    virtual ~TOptimalControlBase(){};

    /** Compute and return the optimal control */
    virtual Vector3 getOptimalControlDirection(const real_type* state_lambda) = 0;
    virtual Vector3 getOptimalControlDirection(const real_type* state, const real_type* lambda) = 0;

    /** Check derivatives of the switching function. */
    int CheckDerivatives(real_type t, const real_type* stateRef, const S7VECTOR& lambdaRef);
};

/* ------------------------------------------------------------------------------- */
#endif  // OPTIMAL_CONTROL_HPP
