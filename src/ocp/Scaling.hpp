#ifndef AVG_PROBLEM_SCALING_HPP
#define AVG_PROBLEM_SCALING_HPP

class ProblemScaling {
 private:

 public:
    double distance;
    double gravParameter;
    double mass;
    double longitude;
    double velocity;
    double time;

    ProblemScaling() {
        distance = 1;
        gravParameter = 1;
        mass = 1;
        longitude = 1;
        velocity = 1;
        time = 1;
    }

    double getTimeScale() {
        double meanMotion = sqrt(distance * distance * distance / gravParameter);  // rad/s
        return meanMotion / longitude;  // / velocity /
    }
};

// ---------------------------------------------------------------------
#endif  // AVG_PROBLEM_SCALING_HPP
