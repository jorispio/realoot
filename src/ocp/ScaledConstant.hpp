// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of SpaceMechanicsToolBox.
 *
 *   Copyright (C) 2010 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#ifndef SCALED_CONSTANT_HPP
#define SCALED_CONSTANT_HPP

#include <stdio.h>

#include <iostream>
#include <limits>
#include <sstream>

#include "HelioLib/Maths.hpp"

class ScaledConstant {
 private:
    double refValue;
    double curValue;

 public:
    ScaledConstant(double val) : refValue(val), curValue(val) { }   //NOLINT

    void scale(double s) {
        curValue = refValue * s;
    }

    void setValue(double val) {
        refValue = val;
        curValue = val;
    }
    double getValue() {
        return curValue;
    }
    double value() {
        return curValue;
    }

    double getUnscaledValue() {
        return refValue;
    }

    bool isZero() {
        return fabs(refValue) < epsilon_double;
    }

    void operator=(const ScaledConstant &c) {
        refValue = c.refValue;
        curValue = c.curValue;
    }

    double operator/(double a) {
        return curValue / a;
    }
    double operator*(double a) {
        return curValue * a;
    }
    double operator-(double a) {
        return curValue - a;
    }
    double operator+(double a) {
        return curValue + a;
    }

    bool operator <(const ScaledConstant& c) const {
         return (curValue < c.curValue);
    }
    bool operator >(const ScaledConstant& c) const {
         return (curValue > c.curValue);
    }
    bool operator ==(ScaledConstant &c) const {
         return (refValue == c.refValue);
    }
    bool operator ==(double value) const {
         return (refValue == value);
    }

    friend std::ostream &operator<<(std::ostream &output, const ScaledConstant &c ) {
        output << "ref : " << c.refValue << " cur : " << c.curValue;
        return output;
    }

    friend std::istream &operator>>(std::istream  &input, ScaledConstant &c) {
        input >> c.refValue >> c.curValue;
        return input;
    }

    friend double operator*(ScaledConstant &a, ScaledConstant &b) {
        return a.value() * b.value();
    }

    friend double operator*(double a, ScaledConstant &b) {
        return a * b.value();
    }

    friend double operator/(ScaledConstant &a, ScaledConstant &b) {
        return a.value() / b.value();
    }

    friend double operator/(double a, ScaledConstant &b) {
        return a / b.value();
    }

    operator double() {
        return curValue;
    }
};
// ---------------------------------------------------------------------
#endif  // SCALED_CONSTANT_HPP
