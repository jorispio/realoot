// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "MinTimeOptimalControl.hpp"
#include "core/LtException.hpp"

// ---------------------------------------------------------------------
MinTimeOptimalControl::MinTimeOptimalControl(TDynamicsEquinoctial* dynamic, OBJECTIVE_TYPE critere)
    : TOptimalControlBase(dynamic, critere) {
}
// ---------------------------------------------------------------------
/**
 * Compute and return the optimal control direction.
 */
// ---------------------------------------------------------------------
Vector3 MinTimeOptimalControl::getOptimalControlDirection(const real_type* x, const real_type* lambda) {
    real_type state_lambda[2 * STATE_SIZE];
    for (int i = 0; i < STATE_SIZE; ++i) {
        state_lambda[i] = x[i];
        state_lambda[STATE_SIZE + i] = lambda[i];
    }

    return getOptimalControlDirection(state_lambda);
}
// ---------------------------------------------------------------------
/** Compute the optimal control direction of the non-average problem.
 * It is the control direction that minimizes the Hamiltonian
 * @param x   [state, costate] vector
 * @return control direction in TNW frame
 */
// ---------------------------------------------------------------------
Vector3 MinTimeOptimalControl::getOptimalControlDirection(const real_type* x) {
    real_type sma = x[IDX_SMA];
    real_type ex = x[IDX_EX];
    real_type ey = x[IDX_EY];
    real_type hx = x[IDX_HX];
    real_type hy = x[IDX_HY];
    real_type Lm = x[IDX_L];

    real_type psma = x[IDX_PSMA];
    real_type pex = x[IDX_PEX];
    real_type pey = x[IDX_PEY];
    real_type phx = x[IDX_PHX];
    real_type phy = x[IDX_PHY];
    real_type pL = x[IDX_PL];

    // convenience variable
    real_type sqrtSma = sqrt(sma);
    real_type ex2 = ex * ex;
    real_type ey2 = ey * ey;
    if ((ex2 + ey2) > 1) {
        char str[255];
        snprintf(str, 255,
                "Command is not valid for hyperbolic orbit!.\n   sma = %f, ex=%f, ey=%f, hx=%f, hy=%f, L=%f\n",
                sma, ex, ey, hx, hy, Lm);
        throw LtException(str);
    }

    real_type cosL = cos(Lm);
    real_type sinL = sin(Lm);
    real_type excosl = ex * cosL;
    real_type eysinl = ey * sinL;
    real_type B = sqrt(1.0 + 2.0 * (excosl + eysinl) + ex2 + ey2);
    real_type A = sqrt(1.0 - (ex2 + ey2));
    real_type A_B = A / B;
    real_type fT = 2.0 * sqrtSma * (sma / A_B * psma + A_B * ((ex + cosL) * pex + (ey + sinL) * pey));

    real_type D = 1.0 + excosl + eysinl;
    real_type exey = ex * ey;
    real_type fN = sqrtSma * A_B / D * (-(2.0 * exey * cosL - sinL * (ex2 - ey2) + 2.0*ey+sinL) * pex
                             + (2.0 * exey*sinL + cosL * (ex2 - ey2) + 2.0 * ex+cosL) * pey);

    real_type fW = sqrtSma * A / D * ((hx * sinL - hy * cosL) * (-ey*pex + ex*pey) + (1.0 + hx * hx + hy * hy) / 2.0 *(cosL*phx + sinL*phy));

    // normalised acceleration direction (we actually assumed in the derivation that it was unbounded... it is simpler to do so with these equations)
    real_type ut = 1, un = 0, uw = 0;
    rho = sqrt(fT * fT + fN * fN + fW * fW);
    if (rho > 0) {
        real_type invNorm = 1.0 / rho;
        ut = invNorm * fT;
        un = invNorm * fN;
        uw = invNorm * fW;
    }

    if (std::isnan(ut) || std::isnan(un) || std::isnan(uw) || std::isnan(rho)) {
        char str[255];
        snprintf(str, 255,
                "Command contains NaN!.\n   sma = %f, ex=%f, ey=%f, hx=%f, hy=%f, L=%f\n   lambda = %f %f %f %f %f %f\n    f = [%f %f %f]\n",
                sma, ex, ey, hx, hy, Lm,
                psma, pex, pey, phx, phy, pL,
                fT, fN, fW);
        throw LtException(str);
    }

    return Vector3(ut, un, uw);
}
