// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LT_CONSTRAINT_TERMINAL_HPP
#define LT_CONSTRAINT_TERMINAL_HPP

#include "core/DataTypes.hpp"
#include "core/Enumerations.hpp"
#include "core/State.hpp"
#include "core/LtProblemContext.hpp"

/* ----------------------------------------------------------------------- */
class TerminalConstraintBase {
 public:

 protected:
    /** constraint size */
    int nc;
    /** state vector size. */
    uint n;
    /** constraint vector size. */
    uint m;
    /** number of elements in the jacobian. */
    uint nelejac;

    State refState;
    double xSmaEnd;
    double xSmaEndRef;
    double xExEnd;
    double xEyEnd;
    double xHxEnd;
    double xHyEnd;

    /** constraint parameter value. */
    double parameterValue;

    /** for the dynamics, sensitivities wrt time */
    double mu;
 public:
    /** */
    TerminalConstraintBase(int nc_) : nc(nc_) {
    }

    /* destructor */
    virtual ~TerminalConstraintBase() {};


    /** -----------------------------------------------------------------------
     * sets the dimensions.
     * ----------------------------------------------------------------------- **/
    inline void SetDimensions(int n_, int m_) {
        n = n_;
        m = m_;
    }

    /** Returns the dimensions.
     */
    inline void getDimensions(int& n_, int& m_, int& nelejac_) {
        n_ = n;
        m_ = m;
        nelejac_ = nelejac;
    }

    /** Get the dimension of the constraints */
    int getConstraintSize() const {
        return m;
    }

    /** Set the reference state for the computation of the constraint
     * @inputs
     *   s_f state and costate vector
     */
    virtual void setReferenceState(State &state);

    /** Get the reference state used in the evaluation of the constraint.
    * @return reference state.
     */
    State getReferenceState() const;

    /** Set the reference costate for the computation of the constraint
     * @inputs
     *   s_f state and costate vector
     */
    void setReferenceTerminalCostate(const double* ref_costate);

    virtual void setScaling(double smaScale) = 0;

    /** Set the reference value (target) for lambda_m. */
    void setReferenceTerminalLambdaM(double lm);

    /** Evaluate the constraints.
     * @param t0 initial time
     * @param p0 decision vector (costate, optimization parameters)
     * @param tf final time
     * @param sf  final [state, costate] vector
     * @param g  constraint vector
     * @return true if evaluation went successful.
     */
    virtual bool eval_g(double tf, const double* sf, double* g) = 0;

    /** Evaluate the constraints.
     * @param x0       decision vector (costate, optimization parameters)
     * @param x        final [state, costate] vector
     * @param stm_x_l  state transition matrix block d x(tf) / d l(t0), x:state, l:costate
     * @param stm_l_l  state transition matrix block d l(tf) / d l(t0), x:state, l:costate
     * @param index_x  position index of the decision vector in a global vector
     * @param index_g  position index of the constraint in a global vector
     * @param n        size of the decision vector.
     * @param m        size of the constraint vector.
     * @param values   jacobian values
     * @return true if evaluation went successful.
     */
    virtual bool eval_jac(const double* x0,
                          const double* s_f,
                          const S7MATRIX& stm_x_l,
                          const S7MATRIX& stm_l_l,
                          uint index_x,
                          uint index_g,
                          uint n,
                          uint m,
                          double* values) = 0;
};
/* ----------------------------------------------------------------------- */
#endif  // LT_CONSTRAINT_TERMINAL_HPP
