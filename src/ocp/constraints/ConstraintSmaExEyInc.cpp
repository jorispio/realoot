// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <cstring>

#include "ConstraintSmaExEyInc.hpp"

// ---------------------------------------------------------------------
void ConstraintSmaExEyInc::setReferenceState(State &state) {
    TerminalConstraintBase::setReferenceState(state);
    eSqEnd = state.getEccentricity() * state.getEccentricity();
    hSqEnd = xHxEnd * xHxEnd + xHyEnd * xHyEnd;
    raanEnd = state.getRightAscensionOfAscendingNode();
    pomEnd = atan2(xEyEnd, xExEnd) - raanEnd;
}
// ---------------------------------------------------------------------
void ConstraintSmaExEyInc::setScaling(double smaScale) {
    xSmaEnd = xSmaEndRef / smaScale;
}
/* --------------------------------------------------------------------- *
 * Evaluate the constraints.
 * @param tf final time
 * @param sf  final [state, costate] vector
 * @param g  constraint vector
 * @return true if evaluation went successful.
 * --------------------------------------------------------------------- */
bool ConstraintSmaExEyInc::eval_g(double tf, const double* sf, double* g) {
    double raan = atan2(sf[IDX_HY], sf[IDX_HX]);
    double e2 = sf[IDX_EX] * sf[IDX_EX] + sf[IDX_EY] * sf[IDX_EY];
    double h2 = sf[IDX_HX] * sf[IDX_HX] + sf[IDX_HY] * sf[IDX_HY];

    // state constraints
    g[0] = sf[IDX_SMA] - xSmaEnd;
    g[1] = (sf[IDX_EX] * sf[IDX_EX] + sf[IDX_EY] * sf[IDX_EY]) - eSqEnd;
    g[2] = (atan2(sf[IDX_EY], sf[IDX_EX]) - raan) - pomEnd;
    g[3] = h2 - hSqEnd;

    // transversality conditions
    double G = 1 + (sf[IDX_HY] * sf[IDX_HY] / (sf[IDX_HX] * sf[IDX_HX]));
    double n2 = 0.5 * (sf[IDX_PEX] * sf[IDX_EY] + sf[IDX_PEY] * sf[IDX_EX]) / e2;
    double n4 = e2 / sf[IDX_EY] * (n2 * sf[IDX_EX] + sf[IDX_PEX]);
    double n3 =-n4 * sf[IDX_HY] / h2 - sf[IDX_PHX];
    g[4] = n3 * sf[IDX_HY] * h2 + sf[IDX_PEY] * sf[IDX_HX] * sf[IDX_HY] * G - n4 * sf[IDX_HY];
    g[5] = sf[IDX_PL];  // lambda_theta

    // free time
    g[6] = sf[IDX_PM];  // lambda_mass
    return true;
}
/* --------------------------------------------------------------------- *
 * Evaluate the constraints.
 * @param x0       decision vector (costate, optimization parameters)
 * @param x        final [state, costate] vector
 * @param stm_x_l  state transition matrix block d x(tf) / d l(t0), x:state, l:costate
 * @param stm_l_l  state transition matrix block d l(tf) / d l(t0), x:state, l:costate
 * @param index_x  position index of the decision vector in a global vector
 * @param index_g  position index of the constraint in a global vector
 * @param n        size of the decision vector.
 * @param m        size of the constraint vector.
 * @param values   jacobian values
 * @return true if evaluation went successful.
 * --------------------------------------------------------------------- */
bool ConstraintSmaExEyInc::eval_jac(const double* x0,
                          const double* s_f,
                          const S7MATRIX& stm_x_l,
                          const S7MATRIX& stm_l_l,
                          uint index_x,
                          uint index_g,
                          uint n,
                          uint m,
                          double* values) {
    return false;
}
