// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <cstring>

#include "ConstraintSmaEccIncRaan.hpp"

/* --------------------------------------------------------------------- *
 *
 * --------------------------------------------------------------------- */
void ConstraintSmaEccIncRaan::setReferenceState(State &state) {
    TerminalConstraintBase::setReferenceState(state);
    eccEnd = state.getEccentricity() * state.getEccentricity();
    incEnd = state.getInclination();
}
/* --------------------------------------------------------------------- *
 *
 * --------------------------------------------------------------------- */
void ConstraintSmaEccIncRaan::setScaling(double smaScale) {
    xSmaEnd = xSmaEndRef / smaScale;
}
/* --------------------------------------------------------------------- *
 * Evaluate the constraints.
 * @param tf final time
 * @param sf  final [state, costate] vector
 * @param g  constraint vector
 * @return true if evaluation went successful.
 * --------------------------------------------------------------------- */
bool ConstraintSmaEccIncRaan::eval_g(double tf, const double* sf, double* g) {
    // state constraints
    g[0] = sf[IDX_SMA] - xSmaEnd;
    g[1] = (sf[IDX_EX] * sf[IDX_EX] + sf[IDX_EY] * sf[IDX_EY]) - eccEnd;
    g[2] = sf[IDX_HX] - xHxEnd;    // FIXME: RAAN and AoP are not updated with epoch
    g[3] = sf[IDX_HY] - xHyEnd;    // FIXME: RAAN and AoP are not updated with epoch

    // transversality conditions
    g[4] = sf[IDX_EX] * sf[IDX_PEY] - sf[IDX_EY] * sf[IDX_PEX];
    g[5] = sf[IDX_PL];  // lambda_theta

    // free time
    g[6] = sf[IDX_PM];  // lambda_mass
    return true;
}
/* --------------------------------------------------------------------- *
 * Evaluate the constraints.
 * @param x0       decision vector (costate, optimization parameters)
 * @param x        final [state, costate] vector
 * @param stm_x_l  state transition matrix block d x(tf) / d l(t0), x:state, l:costate
 * @param stm_l_l  state transition matrix block d l(tf) / d l(t0), x:state, l:costate
 * @param index_x  position index of the decision vector in a global vector
 * @param index_g  position index of the constraint in a global vector
 * @param n        size of the decision vector.
 * @param m        size of the constraint vector.
 * @param values   jacobian values
 * @return true if evaluation went successful.
 * --------------------------------------------------------------------- */
bool ConstraintSmaEccIncRaan::eval_jac(const double* x0,
                          const double* s_f,
                          const S7MATRIX& stm_x_l,
                          const S7MATRIX& stm_l_l,
                          uint index_x,
                          uint index_g,
                          uint n,
                          uint m,
                          double* values) {
    return false;
}
