// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CONSTRAINT_SMA_ECC_INC_HPP
#define CONSTRAINT_SMA_ECC_INC_HPP

#include "TerminalConstraintBase.hpp"

class ConstraintSmaEccInc : public TerminalConstraintBase {
 private:
    double eccEnd;
    double incEnd;

 public:
    ConstraintSmaEccInc() : TerminalConstraintBase(7) { };

    void setReferenceState(State &state);

    void setScaling(double smaScale);

    /** Evaluate the constraints. */
    bool eval_g(double tf, const double* sf, double* g);

    /** Evaluate the constraints. */
    bool eval_jac(const double* x0,
                          const double* s_f,
                          const S7MATRIX& stm_x_l,
                          const S7MATRIX& stm_l_l,
                          uint index_x,
                          uint index_g,
                          uint n,
                          uint m,
                          double* values);
};

#endif  // CONSTRAINT_SMA_ECC_INC_HPP
