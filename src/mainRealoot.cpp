// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see  : http://www.gnu.org/licenses/ : .
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <ctime>

// script utils
#include "utils/UtilsInputScript.hpp"
#include "utils/xml/UtilsStringXml.hpp"
#include "utils/xml/DOMTreeErrorReporter.hpp"

#include "LtAvgOcpMainInterface.hpp"

// ---------------------------------------------------------------------------
//  Usage()
// ---------------------------------------------------------------------------
void usage() {
    std::cout << "\nUsage:\n"
            "    mainJ2 [options]  : [-xml file] [-json file]\n\n"
            "This program solves an average optimal control problem for \n"
            "low-thrust minimum-time space trajectory transfer.\n"
            "Options:\n"
            "    -json file     Read JSON input file\n"
            "    -xml file      Read XML input file\n"
            "    -v={on, off}   Show information. Default is off.\n"
            "    -?             Show this help.\n\n"
            "  * = Default if not provided explicitly.\n\n"
           << std::endl;
}

/* ------------------------------------------------------------------------- */
int main(int argv, char *argc[]) {
    int status = 0;

    // Check command line and extract arguments.
    if (argv < 2) {
        usage();
        return 1;
    }

    bool verbose = false;
    bool json = false;
    bool xml = false;
    int iArg = 1;
    char* gInputScript = 0;
    while (iArg < argv) {

        if (argc[iArg][0] != '-') {
            ++iArg;
            continue;
        }

        if (!strncmp(argc[iArg], "-?", 2)) {
            usage();
            return 1;
            
        }

        if (!strncmp(argc[iArg], "-xml", 3) && !json) {
            xml = true;
            if (iArg + 1 >= argv) {
                std::cout << "Not enough input arguments." << std::endl;
                usage();
                return 0;
            }
            ++iArg;
            gInputScript = argc[iArg];
            
        } else if (!strncmp(argc[iArg], "-json", 3) && !xml) {
            json = true;
            if (iArg + 1 >= argv) {
                std::cout << "Not enough input arguments." << std::endl;
                usage();
                return 0;
            }
            ++iArg;
            gInputScript = argc[iArg];
            
        } else if (!strncmp(argc[iArg], "-v=", 3)) {
            char* gVerboseFlag = &argc[iArg][3];
            if (strcmp(gVerboseFlag, "on") == 0) {
                verbose = true;
            }
        } else {
            printf("Unknown option: %s\n", argc[iArg]);
            return 0;
        }
        ++iArg;
    }

    if ((iArg > argv) || (gInputScript == 0)) {
        usage();
        return 0;
    }
    
    printf("Loading: %s\n", gInputScript);

    LtProblemDefinition problemDefinition;
    LtProblemContext problemContext;
    InputScriptReader *reader = new InputScriptReader();
    if (xml) { 
        if (reader->readXml(gInputScript, problemDefinition, problemContext)) {
            // Initialize the XML4C2 system
            try {
                XMLPlatformUtils::Initialize();
            }
            catch (const XMLException& toCatch) {
                XERCES_STD_QUALIFIER cerr << "Error during Xerces-c Initialization.\n"
                    << "  Exception message:"
                    << StrX(toCatch.getMessage()) << XERCES_STD_QUALIFIER endl;
                return 1;
            }

            process(problemDefinition, problemContext, verbose/* TODO */);

            XMLPlatformUtils::Terminate();
        }
    }
    else if (json) { 
        if (reader->readJson(gInputScript, problemDefinition, problemContext)) {
            process(problemDefinition, problemContext, verbose/* TODO */);
        }
    } 
    else {
        throw LtException("Errors in input script");
    }

    if (verbose) {
        printf("END\n");
    }

    delete reader;
    
    return status;
}
