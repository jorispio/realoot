// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2020 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __CONTINUATION_OPTIONS_HPP__
#define __CONTINUATION_OPTIONS_HPP__

#include <string>

#include "ContinuationVariable.hpp"

struct ContinuationOptions {
	bool enable = false;
	ContinuationVariable variable;
	std::string report_name = "";
	double initial_value;
	double final_value;
	double max_step = 0.5;
	double min_step = 0.1;
	int max_iter = 10;
	double tol = 1e-6;
};

#endif  // __CONTINUATION_OPTIONS_HPP__
