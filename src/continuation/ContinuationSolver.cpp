// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>

#include "ContinuationSolver.hpp"

// -------------------------------------------------------------------------
ContinuationSolver::ContinuationSolver(ContinuationOptions& continuation_data, LtProblemDefinition& problem_definition)
    : variableName(continuation_data.variable),
        initial_value(continuation_data.initial_value),
        final_value(continuation_data.final_value),
        iter(0),
        tol(continuation_data.tol),
        max_iter(continuation_data.max_iter),
        max_step(continuation_data.max_step),
        min_step(continuation_data.min_step) {
    current_value = initial_value;
    old_value = initial_value;
    max_minor_iter = 10;

    // get variable range final value
    if (continuation_data.variable == ContinuationVariable::J2) {
        final_value = problem_definition.cj2;
    }

    current_problem_definition = LtProblemDefinition(problem_definition);
}

// -------------------------------------------------------------------------
ContinuationSolver::~ContinuationSolver() {

}
// -------------------------------------------------------------------------
LtProblemDefinition ContinuationSolver::get_updated_problem_definition(double value) {
    current_problem_definition.cj2 = value;
    return current_problem_definition;
}
// -------------------------------------------------------------------------
/** get the continuation starting point problem */
LtProblemDefinition ContinuationSolver::get_initial_problem() {
    current_problem_definition.cj2 = initial_value;
    return current_problem_definition;
}
// -------------------------------------------------------------------------
/** Progress the continuation and return the new value of the continuation variable */
LtProblemDefinition  ContinuationSolver::step(bool accept, LtProblemSolution& solution) {
    double new_value;
    if (accept) {
        // select progress step
        if (fabs(final_value - current_value) / 2 < min_step * fabs(final_value - initial_value)) {
            new_value = final_value;
        } 
        else {
            new_value = current_value + std::min((final_value - current_value) / 2,  // halved remaining segment
                max_step * (final_value - initial_value));
        }
        old_value = current_value;
        minor_iter = 0;

        path.push_back(ContinuationPathPoint(current_value, solution));        
    }
    else {
        new_value = (old_value + current_value) / 2;
        ++minor_iter;
    }

    ++iter;
    current_value = new_value;

    return get_updated_problem_definition(current_value);
}

// -------------------------------------------------------------------------
bool ContinuationSolver::can_continue() {
    return (iter < max_iter) && (minor_iter < max_minor_iter);
}

// -------------------------------------------------------------------------
bool ContinuationSolver::is_completed() {
    return (fabs(final_value - current_value) < tol);
}

// -------------------------------------------------------------------------
double ContinuationSolver::get_progress() {
    return (current_value - initial_value) / (final_value - initial_value);
}

// -------------------------------------------------------------------------
double ContinuationSolver::get_current_value() {
    return current_value;
}
// -------------------------------------------------------------------------
void ContinuationSolver::to_report(std::string report_name) {
    FILE* fid = fopen(report_name.c_str(), "w");
    if (fid != NULL) {

        for (std::vector<ContinuationPathPoint>::iterator point = path.begin(); point < path.end(); ++point) {
            fprintf(fid, "value = %6f   dv=%8.3f m/s  dt=%8.3f days  dm=%8.3f kg\n", (*point).value, (*point).solution.dvcost, (*point).solution.transferDuration, (*point).solution.dm);
        }

        fclose(fid);
    }
}
// -------------------------------------------------------------------------
