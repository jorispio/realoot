// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __CONTINUATION_SOLVER_HPP__
#define __CONTINUATION_SOLVER_HPP__

#include <vector>

#include "ContinuationData.hpp"
#include "ContinuationVariable.hpp"
#include "core/LtProblemSolution.hpp"
#include "core/LtProblemDefinition.hpp"

struct ContinuationPathPoint {
	double value;
	LtProblemSolution solution;

	ContinuationPathPoint(double value, const LtProblemSolution solution) : value(value), solution(solution) {}
};


class ContinuationSolver {
 private:
	 ContinuationVariable variableName;
	 std::vector<ContinuationPathPoint> path;
	 double initial_value;
	 double final_value;
	 double current_value;
	 double old_value;
	 int iter;
	 int minor_iter;

	 double tol;
	 int max_iter;
	 int max_minor_iter;
	 double max_step;
	 double min_step;

	 LtProblemDefinition current_problem_definition;

	 LtProblemDefinition get_updated_problem_definition(double value);

 public:
	 ContinuationSolver(ContinuationOptions& continuation_data, LtProblemDefinition& problem_definition);
	 ~ContinuationSolver();

	 /** continuation tolerance. See also set_min_step.
	     Continuation stop sucessfully when remaining step is below this tolerance is absolute value. */
	 void set_continuation_tolerance(double tol_) {
		 tol = tol_;
	 }

	 /** maximum iteration when correcting continuation path */
	 void set_continuation_maxiter(int max_iter_) {
		 max_iter = max_iter_;
	 }

	 /** max continuation step as percent of the overall variable change */
	 void set_max_step(double max_step_) {
		 max_step = max_step_;
	 }

	 /** min continuation step as percent of the overall variable change */
	 void set_min_step(double min_step_) {
		 min_step = min_step_;
	 }

	 /** get the continuation starting point problem */
	 LtProblemDefinition get_initial_problem();

	 /** Progress the continuation and return the new value of the continuation variable */
	 LtProblemDefinition step(bool accept, LtProblemSolution& solution);

	 /** return true if continuation can continue */
	 bool can_continue();
	 /** return true when continuation is completed and successful */
	 bool is_completed();

	 /** get continuation progress */
	 double get_progress();

	 double get_current_value();

	 void to_report(std::string report_name);
};

#endif  // __CONTINUATION_SOLVER_HPP__

