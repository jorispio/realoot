// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of REALOOT.
 *
 *   Copyright (C) 2018 Joris Olympio
 *
 *   REALOOT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   REALOOT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with REALOOT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WRAPPER_HPP
#define WRAPPER_HPP

// core
#include "core/Enumerations.hpp"
#include "core/LtSpacecraftData.hpp"
#include "core/LtProblemContext.hpp"
#include "core/LtProblemDefinition.hpp"
#include "core/LtProblemSolution.hpp"
#include "dynamics/StateNumericalPropagator.hpp"
// ocp
#include "ocp/AveragedProblemNlp.hpp"
#include "constraints/TerminalConstraintBase.hpp"
#include "constraints/ConstraintOrbit.hpp"
#include "constraints/ConstraintEnergy.hpp"
#include "constraints/ConstraintSmaEccInc.hpp"
#include "constraints/ConstraintSmaEccIncRaan.hpp"
#include "constraints/ConstraintSmaExEyInc.hpp"

void printSolutionFile(LtProblemContext &problemContext, LtProblemDefinition &problemDefinition,
        ProblemScaling &scaling, LtProblemSolution &solution, bool verbose);

void printEphemeris(StateNumericalPropagator* stateNumericalPropagator, LtProblemDefinition& problemDefintion, 
        LtProblemContext& problemContext, ProblemScaling& scaling, bool verbose);

int processPropagation(LtProblemDefinition &problemDefinition, LtProblemContext &problemContext, bool verbose,
            double *lastPoint, ProblemScaling &scaling);

int processSolving(LtProblemDefinition &problemDefinition, LtProblemContext &problemContext,
            LtProblemSolution &problemSolution, bool verbose);

void process(LtProblemDefinition &problemDefinition, LtProblemContext &problemContext, bool verbose);

#endif  // WRAPPER_HPP
